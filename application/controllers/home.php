<?php

/**
 * Clase de pruebas
 *
 * @author Kyoo  
 */
class Home extends MY_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->library('form_validation');
		$this->load->helper('url');

		// Load MongoDB library instead of native db driver if required
		$this->config->item('use_mongodb', 'ion_auth') ?
		$this->load->library('mongo_db') :

		$this->load->database();

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
		$this->load->helper('language');
	}
	public function index(){
	    /*
		define('ga_email','grupo.mca1@gmail.com');
        define('ga_password','mca.grupo1234');
        define('ga_profile_id','81592226');
        require 'gapi.class.php';
        $ga = new gapi(ga_email,ga_password);
        $ga->requestReportData(ga_profile_id,array('browser','browserVersion'),array('pageviews','visits'));
        $this->data['visitas_analitycs']=$ga->getVisits();
		*/
		
		$this->data['analytics']=TRUE;
		$this->data["comentarios_pendientes"] = $this -> comentariosPendientes();
		$this->data["comentarios_sitio"] = $this -> comentarios();
		$this->data["conteo_usuarios"] = $this -> contarUsuarios();
		$this->data["conteo_tesis"] = $this -> contarTesis();
		$this->data["users"] = $this -> traerUsuarios();
		$this->data["estadisticasPorTesis"] = $this -> estadisticasTesis();
		$this->data["navegador"] = $this ->obtenerNavegador();	

		$this->view("home");
	}

	
	public function comentariosPendientes(){
		$this -> load -> model("tesis_comentarios_m");
		$this -> load -> model("comentarios_sitio_m");
		$n1 = $this -> tesis_comentarios_m -> contarTodos();
        $n2 = $this -> comentarios_sitio_m -> contarTodos(); 
        return $n1 + $n2; 
	}
	
	public function comentarios(){
		$this -> load -> model("comentarios_sitio_m");
		return $this -> comentarios_sitio_m -> buscarAprobados(); 
	}
		
	public function contarUsuarios(){
		$this -> load -> model("ion_auth_model");
		
		return $this -> ion_auth_model -> contarTodosUsuarios();
	}
	
	public function contarTesis(){
		$this -> load -> model("tesis_m");
		
		return $this -> tesis_m -> contarTodasTesis();
	}
	
	public function traerUsuarios(){
		$this -> load -> model("contador_m");
		
		return $this -> contador_m -> ultimosUsuarios();
	}
		
	public function estadisticasTesis(){
		$this -> load -> model("descargas_m");
				
		return $this -> descargas_m -> descargasPorTesis();
	}
	
	public function obtenerNavegador(){
		$u_agent = $_SERVER['HTTP_USER_AGENT']; 
$bname = 'otro';
$platform = 'Unknown';
$version= "";


// Next get the name of the useragent yes seperately and for good reason
	if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
	{ 
	$bname = 'Internet Explorer'; 
	$ub = "MSIE"; 
	} 
		return $bname;
	}
		
} // END
