<?php
class Backup extends MY_Controller
{
    public function create()
    {
        $this->config->load('backup');
        $path = $this->config->item('path');
        $path_app = $this->config->item('path_app');
        $apache_conf = $this->config->item('apache_conf');
        $php_ini = $this->config->item('php_ini');
        
        $name = date('Y-m-d_H.i.s');

        if( ! is_dir( $path . $name ) )
        {
            // Create the paths
            $path_sql = $path .  $name . '/sql/';
            $path_backup_app = $path . $name . '/app/';
            $path_conf = $path . $name . '/conf/';
            
            mkdir( $path_sql, 0777, true);
            mkdir( $path_backup_app, 0777, true);
            mkdir( $path_conf, 0777, true);
            
            ######### Backup the database ##########
            // Load the DB utility class
            $this->load->dbutil();
            
            // Backup your entire database and assign it to a variable
            $backup =& $this->dbutil->backup(); 
            
            // Load the file helper and write the file to your server
            $this->load->helper('file');
            write_file( $path_sql . 'sql.gz', $backup);
            
            
            ###### Backup of the (app) files #############
            shell_exec("cp -r {$path_app} {$path_backup_app}");
            
            
            
            ##### Copy configuration files ######
            copy($apache_conf, $path_conf . 'apache.conf');
            copy($php_ini, $path_conf . 'php.ini');
            
            
            ##@@@ Creamos un readme.txt #####
            $readme = "Estás son las ubicaciones originales de los archivos que se han respaldado:\n
            Archivos: {$path_app}\n
            Configuración de apache: {$apache_conf}\n
            Configuración de php: {$php_ini}\n";
            write_file($path.$name. '/leeme.txt', $readme);
            
            echo "Se ha realizado el backup exitosamente.";
        }
        else
        {
            echo "El backup ya existe";
        } 
        
        
    }
}
