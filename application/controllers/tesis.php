<?php
class Tesis extends MY_Controller{
	
	/**
	 * Descarga la tesis al usuario en formato pdf
	 *
	 * @return void
	 * @author  
	 */
	function descargar( $id_tesis = '' ) {
		$this->is_private();
		
		$this->load->model("tesis_m");
		
		$id_tesis = current( explode('.', $id_tesis ) );
		
		if( $id_tesis and is_numeric($id_tesis) and $tesis = $this->tesis_m->get_by_id( $id_tesis ) ){ //Validamos
		       
			//Contamos su descarga si no es administrador
			$this->load->model('descargas_m');
			if( ! $this->ion_auth->is_admin() )
                $this->descargas_m->add( $id_tesis );
			//Buscamos la ruta del pdf 
			$path = './themes/default/pdf/' . $tesis["tes_ruta_tesis"];
			//Lo enviamos al cliente
			header('Content-type: application/pdf');
			header('Content-Disposition: attachment; filename="' . $tesis['tes_nombre'] .  '.pdf"');
			readfile( $path );
			 
		}else{
			$this->data['content'] = 'La tesis no existe';
			$this->view();
		}
	}
}
