<?php
/**
 * undocumented class
 *
 * @package default
 * @author Sofia
 */
class Busqueda_avanzada extends MY_Controller {
	
	/**
	 * Lista las tesis
	 *
	 * @return void
	 * @author  Sofia
	 */
	function index( $page=0 ) {
		$this->addJs("modulos/tesis.principal.js");
        
        
		//Hacemos persistente los input
		$this->load->library("form_validation");
		$this->form_validation->set_rules("tes_id_institucion", "", "");
		$this->form_validation->set_rules("palabra_clave", "", "");
		$this->form_validation->set_rules("tes_id_disciplina_estudio", "", "");
		$this->form_validation->set_rules("tes_id_subdisciplina", "", "");
		$this->form_validation->set_rules("tes_id_grado_obtenido", "", "");
		$this->form_validation->set_rules("tes_id_especie", "", "");
		$this->form_validation->set_rules("order_by", "", "");
		$this->form_validation->set_rules("order_type", "", "");
		
		$this->form_validation->run();
		
		//Aplicamos los filtros
		$filtros = array(
			"tes_palabra_clave" => $this->input->post("palabra_clave"),
			"tes_id_institucion" => $this->input->post("tes_id_institucion"),
			"tes_id_disciplina_estudio" => $this->input->post("tes_id_disciplina_estudio"),
			"id_subdisciplina" => $this->input->post("tes_id_subdisciplina"),
			"tes_id_grado_obtenido" => $this->input->post("tes_id_grado_obtenido"),
			"id_especie" => $this->input->post("tes_id_especie") 
		);
		
		//Obtenemos las tesis con limite, filtros y orden
		$this->load->model("tesis_m");
		$this->tesis_m->set_filtros( $filtros );
		$this->tesis_m->set_orden( $this->input->post("order_by"), $this->input->post("order_type"));
		$this->db->limit( $this->limit, $page );
		$tesis = $this->tesis_m->get_all();
		$this->data["total_tesis"] = $total_tesis = $this->tesis_m->get_total_rows();
		
		//Creamos el paginador
		$this->data["paginador"] = $this->get_paginador( array(
			"base_url" => site_url("busqueda_avanzada_c/index/"),
			"total_rows" => $total_tesis,
			"uri_segment" => 4			
		) );
		 
		
		
		$this->data["tesis"] = $tesis;
		
		//Formamos los campos necesarios para la vista
		$campos = array(
			"tes_id_institucion" => form_dropdown( "tes_id_institucion", array("" => "Seleccionar") + get_cmb_data("cat_instituciones", "ins_id_institucion", "ins_nombre"), 
											set_value("tes_id_institucion"), "class='input-medium' id='tes_id_institucion'" ),
			"tes_id_disciplina_estudio" => form_dropdown( "tes_id_disciplina_estudio", array("" => "Seleccionar") + get_cmb_data("cat_disciplinas_estudio", "dis_id_disciplina_estudio", "dis_estudio_nombre"), 
											set_value("tes_id_disciplina_estudio"), "class='input-medium' id='tes_id_disciplina_estudio'" ),
			"tes_id_subdisciplina" => form_dropdown( "tes_id_subdisciplina", array("" => "Seleccionar") + get_cmb_data("cat_subdisciplinas", "sub_id_subdisciplina", "sub_nombre"), 
											set_value("tes_id_subdisciplina"), "class='input-medium' id='tes_id_subdisciplina'" ),
			"tes_id_grado_obtenido" => form_dropdown( "tes_id_grado_obtenido", array("" => "Seleccionar") + get_cmb_data("grados_academicos", "gra_id_grado_academico", "gra_descripcion"), 
											set_value("tes_id_grado_obtenido"), "class='input-medium' id='tes_id_grado_obtenido'" ),
			"tes_id_especie" => form_dropdown( "tes_id_especie", array("" => "Seleccionar") + get_cmb_data("cat_especies", "esp_id_especie", "esp_nombre"), 
											set_value("tes_id_especie"), "class='input-medium' id='tes_id_especie'" ),	
			"palabra_clave" => array(
				"name" => "palabra_clave",
				"value" => set_value("palabra_clave"),
				"id" => "palabra_clave"
			)
			
		); 

		//Mandmos los campos a la vista
		$this->data["campos"] = $campos;
		
		
		
		$this->view("busqueda_avanzada/principal");
	}
	
	/**
	 * undocumented function
	 *
	 * @return void
	 * @author  
	 */
	function get_paginador( $conf_data ) {
		$base_data = array(
			'base_url'  => "/", // la url a la que apuntará base_url() . "/propiedad/listado",
			//'total_rows' =>  ,
			'per_page' => $this->limit,
			'num_links' => 8,
			"suffix" => "",
			'last_link' => '>>',
			'first_link' => '<<',
			'next_link'      => 'Siguiente &rarr;',
			
            'next_tag_open'  => '<li>',
            'next_tag_close' => '</li>',
            
			'prev_link'      => '&larr; Anterior',
			'prev_tag_open'  => '<li>',
			'prev_tag_close' => '</li>',
			
			'cur_tag_open'   => '<li class="active"><a>',
			'cur_tag_close'  => '</li></a>',
			
			'num_tag_open'   => '<li>',
			'num_tag_close'  => '</li>',
			
			'first_tag_open' => '<li>',
			'first_tag_close'=> '</li>',
			
			'last_tag_open'  => '<li>',
			'last_tag_close' => '</li>',
			
			"full_tag_open" => "<ul>",
			"full_tag_close" => "</ul>"
			
		);

		$data = array_merge($base_data, $conf_data);
		$this->load->library('pagination');
		$this->pagination->initialize($data);
		return $this->pagination->create_links();			
	}
	
	
	
	function registrar(){
		//Para plugin select2
		$this->addJs("select2/select2.min.js", "plugs");
        $this->addJs("select2/select2.js", "plugs");
		$this->addCss("select2/select2.css", "plugs");
		
		$this->addJs("modulos/tesis.registrar.js");
		
		$this->load->library("form_validation");
		//Establecemos reglas
		$this->set_validaciones();
		
		//Validamos
		if( $this->form_validation->run() === FALSE){ //Abrir la vista de nuevo
			printf("%s", "no pasa la validacion");
			$this->set_campos(); //Cargamos los campos que le pasaremos a la vista
			$this->view("catalogos/tesis/registrar_tesis");	
       
         
             
		}else{ //Guardamos la tesis
		 printf("%s", "pasa la validacion");
			$palabras_clave = $this -> input -> post("tes_palabras_clave");
            $titulo = $this -> input -> post("tes_titulo");
            $anio_titulacion = $this -> input -> post("tes_anio_titulacion");
            $institucion = $this -> input -> post("tes_id_institucion");
            $estados = $this -> input -> post("cat_id_estados");
            $id_disciplina = $this -> input -> post("dis_id_disciplina");
            $tes_id_grado_obtenido = $this -> input -> post("gra_id_grado_academico");
            
            //Abrimos el modelo de instituciones_m
            $this -> load -> model("tesis_m");

            //Una vez abierto, ya podemos usar sus métodos, sólo le pasamos un array asociativo de la forma "campo" => "valor", y lo agrega
            $id = $this -> tesis_m -> agregar2(array("tes_palabras_clave" => $palabras_clave, "tes_nombre" => $titulo, "tes_anio_titulacion" => $anio_titulacion, 
                                            "tes_id_institucion" => $institucion, "tes_id_estado_pais" => $estados, 
                                            "tes_id_disciplina_estudio" => $id_disciplina, "tes_id_grado_obtenido" => $tes_id_grado_obtenido));
             printf("%f",$id);
		}
		
		
	}

/**
 *Eliminar tesis
 */
 
public function eliminar(){
		$this->load->library("form_validation");
		$this->data["titulo"] = "Borrando Tesis";
		$this->form_validation->set_rules("tes_id_tesis","Id Tesis", "required");
				
		if( $this->form_validation->run() === FALSE){
			echo $this->fresponse(  validation_errors()  );
		}else{
			$idTesis = $this->input->post("tes_id_tesis");
			$this->load->model("tesis_m");	
			$this->tesis_m->eliminar( $idTesis );
			echo  $this->sresponse();
		}
	}
 
 

	
	/**
	 * Establece las reglas al registrar/editar una tesis
	 *
	 * @return void
	 * @author  RubiGR
	 */
	function set_validaciones() {
		/*$this->form_validation->set_rules("tes_titulo", "Título", "required|min_length[3]|max_length[50]");
		$this->form_validation->set_rules("tes_nombre_autor", "Autor", "required|min_length[3]|max_length[50]");
		$this->form_validation->set_rules("tes_id_institucion", "Institución de adscripción", "required");
		$this->form_validation->set_rules("grad_id_grado_academico", "Grado obtenido", "required");
		$this->form_validation->set_rules("tes_anio_titulacion", "Año de titulación", "required|min_length[4]|max_length[4]");
		$this->form_validation->set_rules("tes_nombre_director", "Director de tesis", "required|min_length[3]|max_length[50]");
		$this->form_validation->set_rules("dis_id_disciplina", "Disciplina de estudio", "required");
		$this->form_validation->set_rules("id_subdisciplinas_estudio", "Subdisciplina de estudio", "required");*/
	}
	
	/**
	 * Agregalos campos necesarios para el formulario de la alta/edición de la tesis
	 *
	 * @return void
	 * @author  RubiGR
	 */
	function set_campos() {
		$campos = array(
			"tes_titulo" => array(
				"name" => "tes_titulo",
				"value" => set_value("tes_titulo"),
				"class" => "input-xxlarge",
				"placeholder" => "Escriba el título de la tesis"
			),
			"tes_nombre_autor" => array(
				"name" => "tes_nombre_autor",
				"value" => set_value("tes_nombre_autor"),
				"class" => "input-xxlarge",
				"placeholder" => "Escriba el nombre del autor"
			),
			"tes_anio_titulacion" => array(
				"name" => "tes_anio_titulacion",
				"value" => set_value("tes_anio_titulacion"),
				"class" => "input-small",
				"placeholder" => "4 dígitos"
			
			),
			"tes_nombre_director" => array(
				"name" => "tes_nombre_director",
				"value" => set_value("tes_nombre_director"),
				"class" => "input-xxlarge",
				"placeholder" => "Escriba el nombre del director de tesis"
			),
			"tes_id_institucion" => form_dropdown( "tes_id_institucion",array("" => "Seleccionar") +  get_cmb_data("cat_instituciones", "ins_id_institucion", "ins_nombre"),
            set_value("tes_id_institucion"), "id='tes_id_institucion' name='tes_id_institucion' class='input-xlarge' " ),
            "dis_id_disciplina" => form_dropdown( "dis_id_disciplina",array(
            	"" => "Seleccionar") +  get_cmb_data("cat_disciplinas_estudio", 
            	"dis_id_disciplina_estudio", 
            	"dis_estudio_nombre"),
            set_value("dis_id_disciplina"), "id='dis_id_disciplina' name='dis_id_disciplina' class='input-xlarge' " ),
            "gra_id_grado_academico" => form_dropdown( "gra_id_grado_academico", array("" => "Seleccionar") + get_cmb_data("grados_academicos", "gra_id_grado_academico", "gra_descripcion"),
            set_value("gra_id_grado_academico"), "id='gra_id_grado_academico' name='gra_id_grado_academico' class='input-xlarge' " ),
            "est_id_zona_estudio" => form_dropdown( "est_id_zona_estudio", array("" => "Seleccionar") + get_cmb_data("cat_zonas_estudio", "est_id_zona_estudio", "est_nombre"),
            set_value("cat_id_estados"), "class='input-xlarge'" ),
            
            "esp_id_especie" => form_dropdown( "esp_id_especie", get_cmb_data("cat_especies", "esp_id_especie", "esp_nombre"),
            set_value("esp_id_especie"), "id='esp_id_especie' name='esp_id_especie[]' class='input-xlarge' placeholder='Seleccionar la especie' multiple='multiple'" ),
            
            "con_id_condicion_sitio" => form_dropdown( "con_id_condicion_sitio", get_cmb_data("cat_condiciones_sitio", "con_id_condicion_sitio", "con_nombre"),
            set_value("con_id_condicion_sitio"), "id='con_id_condicion_sitio' name='con_id_condicion_sitio' class='input-xlarge' placeholder='Seleccionar la condición del sitio' multiple='multiple'" ),
            "id_subdisciplinas_estudio" => form_dropdown( "id_subdisciplinas_estudio", get_cmb_data("cat_subdisciplinas", "sub_id_subdisciplina", "sub_nombre"),
            set_value("id_subdisciplinas_estudio"), "id='id_subdisciplinas_estudio' name='id_subdisciplinas_estudio' placeholder='Seleccionar la subdisciplina' class='input-xlarge' multiple='multiple'" ),
            "tes_id_grado_academico" => form_dropdown( "tes_id_grado_academico", array("" => "Seleccionar") + get_cmb_data("grados_academicos", "gra_id_grado_academico", "gra_descripcion"),
            set_value("tes_id_grado_academico"), "class='input-xlarge'" ),
			
		);
		
		$this->data["campos"] = $campos;
	}
	
	
} // END