<?php
/**
 * undocumented class
 *
 * @package default
 * @author fgonzalez 
 */
class Errores extends MY_Controller {

	public function __construct(){
        parent::__construct();
    }
	
	function error_404()
	{
       //llamamos a la vista que muestra el error 404 personalizado
		$this->view('tpl/errors/404');
	}									
	
} // END