<?php
/**
 * undocumented class
 *
 * @package default
 * @author fgonzalez 
 */
class about_c extends MY_Controller {

	public function __construct(){
        parent::__construct();
    }
	
	public function somos(){
		$this->setTitle('Acerca de nosotros');
		$this->view("about_us");
	}
	
	public function equipo_desarrollo(){
		$this->setTitle('Equipo de desarrollo');
		$this->view("team_developers");
	}
	
	public function comentarios_sitio(){
		$this->setTitle('Comentarios');
		$this -> data["titulo"] = "Comentario del Sitio";	
		
		$this -> load -> model("comentarios_sitio_m");
		$this -> data["comentarios_sitio"] = $this -> comentarios_sitio_m -> buscarAprobados();
		
		$this->view("comentarios_sitio");
	}				
	
	public function agradecimientos(){
		$this->setTitle('Agradecimientos');
		$this->view("agradecimientos");
	}							
	
} // END