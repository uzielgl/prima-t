<?php

/**
 * Clase de pruebas
 *
 * @author almiis sanchez
 */
class Subdisciplinas_c extends MY_Controller {
	
	public function index(){
		
		$this->data["subdisciplina"] = "subdisciplina";
		$this->data["descripcion"] = "descripcion";
		$this->data["vista"]= "subdisciplinas_c";
	    $this->view("admon_catalogos");
	}
} // END