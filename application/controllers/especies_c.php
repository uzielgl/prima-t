<?php

/**
 * Clase de pruebas
 *
 * @author almiis sanchez
 */
class Especies_c extends MY_Controller {
	
	public function index(){
		$this->addJs("modulos/administracion.catalogos.js");
		
		$this->data["especies"] = array(
			array("esp_id_especie"=>"2",
				  "esp_nombre"=>"especie1"),
			array("esp_id_especie"=> "3",
				  "esp_nombre"=>"especie2"),
			array("esp_id_especie"=>"6",
				  "esp_nombre"=>"especie3"),
		);
		$this->data["vista"]= "especies_c";
	    $this->view("admon_catalogos");
	}
} // END