<?php
/**
 * undocumented class
 *
 * @package default
 * @author yairguz , ggomez 
 */
class Comentarios_sitio_c extends MY_Controller {
	

	public function agregar(){
	   if(!$this->ion_auth->logged_in()){
	       echo $this->fresponse("Su sesión ha caducado favor de iniciar sesión nuevamente");
           exit();
	   }
        
		$this->load->library("form_validation");
		$this->form_validation->set_rules("taComentarSitio","Comentario", "required|regex_match[/^[0-9A-Za-záéíóúÁÉÍÓÚñÑüÜ\s,.!¡¿?()]+$/]|min_length[3]|max_length[500]|trim");
		if( $this->form_validation->run() === FALSE){
			echo $this->fresponse(  validation_errors()  );

		}else{
			$comentario = $this->input->post("taComentarSitio");

			$this->load->model("comentarios_sitio_m");
            
			$agregado=$this->comentarios_sitio_m->agregar( array("com_comentario" => $comentario ) );
			
			if($agregado!=false){
			    //$this->session->set_flashdata("msg.ok", "Gracias por su comentario");
				echo  $this->sresponse( "Gracias por su comentario" );
			}else{
				echo $this->fresponse("hubo error");
			}
		}
        exit();
	}
	
	public function eliminar(){
		$this->load->library("form_validation");
		$this->data["titulo"] = "Borrando especies";
		$this->form_validation->set_rules("esp_id_especie","Id Especie", "required");
				
		if( $this->form_validation->run() === FALSE){
			echo $this->fresponse(  validation_errors()  );
		}else{
			$idEspecie = $this->input->post("esp_id_especie");
			$this->load->model("especies_m");	
			$this->especies_m->eliminar( $idEspecie );
			echo  $this->sresponse();
		}
	}
	
	public function modificar(){
		
		$this->load->library("form_validation");
		$this->data["titulo"] = "Modificando una especie";
		$this->form_validation->set_rules("esp_id_especie","Id Especie", "required");
		$this->form_validation->set_rules("esp_nombre","Nombre especie", "required|regex_match[/^[A-Za-záéíóúÁÉÍÓÚñÑüÜ\s]+$/]|min_length[3]|max_length[50]|is_unique[cat_especies.esp_nombre]|trim");
		if( $this->form_validation->run() === FALSE){
			echo $this->fresponse(  validation_errors()  );
		}else{
			$esp_id_especie = $this->input->post("esp_id_especie");
			$esp_nombre = $this->input->post("esp_nombre");
			$this->load->model("especies_m");	
			$this->especies_m->modificar( $esp_id_especie, array("esp_nombre" => $esp_nombre ) );
			$this->session->set_flashdata("msg.ok", "Catalogo actualizado correctamente " );
			echo  $this->sresponse();
		}		
	}		
	
	public function buscarTodos($json=true,$num_pagina=0){
		$this->load->model("especies_m");
		$this->especies_m->setLimit(5, $num_pagina);
		if($json){
			echo $this->sresponse( array("datos" => $this->especies_m->buscarTodos()) );
			 
		}else{
			$especies=$this->especies_m->buscarTodos();
			$this->load->model("subespecies_m");
			
			for($i=0; $i<count($especies); $i++) {
			    $subespecies = $this->subespecies_m->buscarPorIdEspecie($especies[$i]["esp_id_especie"]);
			    if($subespecies)
					$especies[$i]["subespecies"]=$subespecies;
				else
					$especies[$i]["subespecies"]=array();
			}
			
			return $especies;
		}
		
	}
	
	public function buscarPorId($id){
		$this->load->model("especies_m");
		echo $this->sresponse( array("datos" => $this->especies_m->buscarPorId()) );
		//return $this->buscarPorId();
	}

} // END