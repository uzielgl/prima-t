<?php
class Migrate extends  MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function index($version='')
    {
        $this->view = false;
        $this->load->library('migration');
        
        if( $version ){
            if( ! $this->migration->version($version) )
            {
                show_error( $this->migration->error_string() );    
            }
        }else{
            if( ! $this->migration->current() )
            {
                show_error( $this->migration->error_string() );    
            }
        }
    }
}
