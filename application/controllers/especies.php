<?php
/**
 * undocumented class
 *
 * @package default
 * @author  
 */
class Especies extends MY_Controller {
	
	public function index(){
		
	}
	
	/**
	 * Muestra el formulario de agregar, cuando le dan submit, si pasa todas las validaciones
	 * se agrega un nuevo registro de especies.
	 *
	 * @return void
	 * @author  
	 */
	public function agregar(){
		//Esta librería nos ayuda para hacer las validaciones, 
		$this->load->library("form_validation"); 
		
		//Pasamos una variable de ejemplo al formulario. No tiene nada que ver con el agregar
		//especie, sólo para mostrar también como se pasan variables a la vista.
		//La variable $titulo es la que estará disponible en la vista.
		$this->data["titulo"] = "Agregando una especie";
		
		//Establecemos las validaciones necesarias. Deben de ser de la forma:
		//set_rules("nombre_de_campo", "Nombre que se mostrará al usuario", "reglas"); 
		//Donde el "nombre que se mostrará al usuario" es por ejemplo algo como : El campo "Nombre especie" es requerido.
		$this->form_validation->set_rules("esp_nombre","Nombre especie", "required");
	
		//Con el método run() estamos preguntando si todas las set_rules que establecimos se han pasado
		if( $this->form_validation->run() === FALSE){
			//En caso de FALSE, osea, cuando no paso las validaciones, o cuando es la primera vez que entra a la página
			//Se le muestra simplemente el formulario
			$this->view("form_especies");		
		}else{
			//Si no es igual a FALSE, indica que fué TRUE, osea que si pasó todas las validaciones, por lo que
			//procedemos a insertarlo en la BD.
			
			//Primero recuperamos la información, para este caso sólo el nombre
			$nombre = $this->input->post("esp_nombre");
			
			//Abrimos el modelo de especies_m
			$this->load->model("especies_m");
			
			//Una vez abierto, ya podemos usar sus métodos, sólo le pasamos un array asociativo de la forma "campo" => "valor", y lo agrega
			$this->especies_m->agregar( array("esp_nombre" => $nombre ) );
			
			//Redireccionamos para que no le de otra vez f5 y vuelva a agregar el mismo campo.
			redirect("especies/msj_exito");
		}
	}
	
	public function msj_exito(){
		$this->view("form_especies_exito");		
	}
} // END