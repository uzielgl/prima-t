<?php
/**
 * undocumented class
 *
 * @package default
 * @author fgonzalez 
 */
class admon_comentarios_c extends MY_Controller {

	public function __construct(){
        parent::__construct();
        $this->is_private(1);
    }
		
	
	public function comen_tesis(){
		$this -> addJs("modulos/administracion.comentarios.tesis.js");	
		
		$this->setTitle('Comentarios de tesis');		
		$this -> data["comentarios_tesis"] = $this -> buscarTodosTesis();
		$this->data["vista"]= "tesis";	
		$this->data["comentarios_pendientes"] = $this -> comentariosPendientesTesis();
		$this->view("admon_comentarios");
	}
	
	
	public function comen_sitio(){
		$this -> addJs("modulos/administracion.comentarios.sitio.js");
		
		$this->setTitle('Comentarios de sitio');
	    $this -> data["vista"]= "sitio";	
		$this -> data["comentarios_sitio"] = $this -> buscarTodosSitio();
        $this->data["comentarios_pendientes"] = $this -> comentariosPendientesSitio();
		$this->view("admon_comentarios");
	}
	
	public function comen_aprobados(){
		$this -> addJs("modulos/administracion.comentarios.tesis.js");
			
		$this->setTitle('Comentarios aprobadoss');	
		$this -> data["vista"]= "aprobados";
		$this -> data["comentarios_aprobados"] = $this -> buscarAprobadosTesis();
        
		$this->view("admon_comentarios");
		
	}
		
			
	public function buscarTodosTesis(){
        $this -> load -> model("tesis_comentarios_m");
		
        return $this -> tesis_comentarios_m -> buscarTodos();
    }
	
	public function buscarTodosSitio(){
        $this -> load -> model("comentarios_sitio_m");
		
		return $this -> comentarios_sitio_m -> buscarTodos();
		
    }
	
	public function buscarAprobadosTesis(){
       	$this -> load -> model("tesis_comentarios_m");
		
        return $this -> tesis_comentarios_m -> buscarAprobados();
    }
	
	 public function eliminarComentTesis() {
        $this -> load -> library("form_validation");
        $this -> data["titulo"] = "Borrando Comentario de Tesis";
        $this -> form_validation -> set_rules("id_coment_eliminar", "Id tesis usuario", "required");

        if( $this->form_validation->run() === FALSE){
			echo $this->fresponse(  validation_errors()  );
			
		}else{
			
			$tes_id_tesis_users = $this->input->post("id_coment_eliminar");
			$this->load->model("tesis_comentarios_m");	
			$this->tesis_comentarios_m->eliminar($tes_id_tesis_users);
			
			$this->session->set_flashdata("msg.ok", "Comentario Borrado");
			echo  $this->sresponse();
		}

    }
	
	public function aprobarComentTesis() {
		
        $this -> load -> library("form_validation");
        $this -> data["titulo"] = "Aprobar Comentario de Tesis";
        $this -> form_validation -> set_rules("id_coment_aprobar", "Id tesis usuario", "required");

        if( $this->form_validation->run() === FALSE){
			echo $this->fresponse(  validation_errors()  );
			
		}else{
			
			$tes_id_tesis_users = $this->input->post("id_coment_aprobar");
			$this->load->model("tesis_comentarios_m");	
			$this->tesis_comentarios_m->aprobar($tes_id_tesis_users);
			
			$this->session->set_flashdata("msg.ok", "Comentario Aprobado");
			echo  $this->sresponse();
		}

    }	
        	
	 public function eliminarComentSitio() {
        $this -> load -> library("form_validation");
        $this -> data["titulo"] = "Borrando Comentario de Sitio";
        $this -> form_validation -> set_rules("id_coment_eliminar_sitio", "Id Sitio usuario", "required");

        if( $this->form_validation->run() === FALSE){
			echo $this->fresponse(  validation_errors()  );
			
		}else{
			
			$com_id_comentario_sitio = $this->input->post("id_coment_eliminar_sitio");
			$this->load->model("comentarios_sitio_m");	
			$this->comentarios_sitio_m->eliminar($com_id_comentario_sitio);
			
			$this->session->set_flashdata("msg.ok", "Comentario Borrado");
			echo  $this->sresponse();
		}

    }
	
	public function aprobarComentSitio() {
		
        $this -> load -> library("form_validation");
        $this -> data["titulo"] = "Aprobar Comentario de Sitio";
        $this -> form_validation -> set_rules("id_coment_aprobar_sitio", "Id sitio usuario", "required");

        if( $this->form_validation->run() === FALSE){
			echo $this->fresponse(  validation_errors()  );
			
		}else{
			
			$com_id_comentario_sitio = $this->input->post("id_coment_aprobar_sitio");
			$this->load->model("comentarios_sitio_m");	
			$this->comentarios_sitio_m->aprobar($com_id_comentario_sitio);
			
			$this->session->set_flashdata("msg.ok", "Comentario Aprobado");
			echo  $this->sresponse();
		}

    }
	
	public function comentariosPendientesSitio(){
		$this -> load -> model("comentarios_sitio_m");
		return $this -> comentarios_sitio_m -> contarTodos(); 
    }
	
	public function comentariosPendientesTesis(){
		$this -> load -> model("tesis_comentarios_m");
		return $this -> tesis_comentarios_m -> contarTodos(); 
	}						
	
} // END