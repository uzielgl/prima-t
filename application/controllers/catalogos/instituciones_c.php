<?php
/**
 * undocumented class
 *
 * @package default
 * @author
 */
class Instituciones_c extends MY_Controller {
	
	public function __construct(){
        parent::__construct();
        $this->is_private(1);
    }

    public function index() {
        $this -> addJs("modulos/administracion.catalogos.js");
        $this -> addJs("modulos/administracion.catalogos.instituciones.js");
		/*  $campos_agregar = array(
            "datos_institucion" => form_dropdown( "datos_institucion",array("" => "Seleccionar") +  get_cmb_data("cat_instituciones", "ins_id_institucion", "ins_nombre"),
            set_value("datos_institucion"), "id='ins_campus_agregar' name='ins_campus' class='input-xlarge' " )
        );
        $campos_editar = array(
            "datos_institucion" => form_dropdown( "datos_institucion",array("" => "Seleccionar") +  get_cmb_data("cat_instituciones", "ins_id_institucion", "ins_nombre"),
            set_value("datos_institucion"), "id='ins_campus_editar' name='ins_campus' class='input-xlarge' " )
        );
        
		
        $this->data["campos_agregar"] = $campos_agregar;
		$this->data["campos_editar"] = $campos_editar;
        */
        $this -> data["instituciones"] = $this -> buscarTodos(false,0);
        $this -> data["vista"] = "instituciones_c";
        $this->paginar('/catalogos/instituciones_c/pagina',$this->instituciones_m);
        $this -> view("admon_catalogos");
    }
	public function pagina($num_pagina=0){
		$this -> addJs("modulos/administracion.catalogos.js");
        $this -> addJs("modulos/administracion.catalogos.instituciones.js");
		$this -> data["instituciones"] = $this -> buscarTodos(false,$num_pagina);
		$this -> data["vista"] = "instituciones_c";		
		$this->paginar('/catalogos/instituciones_c/pagina',$this->instituciones_m);
	    $this->view("admon_catalogos");
	}
	private function paginar($url,$model){
		$this->load->library('pagination');
		$config['base_url'] = site_url().$url;
		$config['total_rows'] = $model->count_all_results();
		$config['per_page'] = 5;
		$config["uri_segment"] = 4;
		$config['num_links'] = 5;
		$config['full_tag_open'] = '<div class="pagination pagination-centered"><ul>';
		
		$config['next_link'] = '&gt;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		
		$config['prev_link'] = '&lt;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		
		$config['first_link'] = 'Primera';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_link'] = 'Última';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active" ><a>';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		
		$config['full_tag_close'] = '</ul></div>';
		$this->pagination->initialize($config);
		$this->data["paginacion"]= $this->pagination->create_links();
	}
    /**
     * Muestra el formulario de agregar, cuando le dan submit, si pasa todas las validaciones
     * se agrega un nuevo registro de instituciones.
     *
     * @return void
     * @author
     */

    public function buscarTodos($json = true,$num_pagina=0){
        $this -> load -> model("instituciones_m");
		
		$this->instituciones_m->setLimit(5, $num_pagina);
        if ($json) {
            echo $this -> sresponse(array("datos" => $this -> instituciones_m -> buscarTodos()));

        } else {
            return $this -> instituciones_m -> buscarTodos();
        }

    }


    public function eliminar() {
        $this -> load -> library("form_validation");
        $this -> data["titulo"] = "Borrando instituciones";
        $this -> form_validation -> set_rules("ins_id_institucion", "Id Institución", "required");

        if( $this->form_validation->run() === FALSE){
			echo $this->fresponse(  validation_errors()  );
		}else{
			$ins_id_institucion = $this->input->post("ins_id_institucion");
			$this->load->model("instituciones_m");	
			$this->instituciones_m->eliminar( $ins_id_institucion);
			echo  $this->sresponse();
		}

    }

    public function modificar() {

        $this -> load -> library("form_validation");
        $this -> data["titulo"] = "Modificando una institución de adscripción";
        $this -> form_validation -> set_rules("ins_id_institucion", "Id Institución", "required");

		//$this -> form_validation -> set_rules("ins_nombre", "nombre institución", "is_unique[cat_instituciones.ins_nombre]|required|regex_match[/^[A-Za-záéíóúÁÉÍÓÚñÑüÜ\s]+$/]|min_length[3]|max_length[50]||trim");
        $this -> form_validation -> set_rules("ins_siglas", "siglas Institución", "regex_match[/^[A-Za-záéíóúÁÉÍÓÚñÑüÜ\s]+$/]|min_length[1]|max_length[50]|trim");
        $this -> form_validation -> set_rules("ins_dependencia", "dependencia", "min_length[1]|max_length[100]|trim");
         $this -> form_validation -> set_rules("ins_sede", "sede", "min_length[1]|max_length[100]|trim");

        if ($this -> form_validation -> run() === FALSE) {
             echo $this->fresponse(  validation_errors()  );
        } else {
            $ins_id_institucion = $this -> input -> post("ins_id_institucion");
            //$ins_nombre = $this -> input -> post("ins_nombre");
            $ins_siglas = $this -> input -> post("ins_siglas");
			 $ins_dependencia = $this -> input -> post("ins_dependencia");
            $ins_sede = $this -> input -> post("ins_sede");
            //$ins_campus=$ins_campus==null?null:$ins_campus;
            $this -> load -> model("instituciones_m");
            $this -> instituciones_m -> modificar($ins_id_institucion, array("ins_siglas" => $ins_siglas, "ins_dependencia" => $ins_dependencia,"ins_sede" => $ins_sede));
              $this->session->set_flashdata("msg.ok", "Catalogo actualizado correctamente " );
            echo  $this->sresponse();
        }
    }



    public function buscarPorId($id) {
        $this -> load -> model("instituciones_m");
        echo $this -> sresponse(array("datos" => $this -> instituciones_m -> buscarPorId()));
        //return $this->buscarPorId();
    }

    
    public function agregar() {
        //Esta librería nos ayuda para hacer las validaciones,
        
        $this -> load -> library("form_validation");
        $this -> data["titulo"] = "Agregando una institucion";
		$this -> form_validation -> set_rules("ins_nombre", "Nombre institucion", "required|callback_validaCaracteres|min_length[3]|max_length[50]|is_unique[cat_instituciones.ins_nombre]|trim");
        $this -> form_validation -> set_rules("ins_siglas", "Siglas Institucion", "regex_match[/^[A-Za-záéíóúÁÉÍÓÚñÑüÜ\s]+$/]|min_length[1]|max_length[50]|trim");
        $this -> form_validation -> set_rules("ins_dependencia", "Nombre institucion", "min_length[1]|max_length[100]|trim");
        $this -> form_validation -> set_rules("ins_sede", "Nombre institucion", "min_length[1]|max_length[100]|trim");
        
        //Con el método run() estamos preguntando si todas las set_rules que establecimos se han pasado
        if ($this -> form_validation -> run() === FALSE) {
            //En caso de FALSE, osea, cuando no paso las validaciones, o cuando es la primera vez que entra a la página
            //Se le muestra simplemente el formulario
            echo $this->fresponse(  validation_errors() );
            //echo validation_errors();

            //$this->view("catalogos/instituciones_c");
        } else {
            //Si no es igual a FALSE, indica que fué TRUE, osea que si pasó todas las validaciones, por lo que
            //procedemos a insertarlo en la BD.

            //Primero recuperamos la información, para este caso sólo el nombre
            $nombre = $this -> input -> post("ins_nombre");
            $siglas = $this -> input -> post("ins_siglas");
            $ins_dependencia = $this -> input -> post("ins_dependencia");
            $ins_sede = $this -> input -> post("ins_sede");
            
            // if(!is_numeric($ins_campus)){
           //     $ins_campus=null;
            //}

            //Abrimos el modelo de instituciones_m
            $this -> load -> model("instituciones_m");

            //Una vez abierto, ya podemos usar sus métodos, sólo le pasamos un array asociativo de la forma "campo" => "valor", y lo agrega
            $agregado = $this -> instituciones_m -> agregar(array("ins_nombre" => $nombre, "ins_siglas" => $siglas, "ins_dependencia" => $ins_dependencia,"ins_sede" => $ins_sede));

            if ($agregado) {
                echo $this -> sresponse(array("datos"=>$agregado));
            } else {
                echo $this -> fresponse("hubo error");
            }

            //exit();
            //Redireccionamos para que no le de otra vez f5 y vuelva a agregar el mismo campo.
            //redirect("catalogos/instituciones_c");
        }
    }

    function set_campos() {
        $campos = array(
            "datos_institucion" => form_dropdown( "datos_institucion",array("" => "Seleccionar") +  get_cmb_data("cat_instituciones", "ins_id_institucion", "ins_nombre"),
            set_value("datos_institucion"), "id='datos_institucion' name='datos_institucion' class='input-xlarge' " )
        );
        
        $this->data["campos"] = $campos;
    }
    
        function validaCaracteres($str)
    {
        if (!preg_match("/^[A-Za-z0-9áéíóúÁÉÍÓÚñÑüÜç()<>:_'\"\;\.\,\s-\/]+$/", $str))
        {
            $this->form_validation->set_message('validaCaracteres', 'El campo %s contiene caracteres no válidos');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }    
    

} // END
