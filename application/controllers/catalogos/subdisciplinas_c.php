<?php
/**
 * undocumented class
 *
 * @package default
 * @author yunuen, sofia, yairguz  
 */
class Subdisciplinas_c extends MY_Controller {
	
	public function __construct(){
        parent::__construct();
    }
	
	public function index(){
		$this->is_private(1);
		$this->addJs("modulos/administracion.catalogos.js");
		$this->addJs("modulos/administracion.catalogos.subdisciplinas.js");
		$this->data["subdisciplinas"]=$this->buscarTodos(false,0);
		$this->data["vista"]= "subdisciplinas_c";
		$this->paginar('/catalogos/subdisciplinas_c/pagina',$this->subdisciplinas_m);
	    $this->view("admon_catalogos");
		//exit();
	}
	public function pagina($num_pagina=0){
		$this->is_private(1);
		$this->addJs("modulos/administracion.catalogos.js");
		$this->addJs("modulos/administracion.catalogos.subdisciplinas.js");
		$this->data["subdisciplinas"]=$this->buscarTodos(false,$num_pagina);
		$this->data["vista"]= "subdisciplinas_c";
		
		$this->paginar('/catalogos/subdisciplinas_c/pagina',$this->subdisciplinas_m);
	    $this->view("admon_catalogos");
	}
	private function paginar($url,$model){
		$this->is_private(1);
		$this->load->library('pagination');
		$config['base_url'] = site_url().$url;
		$config['total_rows'] = $model->count_all_results();
		$config['per_page'] = 5;
		$config["uri_segment"] = 4;
		$config['num_links'] = 5;
		$config['full_tag_open'] = '<div class="pagination pagination-centered"><ul>';
		
		$config['next_link'] = '&gt;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		
		$config['prev_link'] = '&lt;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		
		$config['first_link'] = 'Primera';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_link'] = 'Última';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active" ><a>';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		
		$config['full_tag_close'] = '</ul></div>';
		$this->pagination->initialize($config);
		$this->data["paginacion"]= $this->pagination->create_links();
	}
	/**
	 * Muestra el formulario de agregar, cuando le dan submit, si pasa todas las validaciones
	 * se agrega un nuevo registro de subdisciplinas.
	 *
	 * @return void
	 * @author  
	 */
	public function agregar(){
		$this->is_private(1);
		$this->load->library("form_validation"); 
		$this->data["titulo"] = "Agregando una subdisciplina";
		$this->form_validation->set_rules("sub_nombre","nombre subdisciplina", "required|callback_validaCaracteres|min_length[3]|max_length[50]|is_unique[cat_subdisciplinas.sub_nombre]|trim");
		if( $this->form_validation->run() === FALSE){
			echo $this->fresponse( validation_errors() );		
		}else{
			$nombre = $this->input->post("sub_nombre");
			$this->load->model("subdisciplinas_m");
			$agregado=$this->subdisciplinas_m->agregar( array("sub_nombre" => $nombre ) );
			if($agregado!=false){
				echo  $this->sresponse( array("datos"=>$agregado) );
			}else{
				echo $this->fresponse("hubo error");
			}
		}
	}
	
	public function eliminar(){
		$this->is_private(1);
		$this->load->library("form_validation");
		$this->data["titulo"] = "Borrando subdisciplina";
		$this->form_validation->set_rules("sub_id_subdisciplina","Id Subdisciplina", "required");
		
		if( $this->form_validation->run() === FALSE ){
			echo $this->fresponse( validation_errors() );
		}else{
			$idSubdisciplina=$this->input->post("sub_id_subdisciplina");
			$this->load->model("subdisciplinas_m");
			$this->subdisciplinas_m->eliminar( $idSubdisciplina );
			echo $this->sresponse();
		}
	}
	
	public function modificar(){
		$this->is_private(1);
		$this->load->library("form_validation");
		$this->data["titulo"] = "Modificando una subdisciplina";
		$this->form_validation->set_rules("sub_id_subdisciplina","Id Subdisciplina", "required");
		$this->form_validation->set_rules("sub_nombre","nombre Subdisciplina", "required|callback_validaCaracteres|min_length[3]|max_length[50]|is_unique[cat_subdisciplinas.sub_nombre]|trim");
		if( $this->form_validation->run() === FALSE){
			echo $this->fresponse( validation_errors() );
		}else{
			$sub_id_subdisciplina = $this->input->post("sub_id_subdisciplina");
			$sub_nombre = $this->input->post("sub_nombre");
			$this->load->model("subdisciplinas_m");	
			$this->subdisciplinas_m->modificar( $sub_id_subdisciplina, array("sub_nombre" => $sub_nombre ) );
			$this->session->set_flashdata("msg.ok", "Catalogo actualizado correctamente" );
			echo $this->sresponse();	
		}		
	}	
	
	public function buscarTodos($json=true,$num_pagina=0){
		$this->is_private(1);
		$this->load->model("subdisciplinas_m");
		$this->subdisciplinas_m->setLimit(5, $num_pagina);
		if($json){
			echo $this->sresponse( array("datos" => $this->subdisciplinas_m->buscarTodos()));
			 
		}else{
			return $this->subdisciplinas_m->buscarTodos();	
		}
	}
	
	public function buscarPorId($id){
		$this->is_private(1);
		$this->load->model("subdisciplinas_m");
		echo $this->sresponse( array("datos" => $this->subdisciplinas_m->buscarPorId()));
	}
	
	
	/**
	 * Obtiene las subdisciplinas de una disciplina dada
	 *
	 * @return void
	 * @author  UzielGL
	 */
	public function get_combobox_subdisciplinas( $id_disciplina="" ){
		$this->load->model("subdisciplinas_m");
		$subdisciplinas = $this->subdisciplinas_m->get_subdisciplinas(  $id_disciplina );
		
		//formamos el combo
		if( !$id_disciplina)
			$cmb = array("" => "Sin subdisciplinas");
		else
			$cmb = array("" => "Seleccionar");
		foreach( $subdisciplinas as $i){
			$cmb[ $i["sub_id_subdisciplina"] ] = $i["sub_nombre"];
		}
		
		echo form_dropdown( "tes_id_subdisciplina", $cmb, "",  "id=tes_id_subdisciplina");
		
	}
    
        function validaCaracteres($str)
    {
        if (!preg_match("/^[A-Za-z0-9áéíóúÁÉÍÓÚñÑüÜç()<>:_'\"\;\.\,\s-\/]+$/", $str))
        {
            $this->form_validation->set_message('validaCaracteres', 'El campo %s contiene caracteres no válidos');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }



} // END