<?php
/**
 * undocumented class
 *
 * @package default
 * @author yairguz , ggomez 
 */
class Especies_c extends MY_Controller {
	
	public function __construct(){
        parent::__construct();
        $this->is_private(1);
    }
	
	public function index(){
		$this->addJs("modulos/administracion.catalogos.js");
		$this->addJs("modulos/administracion.catalogos.especies.js");
		
		$this->data["especies"]=$this->buscarTodos(false,0);
		$this->data["vista"]= "especies_c";
		
		$this->paginar('/catalogos/especies_c/pagina',$this->especies_m);
	    $this->view("admon_catalogos");
	}
	public function pagina($num_pagina=0){
		$this->addJs("modulos/administracion.catalogos.js");
		$this->addJs("modulos/administracion.catalogos.especies.js");
		
		$this->data["especies"]=$this->buscarTodos(false,$num_pagina);
		$this->data["vista"]= "especies_c";
		
		$this->paginar('/catalogos/especies_c/pagina',$this->especies_m);
	    $this->view("admon_catalogos");
	}
	private function paginar($url,$model){
		$this->load->library('pagination');
		$config['base_url'] = site_url().$url;
		$config['total_rows'] = $model->count_all_results();
		$config['per_page'] = 5;
		$config["uri_segment"] = 4;
		$config['num_links'] = 5;
		$config['full_tag_open'] = '<div class="pagination pagination-centered"><ul>';
		
		$config['next_link'] = '&gt;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		
		$config['prev_link'] = '&lt;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		
		$config['first_link'] = 'Primera';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_link'] = 'Última';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active" ><a>';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		
		$config['full_tag_close'] = '</ul></div>';
		$this->pagination->initialize($config);
		$this->data["paginacion"]= $this->pagination->create_links();
	}
	/**
	 * Muestra el formulario de agregar, cuando le dan submit, si pasa todas las validaciones
	 * se agrega un nuevo registro de especies.
	 *
	 * @return void
	 * @author  
	 */
	public function agregar(){
		//print_r( $_POST );
		//Esta librería nos ayuda para hacer las validaciones, 
		$this->load->library("form_validation"); 
		
		//Pasamos una variable de ejemplo al formulario. No tiene nada que ver con el agregar
		//especie, sólo para mostrar también como se pasan variables a la vista.
		//La variable $titulo es la que estará disponible en la vista.
		$this->data["titulo"] = "Agregando una especie";
		
		//Establecemos las validaciones necesarias. Deben de ser de la forma:
		//set_rules("nombre_de_campo", "Nombre que se mostrará al usuario", "reglas"); 
		//Donde el "nombre que se mostrará al usuario" es por ejemplo algo como : El campo "Nombre especie" es requerido.
		$this->form_validation->set_rules("esp_nombre","nombre especie", "required|callback_validaCaracteres|min_length[3]|max_length[50]|is_unique[cat_especies.esp_nombre]|trim");
	
	
		//Con el método run() estamos preguntando si todas las set_rules que establecimos se han pasado
		if( $this->form_validation->run() === FALSE){
			//En caso de FALSE, osea, cuando no paso las validaciones, o cuando es la primera vez que entra a la página
			//Se le muestra simplemente el formulario
			echo $this->fresponse(  validation_errors()  );
			//echo validation_errors();
			
			//$this->view("catalogos/especies_c");		
		}else{
			//Si no es igual a FALSE, indica que fué TRUE, osea que si pasó todas las validaciones, por lo que
			//procedemos a insertarlo en la BD.
			
			//Primero recuperamos la información, para este caso sólo el nombre
			$nombre = htmlentities ( $this->input->post("esp_nombre")  );
			
			//Abrimos el modelo de especies_m
			$this->load->model("especies_m");
			
			
			//Una vez abierto, ya podemos usar sus métodos, sólo le pasamos un array asociativo de la forma "campo" => "valor", y lo agrega
			$agregado=$this->especies_m->agregar( array("esp_nombre" => $nombre ) );
			
			if($agregado!=false){
			    // Agregamos la subespecia N/A
			    $this->load->model("subespecies_m");
                $this->subespecies_m->agregar( array("sue_nombre" => 'N/A', "sue_id_especie" => $agregado['esp_id_especie'] ) );

                $this->session->set_flashdata('msg.ok', 'Se ha agregado exitosamente la especie.');
				echo  $this->sresponse( array("datos"=>$agregado, 'redirect_to' => site_url('catalogos/especies_c')) );
			}else{
				echo $this->fresponse("hubo error");
			}
			
			//exit();
			//Redireccionamos para que no le de otra vez f5 y vuelva a agregar el mismo campo.
			//redirect("catalogos/especies_c");
		}
	}
	
    function validaCaracteres($str)
    {
        if (!preg_match("/^[A-Za-z0-9áéíóúÁÉÍÓÚñÑüÜç()<>:_'\"\;\.\,\s-\/]+$/", $str))
        {
            $this->form_validation->set_message('validaCaracteres', 'El campo %s contiene caracteres no válidos');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    
	public function eliminar(){
		$this->load->library("form_validation");
		$this->data["titulo"] = "Borrando especies";
		$this->form_validation->set_rules("esp_id_especie","Id Especie", "required");
				
		if( $this->form_validation->run() === FALSE){
			echo $this->fresponse(  validation_errors()  );
		}else{
			$idEspecie = $this->input->post("esp_id_especie");
			$this->load->model("especies_m");	
			$this->especies_m->eliminar( $idEspecie );
			echo  $this->sresponse();
		}
	}
	
	public function modificar(){
		
		$this->load->library("form_validation");
		$this->data["titulo"] = "Modificando una especie";
		$this->form_validation->set_rules("esp_id_especie","Id Especie", "required");
		$this->form_validation->set_rules("esp_nombre","Nombre especie", "required|callback_validaCaracteres|min_length[3]|max_length[50]|is_unique[cat_especies.esp_nombre]|trim");
		if( $this->form_validation->run() === FALSE){
			echo $this->fresponse(  validation_errors()  );
		}else{
			$esp_id_especie = $this->input->post("esp_id_especie");
			$esp_nombre = $this->input->post("esp_nombre");
			$this->load->model("especies_m");	
			$this->especies_m->modificar( $esp_id_especie, array("esp_nombre" => $esp_nombre ) );
			$this->session->set_flashdata("msg.ok", "Catalogo actualizado correctamente " );
			echo  $this->sresponse();
		}		
	}	
	
	public function agregarSubespecie(){
		//print_r( $_POST );
		//Esta librería nos ayuda para hacer las validaciones, 
		$this->load->library("form_validation"); 
		
		//Pasamos una variable de ejemplo al formulario. No tiene nada que ver con el agregar
		//especie, sólo para mostrar también como se pasan variables a la vista.
		//La variable $titulo es la que estará disponible en la vista.
		$this->data["titulo"] = "Agregando una subespecie";
		
		//Establecemos las validaciones necesarias. Deben de ser de la forma:
		//set_rules("nombre_de_campo", "Nombre que se mostrará al usuario", "reglas"); 
		//Donde el "nombre que se mostrará al usuario" es por ejemplo algo como : El campo "Nombre especie" es requerido.
		$this->db->where("cat_subespecies.sue_id_especie", $this->input->post( "sue_id_especie", TRUE ) );
		$this->form_validation->set_rules("sue_id_especie","Id Especie", "required");
		$this->form_validation->set_rules("sue_nombre","nombre subespecie", "required|callback_validaCaracteres|min_length[3]|max_length[50]|is_unique[cat_subespecies.sue_nombre]|trim");
	
	
		//Con el método run() estamos preguntando si todas las set_rules que establecimos se han pasado
		if( $this->form_validation->run() === FALSE){
			//En caso de FALSE, osea, cuando no paso las validaciones, o cuando es la primera vez que entra a la página
			//Se le muestra simplemente el formulario
			echo $this->fresponse(  validation_errors()  );
			//echo validation_errors();
			
			//$this->view("catalogos/especies_c");		
		}else{
			//Si no es igual a FALSE, indica que fué TRUE, osea que si pasó todas las validaciones, por lo que
			//procedemos a insertarlo en la BD.
			
			//Primero recuperamos la información, para este caso sólo el nombre
			$nombre = htmlentities( $this->input->post("sue_nombre")  );
			$id_especie = $this->input->post("sue_id_especie");
			
			//Abrimos el modelo de especies_m
			$this->load->model("subespecies_m");
			
			
			//Una vez abierto, ya podemos usar sus métodos, sólo le pasamos un array asociativo de la forma "campo" => "valor", y lo agrega
			$agregado=$this->subespecies_m->agregar( array("sue_nombre" => $nombre, "sue_id_especie" => $id_especie ) );
			
			if($agregado!=false){
				echo  $this->sresponse( array("datos"=>$agregado) );
			}else{
				echo $this->fresponse("hubo error");
			}
			
			//exit();
			//Redireccionamos para que no le de otra vez f5 y vuelva a agregar el mismo campo.
			//redirect("catalogos/especies_c");
		}
	}
	
	public function eliminarSubespecie(){
		$this->load->library("form_validation");
		$this->data["titulo"] = "Borrando subespecie";
		$this->form_validation->set_rules("sue_id_subespecie","Id Subespecie", "required");
				
		if( $this->form_validation->run() === FALSE){
			echo $this->fresponse(  validation_errors()  );
		}else{
			$idSubespecie = $this->input->post("sue_id_subespecie");
			$this->load->model("subespecies_m");	
			$this->subespecies_m->eliminar( $idSubespecie );
			echo  $this->sresponse();
		}
	}
	
	public function modificarSubespecie(){
		$this->load->library("form_validation");
		$this->data["titulo"] = "Modificando una subespecie";
		
		$sue_id_subespecie = $this->input->post("sue_id_subespecie");
		$sue_nombre = htmlentities( $this->input->post("sue_nombre") );
		
		$this->form_validation->set_rules("sue_id_subespecie","Id Subespecie", "required");
		$this->form_validation->set_rules("sue_nombre","nombre de la subespecie", "required|callback_validaCaracteres|min_length[3]|max_length[50]|is_unique[cat_subespecies.sue_nombre]|trim");
		
		
		if( $this->form_validation->run() === FALSE){
			echo $this->fresponse(  validation_errors()  );
		}else{
			$sue_id_subespecie = $this->input->post("sue_id_subespecie");
			$sue_nombre = htmlentities( $this->input->post("sue_nombre") );
			$this->load->model("subespecies_m");
			$this->subespecies_m->modificar( $sue_id_subespecie, array("sue_nombre" => $sue_nombre ) );
			$this->session->set_flashdata("msg.ok", "Catalogo actualizado correctamente " );
			echo  $this->sresponse();
		}		
	}
	
	public function buscarTodos($json=true,$num_pagina=0){
		$this->load->model("especies_m");
		$this->especies_m->setLimit(5, $num_pagina);
		if($json){
			echo $this->sresponse( array("datos" => $this->especies_m->buscarTodos()) );
			 
		}else{
			$especies=$this->especies_m->buscarTodos();
			$this->load->model("subespecies_m");
			
			for($i=0; $i<count($especies); $i++) {
			    $subespecies = $this->subespecies_m->buscarPorIdEspecie($especies[$i]["esp_id_especie"]);
			    if($subespecies)
					$especies[$i]["subespecies"]=$subespecies;
				else
					$especies[$i]["subespecies"]=array();
			}
			
			return $especies;
		}
		
	}
	
	public function buscarPorId($id){
		$this->load->model("especies_m");
		echo $this->sresponse( array("datos" => $this->especies_m->buscarPorId()) );
		//return $this->buscarPorId();
	}

	/*public function obtener_por_id( $id ){
		$this->load->model("especies_m");
		$especie = $this->especies_m->get_by_id( $id );
		echo  $this->sresponse( array("data" => $especie) );	
	}*/

} // END