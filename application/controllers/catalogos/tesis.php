<?php
/**
 * undocumented class
 *
 * @package default
 * @author UzielGL, RubiGR
 */
class Tesis extends MY_Controller {
    
    
    public function __construct(){
        parent::__construct();
        $this->is_private(1);
    }
    
    /**
     * Lista las tesis
     *
     * @return void
     * @author  RubiGR
     */
    function index( $page=0 ) {
        $this->setTitle('Administración de tesis');
        
        $this->addJs("modulos/tesis.principal.js");
        $this->addJs("select2/select2.min.js", "plugs");
        $this->addJs("select2/select2.js", "plugs");
        $this->addCss("select2/select2.css", "plugs");
        
        
        //Hacemos persistente los input
        $this->load->library("form_validation");
        $this->form_validation->set_rules("palabra_clave", "", "");
        $this->form_validation->set_rules("tes_nombre", "", "");
        $this->form_validation->set_rules("tes_nombre_autor", "", "");
        $this->form_validation->set_rules("tes_id_grado_obtenido", "", "");
        $this->form_validation->set_rules("tes_anio_titulacion", "", "");
        $this->form_validation->set_rules("tes_id_institucion", "", "");
        $this->form_validation->set_rules("tes_id_disciplina_estudio", "", "");
        $this->form_validation->set_rules("tes_id_subdisciplina", "", "");
        $this->form_validation->set_rules("tes_id_tesis_autores", "", "");
        $this->form_validation->set_rules("tes_id_tesis_director", "", "");
        //$this->form_validation->set_rules("vista_subdisciplinas", "", "");
        $this->form_validation->set_rules("con_id_condicion_sitio", "", "");
        $this->form_validation->set_rules("est_id_zona_estudio", "", "");
        $this->form_validation->set_rules("order_by", "", "");
        $this->form_validation->set_rules("order_type", "", "");
        $this->form_validation->set_rules("tes_nombre_director", "", "");
        
        
        $this->form_validation->run();
        
        //Aplicamos los filtros
        $filtros = array(
            "tes_palabra_clave" => $this->input->post("palabra_clave"),
            "tes_id_institucion" => $this->input->post("tes_id_institucion"),
            "tes_id_disciplina_estudio" => $this->input->post("tes_id_disciplina_estudio"),
            "id_subdisciplina" => $this->input->post("tes_id_subdisciplina"),
            "tes_id_grado_obtenido" => $this->input->post("tes_id_grado_obtenido"),
            "id_especie" => $this->input->post("tes_id_especie"),
            "tes_nombre_autor" => $this->input->post("tes_nombre_autor"),
            "tes_id_tesis_director" => $this->input->post("tes_id_tesis_director"),
            "tes_id_tesis_autores" => $this->input->post("tes_id_tesis_autores"),
            "tes_nombre_director" => $this->input->post("tes_nombre_director"),
        );
        
        //Obtenemos las tesis con limite, filtros y orden
        $this->load->model("tesis_m");
        $this->tesis_m->set_filtros( $filtros );
        $this->tesis_m->set_orden( $this->input->post("order_by"), $this->input->post("order_type"));
        $this->db->limit( $this->limit, $page );
        $tesis = $this->tesis_m->get_all();
        $this->data["total_tesis"] = $total_tesis = $this->tesis_m->get_total_rows();
        
        //Creamos el paginador
        $this->data["paginador"] = $this->get_paginador( array(
            "base_url" => site_url("catalogos/tesis/index/"),
            "total_rows" => $total_tesis,
            "uri_segment" => 4          
        ) );
         
        
        
        $this->data["tesis"] = $tesis;
        
        
        //Checamos el campo de disciplina para saber que mostrar en subdisciplinas
        if( $id_disciplina = $this->input->post("tes_id_disciplina_estudio") ){
            $this->load->model("subdisciplinas_m");
            $subdisciplinas = $this->subdisciplinas_m->get_subdisciplinas(  $id_disciplina );
            //formamos el combo
            if( !$id_disciplina)
                $cmb = array("" => "Sin subdisciplinas");
            else
                $cmb = array("" => "Seleccionar");
            foreach( $subdisciplinas as $i){
                $cmb[ $i["sub_id_subdisciplina"] ] = $i["sub_nombre"];
            }
            $data_cmb_subdisciplinas = $cmb;
        }else{
            $data_cmb_subdisciplinas = get_cmb_data("cat_subdisciplinas", "sub_id_subdisciplina", "sub_nombre");
        }
        
        
        //Formamos los campos necesarios para la vista
        $campos = array(
            "tes_id_institucion" => form_dropdown( "tes_id_institucion", array("" => "Seleccionar") + get_cmb_data("cat_instituciones", "ins_id_institucion", "ins_nombre"), 
                                            set_value("tes_id_institucion"), "class='input-medium' id='tes_id_institucion'" ),
            "tes_id_disciplina_estudio" => form_dropdown( "tes_id_disciplina_estudio", array("" => "Seleccionar") + get_cmb_data("cat_disciplinas_estudio", "dis_id_disciplina_estudio", "dis_estudio_nombre"), 
                                            set_value("tes_id_disciplina_estudio"), "class='input-medium' id='tes_id_disciplina_estudio'" ),
            "tes_id_subdisciplina" => form_dropdown( "tes_id_subdisciplina", array("" => "Seleccionar") + $data_cmb_subdisciplinas, 
                                            set_value("tes_id_subdisciplina"), "class='input-medium' id='tes_id_subdisciplina'" ),
            "tes_id_especie" => form_dropdown( "tes_id_especie", array("" => "Seleccionar") + get_cmb_data("cat_especies", "esp_id_especie", "esp_nombre"), 
                                            set_value("tes_id_especie"), "class='input-medium' id='tes_id_especie'" ),
            "tes_id_tesis_director" => form_dropdown( "tes_id_tesis_director", array("" => "Seleccionar") + get_cmb_data("tesis_directores", "tes_id_tesis_director", "tes_nombre_director"), 
                                            set_value("tes_id_tesis_director"), "class='input-medium' id='tes_id_tesis_director'" ),
            "tes_id_tesis_autores" => form_dropdown( "tes_id_tesis_autores", array("" => "Seleccionar") + get_cmb_data("tesis_autores", "tes_id_tesis_autores", "tes_nombre_autor"), 
                                            set_value("tes_id_tesis_autores"), "class='input-medium' id='tes_id_tesis_autores'" ),  
            "palabra_clave" => array(
                "name" => "palabra_clave",
                "value" => set_value("palabra_clave"),
                "id" => "palabra_clave"
            ),
            "tes_nombre_autor" => array(
                "name" => "tes_nombre_autor",
                "value" => set_value("tes_nombre_autor"),
                "id" => "tes_nombre_autor"
            )
            
        );
        
        // El campo de nombre del director
        $this->load->model('directores_grados_m');
        $nombres_directores = $this->directores_grados_m->get_names();
        $data_nombres_directores = array();
        foreach($nombres_directores as $n)
        {
            $data_nombres_directores[ $n['name'] ] = $n['name']; 
        }
        $campos['tes_nombre_director'] = form_dropdown( "tes_nombre_director", array(''=>'Seleccionar') + $data_nombres_directores,
                                        set_value('tes_nombre_director'), "class='input-medium' id='tes_nombre_director'" );


        // El campo de nombre de autor
        $this->load->model('autores_m');
        $nombres_autores = $this->autores_m->get_names();
        $data_nombres_autores = array();
        foreach($nombres_autores as $n)
        {
            $data_nombres_autores[ $n['name'] ] = $n['name']; 
        }
        $campos['tes_nombre_autor'] = form_dropdown( "tes_nombre_autor", array(''=>'Seleccionar') + $data_nombres_autores,
                                        set_value('tes_nombre_autor'), "class='input-medium' id='tes_nombre_autor'" );                                        



        
        $this->db->where('gra_clasificacion', 'a');
        $campos["tes_id_grado_obtenido"] = form_dropdown( "tes_id_grado_obtenido", array("" => "Seleccionar") + get_cmb_data("grados_academicos", "gra_id_grado_academico", "gra_descripcion"), 
                                            set_value("tes_id_grado_obtenido"), "class='input-medium' id='tes_id_grado_obtenido'" );

        //Mandmos los campos a la vista
        $this->data["campos"] = $campos;
        
        
        
        $this->view("catalogos/tesis/principal");
    }
    
    /**
     * undocumented function
     *
     * @return void
     * @author  
     */
     
    
    function get_paginador( $conf_data ) {
        $base_data = array(
            'base_url'  => "/", // la url a la que apuntará base_url() . "/propiedad/listado",
            //'total_rows' =>  ,
            'per_page' => $this->limit,
            'num_links' => 8,
            "suffix" => "",
            'last_link' => '>>',
            'first_link' => '<<',
            'next_link'      => 'Siguiente &rarr;',
            
            'next_tag_open'  => '<li>',
            'next_tag_close' => '</li>',
            
            'prev_link'      => '&larr; Anterior',
            'prev_tag_open'  => '<li>',
            'prev_tag_close' => '</li>',
            
            'cur_tag_open'   => '<li class="active"><a>',
            'cur_tag_close'  => '</li></a>',
            
            'num_tag_open'   => '<li>',
            'num_tag_close'  => '</li>',
            
            'first_tag_open' => '<li>',
            'first_tag_close'=> '</li>',
            
            'last_tag_open'  => '<li>',
            'last_tag_close' => '</li>',
            
            "full_tag_open" => "<ul>",
            "full_tag_close" => "</ul>"
            
        );

        $data = array_merge($base_data, $conf_data);
        $this->load->library('pagination');
        $this->pagination->initialize($data);
        return $this->pagination->create_links();           
    }
    
    function ver_tesis_admin($tesis){
        $this->load->model("tesis_m");
        $this->addJs("modulos/tesis.visualizar.js");  
           
       
       $query = $this->tesis_m->visualizar_tesis_administrador($tesis); 
       if ($query->num_rows() > 0){
            foreach ($query->result() as $row){ 
                $this->data['tes_nombre']=$row->tes_nombre;
                $this->data['tes_ruta_tesis']=$row->tes_ruta_tesis;
                $this->data['tes_anio_titulacion']=$row->tes_anio_titulacion;
                $this->data['tes_palabras_clave']=$row->tes_palabras_clave;
                $this->data['tes_fecha_creacion']=$row->tes_fecha_creacion;
                $this->data['tes_fecha_modificacion']=$row->tes_fecha_modificacion;
                $this->data['tes_num_visitas']=$row->tes_num_visitas;
               // $this->data['tes_num_descargas']=$row->tes_num_descargas;
                $this->data['tes_usuario_crea']=$row->usuario_crea;
                $this->data['tes_usuario_mod']=$row->usuario_mod;
                $this->data['tes_esp_sub_especie']=explode(",", $row->esp_sub_especie);
                $this->data['tes_sub_disciplina']=explode(",", $row->sub_disciplina);
                $this->data['tes_directores']=explode(",", $row->directores);
                $this->data['tes_autores']=explode(",", $row->autores);
                $this->data['tes_grado_obtenido']=$row->grado_obtenido;
                $this->data['tes_ins_nombre']=$row->nombre_institucion;
                $this->data['tes_dependencia']=$row->ins_dependencia;
                 $this->data['tes_sede']=$row->ins_sede;
                $this->data['tes_condicion_sitio']=explode(",", $row->condicion_sitio);
                $this->data['tes_zonas_estudio']=explode(",", $row->zonas_estudio);
                $this->data['id_tesis']=$tesis;
                $this->data['total_calificacion']=$row->tes_total_calificaciones;
                $this->data['cantidad_calificacion']=$row->tes_cantidad_de_calificaciones;
                $this->data['tes_resumen']=$row->tes_resumen;
                $this->cant_cal=$row->tes_cantidad_de_calificaciones;
                $this->total_cal=$row->tes_total_calificaciones;
                if($this->cant_cal!=0){
                    $this->data['prom_cal']=$this->total_cal / $this->cant_cal;
                }
                else{
                    
                     $this->data['prom_cal']=0.0;
                }
				$cantidades=$this->tesis_m->cantidad_material_extra($tesis);
				$this->data['cantidad_imagenes']=$cantidades["cantidad_imagenes"]->row()->num; 
				$this->data['cantidad_documentos']=$cantidades["cantidad_documentos"]->row()->num;
				$this->data['cantidad_videos']=$cantidades["cantidad_videos"]->row()->num;
				$this->data['cantidad_audios']=$cantidades["cantidad_audios"]->row()->num;
            }
        }
        $this->view("catalogos/tesis/visualizar_tesis_administrador");

    }
    
    
    function registrar(){
        //Para plugin select2
       // $this->output->set_content_type("text/html;charset=utf-8");
        $this->addJs("select2/select2.min.js", "plugs");
        $this->addJs("select2/select2.js", "plugs");
        $this->addCss("select2/select2.css", "plugs");
        $this->addJs("modulos/tesis.registrar.js");
        $this -> addJs("jquery-ui-1.10.3.custom/jquery-ui-1.10.3.custom.js");
       $this -> addCss("jquery-ui-1.10.3.custom/ui-lightness/jquery-ui-1.10.3.custom.css", "plugs");
        $this->load->library("form_validation");
        //Establecemos reglas
        
        $this->set_validaciones();
        $nombre_archivo_tesis="";
        $tipo_archivo="";
        
        if(isset($_FILES['tes_ruta_tesis']['name']) && isset($_FILES['tes_ruta_tesis']['type'])){
            $nombre_archivo_tesis=$_FILES['tes_ruta_tesis']['name'];
            $tipo_archivo=$_FILES['tes_ruta_tesis']['type'];
        }
        if( $this->form_validation->run() === FALSE || $nombre_archivo_tesis=="" || $tipo_archivo!= "application/pdf"){ //Abrir la vista de nuevo si las validaciones no pasan o no se subió ningún archivo pdf
            $this->set_campos(); //Cargamos los campos que le pasaremos a la vista
            $this->view("catalogos/tesis/registrar_tesis");
            $this->session->set_flashdata("msg.error", "Llene los campos obligatorios y seleccione un archivo pdf");
        }
        else{ //Guardamos la tesis
            
            $palabras_clave = $this -> input -> post("tes_palabras_clave");
            $titulo = $this -> input -> post("tes_nombre");
            $tes_id_grado_obtenido = $this -> input -> post("gra_id_grado_academico");
            $anio_titulacion = $this -> input -> post("tes_anio_titulacion");
            $institucion = $this -> input -> post("tes_id_institucion");
            $resumen = $this -> input -> post("tes_resumen");
            //Campos multiples 
            $valSubespecies=$this -> input -> post("id_vista_subespecies2");
            $valZonasEstudio=$this -> input -> post("est_id_zona_estudio");

            $valCondicionesSitio=$this -> input -> post("con_id_condicion_sitio");
            $valSubdisciplinas=$this -> input -> post("id_vista_subdisciplinas");
            $valNombresAutores=$this -> input -> post("tes_nombre_autor");
            $valNombresDirectores=$this -> input -> post("tes_nombre_director");
            $valIdGradosAcademicos=$this -> input -> post("tes_id_grado_academico");
             
            //Abrimos el modelo de instituciones_m
            $this -> load -> model("tesis_m");

            //Una vez abierto, ya podemos usar sus métodos, sólo le pasamos un array asociativo de la forma "campo" => "valor", y lo agrega
            $id = $this -> tesis_m -> agregar2(array("tes_palabras_clave" => $palabras_clave, "tes_nombre" => $titulo, "tes_anio_titulacion" => $anio_titulacion, 
                                            "tes_id_institucion" => $institucion, "tes_id_grado_obtenido" => $tes_id_grado_obtenido, "tes_resumen" => $resumen));
            
            $ruta = $id.'.pdf';
            $this -> tesis_m -> modificar($id,array("tes_ruta_tesis" => $ruta));
            
           $idsSubespecie=explode(",", $valSubespecies[0]);
           if($idsSubespecie[0]!=""){
               for ($i = 0; $i < count($idsSubespecie); $i++) 
                {
                    if(!$idsSubespecie[$i]==""){
                        $data = array(
                            'tes_id_sub_especie' =>$idsSubespecie[$i],
                            'tes_id_tesis' =>$id
                        );      
                    $this -> tesis_m -> insertarTablasCruzadas("tesis_sub_especies",$data);
                    }
               }
           }
             
             foreach ($valZonasEstudio as $value) {
                $data = array(
                'tes_id_zona_estudio' =>$value,
                'tes_id_tesis' =>$id
               );
              $this -> tesis_m -> insertarTablasCruzadas("tesis_zona_estudio",$data);
              
            } 
            
             $idsCondiciones=explode(",", $valCondicionesSitio[0]);
             foreach ($valCondicionesSitio as $value) {
                $data = array(
                'tes_id_condicion_sitio' =>$value,
                'tes_id_tesis' =>$id
               );      
              $this -> tesis_m -> insertarTablasCruzadas("tesis_condiciones_sitio",$data);
            } 
             $idsSubdisciplinas=explode(",", $valSubdisciplinas[0]);
			 if($idsSubdisciplinas[0]!=""){
             foreach ($valSubdisciplinas as $value) {
                if($value!=""){
                    $data = array(
                        'tes_id_subdisciplina' =>$value,
                        'tes_id_tesis' =>$id
                    );      
                    $this -> tesis_m -> insertarTablasCruzadas("tesis_subdisciplinas",$data);
                }
             }
			 }
            for ($i = 0; $i < count($valNombresAutores); $i++) 
            {                  
                $data = array(
                'tes_nombre_autor' =>$valNombresAutores[$i],
                'tes_id_tesis' =>$id
               );      
              $this -> tesis_m -> insertarTablasCruzadas("tesis_autores",$data);
            }    
            for ($i = 0; $i < count($valNombresDirectores); $i++) 
            {                  
                $data = array(
                'tes_nombre_director' =>$valNombresDirectores[$i],
                'tes_id_grado_academico'=>$valIdGradosAcademicos[$i],
                'tes_id_tesis' =>$id
               );      
              $this -> tesis_m -> insertarTablasCruzadas("tesis_directores",$data);
            }    
                      
          $this->subir_tesis($id);
            
            //Despliegue de mensaje de registro correcto de tesis
            //printf("%s",$id);     
        }
    }

	function material_extra($id_tesis = 0){
        if($id_tesis==0){
            redirect('/catalogos/tesis/registrar');
        }
        //Para plugin select2
        $this->addJs("select2/select2.min.js", "plugs");
        $this->addJs("select2/select2.js", "plugs");
        $this->addCss("select2/select2.css", "plugs");
        $this->addJs("modulos/tesis.material_extra.js");
		
		$extension_valida=TRUE;
		if (isset($_FILES['tes_ruta_imagenes']['type'])){
		$ext_imgs=$_FILES['tes_ruta_imagenes']['type'];
		foreach($ext_imgs as $img){
			if($img != "image/jpeg" && $img != "image/png" && $img !=""){
				$this->session->set_flashdata("msg.error", "Solo se admiten imagenes .jpeg y .png");					
				$extension_valida = FALSE;
			}
		}
		}
		if (isset($_FILES['tes_ruta_documentos']['type'])){
		$ext_docs=$_FILES['tes_ruta_documentos']['type'];
		foreach($ext_docs as $doc){
			if($doc != "" && $doc != "application/pdf" && $doc != "application/vnd.openxmlformats-officedocument.wordprocessingml.document" && $doc != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"){
				$this->session->set_flashdata("msg.error", "Solo se aceptan documentos .pdf, .docx y .xls");
				$extension_valida = FALSE;
			}
		}
		}
		if (isset($_FILES['tes_ruta_audios']['type'])){
		$ext_auds=$_FILES['tes_ruta_audios']['type'];
		foreach($ext_auds as $aud){
			if($aud != "" && $aud != "audio/mp3"){
				$this->session->set_flashdata("msg.error", "Solo se aceptan audios en .mp3");
				$extension_valida = FALSE;
			}
		}
		}         
        if (isset($_POST['agregar']) && $extension_valida==TRUE){ 
            $tes_ruta_videos=$this -> input -> post("tes_nombre_videos");
            
            if($tes_ruta_videos){
                $this->subirVideos($id_tesis,$tes_ruta_videos);
            }
            if(isset($_FILES['tes_ruta_imagenes'])){
                $this->subirImagenes($id_tesis);    
            }
            if(isset($_FILES['tes_ruta_documentos'])){
                $this->subirDocumentos($id_tesis);
            }
            if(isset($_FILES['tes_ruta_audios'])){
                $this->subirAudios($id_tesis);
            }
            $this->session->set_flashdata("msg.ok", "El material extra se guardo correctamente");
            redirect('/catalogos/tesis/');
        }
        else{
        	$this->load->model("tesis_m");
            $this->data["tesis"] = $this->tesis_m->buscarPorId($id_tesis);
            
            $material_extra = array(
                "imagenes" => $this->tesis_m->get_imagenes($id_tesis),
                "videos" => $this->tesis_m->get_videos($id_tesis),
                "documentos" => $this->tesis_m->get_documentos($id_tesis),
				"audios" => $this->tesis_m->get_audios($id_tesis)  
            );
            $this->data["material_extra"] = $material_extra;
            $campos = array(
                "tes_ruta_imagenes" => array(
                    "name" => "tes_ruta_imagenes[]",
                    "value" => set_value("tes_ruta_imagenes"),
                    "class" => "input-file",
                    "type" => "file",
                    "onChange" => "vistaPrevia(this);",
                    "accept"=> "image/jpeg,image/png"
                ),
                "tes_nombre_videos" => array(
                    "name" => "tes_nombre_videos[]",
                    "value" => set_value("tes_nombre_video"),
                    "class" => "input-xlarge",
                    "id" => "tes_nombre_video",
                    "placeholder" => "Escriba la URL del video youtube"
                ),
                "tes_ruta_documentos" => array(
                    "name" => "tes_ruta_documentos[]",
                    "value" => set_value("tes_ruta_documentos"),
                    "class" => "input-file",
                    "type" => "file",
                    "accept"=> "application/pdf,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                ),
                "tes_ruta_audios" => array(
                    "name" => "tes_ruta_audios[]",
                    "value" => set_value("tes_ruta_audios"),
                    "class" => "input-file",
                    "type" => "file",
                    "accept"=> "audio/mp3"
                )
            );
            $this->data["campos"] = $campos;
            $this->session->set_flashdata("msg.error", "Error al intentar cargar el material extra");
            $this->view("catalogos/tesis/material_extra");
        }           
        
    }
    function eliminarVideo($id_video){
        $this->load->model("tesis_videos_m");
        $this->tesis_videos_m->eliminar($id_video);
    }
    function eliminarImagen($id_imagen){
        $this->load->model("tesis_imagenes_m");
        $imagen=$this->tesis_imagenes_m->buscarPorId($id_imagen);
        $this->tesis_imagenes_m->eliminar($id_imagen);
        unlink( './themes/default/img/'.$imagen['tes_ruta_imagen']	);
    }
    function eliminarDocumento($id_documento){
        $this->load->model("tesis_documentos_m");
        $doc=$this->tesis_documentos_m->buscarPorId($id_documento);
        $this->tesis_documentos_m->eliminar($id_documento);
		unlink( './themes/default/pdf/'.$doc['tes_ruta_documento']	);
    }
    function eliminarAudio($id_audio){
        $this->load->model("tesis_audios_m");
        $aud=$this->tesis_audios_m->buscarPorId($id_audio);
        $this->tesis_audios_m->eliminar($id_audio);
    	unlink( './themes/default/aud/'.$aud['tes_ruta_audio']	);
    }
    function subirVideos($id_tesis,$tes_ruta_videos){
        $this->load->model("tesis_videos_m");
        foreach($tes_ruta_videos as $ruta){
            if(isset($ruta) && !is_null($ruta) && !empty($ruta)){
                $datos=array("tes_id_tesis"=>$id_tesis,"tes_ruta_video"=>$ruta);
                $tesis_video=$this->tesis_videos_m->agregar($datos);
            }  
        }
    }
    private function uploadConfigImagenes($id_tesis,$imagen){
        $config = array();
        $config['upload_path'] = './themes/default/img/';
        $config['allowed_types'] =  'gif|jpg|png';
        $config['max_size'] = '4280';
        $config['overwrite'] = TRUE;
        return $config;
    }
    function subirImagenes($id_tesis){
        $this->load->library('upload');
        $files = $_FILES;
        $cpt = count($_FILES['tes_ruta_imagenes']['name']);
        $this->load->model("tesis_imagenes_m");
        for($i=0; $i<$cpt; $i++){
            $ruta2=$files['tes_ruta_imagenes']['name'][$i];
			$ruta=str_replace(" ", "_", $ruta2);
            if(isset($ruta2) && !is_null($ruta2) && !empty($ruta2)){
                $_FILES['tes_ruta_imagenes']['name']= $ruta;//$files['tes_ruta_imagenes']['name'][$i];
                $_FILES['tes_ruta_imagenes']['type']= $files['tes_ruta_imagenes']['type'][$i];
                $_FILES['tes_ruta_imagenes']['tmp_name']= $files['tes_ruta_imagenes']['tmp_name'][$i];
                $_FILES['tes_ruta_imagenes']['error']= $files['tes_ruta_imagenes']['error'][$i];
                $_FILES['tes_ruta_imagenes']['size']= $files['tes_ruta_imagenes']['size'][$i];
                $this->upload->initialize($this->uploadConfigImagenes($id_tesis,$ruta));
                $this->upload->do_upload('tes_ruta_imagenes');
                $datos=array("tes_id_tesis"=>$id_tesis,"tes_ruta_imagen"=>$_FILES['tes_ruta_imagenes']['name']);
                $tesis_imagen=$this->tesis_imagenes_m->agregar($datos);
            }  
        }
    }
    private function uploadConfigDocumentos($id_tesis,$documento){
        $config = array();
        $config['upload_path'] = './themes/default/pdf/';
        $config['allowed_types'] = 'pdf|doc|xls';
        $config['max_size'] = '20480';
        $config['overwrite'] = TRUE;
        return $config;
    }
    function subirDocumentos($id_tesis){
        $this->load->library('upload');
        $files = $_FILES;
        $cpt = count($_FILES['tes_ruta_documentos']['name']);
        $this->load->model("tesis_documentos_m");
        for($i=0; $i<$cpt; $i++){
            $ruta2=$files['tes_ruta_documentos']['name'][$i];
			$ruta=str_replace(" ", "_", $ruta2);
            if(isset($ruta2) && !is_null($ruta2) && !empty($ruta2)){
                $datos=array("tes_id_tesis"=>$id_tesis,"tes_ruta_documento"=>$ruta);
                $tesis_documento=$this->tesis_documentos_m->agregar($datos);
                $_FILES['tes_ruta_documentos']['name']= $ruta;//$files['tes_ruta_documentos']['name'][$i];
                $_FILES['tes_ruta_documentos']['type']= $files['tes_ruta_documentos']['type'][$i];
                $_FILES['tes_ruta_documentos']['tmp_name']= $files['tes_ruta_documentos']['tmp_name'][$i];
                $_FILES['tes_ruta_documentos']['error']= $files['tes_ruta_documentos']['error'][$i];
                $_FILES['tes_ruta_documentos']['size']= $files['tes_ruta_documentos']['size'][$i];
                $this->upload->initialize($this->uploadConfigDocumentos($id_tesis,$ruta));
                $this->upload->do_upload('tes_ruta_documentos');
            }  
        }
    }

private function uploadConfigAudios($id_tesis,$audio){
        $config = array();
        $config['upload_path'] = './themes/default/aud/';
        $config['allowed_types'] = 'mp3';
		$config['file_name'] = $audio;
        $config['max_size'] = '10240';
        $config['overwrite'] = TRUE;
        return $config;
    }
    function subirAudios($id_tesis){
        $this->load->library('upload');
        $files = $_FILES;
        $cpt = count($_FILES['tes_ruta_audios']['name']);
        $this->load->model("tesis_audios_m");
        for($i=0; $i<$cpt; $i++){
            $ruta2=$files['tes_ruta_audios']['name'][$i];
            $ruta=str_replace(" ", "_", $ruta2);
            if(isset($ruta2) && !is_null($ruta2) && !empty($ruta2)){
                $datos=array("tes_id_tesis"=>$id_tesis,"tes_ruta_audio"=>$ruta);
                $tesis_audio=$this->tesis_audios_m->agregar($datos);
                $_FILES['tes_ruta_audios']['name']= $ruta;//$files['tes_ruta_audios']['name'][$i];
                $_FILES['tes_ruta_audios']['type']= $files['tes_ruta_audios']['type'][$i];
                $_FILES['tes_ruta_audios']['tmp_name']= $files['tes_ruta_audios']['tmp_name'][$i];
                $_FILES['tes_ruta_audios']['error']= $files['tes_ruta_audios']['error'][$i];
                $_FILES['tes_ruta_audios']['size']= $files['tes_ruta_audios']['size'][$i];
                $this->upload->initialize($this->uploadConfigAudios($id_tesis,$ruta));
                $this->upload->do_upload('tes_ruta_audios');
            }  
        }
    }

     function validaCaracteres($str)
    {
        if (!preg_match("/^[A-Za-záéíóúÁÉÍÓÚñÑüÜç():_$'\"\%;\.\,\s-\/]+$/", $str))
        {
            $this->form_validation->set_message('validaCaracteres', 'El campo %s contiene caracteres no válidos');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    function validaCaracteresWithNumbers($str)
    {
        if (!preg_match("/^[0-9A-Za-záéíóúÁÉÍÓÚñÑüÜç():_$'\"\%;\.\,\s-\/]+$/", $str))
        {
            $this->form_validation->set_message('validaCaracteresWithNumbers', 'El campo %s contiene caracteres no válidos');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

/**
 *Eliminar tesis
 */
 
    public function eliminar( $tes_id_tesis ){
        
        $esta=  $this->data["tesis"]=$this->buscarPorId($tes_id_tesis,false);
            
        if($esta==TRUE){    
    
            $this->data["titulo"] = "Borrando Tesis";
            $this->load->model("tesis_m");  
            			
            $eliminada= $this->tesis_m->eliminar( $tes_id_tesis );
            
            if( IS_AJAX ){
                if( $eliminada )
                    echo $this->sresponse("Se eliminó correctamente");
                else
                    echo $this->fresponse("No se eliminó correctamente");
            }else{
                if($eliminada!=FALSE){
                    $this->session->set_flashdata("msg.ok", "Se eliminó correctamente");    
                    redirect("/catalogos/tesis");
                }else{
                    $this->session->set_flashdata("msg.error", "No se eliminó correctamente");
                    redirect("/catalogos/tesis");
                }
            }
        
        }else{
            if( IS_AJAX ){
                echo $this->fresponse("Lo sentimos, la tesis que intenta eliminar no existe.");
            }else{
                $this->data["content"] = "<h3 style='margin:100px 0;'><center>Lo sentimos, la tesis que intenta eliminar no existe.</center><h3>";
                $this->view();
            }
        } 
        
    }
 
 

    
    /**
     * Establece las reglas al registrar/editar una tesis
     *
     * @return void
     * @author  RubiGR
     */
    function set_validaciones() {

        $this->form_validation->set_rules("tes_nombre", "Título", "required|min_length[3]|max_length[1500]|callback_validaCaracteresWithNumbers");
        $this->form_validation->set_rules("tes_nombre_autor[]", "Autor", "required|min_length[3]|max_length[100]|callback_validaCaracteres");

        $this->form_validation->set_rules("tes_resumen", "Resumen de tesis", "required");   

        $this->form_validation->set_rules("tes_id_institucion", "Institución de adscripción", "required");
        $this->form_validation->set_rules("gra_id_grado_academico", "Grado obtenido", "required");

        $this->form_validation->set_rules("tes_anio_titulacion", "Año de titulación", "required|min_length[4]|max_length[4]|numeric|integer");
        $this->form_validation->set_rules("tes_nombre_director[]", "Directores de tesis", "required|min_length[4]|max_length[100]|callback_validaCaracteres");

        $this->form_validation->set_rules("tes_id_grado_academico[]", "Grado académico del Director", "required");
        $this->form_validation->set_rules("con_id_condicion_sitio", "Condición del sitio de estudio", "required");
        $this->form_validation->set_rules("est_id_zona_estudio", "Zona de estudio", "required");
        //$this->form_validation->set_rules("tes_ruta_tesis", "Archivo PDF de la tesis", "required");
        //$this->form_validation->set_rules("id_vista_subdisciplinas[]", "Subdisciplina de estudio", "required");
//      $this->form_validation->set_rules("tes_ruta_tesis", "Archivo de tesis PDF", "required");
        //$this->form_validation->set_rules("vista_subespecies", "Subespecies", "required");
    }
    
    /**
     * Agregalos campos necesarios para el formulario de la alta/edición de la tesis
     *
     * @return void
     * @author  RubiGR
     */
    function set_campos() {
        $campos = array(
            "tes_nombre" => array(
                "name" => "tes_nombre",
                "value" => set_value("tes_nombre"),
                "class" => "input-xxlarge",
                "placeholder" => "Escriba el título de la tesis"
            ),
            "tes_resumen" => array(
                "name" => "tes_resumen",
                "value" => set_value("tes_resumen"),
                "class" => "input-xxlarge",
                "placeholder" => "Escriba el resumen de la tesis"
            ),
            "tes_nombre_autor" => array(
                "name" => "tes_nombre_autor[]",
                "value" => set_value("tes_nombre_autor"),
                "class" => "input-xxlarge",
                "id" => "tes_nombre_autor",
                "placeholder" => "Escriba el nombre del autor"
            ),
            "tes_anio_titulacion" => array(
                "name" => "tes_anio_titulacion",
                "value" => set_value("tes_anio_titulacion"),
                "class" => "input-small",
                //"pattern"=> "[0-9.]*",
                "placeholder" => "4 dígitos"
            
            ),
            "tes_nombre_director" => array(
                "name" => "tes_nombre_director[]",
                "value" => set_value("tes_nombre_director"),
                "class" => "input-xxlarge",
                 "id" => "tes_nombre_director",
                "placeholder" => "Escriba el nombre del director de tesis"
            ),
            "tes_id_institucion" => form_dropdown( "tes_id_institucion",array("" => "Seleccionar") +  get_cmb_data("cat_instituciones", "ins_id_institucion", "ins_nombre"),
            set_value("tes_id_institucion"), "id='tes_id_institucion' class='input-xlarge' " ),
           
            "con_id_condicion_sitio" => form_dropdown( "con_id_condicion_sitio[]", get_cmb_data("cat_condiciones_sitio", "con_id_condicion_sitio", "con_nombre"),
            isset( $_POST["con_id_condicion_sitio"]) ? $_POST["con_id_condicion_sitio"] : "", "id='con_id_condicion_sitio' class='input-xlarge' placeholder='Seleccionar la condición del sitio' multiple='multiple'" ),
           
            /*"tes_id_grado_academico" => form_dropdown( "tes_id_grado_academico[]", array("" => "Seleccionar") + get_cmb_data("grados_academicos", "gra_id_grado_academico", "gra_descripcion"),
            set_value("tes_id_grado_academico"), "id='tes_id_grado_academico' name='tes_id_grado_academico[]' class='input-xlarge' " ),
             * */
             /*
             "tes_id_grado_academico" => array(
                "tes_id_grado_academico[]", 
                array("" => "Seleccionar") + get_cmb_data("grados_academicos", "gra_id_grado_academico", "gra_descripcion"),
                set_value("tes_id_grado_academico"), 
                "id='tes_id_grado_academico' name='tes_id_grado_academico[]' class='input-xlarge' "),
            
              */
            //cargando las vistas
        
             "vista_subespecies" => form_dropdown( "id_vista_subespecies[]", get_cmb_data("vista_especie_subespecie", "sue_id_subespecie", "nombre_esp_subesp"),
            isset($_POST["id_vista_subespecies"]) ? $_POST["id_vista_subespecies"] : "", "id='id_vista_subespecies' class='input-xlarge' placeholder='Seleccionar la especie' multiple='multiple'" ),
        
             "vista_subdisciplinas" => form_dropdown( "id_vista_subdisciplinas[]", get_cmb_data("vista_disciplina_subdisciplina", "sub_id_subdisciplina", "nombre_dis_subdis"),
            isset($_POST["id_vista_subdisciplinas"]) ? $_POST["id_vista_subdisciplinas"]: "", "id='id_vista_subdisciplinas' placeholder='Seleccionar la subdisciplina' class='input-xlarge' multiple='multiple'" ),
            
            "tes_ruta_tesis" => array(
                "name" => "tes_ruta_tesis",
                "value" => set_value("tes_ruta_tesis"),
                "class" => "input-file",
                "type" => "file",
                "accept"=> "application/pdf",
                "placeholder" => "Escriba el nomb re del autor"
            )
        );
        
        $this->db->order_by('est_nombre');
        $campos["est_id_zona_estudio"] = form_dropdown( "est_id_zona_estudio[]", get_cmb_data("cat_zonas_estudio", "est_id_zona_estudio", "est_nombre"),
            isset($_POST["est_id_zona_estudio"])? $_POST["est_id_zona_estudio"]:"", "id='est_id_zona_estudio'  placeholder='Seleccionar la zona de estudio' class='input-xlarge' multiple='multiple'" );
        
        
        $this->db->where('gra_clasificacion', 'a');
        $campos['gra_id_grado_academico'] = form_dropdown( "gra_id_grado_academico", array("" => "Seleccionar") + get_cmb_data("grados_academicos", "gra_id_grado_academico", "gra_descripcion"),
            set_value("gra_id_grado_academico"), "id='gra_id_grado_academico' name='gra_id_grado_academico' class='input-xlarge' " );
        $this->db->where('gra_clasificacion', 'a');
        $campos['tes_id_grado_academico']=array(
                "tes_id_grado_academico[]", 
                array("" => "Seleccionar") + get_cmb_data("grados_academicos", "gra_id_grado_academico", "gra_descripcion"),
                set_value("tes_id_grado_academico"), 
                "id='tes_id_grado_academico' name='tes_id_grado_academico[]' class='input-xlarge' ");
        $this->data["campos"] = $campos;
    }
    
    function subir_tesis($id_tesis){
        $config['upload_path'] = './themes/default/pdf/';
        $config['allowed_types'] = 'pdf';
        $config['file_name'] = ''.$id_tesis;
        $config['max_size'] = '50800';
        //printf("%s", $this->input->post("tes_ruta_tesis"));
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('tes_ruta_tesis')){
            $error = array('error' => $this->upload->display_errors());
            //printf("Error al cargar archivo".$error['error']);
            $this->session->set_flashdata("msg.error", "No se pudo agregar el archivo");
            redirect("/catalogos/tesis/registrar");
        }
        else{
            $data = array('upload_data' => $this->upload->data());
            //printf("Archivo subido con éxito");
            $this->session->set_flashdata("msg.ok", "Se agregó correctamente");
            if(isset($_POST["material_extra"])){
                redirect("/catalogos/tesis/material_extra/".$id_tesis);
            }
            else{
                redirect("/catalogos/tesis");
            }
        }
    }
    
    public function modificar($idTesis){
        $this->addJs("select2/select2.min.js", "plugs");
        $this->addJs("select2/select2.js", "plugs");
        $this->addCss("select2/select2.css", "plugs");
        $this->addJs("modulos/tesis.modificar.js");
        $this -> addJs("jquery-ui-1.10.3.custom/jquery-ui-1.10.3.custom.js");
        $this -> addCss("jquery-ui-1.10.3.custom/ui-lightness/jquery-ui-1.10.3.custom.css", "plugs");
        $existe=$this->data["tesis"]=$this->buscarPorId($idTesis,false);      
        $this->data["vista"]= "tesis"; 
        $this->data["tesis"]=$existe;
        $this->load->library("form_validation");
        $this->set_validaciones();     
        if($existe){
                
            $_POST["tes_nombre_autor"] = array();
             $autores = $this->get_autores($idTesis,false);
             //print_r($autores);exit();
             foreach( $autores as $i){
               $_POST["tes_nombre_autor"][] = $i["tes_nombre_autor"];
             }
             $_POST["tes_nombre_director"]=array();
             $_POST["tes_id_grado_academico"]=array();
             $directores = $this->get_directores($idTesis,false);
            //print_r($directores);exit();
             foreach( $directores as $i){
                       $_POST["tes_nombre_director"][] = $i["tes_nombre_director"];
                       $_POST["tes_id_grado_academico"][]= $i["tes_id_grado_academico"];   
                    }
             $_POST["id_vista_subespecies"]=array();
             $subespecies = $this->get_subespecies($idTesis,false);
             //print_r($subespecies);exit();
             foreach($subespecies as $i){
                 $_POST["id_vista_subespecies"][]=$i["tes_id_sub_especie"];
             }
             $_POST["est_id_zona_estudio"]=array();
             $zonas= $this->get_zonas_estudio($idTesis,false);
             //print_r($directores);exit();
             foreach($zonas as $i){
                 $_POST["est_id_zona_estudio"][]=$i["tes_id_zona_estudio"];
             }
             $_POST["id_vista_subdisciplinas"]=array();
             $subdisciplinas=$this->get_subdisciplinas($idTesis,false);
             foreach($subdisciplinas as $i){
                 $_POST["id_vista_subdisciplinas"][]=$i["tes_id_subdisciplina"];
             }
             $_POST["con_id_condicion_sitio"]=array();
             $sitios=$this->get_condicion_sitio($idTesis,false);
             foreach($sitios as $i){
                 $_POST["con_id_condicion_sitio"][]=$i["tes_id_condicion_sitio"];
             }
             $this->set_tesis();    
             $this->view("catalogos/tesis/modificar_tesis");
        }
        else{
            $this->data["content"] = "<h3 style='margin:100px 0;'><center>La tesis que intenta modificar no existe.</center><h3>";
            $this->view();
        }
        
    }
    public function modificarTesis(){
            $this->addJs("select2/select2.min.js", "plugs");
            $this->addJs("select2/select2.js", "plugs");
            $this->addCss("select2/select2.css", "plugs");
            $this->addJs("modulos/tesis.modificar.js");
            $this->load->library("form_validation");
            $this->set_validaciones();     
            if( $this->form_validation->run() === FALSE){
               echo $this->fresponse(validation_errors());
            }else{
               $id=$this->input->post("id_tesis");
               $palabras_clave = $this -> input -> post("tes_palabras_clave");
               $titulo = $this -> input -> post("tes_nombre");
               $tes_id_grado_obtenido = $this -> input -> post("gra_id_grado_academico");
               $anio_titulacion = $this -> input -> post("tes_anio_titulacion");
               $institucion = $this -> input -> post("tes_id_institucion");
               $resumen = $this -> input -> post("tes_resumen");
               //Campos multiples 
               $valSubespecies=$this -> input -> post("id_vista_subespecies");
               $valZonasEstudio=$this -> input -> post("est_id_zona_estudio");
               $valCondicionesSitio=$this -> input -> post("con_id_condicion_sitio");
               $valSubdisciplinas=$this -> input -> post("id_vista_subdisciplinas");
               $valNombresAutores=$this -> input -> post("tes_nombre_autor");
               $valNombresDirectores=$this -> input -> post("tes_nombre_director");
               $valIdGradosAcademicos=$this -> input -> post("tes_id_grado_academico");
               //$tes_ruta=$this->input->post("tes_ruta_tesis");
               $this -> load -> model("tesis_m");
               $ruta = $id.'.pdf';
               //Una vez abierto, ya podemos usar sus métodos, sólo le pasamos un array asociativo de la forma "campo" => "valor", y lo agrega
               $this -> tesis_m -> modificar($id,array("tes_palabras_clave" => $palabras_clave, "tes_nombre" => $titulo, "tes_anio_titulacion" => $anio_titulacion, 
                                            "tes_id_institucion" => $institucion, "tes_id_grado_obtenido" => $tes_id_grado_obtenido, "tes_resumen" => $resumen,"tes_ruta_tesis" => $ruta));
               if(!empty($valSubespecies)){
                   $this->tesis_m->eliminarRegistrosCruzados("tesis_sub_especies",array("tes_id_tesis"=>$id));
                  for ($i = 0; $i < count($valSubespecies); $i++) 
                  {
                    $data = array(
                        'tes_id_sub_especie' =>$valSubespecies[$i],
                        'tes_id_tesis' =>$id
                        );      
                            $this -> tesis_m -> modificarTablasCruzadas("tesis_sub_especies",$data);
                        }
                }else{
                    $this->tesis_m->eliminarRegistrosCruzados("tesis_sub_especies",array('tes_id_tesis' =>$id));
                }
                if(!empty($valZonasEstudio)){
                   $this->tesis_m->eliminarRegistrosCruzados("tesis_zona_estudio",array('tes_id_tesis' =>$id));
                    for($i=0;$i<count($valZonasEstudio);$i++){
                        $data =array(
                            'tes_id_zona_estudio' =>$valZonasEstudio[$i],
                            'tes_id_tesis' =>$id
                        );
                        $this->tesis_m->modificarTablasCruzadas("tesis_zona_estudio",$data);
                    } 
                }
                if(!empty($valSubdisciplinas)){
                    $this->tesis_m->eliminarRegistrosCruzados("tesis_subdisciplinas",array('tes_id_tesis'=>$id));
                    for($i=0;$i<count($valSubdisciplinas);$i++){
                        $data =array(
                            'tes_id_subdisciplina'=> $valSubdisciplinas[$i],
                            'tes_id_tesis' => $id
                        );
                        $this->tesis_m->modificarTablasCruzadas("tesis_subdisciplinas",$data);
                    }
                }
               if(!empty($valCondicionesSitio)){
                   $this->tesis_m->eliminarRegistrosCruzados("tesis_condiciones_sitio",array('tes_id_tesis'=>$id));
                   for($i=0;$i<count($valCondicionesSitio);$i++){
                       $data =array(
                          'tes_id_condicion_sitio'=>$valCondicionesSitio[$i],
                          'tes_id_tesis' => $id
                       );
                       $this->tesis_m->modificarTablasCruzadas("tesis_condiciones_sitio",$data);
                   }
               }
               if(!empty($valNombresAutores)){
                   $this->tesis_m->eliminarRegistrosCruzados("tesis_autores",array('tes_id_tesis'=>$id));
                   for($i=0;$i<count($valNombresAutores);$i++){
                       $data =array(
                          'tes_nombre_autor'=>$valNombresAutores[$i],
                          'tes_id_tesis'=>$id
                       );
                       $this->tesis_m->modificarTablasCruzadas("tesis_autores",$data);
                   }
               }
               if(!empty($valNombresDirectores)){
                   $this->tesis_m->eliminarRegistrosCruzados("tesis_directores",array('tes_id_tesis'=>$id));
                   for($i=0;$i<count($valNombresDirectores);$i++){
                       $data =array(
                          'tes_nombre_director'=>$valNombresDirectores[$i],
                          'tes_id_grado_academico'=>$valIdGradosAcademicos[$i],
                          'tes_id_tesis'=>$id
                       );
                       $this-> tesis_m -> modificarTablasCruzadas("tesis_directores",$data);
                   }
               }
               $this->session->set_flashdata("msg.ok", "La tesis se edito correctamente");
               echo $this->sresponse();
            }
    }
     public function buscarPorId($id,$json=true){
        $this->load->model("tesis_m");
        if($json){
            echo $this->sresponse( array("datos" => $this->tesis_m->buscarPorId($id)) );
        }else{
            return $this->tesis_m->buscarPorId($id);
        }
        //return $this->buscarPorId();
     }
    function set_tesis(){
        $campos = array(
                "tes_palabras_clave" => array(
                "name" => "tes_palabras_clave",
                "value" => set_value('tes_palabras_clave',$this->data["tesis"]["tes_palabras_clave"]),
                "class" => "input-xxlarge",
                "placeholder" => "Escriba las palabras claves separadas por coma"           
            ),
            "tes_nombre" => array(
                "name" => "tes_nombre",
                "value" => set_value('tes_nombre',$this->data["tesis"]["tes_nombre"]),
                "class" => "input-xxlarge",
                "placeholder" => "Escriba el título de la tesis"
            ),
            "tes_nombre_autor" => array(
                "name" => "tes_nombre_autor[]",
                "value" => set_value("tes_nombre_autor"),
                "class" => "input-xxlarge",
                "placeholder" => "Escriba el nombre del autor"
            ),
            "tes_anio_titulacion" => array(
                "name" => "tes_anio_titulacion",
                "value" => set_value("tes_anio_titulacion",$this->data["tesis"]["tes_anio_titulacion"]),
                "class" => "input-small",
                "placeholder" => "4 dígitos"
            
            ),
            "tes_nombre_director" => array(
                "name" => "tes_nombre_director[]",
                "value" => set_value("tes_nombre_director"),
                "id" => "tes_nombre_director",
                "class" => "input-xxlarge cdirector",
                "placeholder" => "Escriba el nombre del director de tesis"
            ),
            "tes_id_institucion" => form_dropdown( "tes_id_institucion",array("" => "Seleccionar") +  get_cmb_data("cat_instituciones", "ins_id_institucion", "ins_nombre"),
            set_value("tes_id_institucion",$this->data["tesis"]["tes_id_institucion"]), "id='tes_id_institucion' name='tes_id_institucion' class='input-xlarge' " ),
            "dis_id_disciplina" => form_dropdown( "dis_id_disciplina",array(
                "" => "Seleccionar") +  get_cmb_data("cat_disciplinas_estudio", 
                "dis_id_disciplina_estudio", 
                "dis_estudio_nombre"),
            set_value("dis_id_disciplina",$this->data["tesis"]["tes_id_tesis"]), "id='dis_id_disciplina' name='dis_id_disciplina' class='input-xlarge' " ),
            
            "tes_resumen" => array(
                "name" => "tes_resumen",
                "value" => set_value("tes_resumen",$this->data["tesis"]["tes_resumen"]),
                "class" => "input-xxlarge",
                "placeholder" => "Escriba el resumen de la tesis"
            ),
        
             "est_id_zona_estudio" => form_dropdown( "est_id_zona_estudio[]", get_cmb_data("cat_zonas_estudio", "est_id_zona_estudio", "est_nombre"),
            isset($_POST["est_id_zona_estudio"])? $_POST["est_id_zona_estudio"]:$this->data["tesis"]["tes_id_grado_obtenido"], "id='est_id_zona_estudio'  placeholder='Seleccionar la zona de estudio' class='input-xlarge' multiple='multiple'" ),

            /*
            "esp_id_especie" => form_dropdown( "esp_id_especie", get_cmb_data("cat_especies", "esp_id_especie", "esp_nombre"),
            set_value("esp_id_especie",$this->data["tesis"]["tes_id_tesis"]), "id='esp_id_especie' name='esp_id_especie[]' class='input-xlarge' placeholder='Seleccionar la especie' multiple='multiple'" ),
            
            "vista_subespecies" => form_dropdown( "vista_subespecies", get_cmb_data("vista_especie_subespecie", "sue_id_subespecie", "nombre_esp_subesp"),
            set_value("vista_subespecies",$this->data["tesis"]["tes_id_tesis"]), "id='id_vista_subespecies' name='id_vista_subespecies' class='input-xlarge' placeholder='Seleccionar la especie' multiple='multiple'" ),

            "vista_subdisciplinas" => form_dropdown( "vista_subdisciplinas", get_cmb_data("vista_disciplina_subdisciplina", "sub_id_subdisciplina", "nombre_dis_subdis"),
            set_value("vista_subdisciplinas",$this->data["tesis"]["tes_id_tesis"]), "id='id_vista_subdisciplinas' name='id_vista_subdisciplinas' placeholder='Seleccionar la subdisciplina' class='input-xlarge' multiple='multiple'" ),            
              */
            
             "vista_subespecies" => form_dropdown( "id_vista_subespecies[]", get_cmb_data("vista_especie_subespecie", "sue_id_subespecie", "nombre_esp_subesp"),
            isset($_POST["id_vista_subespecies"]) ? $_POST["id_vista_subespecies"] : "", "id='id_vista_subespecies' class='input-xlarge' placeholder='Seleccionar la especie' multiple='multiple'" ),
        
            "vista_subdisciplinas" => form_dropdown( "id_vista_subdisciplinas[]", get_cmb_data("vista_disciplina_subdisciplina", "sub_id_subdisciplina", "nombre_dis_subdis"),
            isset($_POST["id_vista_subdisciplinas"]) ? $_POST["id_vista_subdisciplinas"]: "", "id='id_vista_subdisciplinas' placeholder='Seleccionar la subdisciplina' class='input-xlarge' multiple='multiple'" ),
            "con_id_condicion_sitio" => form_dropdown( "con_id_condicion_sitio[]", get_cmb_data("cat_condiciones_sitio", "con_id_condicion_sitio", "con_nombre"),
            isset( $_POST["con_id_condicion_sitio"]) ? $_POST["con_id_condicion_sitio"] : "", "id='con_id_condicion_sitio' class='input-xlarge' placeholder='Seleccionar la condición del sitio' multiple='multiple'" ),
                                  
            /*"con_id_condicion_sitio" => form_dropdown( "con_id_condicion_sitio", get_cmb_data("cat_condiciones_sitio", "con_id_condicion_sitio", "con_nombre"),
            set_value("con_id_condicion_sitio",$this->data["tesis"]["tes_id_tesis"]), "id='con_id_condicion_sitio' name='con_id_condicion_sitio' class='input-xlarge' placeholder='Seleccionar la condición del sitio' multiple='multiple'" ),
            "id_subdisciplinas_estudio" => form_dropdown( "id_subdisciplinas_estudio", get_cmb_data("cat_subdisciplinas", "sub_id_subdisciplina", "sub_nombre"),
            set_value("id_subdisciplinas_estudio",$this->data["tesis"]["tes_id_tesis"]), "id='id_subdisciplinas_estudio' name='id_subdisciplinas_estudio' placeholder='Seleccionar la subdisciplina' class='input-xlarge' multiple='multiple'" ),

           /*"tes_id_grado_academico" => form_dropdown( "tes_id_grado_academico[]", array("" => "Seleccionar") + get_cmb_data("grados_academicos", "gra_id_grado_academico", "gra_descripcion"),
            set_value("tes_id_grado_academico"), "class='input-xlarge'" ),
            **/
           "tes_id_grado_academico" => array(
           "tes_id_grado_academico[]", 
           array("" => "Seleccionar") + get_cmb_data("grados_academicos", "gra_id_grado_academico", "gra_descripcion"),
           set_value("tes_id_grado_academico"), 
           "id='tes_id_grado_academico' name='tes_id_grado_academico[]' class='input-xlarge' "),
            
           "tes_ruta_tesis" => array(
                "name" => "tes_ruta_tesis",
                "id" => "tes_ruta_tesis",
                "value" => set_value("tes_ruta_tesis"/*,$this->data["tesis"]["tes_ruta_tesis"]*/),
                "class" => "input-file",
                "type" => "file",
                "accept"=> "application/pdf",
                "placeholder" => "Ruta de la tesis"
            ),
            
            
        );
        $this->db->where('gra_clasificacion', 'a');
        $campos['gra_id_grado_academico'] = form_dropdown( "gra_id_grado_academico", array("" => "Seleccionar") + get_cmb_data("grados_academicos", "gra_id_grado_academico", "gra_descripcion"),
            set_value("gra_id_grado_academico",$this->data["tesis"]["tes_id_grado_obtenido"]), "id='gra_id_grado_academico' name='gra_id_grado_academico' class='input-xlarge' " );
            
        
        $this->data["campos"] = $campos;
    }


    public function get_autores($id,$json=true){
        $this->load->model("tesis_m");
        if($json){
            echo $this->sresponse( array("datos" => $this->tesis_m->get_autores($id)));
        }else{
            return $this->tesis_m->get_autores($id);
        }
        //return $this->buscarPorId();
     }
     public function get_directores($id,$json=true){
        $this->load->model("tesis_m");
        if($json){
            echo $this->sresponse( array("datos" => $this->tesis_m->get_directores($id)));
        }else{
            return $this->tesis_m->get_directores($id);
        }         
     }
     public function get_grado_academico($id,$json=true){
        $this->load->model("tesis_m");
        if($json){
            echo $this->sresponse( array("datos" => $this->tesis_m->get_grado_academico($id)));
        }else{
            return $this->tesis_m->get_grado_academico($id);
        }         
     }
     public function get_subespecies($id,$json=true){
        $this->load->model("tesis_m");    
        if($json){
            echo $this->sresponse(array("datos"=> $this->tesis_m->get_subespecies($id)));
        }else{
            return $this->tesis_m->get_subespecies($id);
        } 
     }
     public function get_zonas_estudio($id,$json=true){
         $this->load->model("tesis_m");    
         if($json){
             echo $this->sresponse(array("datos"=> $this->tesis_m->get_zonas_estudio($id)));
         }else{
             return $this->tesis_m->get_zonas_estudio($id);
         }
     }
     public function get_subdisciplinas($id,$json=true){
         $this->load->model("tesis_m");
         if($json){
             echo $this->sresponse(array("datos"=> $this->tesis_m->get_subdisciplinas($id)));
         }else{
             return $this->tesis_m->get_subdisciplinas($id);
         }
     }
     public function get_condicion_sitio($id,$json=true){
         $this->load->model("tesis_m");
         if($json){
             echo $this->sresponse(array("datos" => $this->tesis_m->get_condicion_sitio($id)));
         }else{
             return $this->tesis_m->get_condicion_sitio($id);
         }
     }
     public function editarTesisPDF($id){
            $this->addJs("select2/select2.min.js", "plugs");
            $this->addJs("select2/select2.js", "plugs");
            $this->addCss("select2/select2.css", "plugs");
            //$this->addJs("modulos/tesis.modificar.js");
            $this->addJs("modulos/tesis.modificar.pdf.js");
            $this->load->library("form_validation");
            $existe=$this->buscarPorId($id,false);
            $this->data["tesis"]=$existe;
            if($existe){
              $this->view("catalogos/tesis/modificar_tesis_pdf");   
            }else{
              $this->data["content"] = "<h3 style='margin:100px 0;'><center>El PDF de tesis que intenta modificar no existe.</center><h3>";
              $this->view();
            }
             
     }
     public function remplazarPDF(){
        $this->addJs("modulos/tesis.modificar.pdf.js");
        $id_tesis=$this->input->post("id_tesis");
        $config['upload_path'] = './themes/default/pdf/';
        $config['allowed_types'] = 'pdf';
        $config['file_name'] = ''.$id_tesis;
        $config['max_size'] = '204800';
        $config['overwrite']=TRUE;
        //printf("%s", $this->input->post("tes_ruta_tesis"));
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('tes_ruta_tesis')){
            $error = array('error' => $this->upload->display_errors());
            //printf("Error al cargar archivo".$error['error']);
            $this->session->set_flashdata("msg.error", "No se pudo modificar el archivo");
            redirect("/catalogos/tesis/editarTesisPDF/".$id_tesis);            
        }
        else{
            $data = array('upload_data' => $this->upload->data());
            //printf("Archivo subido con éxito");
            $this->session->set_flashdata("msg.ok", "Se modificó correctamente");
            redirect("/catalogos/tesis/editarTesisPDF/".$id_tesis);
            /*if(isset($_POST["material_extra"])){
                redirect("/catalogos/tesis/material_extra/".$id_tesis);
            }
            else{
                redirect("/catalogos/tesis");
            }*/
        }
     }
        function obtener_directores() {
    
        $this -> load -> model('tesis_m');
        if (isset($_GET['term'])) {
            $q = strtolower($_GET['term']);
            $this -> tesis_m -> obtener_directores($q);
        }
    }



} // END