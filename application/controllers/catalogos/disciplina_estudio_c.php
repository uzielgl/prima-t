<?php
/**
 * @author FerGR , zlopez 
 */

class Disciplina_estudio_c extends My_controller{
	
	public function __construct(){
        parent::__construct();
        $this->is_private(1);
    }
		
	public function index(){
	    $this->addJs("modulos/administracion.catalogos.js");
		$this->addJs("modulos/administracion.catalogos.disciplinas.js");
        $this->data["disciplinas"] = $this->buscarTodos(false,0);
        $this->data["vista"] = "disciplinas_c";
		$this->paginar('/catalogos/disciplina_estudio_c/pagina',$this->disciplinas_estudio_m);
        $this->view("admon_catalogos");
	}
	public function pagina($num_pagina=0){
		$this->addJs("modulos/administracion.catalogos.js");
		$this->addJs("modulos/administracion.catalogos.disciplinas.js");
        $this->data["disciplinas"] = $this->buscarTodos(false,$num_pagina);
		$this->data["vista"] = "disciplinas_c";
		
		$this->paginar('/catalogos/disciplina_estudio_c/pagina',$this->disciplinas_estudio_m);
	    $this->view("admon_catalogos");
	}
    private function paginar($url,$model){
		$this->load->library('pagination');
		$config['base_url'] = site_url().$url;
		$config['total_rows'] = $model->count_all_results();
		$config['per_page'] = 5;
		$config["uri_segment"] = 4;
		$config['num_links'] = 5;
		$config['full_tag_open'] = '<div class="pagination pagination-centered"><ul>';
		
		$config['next_link'] = '&gt;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		
		$config['prev_link'] = '&lt;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		
		$config['first_link'] = 'Primera';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_link'] = 'Última';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active" ><a>';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		
		$config['full_tag_close'] = '</ul></div>';
		$this->pagination->initialize($config);
		$this->data["paginacion"]= $this->pagination->create_links();
	}
	public function agregar(){
		$this->load->library("form_validation");
		$this->data["título"] = "Agregando una disciplina de estudio";
		//$this->form_validation->set_rules("dis_estudio_nombre","Nombre de disciplina de estudio", "required");
				
		$this->form_validation->set_rules("dis_estudio_nombre","nombre de disciplina de estudio", "required|callback_validaCaracteres|min_length[3]|max_length[50]|is_unique[cat_disciplinas_estudio.dis_estudio_nombre]|trim");
	    
	    if( $this->form_validation->run() === FALSE){
			echo $this->fresponse(validation_errors());	
		}else{
			$nombre = htmlentities( $this->input->post("dis_estudio_nombre") );
			$this->load->model("disciplinas_estudio_m");
			$agregado = $this->disciplinas_estudio_m->agregar( array("dis_estudio_nombre" => $nombre ) );
		
			if($agregado != false){
				//Datos agregados correctamente
				
				$this->load->model("subdisciplinas_m");
                $this->subdisciplinas_m->agregar( array("sub_nombre" => 'N/A', "sub_id_disciplina_estudio" => $agregado['dis_id_disciplina_estudio']) );
                $this->session->set_flashdata('msg.ok', 'Se ha agregado la disciplina exitosamente.');
				
				echo  $this->sresponse( array("datos"=>$agregado, 'redirect_to' => site_url('/catalogos/disciplina_estudio_c/') ) );
			}else{
				echo $this->fresponse("hubo error");
			}
		//	redirect("catalogos/disciplina_estudio_c");
		}
	}

	public function eliminar(){
		$this->load->library("form_validation");
		$this->data["título"] = "Borrando disciplina de estudio";	
		$this->form_validation->set_rules("dis_id_disciplina_estudio","Id Disciplina", "required");
	
	
	  if( $this->form_validation->run() === FALSE){
			//Si no pasa las validaciones	
			echo $this->fresponse(  validation_errors()  );
		}else{
			$dis_id_disciplina_estudio = $this->input->post("dis_id_disciplina_estudio");
			$this->load->model("disciplinas_estudio_m");	
			$this->disciplinas_estudio_m->eliminar($dis_id_disciplina_estudio);
			echo  $this->sresponse();
	 	}
	}
	
	public function modificar(){
		$this->load->library("form_validation");
		$this->data["título"] = "Modificando una disciplina de estudio";
		$this->form_validation->set_rules("dis_id_disciplina_estudio","ID de disciplina de estudio","requeried");
	   $this->form_validation->set_rules("dis_estudio_nombre","nombre de disciplina de estudio", "required|callback_validaCaracteres|min_length[3]|max_length[50]|is_unique[cat_disciplinas_estudio.dis_estudio_nombre]");		
     //$this->form_validation->set_rules("dis_estudio_nombre","Nombre de disciplina de estudio", "required");		
       
		if($this->form_validation->run() === FALSE){
			echo $this->fresponse(  validation_errors()  );
		}else{
			$dis_id_disciplina_estudio = $this->input->post("dis_id_disciplina_estudio");
			$dis_estudio_nombre = htmlentities( $this->input->post("dis_estudio_nombre") );
			$this->load->model("disciplinas_estudio_m");
			$this->disciplinas_estudio_m->modificar( $dis_id_disciplina_estudio, array("dis_estudio_nombre" => $dis_estudio_nombre));
			$this->session->set_flashdata("msg.ok", "Catalogo actualizado correctamente" );
			echo  $this->sresponse();
		}
	}
	public function agregarSubdisciplina(){
		$this->load->library("form_validation"); 
		$this->data["titulo"] = "Agregando una subdisciplina";
		$this->form_validation->set_rules("sub_id_disciplina_estudio","Id Disciplina", "required");
		$this->form_validation->set_rules("sub_nombre","nombre subdisciplina", "required|callback_validaCaracteres|min_length[3]|max_length[50]|is_unique[cat_subdisciplinas.sub_nombre]|trim");
		if( $this->form_validation->run() === FALSE){
			echo $this->fresponse( validation_errors() );		
		}else{
			$nombre = htmlentities( $this->input->post("sub_nombre") );
			$id_disciplina = $this->input->post("sub_id_disciplina_estudio");
			$this->load->model("subdisciplinas_m");
			$agregado=$this->subdisciplinas_m->agregar( array("sub_nombre" => $nombre, "sub_id_disciplina_estudio" => $id_disciplina) );
			if($agregado!=false){
				echo  $this->sresponse( array("datos"=>$agregado) );
			}else{
				echo $this->fresponse("hubo error");
			}
		}
	}
		
	public function eliminarSubdisciplina(){
		$this->load->library("form_validation");
		$this->data["titulo"] = "Borrando subdisciplina";
		$this->form_validation->set_rules("sub_id_subdisciplina","Id Subdisciplina", "required");
		
		if( $this->form_validation->run() === FALSE ){
			echo $this->fresponse( validation_errors() );
		}else{
			$idSubdisciplina=$this->input->post("sub_id_subdisciplina");
			$this->load->model("subdisciplinas_m");
			$this->subdisciplinas_m->eliminar( $idSubdisciplina );
			echo $this->sresponse();
		}
	}
	
	public function modificarSubdisciplina(){
		
		$this->load->library("form_validation");
		$this->data["titulo"] = "Modificando una subdisciplina";
		$this->form_validation->set_rules("sub_id_subdisciplina","Id Subdisciplina", "required");
		$this->form_validation->set_rules("sub_nombre","nombre subdisciplina", "required|callback_validaCaracteres|min_length[3]|max_length[50]|is_unique[cat_subdisciplinas.sub_nombre]|trim");
		if( $this->form_validation->run() === FALSE){
			echo $this->fresponse( validation_errors() );
		}else{
			$sub_id_subdisciplina = $this->input->post("sub_id_subdisciplina");
			$sub_nombre = htmlentities( $this->input->post("sub_nombre") );
			$this->load->model("subdisciplinas_m");	
			$this->subdisciplinas_m->modificar( $sub_id_subdisciplina, array("sub_nombre" => $sub_nombre ) );
			$this->session->set_flashdata("msg.ok", "Catalogo actualizado correctamente" );
			echo $this->sresponse();	
		}		
	}
	public function buscarTodos($json=true,$num_pagina=0){
     	$this->load->model("disciplinas_estudio_m");
     	$this->disciplinas_estudio_m->setLimit(5, $num_pagina);
		if($json){  
			echo $this->sresponse( array("datos" => $this->disciplinas_estudio_m->buscarTodos()) );
		}else{
			$disciplinas=$this->disciplinas_estudio_m->buscarTodos();
			$this->load->model("subdisciplinas_m");
			
			for($i=0; $i<count($disciplinas); $i++) {
			    $subdisciplinas = $this->subdisciplinas_m->buscarPorIdDisciplina($disciplinas[$i]["dis_id_disciplina_estudio"]);
			    if($subdisciplinas)
					$disciplinas[$i]["subdisciplinas"]=$subdisciplinas;
				else
					$disciplinas[$i]["subdisciplinas"]=array();
			}
			
			return $disciplinas;
		}
	}
	
	public function buscarPorId($id){
		$this->load->model("disciplinas_estudio_m");
		echo $this->sresponse( array("datos" => $this->disciplinas_estudio_m->buscarPorId()) );
		//return $this->buscarPorId();
	}
    
    
    function validaCaracteres($str)
    {
        if (!preg_match("/^[A-Za-z0-9áéíóúÁÉÍÓÚñÑüÜç()<>:_'\"\;\.\,\s-\/]+$/", $str))
        {
            $this->form_validation->set_message('validaCaracteres', 'El campo %s contiene caracteres no válidos');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    
	
}//END
