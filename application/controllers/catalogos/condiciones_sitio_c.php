<?php
class Condiciones_sitio_c extends MY_Controller{
	
	public function __construct(){
        parent::__construct();
        $this->is_private(1);
    }
	
    public function index(){
        $this->addJs("modulos/administracion.catalogos.js");
        $this->addJs("modulos/administracion.catalogos.condiciones.js");
        $this->data["condiciones"]= $this->buscarTodos(false,0);
        $this->data["vista"]= "condiciones_c";
		$this->paginar('/catalogos/condiciones_sitio_c/pagina',$this->condiciones_sitio_m);
        $this->view("admon_catalogos");
    }
	public function pagina($num_pagina=0){
		$this->addJs("modulos/administracion.catalogos.js");
        $this->addJs("modulos/administracion.catalogos.condiciones.js");
        $this->data["condiciones"]= $this->buscarTodos(false,$num_pagina);
		$this->data["vista"]= "condiciones_c";
		
		$this->paginar('/catalogos/condiciones_sitio_c/pagina',$this->condiciones_sitio_m);
	    $this->view("admon_catalogos");
	}
	private function paginar($url,$model){
		$this->load->library('pagination');
		$config['base_url'] = site_url().$url;
		$config['total_rows'] = $model->count_all_results();
		$config['per_page'] = 5;
		$config["uri_segment"] = 4;
		$config['num_links'] = 5;
		$config['full_tag_open'] = '<div class="pagination pagination-centered"><ul>';
		
		$config['next_link'] = '&gt;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		
		$config['prev_link'] = '&lt;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		
		$config['first_link'] = 'Primera';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_link'] = 'Última';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active" ><a>';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		
		$config['full_tag_close'] = '</ul></div>';
		$this->pagination->initialize($config);
		$this->data["paginacion"]= $this->pagination->create_links();
	}
    public function agregar(){
        $this->load->library("form_validation"); 
        $this->data["titulo"] = "Agregando una condición de sitio";
        $this->form_validation->set_rules("con_nombre","nombre condición del sitio","required|callback_validaCaracteres|min_length[3]|max_length[50]|is_unique[cat_condiciones_sitio.con_nombre]");
        if( $this->form_validation->run() === FALSE){
            echo $this->fresponse(  validation_errors()  );
            
            //$this->view("catalogos/especies_c");      
        }else{
            $nombre = $this->input->post("con_nombre");
            $this->load->model("condiciones_sitio_m");
            $agregado=$this->condiciones_sitio_m->agregar( array("con_nombre" => $nombre ) );
            if($agregado!=false){
                //echo  $this->sresponse();
                echo  $this->sresponse( array("datos"=>$agregado) );
            }else{
                echo $this->fresponse("hubo error");
            }
            
            //exit();
            //Redireccionamos para que no le de otra vez f5 y vuelva a agregar el mismo campo.
            //redirect("catalogos/especies_c");
        }
        
    }
    public function eliminar(){
        $this->load->library("form_validation");
        $this->data["titulo"] = "Borrando Condiciones de Sitio";
        $this->form_validation->set_rules("con_id_condicion_sitio","ID condicion", "required");
        if($this->form_validation->run()===FALSE){
            echo $this-fresponse( validation_erros());
        }else{
            $idCondicion = $this->input->post("con_id_condicion_sitio");
            $this->load->model("condiciones_sitio_m");
            $this->condiciones_sitio_m->eliminar($idCondicion);
            echo $this->sresponse();
        }
    }
    public function modificar(){
        $this->load->library("form_validation");
        $this->data["titulo"] = "Modificando una condición de sitio";
        $this->form_validation->set_rules("con_id_condicion_sitio","ID condicion", "required");
        $this->form_validation->set_rules("con_nombre","nombre condición sitio", "required|callback_validaCaracteres|min_length[3]|max_length[50]|is_unique[cat_condiciones_sitio.con_nombre]");
        if( $this->form_validation->run() === FALSE){            
            echo $this->fresponse(  validation_errors()  );
        }else{
        	
            $con_id_condicion_sitio = $this->input->post("con_id_condicion_sitio");
            $con_nombre = $this->input->post("con_nombre");
            $this->load->model("condiciones_sitio_m");   
            
            $this->condiciones_sitio_m->modificar( $con_id_condicion_sitio, array("con_nombre" => $con_nombre ) );
            $this->session->set_flashdata("msg.ok", "Has actualizado el catálogo con id " . $con_id_condicion_sitio );
            echo  $this->sresponse();
        }
    }
    public function buscarTodos($json=true,$num_pagina=0){
        $this->load->model("condiciones_sitio_m");
        $this->condiciones_sitio_m->setLimit(5, $num_pagina);
        if($json){
            echo $this->sresponse(array("datos" => $this->condiciones_sitio_m->buscarTodos()));
        }else{
            return $this->condiciones_sitio_m->buscarTodos();    
        }
        
    }
    
    function validaCaracteres($str)
    {
        if (!preg_match("/^[A-Za-z0-9áéíóúÁÉÍÓÚñÑüÜç()<>:_'\"\;\.\,\s-\/]+$/", $str))
        {
            $this->form_validation->set_message('validaCaracteres', 'El campo %s contiene caracteres no válidos');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    
}
