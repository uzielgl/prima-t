<?php

/**
 * Clase de pruebas
 *
 * @author almiis sanchez
 */
class Disciplinas_c extends MY_Controller {
	
	public function index(){
		$this->addJs("modulos/administracion.catalogos.js");
		
		$this->data["disciplinas"] = array(
			array("dis_id_disciplina_estudio"=>"1",
				  "dis_estudio_nombre"=>"disciplina1"),
			array("dis_id_disciplina_estudio"=>"2",
				  "dis_estudio_nombre"=>"disciplina2"),
			array("dis_id_disciplina_estudio"=>"3",
				  "dis_estudio_nombre"=>"disciplina3"),
		);
		$this->data["vista"] = "disciplinas_c";
	    $this->view("admon_catalogos");
	}
} // END