<?php
/**
 * undocumented class
 *
 * @package default
 * @author fgonzalez 
 */
class contacto_c extends MY_Controller {

	public function __construct(){
        parent::__construct();
    }
	
	function index(){
		$this -> addJs("contacto.js");
				
		$this->setTitle('Contacto');
		$this->view("contacto");
	}
	
		
} // END