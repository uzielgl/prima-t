<?php
class Tesis_c extends MY_Controller{
    
    var $visualizar=1;
    var $descargarCSV=2;
    
    public function index()
    {
        $this->visitas_por_tesis($this->visualizar);
    }
    
    
    /**
     * Reporte de visitas por tesis.
     *
     * @return void
     * @author  
     */
    function visitas_por_tesis($accion = 1 ) {
        
        $this->view("reportes/tesis/visitas_por_tesis");
        
        /*
        $this->load->model("tesis_m");
        $aTesis = $this->tesis_m->reporte_visitas();        
        if($accion==$this->descargarCSV){
            $this->tesis_m->load->dbutil();
            $this->load->helper('download');
            force_download("visitas_tesis.csv", utf8_decode($this->tesis_m->dbutil->csv_from_result($aTesis)) );            
            
        }else{
            $this->data["aTesis"]=$aTesis->result_array();
            $this->view("reportes/tesis/visitas_por_tesis");
        }
         * 
         */
        
    } 
	
	function descargas_por_tesis($accion = 1 ) {
        $this->load->model("tesis_m");
		$filtros = array(
			'from' => $this->input->post('from'),
			'to' => $this->input->post('to')
		);
		
		$accion = $this->input->post('accion') ?: $accion;
		
        $aTesis = $this->tesis_m->reporte_descargas( $filtros );        
        if($accion==$this->descargarCSV){
            $this->tesis_m->load->dbutil();
            $this->load->helper('download');
            force_download("descargas_tesis.csv", utf8_decode($this->tesis_m->dbutil->csv_from_result($aTesis)) );            
            
        }else{
        	//Hacemos persistentes los cambios
        	$this->load->library('form_validation');
			$this->form_validation->set_rules('from', 'Desde', '')
				->set_rules('to', 'Hasta', '');
			$this->form_validation->run();
			
            //Cargamos archivos para calendario
            $this->addJs("jquery-ui-1.10.3.custom/jquery-ui-1.10.3.custom.min.js", "plugs");
            $this->addJs("jquery-ui-1.10.3.custom/jquery.ui.datepicker-es.js", "plugs");
        
            $this->addCss("jquery-ui-1.10.3.custom/smoothness/jquery-ui-1.10.3.custom.min.css", "plugs");
            
            $this->addJs('modulos/reportes.descargas.js');
        
            $this->data["aTesis"]=$aTesis->result_array();
            $this->view("reportes/tesis/descargas_tesis");
        }
        
    }
    
    function especiesSubespecies_por_tesis($accion=1){
        
        if($accion==$this->descargarCSV){
            $this->load->dbutil();
            $this->load->helper('download');
            $this->load->model('subespecies_m');
            $result=$this->subespecies_m->getCantidadPorTesis(1);
            
            $data = utf8_decode( $this->dbutil->csv_from_result( $this->subespecies_m->getCantidadPorTesis(-1)  ) );
            $name = 'reporte_especies_subespecies.csv';
            
            force_download($name, $data);                 
            
        }else{
            $this->load->model("especies_m");    
            $this->data['especies'] = $this->especies_m->getCantidadPorTesis();
            $this->view('reportes/tesis/especies_subespecies_por_tesis');
        }
    } 
    
    public function getSubespeciesPorTesis( $id_especie ) {
        $this->load->model('subespecies_m');
        return $this->subespecies_m->getCantidadPorTesis( $id_especie  );
    }
    
}
