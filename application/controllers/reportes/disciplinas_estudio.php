<?php
class Disciplinas_estudio extends MY_Controller{
	
	public function index()
	{
		$this->load->model('disciplinas_estudio_m');
		$this->data['disciplinas'] = $this->disciplinas_estudio_m->get_con_cantidad();
		
		$this->view('reportes/disciplinas_estudio/index');
	}
	
	/**
	 * Obtiene las subdisciplinas de cierta disciplinas con los totales de tesis asociadas
	 *
	 * @return void
	 * @author  
	 */
	public function _get_subdisciplinas_con_cantidad( $id_disciplina ) 
	{
		$this->load->model('subdisciplinas_m');
		return $this->subdisciplinas_m->get_subdisciplinas_con_cantidad( $id_disciplina  );
	}
	
	/**
	 * Crea el reporte de disciplinas de estudios en csv
	 *
	 * @return void
	 * @author  
	 */
	function csv() {
		$this->load->dbutil();
		$this->load->helper('download');
		$this->load->model('subdisciplinas_m');
		
		$data = utf8_decode( $this->dbutil->csv_from_result( $this->subdisciplinas_m->get_all_subdisciplinas_con_cantidad()  ) );
		$name = 'reporte_disciplinas_subdisciplinas.csv';
		
		force_download($name, $data); 	
	}
}
