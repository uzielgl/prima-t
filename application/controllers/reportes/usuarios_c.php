<?php
class Usuarios_c extends MY_Controller{
    
    var $visualizar=1;
    var $descargarCSV=2;
    
    public function index()
    {
        $this->setTitle('Reporte de grado acádemico y género por usuarios');
        $this->grado_academico_genero($this->visualizar);
    }
    
    
    /**
     * Reporte de grado_academico_genero por usuario.
     *
     * @return void
     * @author  
     */
    function grado_academico_genero($accion = 1 ) {
        $this->load->model("reportes/usuarios_m");
        $aUsuarios = $this->usuarios_m->reporte_usuarios();        
        if($accion==$this->descargarCSV){
            $this->usuarios_m->load->dbutil();
            $this->load->helper('download');
            force_download("grado_academico_genero_usuarios.csv", utf8_decode($this->usuarios_m->dbutil->csv_from_result($aUsuarios)) );            
            
        }else{
            $this->data["aUsuarios"]=$aUsuarios->result_array();
            $this->view("reportes/usuarios/grado_academico_genero");
        }
        
    } 

    
}
