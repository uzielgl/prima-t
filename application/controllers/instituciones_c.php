<?php

/**
 * Clase de pruebas
 *
 * @author almiis sanchez
 */
class Instituciones_c extends MY_Controller {
	
	public function index(){
		
		$this->data["institucion"] = "institucion";
		$this->data["estado"] = "estado";
		$this->data["vista"]= "instituciones_c";
	    $this->view("admon_catalogos");
	}
} // END