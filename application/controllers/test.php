<?php

/**
 * Clase de pruebas
 *
 * @author Uziel García López  
 */
class Test extends MY_Controller {
    public $cant_cal;
    public $total_cal;
    public function index($tesis=1){
        //$this->data["content"] = "HOLA";
    //  $this->addJs("mi_js.js");
        //$this->addCss("mi_css.js");
        //$this->view("test");
        $this->addJs("select2/select2.min.js", "plugs");
        $this->addJs("select2/select2.js", "plugs");
        $this->addCss("select2/select2.css", "plugs");
        
       $this->load->model("tesis_m");
       $query = $this->tesis_m->fichatecnica($tesis);
       if ($query->num_rows() > 0){
            foreach ($query->result() as $row){
                $this->data['tes_nombre']=$row->tes_nombre;
                $this->data['tes_anio_titulacion']=$row->tes_anio_titulacion;
                $this->data['tes_resumen']=$row->tes_resumen;
                $this->data['tes_num_visitas']=$row->tes_num_visitas;
                $this->data['tes_num_descargas']=$row->tes_num_descargas;
                $this->data['tes_nombre_autor']=$row->tes_nombre_autor;
                $this->data['ins_nombre']=$row->ins_nombre;
                $this->data['est_nombre']=$row->est_nombre;
                $this->data['gra_descripcion']=$row->gra_descripcion;
                
            }
        }
       
        $this->view("visualizar_tesis");           
    }
    
    public function guardar_calificacion(){
        
    
        if(htmlentities($_POST['action'], ENT_QUOTES, 'UTF-8') == 'rating')
        {
        
        $aResponse['rate'] = floatval($_POST['rate']);
        $aResponse['idBox']=intval($_POST['idBox']);
        $this->load->model("tesis_m");
       $datos_tesis = $this->tesis_m->buscarPorId($aResponse['idBox']);
       $aResponse['total_calificacion']=$datos_tesis['tes_total_calificaciones'];
       $aResponse['cant_calificaciones']=$datos_tesis['tes_cantidad_de_calificaciones'];
       $cant_cal=$aResponse['cant_calificaciones']+1;
       $total_calificacion=$aResponse['total_calificacion']+ $aResponse['rate'];
        $this->load->model("tesis_m");
        $success= $this->tesis_m->modificarCalificacion($aResponse['idBox'], array("tes_cantidad_de_calificaciones"=>$cant_cal,"tes_total_calificaciones"=>$total_calificacion));
        if($success)
        {
        // save into your database or anything else
   
    // send the rate to your javascript file
        echo json_encode($aResponse);
         }
    }
        
        
    }
    public function ver_tesis($tesis=1){
        //Cosas por Marco
         //$this->addJs("jRating/jquery.js", "plugs");
          $this->addJs("jRating/jRating.jquery.js", "plugs");
        $this->addCss("jRating/jRating.jquery.css", "plugs");       
          $this->addJs("calificaciones/calificaciones.js");
          $this->addJs("modulos/tesis.visualizar.js");  
        
        //Agregamos lo necesario para la Galeria
        $this->addJs('jquery.prettyPhoto.js');
        $this->addCss('pretty-photo/prettyPhoto.css');
        
    
        //$this->addCss("mi_css.js");
        //$this->view("test");
       $this->addJs("modulos/tesis.comentar.js");    
       $this->load->model("tesis_m");
       
       //Agregamos una visita
       $this->tesis_m->incrementar_visitas( $tesis );
       
       $query = $this->tesis_m->visualizar_tesis_administrador($tesis); 
       if ($query->num_rows() > 0){
           $this->load->model('descargas_m');
           
            foreach ($query->result() as $row){ 
                $this->data['tes_nombre']=$row->tes_nombre;
                $this->data['tes_ruta_tesis']=$row->tes_ruta_tesis;
                $this->data['tes_anio_titulacion']=$row->tes_anio_titulacion;
                $this->data['tes_resumen']=$row->tes_resumen;
                $this->data['tes_num_visitas']=$row->tes_num_visitas;
                $this->data['tes_num_descargas']=$this->descargas_m->get_total($tesis);;
                $this->data['tes_nombre_autor']=$row->autores;
                $this->data['ins_nombre']=$row->nombre_institucion;
                $this->data['est_nombre']=$row->zonas_estudio;
                $this->data['gra_descripcion']=$row->grado_obtenido;
                $this->data['id_tesis']=$tesis;
                //$this->data['total_calificacion']=$row->tes_total_calificaciones;
                //$this->data['cantidad_calificacion']=$row->tes_cantidad_de_calificaciones;
                $this->cant_cal=$row->tes_cantidad_de_calificaciones;
                $this->total_cal=$row->tes_total_calificaciones;
                if($this->cant_cal!=0){
                    $this->data['prom_cal']=$this->total_cal / $this->cant_cal;
                }
                else{
                    
                     $this->data['prom_cal']=0.0;
                }
            }
        }
        $this->data["comentarios"]=$this->cargarComentarios($tesis,false);
        $cantidades=$this->tesis_m->cantidad_material_extra($tesis);
				$this->data['cantidad_imagenes']=$cantidades["cantidad_imagenes"]->row()->num; 
				$this->data['cantidad_documentos']=$cantidades["cantidad_documentos"]->row()->num;
				$this->data['cantidad_videos']=$cantidades["cantidad_videos"]->row()->num;
				$this->data['cantidad_audios']=$cantidades["cantidad_audios"]->row()->num;
        $this->view("visualizar_tesis");           
    }
    public function materialExtraVideos($id_tesis){
        $this->load->model("tesis_m");
        echo $this->sresponse($this->tesis_m->get_videos($id_tesis));
    }
    public function materialExtraImagenes($id_tesis){
        $this->load->model("tesis_m");
        echo $this->sresponse($this->tesis_m->get_imagenes($id_tesis));
    }
    public function materialExtraDocumentos($id_tesis){
        $this->load->model("tesis_m");
        echo $this->sresponse($this->tesis_m->get_documentos($id_tesis));
    }
	public function materialExtraAudios($id_tesis){
        $this->load->model("tesis_m");
        echo $this->sresponse($this->tesis_m->get_audios($id_tesis));
    }
    public function agregarComentario(){
        $this->load->library("form_validation");
        $this->form_validation->set_rules("comentario", "comentario", "required|min_length[3]|max_length[500]");
        $this->form_validation->run();
        $comentario=$_POST["comentario"];
        if($this->form_validation->run()=== FALSE){
            echo $this->fresponse(validation_errors());
        }else{
            $id_tesis=$this->input->post("id_tesis");
            $this->load->model("tesis_m");
            $this->tesis_m->agregarComentario(array("tes_id_tesis"=>$id_tesis,"tes_comentario"=>$comentario,"tes_estado_comentario"=>0));
            $this->session->set_flashdata("msg.ok", "Su comentario será publicado en cuanto sea aprobado por el administrador");
            echo $this->sresponse("Guardado correctamente");
        }
       
    }
    public function cargarComentarios($id,$json=true){
        $this->load->model("tesis_comentarios_m");
        if($json){
            echo $this->sresponse( array("datos" => $this->tesis_comentarios_m->cargarComentarios($id)));
        }else{
            return $this->tesis_comentarios_m->cargarComentarios($id);
        }  
    }
} // END