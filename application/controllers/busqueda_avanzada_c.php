<?php
/**
 * undocumented class
 *
 * @package default
 * @author Sofia
 */
class Busqueda_avanzada_c extends MY_Controller {
	
	/**
	 * Lista las tesis
	 *
	 * @return void
	 * @author  Sofia
	 */
	function index( $page=0 ) {
		
		$this->data['analytics']=TRUE;
		$this->addJs("modulos/tesis.principal.js");
		$this->addJs("modulos/tesis.visualizar.js");
		$this->addJs("modulos/busqueda_avanzada.js");	
        $this->addJs("select2/select2.min.js", "plugs");
        $this->addJs("select2/select2.js", "plugs");
		$this->addCss("select2/select2.css", "plugs");
        
		//Hacemos persistente los input
		$this->load->library("form_validation");
		$this->form_validation->set_rules("tes_id_institucion", "", "");
		$this->form_validation->set_rules("palabra_clave", "", "");
		$this->form_validation->set_rules("tes_id_disciplina_estudio", "", "");
		$this->form_validation->set_rules("tes_id_subdisciplina", "", "");
		$this->form_validation->set_rules("tes_id_grado_obtenido", "", "");
		$this->form_validation->set_rules("tes_id_especie", "", "");
		$this->form_validation->set_rules("order_by", "", "");
		$this->form_validation->set_rules("order_type", "", "");
		$this->form_validation->set_rules("tes_id_tesis_autores", "", "");
		$this->form_validation->set_rules("tes_id_tesis_director", "", "");
		$this->form_validation->set_rules("tes_nombre_director", "", "");
		$this->form_validation->set_rules("tes_nombre_autor", "", "");
		
		$this->form_validation->run();
		
		//Aplicamos los filtros
		$filtros = array(
			"tes_palabra_clave" => $this->input->post("palabra_clave"),
			"tes_id_institucion" => $this->input->post("tes_id_institucion"),
			"tes_id_disciplina_estudio" => $this->input->post("tes_id_disciplina_estudio"),
			"id_subdisciplina" => $this->input->post("tes_id_subdisciplina"),
			"tes_id_grado_obtenido" => $this->input->post("tes_id_grado_obtenido"),
			"id_especie" => $this->input->post("tes_id_especie"), 
			"tes_nombre_autor" => $this->input->post("tes_nombre_autor"),
			"tes_id_tesis_director" => $this->input->post("tes_id_tesis_director"),
			"tes_id_tesis_autores" => $this->input->post("tes_id_tesis_autores"),
			"tes_nombre_director" => $this->input->post("tes_nombre_director")
		);
		
		//Obtenemos las tesis con limite, filtros y orden
		$this->load->model("tesis_m");
		$this->tesis_m->set_filtros( $filtros );
		$this->tesis_m->set_orden( $this->input->post("order_by"), $this->input->post("order_type"));
		$this->db->limit( $this->limit, $page );
		$tesis = $this->tesis_m->get_all();
		$this->data["total_tesis"] = $total_tesis = $this->tesis_m->get_total_rows();
		
		//Creamos el paginador
		$this->data["paginador"] = $this->get_paginador( array(
			"base_url" => site_url("busqueda_avanzada_c/index/"),
			"total_rows" => $total_tesis,
			"uri_segment" => 3			
		) );
		 
		
		
		$this->data["tesis"] = $tesis;
		
		//Formamos los campos necesarios para la vista
		$campos = array(
			"tes_id_institucion" => form_dropdown( "tes_id_institucion", array("" => "Seleccionar") + get_cmb_data("cat_instituciones", "ins_id_institucion", "ins_nombre"), 
											set_value("tes_id_institucion"), "class='input-medium' id='tes_id_institucion'" ),
			"tes_id_disciplina_estudio" => form_dropdown( "tes_id_disciplina_estudio", array("" => "Seleccionar") + get_cmb_data("cat_disciplinas_estudio", "dis_id_disciplina_estudio", "dis_estudio_nombre"), 
											set_value("tes_id_disciplina_estudio"), "class='input-medium' id='tes_id_disciplina_estudio'" ),
			"tes_id_subdisciplina" => form_dropdown( "tes_id_subdisciplina", array("" => "Seleccionar") + get_cmb_data("cat_subdisciplinas", "sub_id_subdisciplina", "sub_nombre"), 
											set_value("tes_id_subdisciplina"), "class='input-medium' id='tes_id_subdisciplina'" ),
			"tes_id_especie" => form_dropdown( "tes_id_especie", array("" => "Seleccionar") + get_cmb_data("cat_especies", "esp_id_especie", "esp_nombre"), 
											set_value("tes_id_especie"), "class='input-medium' id='tes_id_especie'" ),
			"tes_id_tesis_director" => form_dropdown( "tes_id_tesis_director", array("" => "Seleccionar") + get_cmb_data("tesis_directores", "tes_id_tesis_director", "tes_nombre_director"), 
											set_value("tes_id_tesis_director"), "class='input-medium' id='tes_id_tesis_director'" ),
			"tes_id_tesis_autores" => form_dropdown( "tes_id_tesis_autores", array("" => "Seleccionar") + get_cmb_data("tesis_autores", "tes_id_tesis_autores", "tes_nombre_autor"), 
											set_value("tes_id_tesis_autores"), "class='input-medium' id='tes_id_tesis_autores'" ),		
			"palabra_clave" => array(
				"name" => "palabra_clave",
				"value" => set_value("palabra_clave"),
				"id" => "palabra_clave"
			)
		); 
		$this->db->where('gra_clasificacion', 'a');
		$campos["tes_id_grado_obtenido"] = form_dropdown( "tes_id_grado_obtenido", array("" => "Seleccionar") + get_cmb_data("grados_academicos", "gra_id_grado_academico", "gra_descripcion"), 
											set_value("tes_id_grado_obtenido"), "class='input-medium' id='tes_id_grado_obtenido'");
                                            
        
        // El campo de nombre del director
        $this->load->model('directores_grados_m');
        $nombres_directores = $this->directores_grados_m->get_names();
        $data_nombres_directores = array();
        foreach($nombres_directores as $n)
        {
            $data_nombres_directores[ $n['name'] ] = $n['name']; 
        }
        $campos['tes_nombre_director'] = form_dropdown( "tes_nombre_director", array(''=>'Seleccionar') + $data_nombres_directores,
                                        set_value('tes_nombre_director'), "class='input-medium' id='tes_nombre_director'" );


        // El campo de nombre de autor
        $this->load->model('autores_m');
        $nombres_autores = $this->autores_m->get_names();
        $data_nombres_autores = array();
        foreach($nombres_autores as $n)
        {
            $data_nombres_autores[ $n['name'] ] = $n['name']; 
        }
        $campos['tes_nombre_autor'] = form_dropdown( "tes_nombre_autor", array(''=>'Seleccionar') + $data_nombres_autores,
                                        set_value('tes_nombre_autor'), "class='input-medium' id='tes_nombre_autor'" );                                        
        
        

		//Mandmos los campos a la vista
		$this->data["campos"] = $campos;
		
		
		
		$this->view("busqueda_avanzada/principal");
	}
	
	/**
	 * undocumented function
	 *
	 * @return void
	 * @author  
	 */
	function get_paginador( $conf_data ) {
		$base_data = array(
			'base_url'  => "/", // la url a la que apuntará base_url() . "/propiedad/listado",
			//'total_rows' =>  ,
			'per_page' => $this->limit,
			'num_links' => 8,
			"suffix" => "",
			'last_link' => '>>',
			'first_link' => '<<',
			'next_link'      => 'Siguiente &rarr;',
			
            'next_tag_open'  => '<li>',
            'next_tag_close' => '</li>',
            
			'prev_link'      => '&larr; Anterior',
			'prev_tag_open'  => '<li>',
			'prev_tag_close' => '</li>',
			
			'cur_tag_open'   => '<li class="active"><a>',
			'cur_tag_close'  => '</li></a>',
			
			'num_tag_open'   => '<li>',
			'num_tag_close'  => '</li>',
			
			'first_tag_open' => '<li>',
			'first_tag_close'=> '</li>',
			
			'last_tag_open'  => '<li>',
			'last_tag_close' => '</li>',
			
			"full_tag_open" => "<ul>",
			"full_tag_close" => "</ul>"
			
		);

		$data = array_merge($base_data, $conf_data);
		$this->load->library('pagination');
		$this->pagination->initialize($data);
		return $this->pagination->create_links();			
	}
	
} // END