<div id="modal_info" class="modal hide fade">
    <div id="modal_info_header" class="modal-header">
        
    </div>
    <div id="modal_info_body" class="modal-body">
    
    </div>
</div>

<div id="modal_aprobar" class="modal hide fade">
	<form id="formAprobar">
	    <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h3>Aprobar</h3>
	    </div>
	    
	    <div id="modal_aprobar-body" class="modal-body">
	    Desea aprobar el comentario: <b><p id="comentario_para_aprobar"></p></b> 
	    
	    <input type="hidden" id="id_coment_aprobar" name="id_coment_aprobar">
	        
	    </div>
	    <div class="modal-footer">
	        <a id="btn_aprobar" class="btn btn-success">Aceptar</a>
	        <a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</a>
	    </div>
	</form>
</div>