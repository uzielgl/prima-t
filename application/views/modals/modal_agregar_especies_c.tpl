<div id="modal_agregar" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Agregar</h3>
    </div>
    <div class="modal-body">
        <div class="thumbnail row-fluid">
            <div class="row-fluid">
                <div class="span12" style="margin-top: -13px">
                    <span class="label  pull-left">Datos de especie</span>  
                </div>
            </div>
            <div class="row-fluid">
                <div class="span4" style="text-align: right; margin-right: 20px">
                    Nombre especie:
                </div>
                <div class="span6" style="text-align: left; margin-top: -5px">
                    <input id="esp_nombre_agregar" class="input-xlarge" type="text" placeholder="Nombre científico">
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a id="btn_agregar"  class="btn btn-success">Agregar</a>
        <a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</a>
    </div>
</div>