<div id="modal_comentar_sitio_c" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Agrega tu comentario del sitio</h3>
    </div>
    <div class="modal-body">
    	<form id="formComentarioSitio">
    
	        <div class="thumbnail row-fluid">
	            <div class="row-fluid">
	                <div class="span12" style="margin-top: -13px">
	                    <span class="label  pull-left">Comentario</span>  
	                </div>
	            </div>
	            <div class="row-fluid">
	                <div class="span10" style="text-align: right; margin-right: 20px">
	                    <textarea style="width:100%" name="taComentarSitio" id="taComentarSitio"></textarea> 
	                </div>
	        </div>
	        
		</form>
            
        </div>
    </div>
    <div class="modal-footer">
    	<!-- 
        <a class="btn btn-success" href="{site_url("auth/create_user")}" >Enviar comentario</a>
        -->
        <a id="bComentarSitio" url="{site_url()}/comentarios_sitio_c/agregar" class="btn btn-success" href="#" >Enviar comentario</a>
        <a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</a>
    </div>
</div>