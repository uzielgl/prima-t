<div id="modal_eliminar_subespecies" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Eliminar</h3>
    </div>
    <div id="modal_eliminar_subespecies-body" class="modal-body">
        
    </div>
    <div class="modal-footer">
        <a id="btn_eliminar_subespecies" eliminar-id="0" class="btn btn-danger">Eliminar</a>
        <a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</a>
    </div>
</div>