<div id="modal_info" class="modal hide fade">
    <div id="modal_info_header" class="modal-header">
        
    </div>
    <div id="modal_info_body" class="modal-body">
    
    </div>
</div>

<div id="modal_eliminar_sitio" class="modal hide fade">
	<form id="formEliminar_sitio">
	    <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h3>Eliminar</h3>
	    </div>
	    
	    <div id="modal_eliminar-body" class="modal-body">
	    ¿Desea eliminar el comentario?: </br> <b><p align="justify" id="comentario_para_eliminar_sitio"></p></b> 
	    
	    
	    <input type="hidden" id="id_coment_eliminar_sitio" name="id_coment_eliminar_sitio">
	        
	    </div>
	    <div class="modal-footer">
	        <a id="btn_eliminar_sitio" eliminar-id="0" class="btn btn-danger">Eliminar</a>
	        <a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</a>
	    </div>
	</form>
</div>

