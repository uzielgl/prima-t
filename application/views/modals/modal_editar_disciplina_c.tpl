<div id="modal_editar" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Editar</h3>
    </div>
    <div class="modal-body">
        <div class="thumbnail row-fluid">
            <div class="row-fluid">
                <div class="span12" style="margin-top: -13px">
                    <span class="label  pull-left">Datos de disciplina</span>  
                </div>
            </div>
            <div class="row-fluid">
                <div class="span4" style="text-align: right; margin-right: 20px">
                    Nombre disciplina:
                </div>
                <div class="span6" style="text-align: left; margin-top: -5px">
                    <input id="dis_estudio_nombre_editar" class="input-xlarge" type="text" placeholder="Nombre disciplina">
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a id="btn_editar"  editar-id="0" class="btn btn-success">Editar</a>
        <a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</a>
    </div>
</div>