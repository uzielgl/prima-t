<div id="modal_info" class="modal hide fade">
    <div id="modal_info_header" class="modal-header">
        
    </div>
    <div id="modal_info_body" class="modal-body">
    
    </div>
</div>

<div id="modal_eliminar" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Eliminar</h3>
    </div>
    <div id="modal_eliminar-body" class="modal-body">
        
    </div>
    <div class="modal-footer">
        <a id="btn_eliminar" eliminar-id="0" class="btn btn-danger">Eliminar</a>
        <a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</a>
    </div>
</div>