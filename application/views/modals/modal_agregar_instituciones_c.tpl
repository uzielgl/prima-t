<div id="modal_agregar" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Agregar</h3>
    </div>
    <div class="modal-body">
    <form class="form-horizontal">
<fieldset>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="ins_nombre">Nombre:</label>
  <div class="controls">
    <input id="ins_nombre_agregar" name="ins_nombre" placeholder="Nombre de la institución" class="input-xlarge" type="text">
  </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="ins_siglas">Siglas</label>
  <div class="controls">
    <input id="ins_siglas_agregar" name="ins_siglas" placeholder="Siglas de la institución" class="input-xlarge" type="text">
    
  </div>
</div>
<!-- Dependencia-->
<div class="control-group">
  <label class="control-label" for="ins_nombre">Dependencia:</label>
  <div class="controls">
    <input id="ins_dependencia_agregar" name="ins_dependencia" placeholder="Nombre de la dependencia" class="input-xlarge" type="text">
  </div>
</div>

<!-- Sede-->
<div class="control-group">
  <label class="control-label" for="ins_nombre">Sede:</label>
  <div class="controls">
    <input id="ins_sede_agregar" name="ins_sede" placeholder="Nombre de la sede" class="input-xlarge" type="text">
  </div>
</div>

</form>
    </div>
    <div class="modal-footer">
        <a id="btn_agregar"  class="btn btn-success">Agregar</a>
        <a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</a>
    </div>
</div>