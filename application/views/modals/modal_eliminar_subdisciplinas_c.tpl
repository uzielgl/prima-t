<div id="modal_eliminar_subdisciplinas" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Eliminar</h3>
    </div>
    <div id="modal_eliminar_subdisciplinas-body" class="modal-body">
        
    </div>
    <div class="modal-footer">
        <a id="btn_eliminar_subdisciplinas" eliminar-id="0" class="btn btn-danger">Eliminar</a>
        <a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</a>
    </div>
</div>