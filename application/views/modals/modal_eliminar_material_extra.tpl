<div id="modal_eliminar_material_extra" class="modal hide fade">
	<form id="formEliminar">
	    <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h3>Eliminar</h3>
	    </div>
	    
	    <div id="modal_eliminar-body" class="modal-body">
	    ¿Desea eliminar el material extra?
	    </div>
	    <div class="modal-footer">
	        <a id="btn_eliminar" eliminar-id="0" class="btn btn-danger">Eliminar</a>
	        <a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</a>
	    </div>
	</form>
</div>

