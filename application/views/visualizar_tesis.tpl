<html lang='es'>
  <head>  
    
  </head>
<body>
    <style type="text/css">
      #videoDiv { 
        margin-right: 3px;
      }
      #videoInfo {
        margin-left: 3px;
      }
    </style>
    <script src="//www.google.com/jsapi" type="text/javascript"></script>
    <script type="text/javascript">
      google.load("swfobject", "2.1");
    </script>
    
    
    
 <div class="container-fluid" >
     
     
     
     {include file="modals/modal_tesis_videos.tpl"}
     {include file="modals/modal_tesis_documentos.tpl"}
     {include file="modals/modal_tesis_imagenes.tpl"} 
     {include file="modals/modal_tesis_audios.tpl"} 
     <div class="row-fluid">
       <h3 class="text-success" style="margin-bottom: 30px">Visualizar Tesis</h3>
           <div class="row-fluid">
                <div class="span12">
                     <div class="span9">
                          <div class="row-fluid" style="margin-bottom: 20px">
                               <div class="span12" >
                                 <iframe style="width:98%; height:650px" src={base_url()}{$tes_ruta_tesis|string_format:"themes/default/pdf/%s"} ></iframe>
                             
                       <!--      <iframe style="width:98%; height:650px" src={site_url()}{"themes/default/pdf/%s"} ></iframe> -->
                                </div>
                          </div>
                           <div class="row-fluid" >
                                <ul class="thumbnails" >
                                    <li  class="span12" style="margin-bottom: 0px" >
                                        <div class="thumbnail">
                                            <div class="row-fluid" >
                                                <div class="span12" style="margin-top: -13px">
                                                  <span class="label  pull-left">{if $comentarios|@count > 0}{$comentarios|@count}{/if}  Comentarios </span>  
                                                </div>
                                            </div>
                                        <div style="overflow: scroll; max-height: 180px">
                                        		{if $comentarios|@count > 0} 
                                        			{foreach  from=$comentarios item=comentario name=foo}
																<div class="thumbnail" style="margin-bottom: 5px" >
                                                    			<div class="span12" style="margin-top: -6px">
                                                        			<label class="label label-success" >{$comentario.username}</label>
                                                    			</div>
                                                    			<p align="justify">
                                                        			{$comentario.tes_comentario}
                                                    			</p>                                        
                                                			</div>
                                        			{/foreach}
                                        			{else}
                                        			<div class="thumbnail" style="margin-bottom: 5px" >
                                                    	<div class="span12" style="margin-top: -6px">
                                                        		<!--label class="label label-success" >{$comentario.username}</label-->
                                                    		</div>
                                                    			<p>
																	<center><h6>No se ha comentado ésta tesis.</h6></center>
                                                    			</p>                                        
                                                		</div>                          
                                        			{/if}
                                            </div>
                                        </div>
										<form id="formComentario" >
											<input type="hidden" name="id_tesis" value="{$id_tesis}" />
											<div class="row-fluid">
													<div style="overflow: scroll: auto; max-height: 180px"> 
													<label class="label label-success" for="tes_comentario">Escriba su comentario:</label>
													<textarea placeholder="Escriba su comentario aquí" id="txtacomentario"  name="comentario" style="width:100%" maxlength="500"></textarea>
												</div>												
											</div>

										</form>
										<button id="agregarComentario" OURL="{site_url()}/test/ver_tesis/{$id_tesis}" URL="{site_url()}/test/agregarComentario" type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i> Comentario</button>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="span3" >
                            <ul class="thumbnails">
                                <li  class="span12" >
                                    <div class="thumbnail" style="margin-bottom: 20px">
                                        <div class="row-fluid" >
                                            <div class="span12" style="margin-top: -15px">
                                                <span class="label  pull-left">Ficha tecnica</span>  
                                            </div>
                                            <div class="span12" >
                                            	<p> {$tes_nombre|string_format:"<h5>Titulo:</h5> %s"}</p>
                                                <p> {$tes_nombre_autor|string_format:"<h5>Autor:</h5> <h7 >%s</h7 >"}</p>
                                                <p> {$ins_nombre|string_format:"<h5>Institución:</h5> <h7>%s</h7>"} </p>
                                                <p> {$tes_anio_titulacion|string_format:"<h5>Año:</h5><h7>%s</h7>"} </p>
                                                <p> {$gra_descripcion|string_format:"<h5>Grado:</h5> <h7>%s</h7>"} </p>
                                                <p> {$est_nombre|string_format:"<h5>Lugar:</h5> <h7>%s</h7>"} </p>
                           
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="thumbnail" style="margin-bottom: 20px">
                                        <div class="row-fluid" >
                                            <div class="span12" style="margin-top: -15px">
                                                <span class="label  pull-left">Estadísticas</span>  
                                            </div>
                                            <div class="span10 offset1">
                                                <div class="label label-success">
                                                	Descargas: <span class="descargas-totales">{$tes_num_descargas}</span>
                                                </div>
                                                <div class="label label-success">
                                                    {$tes_num_visitas|string_format:"Visitas: %d"}
                                                </div>
                                                                                      
                                            </div>
                                          </div>
                                                                                
                                    </div>
                                    
                                    <div class="thumbnail" style="margin-bottom: 20px">
                                        <div class="row-fluid" >
                                            <div class="span12" style="margin-top: -15px">
                                                <span class="label  pull-left">Material Extra</span>  
                                            </div>
                                            <div class="span12 text-left">
                                                <a id_tesis="{$id_tesis}" class="btn btn-info" onclick="modalMostrarImagenes(this);"> Imágenes ({$cantidad_imagenes})</a></br>
                                                </br>
                                                <a id_tesis="{$id_tesis}" class="btn btn-info" onclick="modalMostrarVideos(this);"> Videos ({$cantidad_videos})</a></br>
                                                </br>
                                                <a id_tesis="{$id_tesis}" class="btn btn-info" onclick="modalMostrarDocumentos(this);"> Documentos ({$cantidad_documentos})</a></br>
                                                </br>
                                                <a id_tesis="{$id_tesis}" class="btn btn-info" onclick="modalMostrarAudios(this);"> Audios ({$cantidad_audios})</a></br>
                                            </div>
                                          </div>
                                                                                
                                    </div>
                                    <div class="thumbnail" style="margin-bottom: 20px">
                                        <div class="row-fluid"  >
                                            <div class="span12" style="margin-top: -15px">
                                                <span class="label  pull-left">Descargar</span>  
                                            </div>
                                            <div class="span10 offset1">
                                            	<center><a class="btn-descargar" href="{site_url("tesis/descargar/$id_tesis.pdf")}">{image('download.png')}</a></center>
                                            	<center><a class="btn-descargar" href="{site_url("tesis/descargar/$id_tesis.pdf")}">Descargar tesis</a></center>
                                            </div>
                                          </div>
                                                                                
                                    </div>
                    					
                    					
                    					<div class="thumbnail" style="margin-bottom: 20px">
										<div class="row-fluid">
										<div class="span12" style="margin-top: -15px">
										
										</div>
										<div class="span10 offset1">
										<div class="basic" id="cal" data-average="{$prom_cal}" url="{base_url()}" id="cal" data-id="{$id_tesis}"></div>
										<p id="mensaje" class="text-success"></p>	
										<p id="mensajeError" class="text-error"></p>
											</div>
											</div>
										</div>	
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
      </div>
             
  </body>
</html>


