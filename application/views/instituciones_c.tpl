 	
<div class="tab-pane active" id="instituciones">
    <ul class="thumbnails">                    
        <li class="span12">
            <div class="thumbnail">
                <div class="row-fluid">
                    <div class="span12" style="overflow-y: auto;">
                        <a id="nueva_institucion" style="margin:5px 10px 10px 0px" href="#modal_agregar" data-toggle="modal" role="button" class="btn btn-success" > <i class="icon-plus icon-white"></i> Institución</a>
                        <table id="tabla_catalogos" class="table table-hover table-striped" style="position: static">
                            <thead>
                              <tr>
                                <th data-field="ins_nombre" data-type>
                                    <div class="filter">
                                        <span>Nombre Institución</span>
                                    </div>
                                </th>
                                <th data-field="ins_siglas" data-type>
                                    <div class="filter">
                                        <span>Siglas</span>
                                    </div>
                                </th>
                              <th data-field="ins_dependencia" data-type>
                                    <div class="filter">
                                        <span>Dependencia</span>
                                    </div>
                                </th>
								 <th data-field="ins_sede" data-type>
                                    <div class="filter">
                                        <span>Sede</span>
                                    </div>
                                </th>
                                <th style="width: 59px" > </th> <!-- Editar -->
                                <th style="width: 67px" > </th><!-- Eliminar -->
                              </tr>
                            </thead>
                            <tbody>
                            {foreach from=$instituciones item=inst name=foo2}
                                {if $smarty.foreach.foo2.index %2 == 0}
                                    <tr id={$inst.ins_id_institucion|string_format:"tr_%d"} class="success">
                                {else}
                                    <tr id={$inst.ins_id_institucion|string_format:"tr_%d"} >
                                {/if}
                                    <td id={$inst.ins_id_institucion|string_format:"ins_nombre_%d"} >
                                        {$inst.ins_nombre}
                                    </td>
                                    <td id={$inst.ins_id_institucion|string_format:"ins_siglas_%d"} >
                                        {$inst.ins_siglas}
                                    </td>
									 <td id={$inst.ins_id_institucion|string_format:"ins_dependencia_%d"} >
                                        {$inst.ins_dependencia}
                                    </td>
									 <td id={$inst.ins_id_institucion|string_format:"ins_sede_%d"} >
                                        {$inst.ins_sede}
                                    </td>
                                    <td id={$inst.ins_id_institucion|string_format:"td_editar_%d"} style="text-align: center;" >
                                        <a id={$inst.ins_id_institucion|string_format:"a_editar_%d"} class="btn btn-warning btn-mini" href="#modal_editar" ><i class="icon-pencil icon-white"></i> Editar</a>
                                    </td>
                                    <td id={$inst.ins_id_institucion|string_format:"td_eliminar_%d"} style="text-align: center;" >
                                        <a id={$inst.ins_id_institucion|string_format:"a_eliminar_%d"} class="btn btn-danger btn-mini" href="#modal_eliminar" ><i class="icon-remove icon-white"></i> Eliminar</a>
                                    </td>
                                </tr>
                            {/foreach}
                            </tbody>
                        </table>
                    </div>
                    {$paginacion}
                </div>
            </div>
        </li>
    </ul>
</div>