<div class="tab-pane active" id="subdisciplinas">
    <ul class="thumbnails">                    
        <li class="span12">
            <div class="thumbnail">
                <div class="row-fluid">
                    <div class="row-fluid">
                        <a id="nueva_subdisciplina" style="margin:5px 10px 10px 0px" href="#modal_agregar" data-toggle="modal" role="button" class="btn btn-success pull-right" > <i class="icon-plus icon-white"></i> Nueva Subdisciplina</a>
                    </div>
                    <div class="row-fluid">
                        <div class="span12" style="overflow-y: auto; max-height: 260px">
                            <table id="tabla_catalogos" class="table table-hover table-bordered" style="position: static">
                                <thead>
                                  <tr>
                                    <th> Nombre Subdisciplina <a id="ordernar_tabla" class="pull-right" href="#"><i class="icon-tasks"></i> </a></th>
                                    <th style="width:70px"> Editar</th>
                                    <th style="width:70px"> Eliminar</th>
                                  </tr>
                                </thead>
                                <tbody>
                                {foreach from=$subdisciplinas item=subdisciplina name=foo}
                                    {if $smarty.foreach.foo.index % 2 == 0}
                                        <tr id={$subdisciplina.sub_id_subdisciplina|string_format:"tr_%d"} class="success" >
                                    {else}
                                        <tr id={$subdisciplina.sub_id_subdisciplina|string_format:"tr_%d"}>
                                    {/if}
                                        <td id={$subdisciplina.sub_id_subdisciplina|string_format:"sub_nombre_%d"} >
                                            {$subdisciplina.sub_nombre}
                                        </td>
                                        <td id={$subdisciplina.sub_id_subdisciplina|string_format:"td_editar_%d"} style="text-align: center;" >
                                            <a id={$subdisciplina.sub_id_subdisciplina|string_format:"a_editar_%d"} class="btn btn-info btn-mini" href="#modal_editar" ><i class="icon-edit icon-white"></i> Editar</a>
                                        </td>
                                        <td id={$subdisciplina.sub_id_subdisciplina|string_format:"td_eliminar_%d"} style="text-align: center;" >
                                            <a id={$subdisciplina.sub_id_subdisciplina|string_format:"a_eliminar_%d"} class="btn btn-danger btn-mini" href="#modal_eliminar" ><i class="icon-remove icon-white"></i> Eliminar</a>
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>
                            </table>
                        </div>
                        {$paginacion}
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>