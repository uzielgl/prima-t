<div class="tab-pane active" id="condiciones">
    <ul class="thumbnails">                    
        <li class="span12">
            <div class="thumbnail">
                <div class="row-fluid">
                    <div class="span12" style="overflow-y: auto; ">
                        <a id="nueva_condicion" style="margin:5px 10px 10px 0px" href="#modal_agregar" data-toggle="modal" role="button" class="btn btn-success" > <i class="icon-plus icon-white"></i> Condición de sitio</a>
                        <table id="tabla_catalogos" class="table table-hover table-striped" style="position: static">
                            <thead>
                              <tr>
                                <th data-field="con_nombre" data-type>
                                    <div class="filter">
                                        <span>Nombre Condición de Sitio</span>
                                    </div>
                                </th>
                                <th style="width: 59px" > </th> <!-- Editar -->
                                <th style="width: 67px" > </th><!-- Eliminar -->
                              </tr>
                            </thead>
                            {foreach from=$condiciones item=condicion name=foo}
                                {if $smarty.foreach.foo.index % 2 == 0}
                                    <tr id={$condicion.con_id_condicion_sitio|string_format:"tr_%d"} class="success" >
                                {else}
                                    <tr id={$condicion.con_id_condicion_sitio|string_format:"tr_%d"}>
                                {/if}
                                    <td id={$condicion.con_id_condicion_sitio|string_format:"con_nombre_%d"} >
                                        {$condicion.con_nombre}
                                    </td>
                                    
                                    <td id={$condicion.con_id_condicion_sitio|string_format:"td_editar_%d"} style="text-align: center;" >
                                      <a id={$condicion.con_id_condicion_sitio|string_format:"a_editar_%d"} class="btn btn-warning btn-mini" href="#modal_editar" ><i class="icon-pencil icon-white"></i> Editar</a> 
                                    </td>
                                    <td id={$condicion.con_id_condicion_sitio|string_format:"td_eliminar_%d"} style="text-align: center;" >
                                        <a id={$condicion.con_id_condicion_sitio|string_format:"a_eliminar_%d"} class="btn btn-danger btn-mini" href="#modal_eliminar" ><i class="icon-remove icon-white"></i> Eliminar</a> 
                                    </td>
                                </tr>
                            {/foreach}
                        </table>
                    </div>
                    {$paginacion}
                </div>
            </div>
        </li>
    </ul>
</div>