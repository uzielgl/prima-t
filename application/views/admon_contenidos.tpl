<div class="container-fluid hero-unit" >
          <div class="row-fluid" >
            <h3 class="text-success" style="margin-bottom: 30px">Administración de Contenidos</h3>
            <div class="span10 offset1 row-fluid">
                <form class="span7 offset2 form-search" style="margin-left: 0px; margin-right: 0px">
                    <div class="input-append">
                      <input type="text" name='buscar' class="input-xxlarge search-query" placeholder="Buscar Tesis">
                      <button type="submit" style="height: 30px;" class="btn btn-success">Buscar</button>
                    </div>
                </form>
                <div class="span2 offset1 form-horizontal">
                    <a href="RegistroTesis.php" class="btn btn-success">
                        <i class='icon-plus icon-white'></i> Registrar Tesis
                    </a>
                </div>
          </div>
         </div>
            <!-- Resultado de la busqueda-->
         <div class="row-fluid" >
                <div class="span10 offset1">
                    <div class="row-fluid" style="margin-bottom: 20px">
                        <div class="span12">
                            <h3>
                                <a href="ModificarTesis.php">ESTUDIO DE LA VARIABILIDAD GENÉTICA EN POBLACIONES DE...</a>
                            </h3>
                            <p>
                                Autor: Juan Pérez García, Institución: Instituto de Ecología, A.C,
                                Año: 2009, Grado de Tesis: Maestría, Lugar: Xalapa, Veracruz, México
                            </p>
                            <div class="row-fluid">
                                <div class="span5">
                                    <span class="label label-success">Descargas: 1086</span>
                                    <span class="label">Visitas: 12983</span>
                                </div>
                                <div  class="btn-group pull-right" >
                                    <button class="btn">Visualizar Resumen</button>
                                    <a href="VisualizarTesis.php" class="btn">Visualizar Tesis</a>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                    <div class="row-fluid" style="margin-bottom: 20px">
                        <div class="span12">
                            <h3>
                                <a href="ModificarTesis.php">ESTUDIO DE LA VARIABILIDAD GENÉTICA EN POBLACIONES DE...</a>
                            </h3>
                            <p>
                                Autor: Juan Pérez García, Institución: Instituto de Ecología, A.C,
                                Año: 2009, Grado de Tesis: Maestría, Lugar: Xalapa, Veracruz, México
                            </p>
                            <div class="row-fluid">
                                <div class="span5">
                                    <span class="label label-success">Descargas: 1086</span>
                                    <span class="label">Visitas: 12983</span>
                                </div>
                                <div  class="btn-group pull-right" >
                                    <button class="btn">Visualizar Resumen</button>
                                    <a href="VisualizarTesis.php" class="btn">Visualizar Tesis</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid" style="margin-bottom: 20px">
                        <div class="span12">
                            <h3>
                            <a href="ModificarTesis.php">ESTUDIO DE LA VARIABILIDAD GENÉTICA EN POBLACIONES DE...</a>
                            </h3>
                            <p>
                                Autor: Juan Pérez García, Institución: Instituto de Ecología, A.C,
                                Año: 2009, Grado de Tesis: Maestría, Lugar: Xalapa, Veracruz, México
                            </p>
                            <div class="row-fluid">
                                <div class="span5">
                                    <span class="label label-success">Descargas: 1086</span>
                                    <span class="label">Visitas: 12983</span>
                                </div>
                                <div  class="btn-group pull-right" >
                                    <button class="btn">Visualizar Resumen</button>
                                    <a href="VisualizarTesis.php" class="btn">Visualizar Tesis</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid" style="margin-bottom: 20px">
                        <div class="span12">
                            <h3>
                            <a href="#">ESTUDIO DE LA VARIABILIDAD GENÉTICA EN POBLACIONES DE...</a>
                            </h3>
                            <p>
                                Autor: Juan Pérez García, Institución: Instituto de Ecología, A.C,
                                Año: 2009, Grado de Tesis: Maestría, Lugar: Xalapa, Veracruz, México
                            </p>
                            <div class="row-fluid">
                                <div class="span5">
                                    <span class="label label-success">Descargas: 1086</span>
                                    <span class="label">Visitas: 12983</span>
                                </div>
                                <div  class="btn-group pull-right" >
                                    <button class="btn">Visualizar Resumen</button>
                                    <button class="btn">Visualizar Tesis</button>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
         </div>
 </div>     