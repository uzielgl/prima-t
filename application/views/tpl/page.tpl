<!DOCTYPE html>
<html lang="es">
	<head>
	    <meta charset='utf-8'>
        <title>{$tag_title}</title>
        
		<meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<script type="text/javascript">
			//Global variables
			var b_url = "{base_url()}";
			var s_url = "{site_url()}";
		</script>
		
		{foreach $css as $i}
		{css( $i )}
		{/foreach}

		
		<!--[if lt IE 9]>{css( "ielt9.css")}<![endif]-->
		
		<link rel="icon" href="{base_url()}themes/default/img/favicon.png" type="image/gif">
	</head>
	<body >
	    <div class="container-fluid"> 
		    {include file="tpl/top.tpl"}
		    <div id="body" >
			{if isset($subview) }
			    {include file="$subview.tpl"}
			{/if}
			</div>
    		{include file="tpl/footer.tpl"}
    		
    		{foreach $js as $i}
            {js( $i )}
            {/foreach}
        
        	
    		<script type="text/javascript">
    		    $(function(){
    		       {if $this->session->flashdata("msg.ok")}
    		          msg.ok("{$this->session->flashdata("msg.ok")}");
    		       {/if}
    		       {if $this->session->flashdata("msg.error")}
                      msg.error("{$this->session->flashdata("msg.error")}");
                   {/if}
                   
                   {*Para sólo la variable msg_error o msg_ok*}
                   {if isset($msg_ok)}
                      msg.ok("{$msg_ok}");
                   {/if}
                   {if isset($msg_error)}
                      msg.error("{$msg_error}");
                   {/if}
                   
    		    });
    		</script>
		</div>
		
		{literal}
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-47598718-1', 'inecol.edu.mx');
		  ga('send', 'pageview');
		
		</script>
		{/literal}
		
	</body>
</html>