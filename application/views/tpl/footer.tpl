<footer class='row-fluid'>
    <div class='navbar-inner'>
        <p class='navbar-text text-right'>
                <a href='{base_url()}index.php/about_c/agradecimientos'> Agradecimiento a Colaboradores </a>   
        |
                <a href='{base_url()}index.php/about_c/somos'> ¿Quiénes somos? </a>   
        | 
                <a href='{base_url()}index.php/about_c/equipo_desarrollo'> Equipo de desarrollo </a>
        |
            <label class='label label-success'>
            	© Copyright Laboratorio Nacional de Informática Avanzada
            </label>
        </p>
    </div>
</footer>