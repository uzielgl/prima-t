<div class='navbar navbar-inverse navbar-fixed-top'>
    <div class='navbar-inner'>
        <div class="container-fluid">
            <a class="brand" href='{site_url('home')}' style="padding: 0; float:left">
                <img src='{base_url()}themes/default/img/HeaderPrima.png'>
            </a>
            <button id="b" type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <i class='icon-list icon-white'> </i>  
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            
            <div class="nav-collapse collapse navbar-responsive-collapse">
                <ul class="nav">
                    <li>
                        <a href='{base_url()}index.php/busqueda_avanzada_c'>
                            <i class='icon-search icon-white'> </i> Tesis
                        </a>
                    </li>
                    {if ($this->ion_auth->logged_in()) }
                    <li>
                        <a id='Comentario' href='#'>
                            <i class='icon-comment icon-white'> </i> Comentario
                        </a>
                    </li>
                    {/if}
                    <li>
                        <a href='{site_url("contacto_c")}'>
                            <i class='icon-envelope icon-white'> </i> Contacto
                        </a>
                    </li>
                    <li>
                        <form class='navbar-search' action='{base_url()}index.php/busqueda_avanzada_c' method='post'>
                            <input id='buscar' name='palabra_clave' type='search' class='search-query' placeholder='Buscar'>
                        </form>
                    </li>
                   
                </ul>
            </div>
            
            {if $this->ion_auth->is_admin()}
                <div class="nav-collapse collapse navbar-responsive-collapse">
                    <ul class="nav">
                        <li class='dropdown'>
                            <a class='dropdown-toggle' href="#" data-toggle='dropdown'>
                                <i class='icon-wrench icon-white'> </i> Administración 
                                <span class='caret' > </span>
                            </a>
                            
                            <ul class="dropdown-menu">
                                <li>
                                    <a href='{site_url("catalogos/tesis")}'><i class='icon-lock'></i> Tesis</a>
                                </li>
                                <li>
                                    <a href='{site_url('admon_comentarios_c/comen_sitio')}'><i class='icon-inbox'></i> Comentarios</a>
                                </li>
                                <li>
                                    <a href='{site_url('catalogos/especies_c')}'><i class='icon-list-alt'></i> Especies</a>
                                </li>
                                <li>
                                
                                    <a href='{site_url('catalogos/disciplina_estudio_c')}'><i class='icon-list-alt'></i> Disciplinas de estudio</a>
                                </li>
                                <li>
                                    <a href='{site_url('catalogos/instituciones_c')}'><i class='icon-list-alt'></i> Instituciones</a>
                                </li>
                                <li>
                                    <a href='{base_url()}index.php/catalogos/condiciones_sitio_c'><i class='icon-list-alt'></i> Condiciones de sitio</a>
                                </li>
								<li class="dropdown-submenu">
									
									<a href='{site_url('reportes')}'></i> Reportes</a>
									<ul class="dropdown-menu">
										<li>
											<a href='{site_url('reportes/disciplinas_estudio')}'><i class='icon-list-alt'></i> Disciplinas de estudio</a>
										</li>
										<li>
											<a href='{site_url('reportes/tesis_c/especiesSubespecies_por_tesis/1')}'><i class='icon-list-alt'></i> Especies y subespecies </a>
										</li>
										<li>
											<a href='{site_url('reportes/tesis_c/visitas_por_tesis/1')}'><i class='icon-list-alt'></i> Visitas por tesis</a>
										</li>
										<li>
											<a href='{site_url('reportes/tesis_c/descargas_por_tesis/1')}'><i class='icon-list-alt'></i> Descargas por tesis</a>
										</li>

										<li>
											<a href='{site_url('reportes/usuarios_c/')}'><i class='icon-list-alt'></i> Grado académico y género de usuarios </a>
										</li>
									</ul>
								</li>                                
                                <li>
                                    <a href='{site_url('auth/index')}'><i class='icon-user'></i> Usuarios</a>
                                </li>                                                         
                         
                            
                            </ul>
                        </li>
                    </ul>
                </div>
            {/if}
                
            <div class="nav-collapse collapse navbar-responsive-collapse">
                <ul class="nav pull-right" style="margin-right: 10px">
                    <li class='dropdown'>
                        <a class='dropdown-toggle' href="#" data-toggle='dropdown'>
                            <i class='icon-user icon-white'> </i>
                            {if $this->ion_auth->logged_in()}
                                {$this->ion_auth->user()->row()->first_name} {$this->ion_auth->user()->row()->last_name}
                            {else}
                                Iniciar sesión
                            {/if}
                             <b class="caret"></b>
                        </a>
                        {if !$this->ion_auth->logged_in()}
                        <ul class="dropdown-menu">
                            <form style="margin: 15px;" method="post" class="frm-login" action="{site_url("auth/login")}">
                                <div class="control-group">
                                  <div class="controls">
                                        <div class="input-prepend">
                                            <span class="add-on"><i class="icon icon-user"></i></span>
                                            <input class="input-medium" id="prependedInput" name="identity" type="text" placeholder="Usuario@dominio">
                                        </div>
                                  </div>
                                </div>
                                <div class="control-group">
                                  <div class="controls">
                                        <div class="input-prepend">
                                            <span class="add-on"><i class="icon icon-key"></i></span>
                                            <input class="input-medium" id="prependedInput" name="password" type="password" placeholder="Contraseña">
                                        </div>
                                        <label class="checkbox">
                                            <input type="checkbox" name="remember"> Recordar
                                        </label>
                                  </div>
                                </div>
                                <div class="control-group">
                                    <div class="controls">
                                        
                                        <button class="btn btn-block" type="submit"><i class="icon-play icon"></i> Iniciar sesión</button>
                                    </div>
                                </div>
                            </form>
                            <li><a href="{site_url("auth/forgot_password")}"><i class="icon-refresh"></i> Recuperar contraseña</a></li>
                            <li><a href="{site_url("auth/create_user")}"><i class="icon-plus"></i> Registrarse</a></li>                                                         
                        </ul>
                        {else}
                        <ul class='dropdown-menu'>
                            <li>
                                <a href="{site_url("auth/change_password")}">
                                    <i class="icon-pencil"> </i> Cambiar contraseña
                                </a>
                            </li>
                            <li class="divider"> </li>
                            <li>
                                <a href="{site_url("auth/logout")}">
                                    <i class="icon-off"> </i> Cerrar sesión
                                </a>
                            </li>
                            
                            
                        </ul>
                        {/if}
                    </li>
                     <li>
                      	  <a href='http://www.inecol.edu.mx/' style="padding: 0"> <img style='max-height: 40px' src='{base_url()}themes/default/img/logo_inecol.png'></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>    
</div>
{include file="modals/modal_comentar_sitio_c.tpl"}