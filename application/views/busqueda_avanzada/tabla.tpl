<table class="table table-striped table-bordered">
	<thead>
		<tr class="order">
			<th data-field="tes_nombre" data-type="{ter(set_value("order_by")=="tes_nombre", set_value("order_type"))}" >
				<div class="filter {set_cls_order("tes_nombre")}">
					<span>Resultados de búsqueda</span> 
				</div>
			</th>
		</tr>
	</thead>
	<tbody>
	    {foreach $tesis as $i}
    		<tr>
    			<td >
    				<strong>{highlight(set_value('palabra_clave'), $i.tes_nombre)}</strong> <br/>
    				Autores:{highlight(set_value('palabra_clave'), substr($i.autores,0,strlen($i.autores)-2))} <br/>
    				Directores:{highlight(set_value('palabra_clave'), $i.directores)} <br/>
    				Grado académico: {highlight(set_value('palabra_clave'), $i.gra_descripcion)} <br/>
    				<a href="#btn_abstract" id_tesis="{$i.tes_id_tesis}" class="btn btn-mini btn-action"><i class="icon-black icon-eye-open"></i> Resumen</a> 
    				{if ($this->ion_auth->logged_in()) }
    				<a class="btn btn-mini btn-action" href= "{site_url()}/test/ver_tesis/{$i.tes_id_tesis}" ><i class="icon-black icon-eye-open"></i> Tesis completa</a>
 					{else}
 					<a class="btn btn-mini btn-action" href="#modal_editar"  ><i class="icon-black icon-eye-open"></i> Tesis completa</a>
 					{/if}
 					
 					
 					<p style="display:none" id="abstract_{$i.tes_id_tesis}">{highlight(set_value('palabra_clave'), $i.tes_resumen|string_format:"Resumen: %s")}</p>
 					 			
    			</td>
    		</tr>
		{/foreach}
	</tbody>
</table>