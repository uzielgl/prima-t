<!-- 
    Seccion de busqueda de tesis para el usuario
-->

<div class="filtros">
    
<form class="form-horizontal" method="post" action="{site_url("busqueda_avanzada_c/index")}">
	<input type="hidden" value="{set_value("order_by")}" name="order_by" id="order_by">
	<input type="hidden" value="{set_value("order_type")}" name="order_type" id="order_type">
	
    
    <!-- Text input-->
        <div class="control-group">
          <label class="control-label" for="palabra_clave">Palabra clave</label>
          <div class="controls">
            <div class="input-append">
                {form_input($campos.palabra_clave)}
                
                <button class="btn btn-primary" type="submit"><i class="icon-white icon-search"> </i> Buscar</button>
                <button  class="btn" id="mostrar-filtros" type="button"><i class="icon-cog"> </i> Opciones avanzadas</button>
            </div>
            <a href="{site_url("busqueda_avanzada_c")}" class="btn"><i class="icon-eye-open"> </i> Mostrar todo</a>
          </div>
        </div>
    
    
<fieldset >

<!-- Form Name -->

<div class="row-fluid {ter($this->input->post("mostrar"), "", "hide")}" id="fls-filtros" >
    
    <div class="span3"> <!-- Columna 1 -->
        <input id="mostrar" name="mostrar" value="{ter($this->input->post("mostrar"), "1", "0")}" type="hidden" />

        <!-- Select Basic -->
        <div class="control-group">
          <label class="control-label" for="tes_id_institucion">Institución</label>
          <div class="controls">
            {$campos.tes_id_institucion}
          </div>
        </div>
        
         <!-- Select Basic -->
        <div class="control-group">
          <label class="control-label" for="tes_id_grado_obtenido">Grado obtenido</label>
          <div class="controls">
            {$campos.tes_id_grado_obtenido}
          </div>
        </div>
        
       <!-- Select Basic -->
        <div class="control-group">
          <label class="control-label" for="tes_id_especie">Especie</label>
          <div class="controls">
            {$campos.tes_id_especie}
          </div>
        </div>
           
    </div>
    
    <div class="span3"><!-- Columna 2 -->

        
         <div class="control-group">
          <label class="control-label" for="tes_id_disciplina_estudio">Disciplina</label>
          <div class="controls">
            {$campos.tes_id_disciplina_estudio}
          </div>
        </div>
           
                <!-- Select Basic -->
         <div class="control-group">
          <label class="control-label" for="tes_id_subdisciplina">Subdisciplina</label>
          <div class="controls">
            {$campos.tes_id_subdisciplina}
          </div>
        </div>        
        
    </div>

    <div class="span3"> <!-- Columna 3 -->
        
        <div class="control-group {cls_error("tes_nombre_director")}">
		  <label class="control-label" for="tes_nombre_director">Director</label>
		  <div class="controls">
		    {$campos.tes_nombre_director}
		    <!--          {help_inline(form_error_msg("tes_id_subdisciplina", " "))}
                </select>-->
		  </div>
		</div>
		
		
	   <!-- Select Basic -->
	   <!--
		<div class="control-group {cls_error("tes_id_subdisciplina")}">
		  <label class="control-label" for="tes_id_subdisciplina">Director</label>
		  <div class="controls">
		    {$campos.tes_id_tesis_director}
		  </div>
		</div>-->
        
        <!-- Busqueda por autor -->
        <div class="control-group">
          <label class="control-label" for="tes_nombre_autor">
              Autor
          </label>
          <div class="controls">
            {$campos.tes_nombre_autor} 
          </div>
        </div>
        
        
        <!--
        <div class="control-group">
          <label class="control-label" for="tes_nombre_autor">
              Autor
          </label>
          <div class="controls">
            {$campos.tes_id_tesis_autores} 
          </div>
        </div>
        -->
        
       <!-- Button -->
         <div class="control-group">
          <label class="control-label no-padding" for="">
            <button type="button" style="width:100px;" id="btn-clean" name="" class="btn  "><i class="icon-filter icon"></i> Limpiar</button>  
          </label>
          <div class="controls">
            <button style="width:100px;" id="singlebutton" name="singlebutton" class="btn-block btn btn-primary "><i class="icon-search icon-white"></i> Buscar</button> 
          </div>
        </div>

        
    </div>
       
</div>

</fieldset>
</form>

    
</div>
