<h3 class="text-success">Búsqueda avanzada de Tesis</h3>
        {include file="busqueda_avanzada/filtros.tpl"}
   
        {include file="busqueda_avanzada/tabla.tpl"}


<div class="pagination">
	<center>{$paginador}  	
		<ul>
			<li class="active"><a>Se han encontrado {$total_tesis} tesis.</a></li>
		</ul>
	</center>  
</div>


{include file="modals/modal_loguear_usuario_c.tpl"}
