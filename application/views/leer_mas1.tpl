<div class="row-fluid">
	<br>
	<div class="span7" align="center">
	<h3 class="text-success" style="margin-bottom: 0px"><em>Alouatta palliata ssp. mexicana</em></h3>
	<h3 class="text-success" style="margin-bottom: 20px">(Nombres comunes: mono aullador de manto, saraguato pardo)</h3>
	</div>
	<div class="span3 offset1" align="center">
	 	<img style="width:100%"  src="{base_url()}themes/default/img/mono1.jpg" alt>
	</div>
	<br>
	<div class="span10" >
	<font face="times">
	<p class="lead" align="justify">
	Los monos aulladores de manto (<em>A. palliata</em>) son primates robustos y de gran talla, los adultos suelen medir entre 99 y 125 centímetros, 
	y la cola supera la longitud promedio del cuerpo. Los machos pesan entre 4.5 y 9.8 kilogramos y las hembras entre 3.1 y 7.6 kilogramos 
	(Rowe, 1996). Su pelaje suele ser denso y de un color característico, dorado en los flancos y región de las axilas, y áreas sin pigmento 
	en manos, patas y cola. La cara de esta especie se encuentra desprovista de pelo. Igual que en otras especies de <em>Alouatta</em>, los mentones 
	de <em>A. palliata</em> suelen ser elongados con la apariencia de una barba como una característica que se destaca en los machos. La cola es larga 
	y prensil y desprovista de pelo en su último tercio inferior cubierto por dermatoglifos. La mandíbula y hueso hioides se encuentran 
	aumentados de tamaño y ambos conforman una caja de resonancia que incrementa la potencia de las vocalizaciones propias de <em>Alouatta</em>. 
	<em>Alouatta</em> palliata es un primate arbóreo de hábitos diurnos, suele preferir la cobertura vegetal primaria aunque, de acuerdo con algunos 
	especialistas, también se le puede encontrar en una amplia gama de hábitats, incluyendo vegetación secundaria perturbada (Rodríguez-Luna,
	1996 ab.). <em>Alouatta palliata</em> es unánimemente considerado como folívoro (fermentador cecocólico) ya que su dieta se basa en el consumo de
	hojas de una amplísima variedad de especies vegetales en diversidad de estadíos fenológicos. Sin embargo, Crockett y Eisenberg (1987) y
	Kinzey (1995) sugieren que <em>Alouatta</em> sea considerado un folivoro-frugivoro más que un simple folívoro. <em>A. palliata</em> mexicana es la 
	subespecie que se encuentra en México y se distingue de las otras dos (<em>A.palliata equatorialis</em> y <em>A.palliata palliata</em>) por algunas 
	características de morfología craneana (Rylands et al. 2006). Según la IUCN (Unión Internacional para la Conservación de la Naturaleza), 
	esta subespecie se encuentra en peligro crítico de extinción (Cuarón et al. 2008).
	<br>
	</p>
	<p class="lead" align="justify">
	<strong>Texto tomado de:
	SEMARNAT / CONANP. 2012. Programa de acción para la conservación de las especies: Primates, Mono Araña (<em>Ateles geoffroyi</em>) y Monos 
	Aulladores (<em>Alouatta palliata, Alouatta pigra</em>). Oropeza-Hernández, P.; Rendón-Hernández, E. (Eds.). Comisión Nacional de Áreas Naturales
	 Protegidas, Primera Edición. 53 Pp.
	 <strong>
	 </p>
	</font>
	</div>

</div>