<div class="row-fluid">

  <div class="row">
  <div class="span3 offset2">  
		<a class="brand" href='http://www.lania.edu.mx/' style="padding: 0; float:center">
                <img src='{base_url()}themes/default/img/logo_lania.png'>
        </a>
  </div>
  <div class="span3 offset3">  
    
		<a class="brand" href='http://www.lania.edu.mx/index.php?option=com_content&task=section&id=4&Itemid=165' style="padding: 0; float:center">
                <img src='{base_url()}themes/default/img/logo_MCA.png'>
        </a>
    
  </div>
 </div>
 
 <br>
 
  <div class="span10"> 
    <p class='lead' align="justify">
		<small>
		<img width="20%" src='{base_url()}themes/default/img/ocharan.jpg' align='right'>Este proyecto fue realizado por alumnos de la <strong>Maestría en Computación Aplicada</strong> del <strong>Laboratorio Nacional de Informática Avanzada</strong>
    	bajo la dirección del <strong>Maestro Jorge Octavio Ocharán Hernández</strong> (<em>Scrum Master</em>)
    	durante el periodo Enero - Agosto 2013.
    	</small>
    </p>
 </div>
 
 <div class="span10"> 
    

<center><img src='{base_url()}themes/default/img/mini-escudo-primat.png'></center>		
  <h3 class="text-success" style="text-align: center" > Equipo de desarrollo </h3>
      	
    
 </div>
  
<div class="row">
  <div class="span4 offset1">  
    <p>
     <img src='{base_url()}themes/default/img/foto-zulema.png'>
     <strong>I.S.C. Zulema López Márquez.</strong>  (<em>Product Owner</em>) <br>
     Egresada del Instituto Tecnológico de Veracruz.<br> 
     Áreas de interés: Inteligencia Artificial, sistemas Distribuidos.<br>
     Correo: zulema.lopez.m@gmail.com<br>
    </p>
 </div>
 
  <div class="span4 offset2"> 
    <p>
     <img src='{base_url()}themes/default/img/foto-sofia.png'>
     <strong>I.S.C. Sofía Isabel Fernández Gregorio.</strong> <br>
     Egresada del Instituto Tecnológico Superior de Misantla. <br>
     Áreas de interés: Análisis y diseño de sistemas.<br>
	Correo: sofiis.isa@gmail.com<br>
    </p>
  </div> 
</div>

 

<div class="row">
  <div class="span4 offset1">  
    <p>
     <img src='{base_url()}themes/default/img/foto-uziel.png'>
     <strong>I.S.C. Uziel García López.</strong> <br>
     Egresado del Instituto Tecnológico de Veracruz.<br> 
     Áreas de Interés: Consultoría en tecnologías WEB.<br>
     Correo: uzielgl@hotmail.com<br>
    </p>
 </div>
 

  <div class="span4 offset2"> 
    <p>
     <img src='{base_url()}themes/default/img/foto-rubi.png'>
     <strong>L.S.C.A Ismeraí Rubí García Román.</strong> <br>
     Egresada de la Universidad Veracuzana,<br> de la facultad de Administración del Puerto de Veracruz.<br> 
	 Áreas de Interés: Consultoría, Ingeniería de Software.<br>
	 Correo: ismerai.groman@gmail.com<br>
    </p>
  </div> 
</div>
  
<div class="row">
  <div class="span4 offset1">  
    <p>
     <img src='{base_url()}themes/default/img/foto-german.png'>
     <strong>I.S.C. Germán Gómez Castro.</strong> <br>
     Egresado de Instituto Tecnológico Superior de Cosamaloapan.<br> 
     Áreas de interés Aplicaciones móviles.<br>
     Correo: germangomezc@gmail.com<br>
    </p>
 </div>
 
 
  <div class="span4 offset2"> 
    <p>
     <img src='{base_url()}themes/default/img/foto-ferGR.png'>
     <strong>I.S.C. Fernando González Ramírez.</strong> <br>
	Egresado del Instituto Tecnológico de Veracruz. <br>
	Areas de Interés: Programación WEB y diseño gráfico.<br>
	Correo: fer.mxvr@gmail.com<br>
    </p>
  </div> 
</div>

<div class="row">
  <div class="span4 offset1">  
    <p>
     <img src='{base_url()}themes/default/img/foto-yair.png'>
     <strong>I.S.C. José Yaír Guzmán Gaspar.</strong> <br>
     Egresado del Instituto Tecnológico Superior de Xalapa.<br> 
     Áreas de Interés: Inteligencia Artificial (Algoritmos bio-inspirados).<br>
     Correo: yairguz@hotmail.com<br>
    </p>
 </div>
 
  <div class="span4 offset2"> 
    <p>
     <img src='{base_url()}themes/default/img/foto-yunuem.png'>
     <strong>L.I. Lucía Yunuén Juárez López.</strong> <br>
	Egresada de la Universidad Veracruzana. <br>
	Áreas de interés: Ingeniería de software, cómputo evolutivo, <br>administración de proyectos de tecnologías de la información y comunicación.<br>
	Correo: yunuen.jl@gmail.com<br>
    </p>
  </div> 
</div>



<div class="row">
  <div class="span4 offset1">  
    <p>
     <img src='{base_url()}themes/default/img/foto-elyar.png'>
     <strong>I.S.C. Elyar Alberto López Dávila.</strong> <br>
     Egresado del Instituto Tecnológico de Orizaba. <br>
	 Áreas de Interés: Cómputo Inteligente<br> (Algoritmos evolutivos y bio-inspirados).<br>
	 Correo: elyar.rt.spdr@hotmail.com<br>
    </p>
 </div>
 

  <div class="span4 offset2"> 
    <p>
     <img src='{base_url()}themes/default/img/foto-marco.png'>
     <strong>I.S.C. Marco Antonio López Martínez.</strong> <br>
	Egresado del Instituto Tecnológico de Tuxtla Gutiérrez, Chiapas.<br> 
	Áreas de Interés: Programación y Bases de datos.<br>
	Correo: mmarcoantoniolopez@gmail.com;<br>

    </p>
  </div> 
</div>

<div class="row">
  <div class="span4 offset1">  
    <p>
     <img src='{base_url()}themes/default/img/foto-alma.png'>
     <strong>L.I. Alma Delia Méndez Sánchez.</strong> <br>
     Egresada de la Universidad Veracruzana.<br> 
	Área de interés: Analista de sistemas y base de datos.<br>
	Correo:  eikaao@gmail.com <br>
    </p>
 </div>
 
  <div class="span4 offset2"> 
    <p>
     <img src='{base_url()}themes/default/img/foto-rodrigo.png'>
     <strong>I.S.C. Rodrigo Arturo San Martin Monroy.</strong> <br>
	Egresado del Instituto Tecnológico Superior de Poza Rica.<br>
	Áreas de Interés: Algoritmos y Estructuras de Datos<br>
	Correo: kyoo_16@hotmail.com<br>
    </p>
  </div> 
</div>

  
 </div>