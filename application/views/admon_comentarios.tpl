<div class="container-fluid" >
    <div class="row-fluid">
        <h3 class="text-success" style="text-align: left" >Administración de Comentarios</h3>
        <div class="row-fluid">
            <div class="span12">
                {if isset($vista)}
                <div class="tabbable"> <!-- Only required for left/right tabs -->
                    <ul class="nav nav-tabs" style="margin-bottom: 0px">
                        {if $vista === "sitio"}
                            <li class="active">
                                <a href="{base_url()}index.php/admon_comentarios_c/comen_sitio" >Sitio</a>
                            </li>
                        {else}
                            <li>
                                <a href="{base_url()}index.php/admon_comentarios_c/comen_sitio" >Sitio</a>
                            </li>
                        {/if}
                        {if $vista === "tesis"}
                            <li class="active">
                                <a href="{base_url()}index.php/admon_comentarios_c/comen_tesis" >Tesis</a>
                            </li>
                        {else}
                            <li>
                                <a href="{base_url()}index.php/admon_comentarios_c/comen_tesis" >Tesis</a>
                            </li>
                        {/if}
                        
                        {if $vista === "aprobados"}
                            <li class="active">
                                <a href="{base_url()}index.php/admon_comentarios_c/comen_aprobados" >Aprobados</a>
                            </li>
                        {else}
                            <li>
                                <a href="{base_url()}index.php/admon_comentarios_c/comen_aprobados" >Aprobados</a>
                                
                            </li>
                        {/if}

                    </ul>
                    <div class="tab-content">
                        {if $vista === "sitio"}
                            {include file="modals/modal_aprobar_comentario_sitio.tpl"}
                            {include file="modals/modal_eliminar_comentario_sitio.tpl"}
                            {include file="admon_comentarios_sitio_c.tpl"}
                        {elseif $vista === "tesis"}
                            {include file="modals/modal_aprobar_comentario_tesis.tpl"}
                            {include file="modals/modal_eliminar_comentario_tesis.tpl"}
                            {include file="admon_comentarios_tesis_c.tpl"}
                        {elseif $vista === "aprobados"}
                        	{include file="modals/modal_eliminar_comentario_tesis.tpl"}
                        	{include file="admon_comentarios_aprobados_c.tpl"}
                        {/if}
                        
                    </div>
                </div>
                {/if}
            </div>
        </div>
    </div>
</div>
               