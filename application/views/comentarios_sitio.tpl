<h3 class="text-success">{$titulo}</h3>
<div class="tab-pane active" id="admon_comentarios_sitio">
    <ul class="thumbnails">                    
        <li class="span12">
            <div class="thumbnail">
                <div class="row-fluid">
                    <div class="row-fluid">
                        <div class="span12" style="overflow-y: auto;">
                            <table id="tabla_comentarios_sitio" class="table table-hover table-bordered" >
                                <thead> 
                                  <tr id={$comsit.com_id_comentario_sitio|string_format:"tr_%d"}>
                                    <th style="width:100px"> Fecha </th>
                                    <th> Usuario </th>
                                    <th> Comentario  </th>
                                  </tr>
                                  </thead> 
                                   {foreach from=$comentarios_sitio item=comsit name=foo}
                                   {if $smarty.foreach.foo.index % 2 == 0}
                                        <tr id={$comsit.com_id_comentario_sitio|string_format:"tr_%d"} class="success" >
                                   {else}
                                        <tr id={$comsit.com_id_comentario_sitio|string_format:"tr_%d"}>
                                   {/if}     
                                   <td id={$comsit.com_id_comentario_sitio|string_format:"com_fecha_creacion_%d"} >
                                            {$comsit.com_fecha_creacion}
                                   </td>               
                                   <td id={$comsit.com_id_comentario_sitio|string_format:"username_%d"} >
                                           {$nombre = ucwords($comsit.username)}
                                           {$nombre}
                                   </td>
                                   <td id={$comsit.com_id_comentario_sitio|string_format:"com_comentario_%d"} >
                                            {$comsit.com_comentario}
                                   </td>
                                  </tr>
                               {/foreach}
                             </table>
                        </div>
                        {$paginacion}
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>