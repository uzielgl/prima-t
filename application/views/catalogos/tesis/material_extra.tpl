{include file="modals/modal_eliminar_material_extra.tpl"}
<form id="form_agregar" class="form-horizontal" method="post" ENCTYPE="multipart/form-data">
    <legend>Material Extra: <span style="font-size: 16px">{$tesis.tes_nombre}</span></legend>
    <input id="{$tesis.tes_nombre}"  type="hidden" name="tesis" value="{$tesis.tes_nombre}" />
    <ul class="thumbnails">
        <li class="span12">
            <div class="thumbnail">
                <div class="span12" style="margin-top: -13px">
                    <span class="label  pull-left">Imagen(es):</span>  
                </div>
                <div class="control-group">
                    <label class="control-label" >Imagen:</label>
                    <div class="controls" id="camposImagenes">
                        <div id="imagen1">
                            {form_input($campos.tes_ruta_imagenes)}
                            <a id="agregar_imagen" name="agregar_imagen" class="btn btn-success" onClick="AgregarCamposImagen(this);">+</a>
                        </div>
                    </div>
                    {help_inline(form_error_msg("tes_ruta_imagen[]", " "))}
                    <div class="controls" id="camposImagenes_materialExtra">
                        <table>
                            <thead>
                                <th></th>
                                <th></th>
                            </thead>
                            <tbody>
                            {foreach $material_extra.imagenes as $imagen}
                                <tr id="imagen_materialExtra_{$imagen.tes_id_tesis_imagenes}">
                                    <td>
                                        {$imagen.tes_ruta_imagen}
                                    </td>
                                    <td>
                                        <a id="eliminar_imagen" id_imagen="{$imagen.tes_id_tesis_imagenes}" name="eliminar_imagen" onClick="modalEliminarImagen(this);" class="btn btn-mini btn-danger">
                                            <i class="icon-white icon-remove"></i> Eliminar
                                        </a>
                                    </td>
                                </tr>
                            {/foreach}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </li>
        <li class="span12">
            <div class="thumbnail">
                <div class="span12" style="margin-top: -13px">
                    <span class="label  pull-left">Video(s):</span>  
                </div>
                <div class="control-group">
                    <label class="control-label" >Video:</label>
                    <div class="controls" id="camposVideos">
                        <div id="video1">
                            {form_input($campos.tes_nombre_videos)}
                            <a id="agregar_video" name="agregar_video" class="btn  btn-success" onClick="AgregarCamposVideo(this);">+</a>
                        </div>
                    </div>
                    {help_inline(form_error_msg("tes_nombre_video[]", " "))}
                    <div class="controls" id="camposVideos_materialExtra">
                        <table>
                            <thead>
                                <th></th>
                                <th></th>
                            </thead>
                            <tbody>
                            {foreach $material_extra.videos as $video}
                                <tr id="video_materialExtra_{$video.tes_id_tesis_videos}">
                                    <td>
                                        <a href="{$video.tes_ruta_video}">{$video.tes_ruta_video}</a>
                                    </td>
                                    <td>
                                        <a id="eliminar_video" id_video="{$video.tes_id_tesis_videos}" name="eliminar_video" onClick="modalEliminarVideo(this);" class="btn btn-mini btn-danger">
                                            <i class="icon-white icon-remove"></i> Eliminar
                                        </a>
                                    </td>
                                </tr>
                            {/foreach}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </li>
        <li class="span12">
            <div class="thumbnail">
                <div class="span12" style="margin-top: -13px">
                    <span class="label  pull-left">Documento(s):</span>  
                </div>
                <div class="control-group">
                    <label class="control-label" >Documento:</label>
                    <div class="controls" id="camposDocumentos">
                        <div id="documento1">
                            {form_input($campos.tes_ruta_documentos)}
                            <a id="agregar_documento" name="agregar_documento" class="btn btn-success" onClick="AgregarCamposDocumento(this);">+</a>
                        </div>
                    </div>
                    {help_inline(form_error_msg("tes_nombre_documento[]", " "))}
                    <p>El tipo de documento aceptable es PDF, WORD y EXCEL</p>
                    <div class="controls" id="camposDocumentos_materialExtra">
                        <table>
                            <thead>
                                <th></th>
                                <th></th>
                            </thead>
                            <tbody>
                            {foreach $material_extra.documentos as $documento}
                                <tr id="documento_materialExtra_{$documento.tes_id_tesis_documentos}">
                                    <td>
                                        {$documento.tes_ruta_documento}
                                    </td>
                                    <td>
                                        <a id="eliminar_documento" id_documento="{$documento.tes_id_tesis_documentos}" name="eliminar_documento" onClick="modalEliminarDocumento(this);" class="btn btn-mini btn-danger">
                                            <i class="icon-white icon-remove"></i> Eliminar
                                        </a>
                                    </td>
                                </tr>
                            {/foreach}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </li>
        <li class="span12">
            <div class="thumbnail">
                <div class="span12" style="margin-top: -13px">
                    <span class="label  pull-left">Audio(s):</span>  
                </div>
                <div class="control-group">
                    <label class="control-label" >Audio:</label>
                    <div class="controls" id="camposAudios">
                        <div id="audio1">
                            {form_input($campos.tes_ruta_audios)}
                            <a id="agregar_documento" name="agregar_audio" class="btn btn-success" onClick="AgregarCamposAudio(this);">+</a>
                        </div>
                    </div>
                    {help_inline(form_error_msg("tes_nombre_audio[]", " "))}
                    <div class="controls" id="camposAudios_materialExtra">
                        <table>
                            <thead>
                                <th></th>
                                <th></th>
                            </thead>
                            <tbody>
                            {foreach $material_extra.audios as $audio}
                                <tr id="audio_materialExtra_{$audio.tes_id_tesis_audios}">
                                    <td>
                                        {$audio.tes_ruta_audio}
                                    </td>
                                    <td>
                                        <a id="eliminar_audio" id_audio="{$audio.tes_id_tesis_audios}" name="eliminar_audio" onClick="modalEliminarAudio(this);" class="btn btn-mini btn-danger">
                                            <i class="icon-white icon-remove"></i> Eliminar
                                        </a>
                                    </td>
                                </tr>
                            {/foreach}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </li>
         <li class="span12">
            <div class="thumbnail">
                <button id="agregar" name="agregar" type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i> Guardar</button
            </div>
         </li>
    </ul>
</form>