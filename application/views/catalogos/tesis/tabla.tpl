<table class="table table-striped tesis ">
	<thead>
		<tr class="order">
			<!--
			<th data-field="tes_nombre" data-type="{ter(set_value("order_by")=="tes_nombre", set_value("order_type"))}" >
				Título{set_order_type("tes_nombre")}
			</th>
			-->
			<th data-field="tes_nombre" data-type="{ter(set_value("order_by")=="tes_nombre", set_value("order_type"))}" >
				<div class="filter {set_cls_order("tes_nombre")}">
					<span>Título</span>
				</div>
			</th>
			<th data-field="autores" data-type="{ter(set_value("order_by")=="autores", set_value("order_type"))}">
				<div class="filter {set_cls_order("autores")}">
					<span>Autores</span>
				</div>
			</th>
			<th data-field="directores" data-type="{ter(set_value("order_by")=="directores", set_value("order_type"))}">
                <div class="filter {set_cls_order("directores")}">
                    <span>Directores</span>
                </div>
            </th>
			<th data-field="tes_id_grado_obtenido" data-type="{ter(set_value("order_by")=="tes_id_grado_obtenido", set_value("order_type"))}">
				<div class="filter {set_cls_order("tes_id_grado_obtenido")}">
					<span>Grado</span>
				</div>
			</th>
			<th data-field="descargas" data-type="{ter(set_value("order_by")=="descargas", set_value("order_type"))}">
				<div class="filter {set_cls_order("descargas")}">
					<span>Descargas</span>
				</div>
			</th>
			<th data-field="visitas" data-type="{ter(set_value("order_by")=="visitas", set_value("order_type"))}">
				<div class="filter {set_cls_order("visitas")}">
					<span>Visitas</span>
				</div>
			</th>
			<th> </th>
			<th> </th>
			<th> </th>
			<th> </th>
		</tr>
	</thead>
	<tbody>
	    {foreach $tesis as $i}
    		<tr>
    			<td >
    				{highlight(set_value('palabra_clave'), $i.tes_nombre)}
    			</td>
    			<td>
    				{highlight(set_value('palabra_clave'), substr($i.autores,0,strlen($i.autores)-2))}
    			</td>
    			<td>
                    {highlight(set_value('palabra_clave'), $i.directores)}
                </td>
    			<td>
    				{highlight(set_value('palabra_clave'), $i.gra_descripcion)}
    			</td>
    			<td >
    				<div class="text-right">
    					{highlight(set_value('palabra_clave'), $i.descargas)}
    				</div>
    			</td>
    			<td class="separation">
    				<div class="text-right">
    					{$i.visitas}
    				</div>
    			</td>
    			<td class="btn-container">
    				<a id="btn-ver-tesis-{$i.tes_id_tesis}" href="{site_url("catalogos/tesis/ver_tesis_admin/"|cat:$i.tes_id_tesis)}"  class="btn btn-mini btn-action"><i class="icon-black icon-eye-open"></i> Ver</a>
    			
    	<!--		<a id="btn-ver-tesis-{$i.tes_id_tesis}"  href="href='{base_url()}index.php/visualizar_tesis_administrador"  class="btn btn-mini btn-action"><i class="icon-black icon-eye-open"></i> Ver</a>
    		-->		
    			</td>
    			<td class="btn-container" >
                    <a style="width:82px" href="{site_url("catalogos/tesis/material_extra/"|cat:$i.tes_id_tesis)}" class="btn btn-mini"><i class="icon-pencil"></i> Material Extra</a>
                </td>
    			<td class="btn-container">
    				<a href="{site_url("catalogos/tesis/modificar/"|cat:$i.tes_id_tesis)}" class="btn btn-mini btn-warning"><i class="icon-white icon-pencil"></i> Editar</a>
    			</td>
    			<td class="btn-container">
    				<a id="btn-eliminar-tesis-{$i.tes_id_tesis}" href="{site_url("catalogos/tesis/eliminar/"|cat:$i.tes_id_tesis)}" class="btn btn-mini btn-danger"><i class="icon-white icon-remove"></i> Eliminar</a>
    			</td>
    		</tr>
		{/foreach}
	</tbody>
</table>