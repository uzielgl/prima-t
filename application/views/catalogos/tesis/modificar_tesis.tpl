<form class="form-horizontal"  id="formModificarTesis" action="" method="post" enctype="multipart/form-data" >
<fieldset>

<!-- Form Name -->
<Legend><h3 class="text-success">Modificar Tesis</h3></Legend>
<input type="hidden" id="id_tesis" name="id_tesis" value="{$tesis.tes_id_tesis}" />
<!-- Text input-->
<div class="control-group">
  
  <label class="control-label" for="tes_palabras_clave">Palabras clave:</label>
  <div class="controls">
    {form_input($campos.tes_palabras_clave)}
    {help_inline(form_error_msg("tes_palabras_clave", " "))}
  	<span class="muted">Opcional </span>
  </div>
</div>

<div class="control-group {cls_error("tes_nombre")}">
  <label class="control-label" for="tes_nombre">Titulo: </label>
  <div class="controls">
    {form_input($campos.tes_nombre)}
    {help_inline(form_error_msg("tes_nombre", " "))}
  </div>
</div>


{if $this->input->post("tes_nombre_autor") }
	{$autores_post = $this->input->post("tes_nombre_autor")}
	
	<!-- Text input-->
	<div class="control-group {cls_error("tes_nombre_autor[$i]")}">
	  <label class="control-label" for="tes_nombre_autor">Autor:</label>
	  <div class="controls" id="camposAutores">
	    <div id="autor1">
		{$campos.tes_nombre_autor.value = $autores_post[0]}
	    {form_input($campos.tes_nombre_autor)}
	    <a id="agregar_autor" name="agregar_autor" class="btn btn-default" onClick="AgregarCamposAutor(this);">+</a>
		{help_inline(form_error_msg("tes_nombre_autor["|cat:$autores_post[0]|cat:"]", " "))}
	    </div>
	    
	    {foreach $autores_post as $i}
	    	{if $i@index != 0 }
	    		<div id="autor{$i@index+1}">
				{$campos.tes_nombre_autor.value = $i}
			    {form_input($campos.tes_nombre_autor)}
				<a onclick="QuitarCampoAutor('autor{$i@index+1}');" class="btn btn-default" name="agregar_autor" id="agregar_autor">-</a>			    
				{help_inline(form_error_msg("tes_nombre_autor[$i]", " "))}
			    </div>
	    	{/if}
	    {/foreach}
	    
	  </div>
	</div>
	
{else}
	
	<!-- Text input-->
	<div class="control-group {cls_error("tes_nombre_autor[]")}">
	  <label class="control-label" for="tes_nombre_autor">Autor:</label>
	  <div class="controls" id="camposAutores">
	    <div id="autor1">
	    {form_input($campos.tes_nombre_autor)}
	    <a id="agregar_autor" name="agregar_autor" class="btn btn-default" onClick="AgregarCamposAutor(this);">+</a>
		{help_inline(form_error_msg("tes_nombre_autor[]", " "))}
	    </div>
	  </div>
	</div>

{/if}






	<div class="control-group">
		 <label class="control-label" for="tes_resumen">Resumen:</label>
 	 	<div class="controls">                     
    	{form_textarea($campos.tes_resumen)}
    	{help_inline(form_error_msg("tes_resumen", " "))}
  		</div>
	</div>

<!-- Select Basic -->
<div class="control-group {cls_error("gra_id_grado_academico")}">
  <label class="control-label" for="gra_id_grado_academico">Grado obtenido:</label>
  <div class="controls">
    {$campos.gra_id_grado_academico}
    {help_inline(form_error_msg("gra_id_grado_academico", " "))}
    </select>
  </div>
</div>
		

<!-- Text input-->
<div class="control-group {cls_error("tes_anio_titulacion")}">
  <label class="control-label" for="tes_anio_titulacion">Año de titulación:</label>
  <div class="controls">
    {form_input($campos.tes_anio_titulacion)}
    {help_inline(form_error_msg("tes_anio_titulacion", " "))}
    
  </div>
</div>


<!-- Select Basic -->
<div class="control-group {cls_error("tes_id_institucion")}">
  <label class="control-label" for="tes_id_institucion">Institución de adscripción:</label>
  <div class="controls">
    {$campos.tes_id_institucion}
    {help_inline(form_error_msg("tes_id_institucion", " "))}
    </select>
  </div>
</div>


<div id="camposDirectores">
	
	{if $this->input->post("tes_nombre_director") || $this->input->post("tes_id_grado_academico") }
		{$directores_post = $this->input->post("tes_nombre_director")}
		{$grados_post =  $this->input->post("tes_id_grado_academico")}
		
		{foreach $this->input->post("tes_nombre_director") as $i }
			{$in = $i@index}
			
			<div id="director{$i@index+1}">
				<!-- Text input-->
				<div class="control-group {cls_error("tes_nombre_director[$i]")}">
		  			<label class="control-label" for="tes_nombre_director">Directores de tesis:</label>
		  
		  			<div class="controls">
		  				{$campos.tes_nombre_director.value = $directores_post[$in]}
		    			{form_input($campos.tes_nombre_director)}
		    			
		    			{if $i@index == 0}
		    				<a id="agregar_director" name="agregar_director" class="btn btn-default" onClick="AgregarCamposDirector(this);">+</a>
		    			{else}
		    				<a onclick="QuitarCampoDirector('director{$i@index+1}');" class="btn btn-default" name="agregar_director" id="agregar_director">-</a>
		    			{/if}
		    			
		    			{help_inline(form_error_msg("tes_nombre_director[$i]", " "))}
		   			</div >
		  		</div>
		  		
		  		<!-- Select Basic -->
		  		<div class="control-group {cls_error("tes_id_grado_academico["|cat:$grados_post[$in]|cat:"]")}">
		  			<label class="control-label" for="tes_id_grado_academico">Grado académico del Director:</label>
		  			<div class="controls">
		  				{form_dropdown("tes_id_grado_academico[]", $campos.tes_id_grado_academico[1], $grados_post[$in], $campos.tes_id_grado_academico[3])}
		    	    	{help_inline(form_error_msg("tes_id_grado_academico["|cat:$grados_post[$in]|cat:"]", " "))}
				    	</select>
		  			</div>
				</div>
		  	</div>
		  	
  			{/foreach}
		{else}
	
		<div id="director1">
			<!-- Text input-->
			<div class="control-group {cls_error("tes_nombre_director[]")}">
	  			<label class="control-label" for="tes_nombre_director">Directores de tesis:</label>
	  
	  			<div class="controls">
	    			{form_input($campos.tes_nombre_director)}
	    			<a id="agregar_director" name="agregar_director" class="btn btn-default" onClick="AgregarCamposDirector(this);">+</a>
	    			{help_inline(form_error_msg("tes_nombre_director[]", " "))}
	   			</div >
	  		</div>
	  		
			<!-- Select Basic -->
			<div class="control-group {cls_error("tes_id_grado_academico[]")}">
	  			<label class="control-label" for="tes_id_grado_academico">Grado académico del Director:</label>
	  			<div class="controls">
	    	    	{form_dropdown($campos.tes_id_grado_academico[0], $campos.tes_id_grado_academico[1], $campos.tes_id_grado_academico[2], $campos.tes_id_grado_academico[3])}
	    	    	{help_inline(form_error_msg("tes_id_grado_academico[]", " "))}
			    	</select>
	  			</div>
			</div>
		</div>
	
	{/if}
	
	
		
	</div>
{help_inline(form_error_msg("tes_nombre_director[]", " "))}
	{help_inline(form_error_msg("tes_id_grado_academico[]", " "))}




<!-- Select Basic -->
<div class="control-group {cls_error("vista_subespecies")}">
  <label class="control-label" for="vista_subespecies">Subespecies:</label>
  <div class="controls">
    {$campos.vista_subespecies}
    </select>
    <span class="muted">Opcional </span>
  </div>
</div>



<!-- Select Basic -->
<div class="control-group {cls_error("con_id_condicion_sitio")}">
  <label class="control-label" for="con_id_condicion_sitio">Condición del sitio de estudio:</label>
  <div class="controls">
    {$campos.con_id_condicion_sitio}
    {help_inline(form_error_msg("con_id_condicion_sitio", " "))}
    </select>
  </div>
</div>

<!-- Select Basic -->
<div class="control-group {cls_error("est_id_zona_estudio")}">
  <label class="control-label" for="est_id_zona_estudio">Zona de estudio:</label>
  <div class="controls">
 	{$campos.est_id_zona_estudio}
    {help_inline(form_error_msg("est_id_zona_estudio", " "))}
  </select>
  </div>
</div>


<!-- Select Basic -->
<div class="control-group {cls_error("vista_subdisciplinas")}">
  <label class="control-label" for="vista_subdisciplinas">Subdisciplina de estudio:</label>
  <div class="controls">
    {$campos.vista_subdisciplinas}
    {help_inline(form_error_msg("vista_subdisciplinas", " "))}
    </select>
    <span class="muted">Opcional </span>
  </div>
</div>


<!-- File Button --> 
{*<div class="control-group {cls_error("tes_ruta_tesis")}">
  <label class="control-label" for="tes_ruta_tesis">Archivo PDF de la tesis</label>
  <div class="controls">
    <!--input id="tes_ruta_tesis" name="tes_ruta_tesis" class="input-file" type="file" accept="application/pdf"-->
    {form_input($campos.tes_ruta_tesis)}
  </div>
</div>*}

<div>
	<label class="control-label" for="tes_ruta_tesis">Archivo PDF de la tesis:</label>
	<div class="controls">
		<button type="button" URL="{site_url()}/catalogos/tesis/editarTesisPDF/{$tesis.tes_id_tesis}" class="btn btn-warning btn-mini" id="modal_editar_tesis" ><i class="icon-pencil icon-white"></i> Editar PDF</a>		
	</div>
</div>

<!-- Button -->
<!--div class="control-group">
  <label class="control-label" for="subir_archivo">  </label>
  <div class="controls">
    <button id="subir_archivo" name="subir_archivo" class="btn btn-primary"> + Adjuntar Archivo</button>
    
  </div>
</div-->




<!-- File Button --> 
<!--div class="control-group">
  <label class="control-label" for="subir_material_extra">Material extra:</label>
  <div class="controls">
    <input id="subir_material_extra" name="subir_material_extra" class="input-file" type="file">
    
  </div>
</div-->

<!-- Textarea -->
<!--div class="control-group">
  <label class="control-label" for="textarea"> </label>
  <div class="controls">                     
    <textarea id="textarea" name="textarea">Listado de material extra</textarea>
    <button id="agregar_material" name="agregar_material" class="btn btn-default">+</button>
    <button id="quitar_material" name="quitar_material" class="btn btn-default">- </button>
  </div>
</div-->


</fieldset>
<br>
<button id="editarTesis" URL="{site_url()}/catalogos/tesis/modificarTesis" type="button" class="btn btn-warning"><i class="icon-pencil icon-white"></i> Guardar</button>
</form>
