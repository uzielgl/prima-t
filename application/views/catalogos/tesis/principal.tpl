

<h3 class="text-success">Administración de tesis</h3>
        {include file="catalogos/tesis/filtros.tpl"}

<div class="pull-left">
    <a class="btn btn-success" href="{site_url("catalogos/tesis/registrar")}"><i class="icon-white icon-plus"></i> Tesis</a>
</div>

<h4 class="pull-left"  style="margin-left:20px;" >Se han encontrado <span class="total-tesis">{$total_tesis}</span> tesis.</h4>
<!-- 
<div class="pagination" style="margin-left:10px;">
    <ul>
        <li class="active"><a>Se han encontrado {$total_tesis} tesis.</a></li>
    </ul>
</div>-->
    
{include file="catalogos/tesis/tabla.tpl"}


<div class="pagination">
	<center>{$paginador}</center>
</div>


<div class="clearfix"></div>




<!-- Para los modales -->

<!-- Modal de confirmación-->
<div id="mdl-eliminar-tesis" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Eliminar tesis</h3>
  </div>
  <div class="modal-body">
    <p>¿Realmente deseas eliminar la tesis <span class="nombre-tesis"></span>?</p>
  </div>
  <div class="modal-footer">
    <button id='btn-eliminar-tesis' class="btn btn-danger"><i class="icon-white icon-warning-sign"> </i> Eliminar</button>
    <button class="btn" data-dismiss="modal" aria-hidden="true"><i class="icon icon-remove"> </i> Cancelar</button>
  </div>
</div>


<div id='mdl-espera-eliminar-tesis' class="modal hide fade">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3>Eliminando...</h3>
  </div>
  <div class="modal-body">
    <p>
    	Eliminando... {image('loading.gif')}
    </p>
  </div>
</div>


<div id="modal_info" class="modal hide fade">
    <div id="modal_info_header" class="modal-header">
        
    </div>
    <div id="modal_info_body" class="modal-body">
    
    </div>
</div>


