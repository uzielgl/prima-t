﻿       
  <html lang='es'>
  <head>  
    
  </head>
<body>             
 <div class="container-fluid" >
 {include file="modals/modal_tesis_videos.tpl"}
     {include file="modals/modal_tesis_documentos.tpl"}
     {include file="modals/modal_tesis_audios.tpl"}
     {include file="modals/modal_tesis_imagenes.tpl"}
     <div class="row-fluid">
       <h3 class="text-success" style="margin-bottom: 30px">Visualizar Tesis Administrador</h3>
           <div class="row-fluid">
                <div class="span12">
                     <div class="span9">
                       <!--   <div class="row-fluid" style="margin-bottom: 20px">
                               <div class="span12" >
                                    <iframe style="width:98%; height:650px" src={site_url()}{$tes_ruta_tesis|string_format:"themes/default/pdf/%s"} ></iframe>
                                </div>
                          </div>
                          -->
                           <div class="row-fluid" >
                                <ul class="thumbnails" >
                                    <li  class="span12" style="margin-bottom: 0px" >
                                        <div class="thumbnail">
                                            <div class="row-fluid" >
                                                <div class="span12" style="margin-top: -13px">
                                                  <span class="label  pull-left">Datos Generales</span>    
                                                </div>
                                                <div class="span12" >
                                                 <p> <h5>Titulo:</h5></p>
                                                 <div class="span11" >
                                             		<p> {$tes_nombre|string_format:"<h7>%s</h7>"} </p>
                                            	 </div>
                                                <p> <h5>Resumen:</h5></p>
                                                 <div class="span11" >
                                             		<p> {$tes_resumen|string_format:"<h7>%s</h7>"} </p>
                                            	 </div>
                                                 </div>
                                                 
												<div class="span12" >
                                           <!-- 	<p> {$tes_nombre|string_format:"<h5>Titulo:</h5> %s"}</p> -->
                                             	<p> {$tes_palabras_clave|string_format:"<h5>Palabras claves:</h5> %s"}</p>
                                             	<p> <h5>Autor(es):</h5></p>
                                        		 {foreach  from=$tes_autores item=autor }
                                            	<p> {$autor|string_format:" <h7 >%s</h7 >"}</p>
                                            	{/foreach}
                                          <!--  <p> <h5>Resumen:</h5></p>
                                             <div class="span11" >
                                             <p> {$tes_resumen|string_format:"<h7>%s</h7>"} </p>
                                             </div>
                                        -->     
                                               <p> {$tes_grado_obtenido|string_format:"<h5>Grado:</h5> <h7>%s</h7>"} </p>
                                               <p> {$tes_anio_titulacion|string_format:"<h5>Año:</h5><h7>%s</h7>"} </p>
                                            	<p> {$tes_ins_nombre|string_format:"<h5>Institución:</h5> <h7>%s</h7>"} </p>
                                                                                                                     
												</div>
												
									     </div>
										</div>
										
										
										<div class="thumbnail">
                                            <div class="row-fluid" >
                                                <div class="span12" style="margin-top: -13px">
                                                  <span class="label  pull-left">Director(es)</span>    
                                                </div>
												<div class="span12" >
                                            	<p> <h5>Director(es):</h5></p>
                                            	 {foreach  from=$tes_directores item=director }
                                                <p> {$director|string_format:" <h7 >%s</h7 >"}</p>
                                                 {/foreach}                          
												</div>
																						
									     </div>
										</div>
										
										<div class="thumbnail">
                                            <div class="row-fluid" >
                                                <div class="span12" style="margin-top: -13px">
                                                  <span class="label  pull-left">Datos de Estudio</span>    
                                                </div>
												<div class="span12" >
                                            	<p> <h5>Subespecies:</h5></p>
                                            	 {foreach  from=$tes_esp_sub_especie item=sub_especie }
                                                <p> {$sub_especie|string_format:" <h7 >%s</h7 >"}</p>
                                                 {/foreach}  
                                                 <p> <h5>Condición del sitio de estudio:</h5></p>
                                            	 {foreach  from=$tes_condicion_sitio item=condicion }
                                                <p> {$condicion|string_format:" <h7 >%s</h7 >"}</p>
                                                 {/foreach}
                                                 <p> <h5>Zona de estudio:</h5></p>
                                            	 {foreach  from=$tes_zonas_estudio item=zona }
                                                <p> {$zona|string_format:" <h7 >%s</h7 >"}</p>
                                                 {/foreach}  
                                                  <p> <h5>Subdisciplinas de estudios:</h5></p>
                                            	 {foreach  from=$tes_sub_disciplina item=sub_disciplina }
                                                <p> {$sub_disciplina|string_format:" <h7 >%s</h7 >"}</p>
                                                 {/foreach}   
                                                                                                                        
												</div>
												
									     </div>
										</div>
										
										
										
										
										
                                    </li>
                                </ul>
                            </div>
                            
                            <div class="row-fluid" style="margin-bottom: 20px">
                               <div class="span12" >
                                    <iframe style="width:98%; height:650px" src={base_url()}{$tes_ruta_tesis|string_format:"themes/default/pdf/%s"} ></iframe>
                                </div>
                          </div>
                           
                        </div>
                        
                        
                        <div class="span3" >
                            <ul class="thumbnails">
                                <li  class="span12" >
                                    <div class="thumbnail" style="margin-bottom: 20px">
                                        <div class="row-fluid" >
                                            <div class="span12" style="margin-top: -15px">
                                                <span class="label  pull-left">Otros datos</span>  
                                            </div>
                                            <div class="span12" >
                                            	 <p> {$id_tesis|string_format:"<h5>Identificador de la tesis:</h5> <h7>%s</h7>"} </p>
                                            	<p> {$tes_fecha_creacion|string_format:"<h5>Fecha de creación:</h5> %s"}</p>
                                            	<p> {$tes_usuario_crea|string_format:"<h5>Usuario de creación:</h5> <h7>%s</h7>"} </p>
                                                <p> {$tes_fecha_modificacion|string_format:"<h5>Ultima modificación:</h5> <h7 >%s</h7 >"}</p>
                                                <p> {$tes_usuario_mod|string_format:"<h5>Usuario de modificación:</h5><h7>%s</h7>"} </p>
                                               
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="thumbnail" style="margin-bottom: 20px">
                                        <div class="row-fluid" >
                                            <div class="span12" style="margin-top: -15px">
                                                <span class="label  pull-left">Estadísticas</span>  
                                            </div>
                                             <p> {$tes_num_visitas|string_format:"<h5>Número de visitas:</h5> %s"}</p>
                                            <!-- <p> {$tes_num_descargas|string_format:"<h5>Número de descargas:</h5> %s"}</p>-->
                                             <p> {$prom_cal|string_format:"<h5>Promedio de calificación:</h5> <h7>%s</h7>"} </p>
											
                                          </div>
                                                          
                                    </div>
                                  <div class="thumbnail" style="margin-bottom: 20px">
                                        <div class="row-fluid" >
                                            <div class="span12" style="margin-top: -15px">
                                                <span class="label  pull-left">Material extra</span>  
                                            </div>
                                             <div class="span12 text-left">
                                                <a id_tesis="{$id_tesis}" class="btn btn-info" onclick="modalMostrarImagenes(this);"> Imágenes ({$cantidad_imagenes})</a></br>
                                                </br>
                                                <a id_tesis="{$id_tesis}" class="btn btn-info" onclick="modalMostrarVideos(this);"> Videos ({$cantidad_videos})</a></br>
                                                </br>
                                                <a id_tesis="{$id_tesis}" class="btn btn-info" onclick="modalMostrarDocumentos(this);"> Documentos ({$cantidad_documentos})</a></br>
                                                </br>
                                                <a id_tesis="{$id_tesis}" class="btn btn-info" onclick="modalMostrarAudios(this);"> Audios ({$cantidad_audios})</a></br>
                                            </div>
                                          </div>
                                                          
                                    </div>
                                    
                                    
									
                                </li>
                            </ul>
                        </div>
                        
                        
                        

					
						
                    </div>
                </div>
            </div>
      </div>
        
  </body>
</html>