<div id="editar_tesis_pdf" class="control-label">
	<form class="form-horizontal"  id="modificarPDF" action="../remplazarPDF" method="post" enctype="multipart/form-data" >
    <div class="modal-header">
        <!--button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button-->
        <h3 class="text-success">Editar PDF de Tesis</h3>
        <input type="hidden" id="id_tesis" name="id_tesis" value="{$tesis.tes_id_tesis}" />
    </div>
    <div class="modal-body">
        <div class="thumbnail row-fluid">
            <div class="row-fluid">
                <div class="span12" style="margin-top: -13px">
                    <span class="label  pull-left">Seleccionar el archivo PDF</span>  
                </div>
            </div>
            <div class="row-fluid">
                <!--div class="span4" style="text-align: right; margin-right: 20px">
                    PDF Tesis:
                </div-->
				<div class="control-group">
				  <div class="controls">
				    <input id="tes_ruta_tesis" name="tes_ruta_tesis" class="input-file" type="file" accept="application/pdf"-->
				  </div>
				</div>
                <!--div class="span6" style="text-align: left; margin-top: -5px">
                    <input id="con_nombre_agregar" class="input-xlarge" type="text" placeholder="Nombre condición del Sitio">
                </div-->
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button id="btn_editar"  class="btn btn-warning"><i class="icon-pencil icon-white"></i>Guardar</a>
        <button id="btn_cancelar" class="btn" >Cerrar</a>
    </div>
    </form>
</div>
