<div class="row-fluid">

<div class = "span12">
	{if {$navegador == 'Internet Explorer'}} 
	
			<strong>
				<center>
			<p style="background-color:#F9F26B">
				<font size=3>	
				 Para una mejor experiencia de navegación en el sitio te recomendamos usar <font color=red>Mozilla Firefox</font> o <font color=red>Google Chrome </font>como navegador.
				</font>
			</p>
				</center>
		</strong>
	{/if}
</div>
	
	{if $this->ion_auth->is_admin()}
 <br>
<div class="span7">
	<a class="brand" style="margin-bottom: 0px" >
		<img src='{base_url()}themes/default/img/modo_admin.png'>
	</a>
	<h3 class="text-success" style="margin-bottom: 50x">
  		Panel de Control.
  	</h3>
		{if {$comentarios_pendientes > 0}}
			<div>
				<br>
  				{if {$comentarios_pendientes == 1}}
  					<center>
  					<p style="background-color:#F9F26B">
  					<font size=4 background-color: yellow> *
  					<font size=4 color="red" > {$comentarios_pendientes} </font> 
  					comentario pendiente por revisar. 
  					<a href='{site_url('admon_comentarios_c/comen_sitio')}'> Ir a comentarios </a></font>
  					</p>
  					</center>
				{else}
  					<center>
  					<p style="background-color:#F9F26B">
  					<font size=4> *
  					<font size=4 color="red" > {$comentarios_pendientes} </font> 
  					comentarios pendientes por revisar. 
  					<a href='{site_url('admon_comentarios_c/comen_sitio')}'> Ir a comentarios </a></font>
  					</p>
  					</center>
				{/if}
					
			</div>
		{else}
			<div>
				<br>
  					<center><font size=4 > No hay comentarios pendientes por revisar </font></center>
			</div>
		{/if}
		
	<div>
		
		<h4 class="text-success" >Tesis Registradas:  <font color="red" >{$conteo_tesis}   
		<a class="btn btn-success"  href='{site_url('catalogos/tesis/registrar')}'>+ Tesis</a>
		</font></h4>
	
	<br>
	
	<Table class="table table-hover table-bordered" >
		<tr>
		<th><h5 class="text-success" >5 tesis más descargadas en el mes</h5></th>
		</tr>
		
		<tr>
			<td style="text-align: center;"><strong>Nombre de tesis</strong></td>
			<td style="text-align: center;"><strong>Descargas</strong></td>
		</tr>
		{foreach from=$estadisticasPorTesis item=usar name=foo}
			{if $smarty.foreach.foo.index % 2 == 0}
                 <tr class="success">
              {else}
                 <tr>
              {/if}
			<td>	{$usar.tes_nombre} </td>
			<td><strong>	{$usar.CANTIDAD} </strong></td>
		  </tr>				
		{/foreach}
</table>		
<a class="btn btn-success"  href='{site_url('reportes/tesis_c/descargas_por_tesis/1')}'>Ver reporte de descargas</a>
	</div>	
	

	<br>
	<div>
	<h4 class="text-success" >Usuarios Registrados: <font color="red" >{$conteo_usuarios}</font></h4>
	
	<Table class="table table-hover table-bordered" >
		<tr> 
			<th><h5 class="text-success" > 5 Últimos usuarios registrados </h5></th>
		</tr>
		<tr>
			<td style="text-align: center;"><strong>Nombre</strong></td>
			<td style="text-align: center;"><strong>Apellidos</strong></td>
			<td style="text-align: center;"><strong>Correo</strong></td>
			<td style="text-align: center;"><strong>Estado</strong></td>
			<td style="text-align: center;"><strong>Registro</strong></td>
			<td style="text-align: center;"><strong>Acción</strong></td>
		</tr>
		 {foreach from=$users item=user name=foo}
		  {if $smarty.foreach.foo.index % 2 == 0}
                 <tr class="success">
              {else}
                 <tr>
              {/if}
				<td>{$user.first_name}</td>
				<td>{$user.last_name}</td>
				<td>{$user.email}</td>
				<td>{ter(($user.active), lang('index_active_link'), lang('index_inactive_link'))}</td>
				<td style="width:140px">{$user.fecha_registro}</td>
				<td>{ter( ($user.active), anchor("auth/deactivate/"|cat:$user->id, "Desactivar"), anchor("auth/activate/"|cat: $user->id, "Activar"))}</td>
			</tr>
		{/foreach}
	</table>
	<a class="btn btn-success"  href='{site_url('auth/index')}'>Ver todos los usuarios</a>
	</div>
	
	
</div>
		
		<div class="span3 offset1">
		<div class="thumbnail" style="margin-bottom: 0px">
			<div class="row-fluid">
				<div class="span10 offset1">
					
					<p class="text-center">
						<script type="text/javascript" src="http://jf.revolvermaps.com/2/1.js?i=5i4pfadzzu9&amp;s=220&amp;m=0&amp;v=true&amp;r=false&amp;b=000000&amp;n=true&amp;c=ff0000" async="async"></script>
					<!--<img class="img-rounded" src="{base_url()}themes/default/img/Mundo.png"> -->
					</p>
					{*
					<p class="text-center">
					<strong>
					Visitas: {$visitas_analitycs}
					</strong>
					</p>
					*}
                                      
				</div>
			</div>
			<div>
		<center>
		<p style="background-color:#088A29">
            <font size=4 color="white" >
            	Menú
            </font>
         </p>
            </center>
	<h4 class="text-success" style="margin-bottom: 20px">Catálogos</h4>
	<ul>
		<li>
     		<a href='{site_url('catalogos/especies_c')}'><i class='icon-list-alt'></i> Especies</a>
        </li>
		<li>                             
        	 <a href='{site_url('catalogos/disciplina_estudio_c')}'><i class='icon-list-alt'></i> Disciplinas de estudio</a>
        </li>
        <li>
             <a href='{site_url('catalogos/instituciones_c')}'><i class='icon-list-alt'></i> Instituciones</a>
        </li>
        <li>
        	<a href='{base_url()}index.php/catalogos/condiciones_sitio_c'><i class='icon-list-alt'></i> Condiciones de sitio</a>
        </li>
		</ul>
		<h4 class="text-success" style="margin-bottom: 20px">Reportes</h4>
									<ul>
										 
										<li>
											<a href='{site_url('reportes/disciplinas_estudio')}'><i class='icon-list-alt'></i> Disciplinas de estudio</a>
										</li>
										<li>
											<a href='{site_url('reportes/tesis_c/especiesSubespecies_por_tesis/1')}'><i class='icon-list-alt'></i> Especies y subespecies </a>
										</li>
										<li>
											<a href='{site_url('reportes/tesis_c/visitas_por_tesis/1')}'><i class='icon-list-alt'></i> Visitas por tesis</a>
										</li>
										<li>
											<a href='{site_url('reportes/tesis_c/descargas_por_tesis/1')}'><i class='icon-list-alt'></i> Descargas por tesis</a>
										</li>

										<li>
											<a href='{site_url('reportes/usuarios_c/')}'><i class='icon-list-alt'></i> Grado académico y género de usuarios </a>
										</li>
									</ul>
	</div>
			
		
</div>			

{else} <!-- Punto sin retorno -->

	<br>
	
	
	<div class="span7">
		<a class="brand" href='{base_url()}index.php/home' style="margin-bottom: 0px" >
        <img src='{base_url()}themes/default/img/Prima-T-Bienvenido.png'>
		</a>
        <font face="times">
  		<h3 class="text-success" style="margin-bottom: 20px">
  			Prima-T es una plataforma de búsqueda y consulta de tesis de primates.
  		</h3>
  
   		<h4 class="text-success" style="margin-bottom: 30px">
  			<p align="justify">
				<em>
				En este foro de comunicación pretendemos abrir una nueva ventana a la literatura especializada 
				en formato de tesis. Desde esta plataforma se ponen a su disposición los documentos de cualquier grado 
				(Licenciatura, Maestría, Doctorado, etc.) que se hubieran obtenido desde el origen de la primatología Mexicana hasta la fecha. Por supuesto esta 
				“biblioteca virtual” será cada día mejor conforme los autores de años previos y actuales de estas tesis, 
				contribuyan para que este acervo continúe complementándose.
				</em><br>
			  <p align="right">
			  <a class="btn btn-info btn-mini" href='{site_url('about_c/somos')}' >Mensaje de bienvenida</a>
			  </p>
  			</p>
  		</h4>
		</font>
  	 
	     <div id="myCarousel" class="carousel slide">
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>
                <div class="carousel-inner">
                <div class="active item">
                    <img style="width:100%"  src="{base_url()}themes/default/img/mono1.jpg" alt>
                    <div class="carousel-caption">
                        <h4><em>Alouatta palliata ssp. mexicana</em></h4>
						<h4>(Nombres comunes: mono aullador de manto, saraguato pardo)</h4>
						<font color="white">
						Son primates robustos y de gran talla, los adultos suelen medir entre 99 y 125 centímetros, 
						y la cola supera la longitud promedio del cuerpo. Los machos pesan entre 4.5 y 9.8 kilogramos 
						y las hembras entre 3.1 y 7.6 kilogramos (Rowe, 1996). Su pelaje suele ser denso y de un color 
						característico, dorado en los flancos y región de las axilas, y áreas sin pigmento en manos, patas y cola.
						<a class="btn btn-info btn-mini" href='{site_url('leer_mas_c/texto1')}' >Leer más</a>
						</font>
                    </div>
                </div>
                <div class="item">
                    <img style="width:100%" src="{base_url()}themes/default/img/mono2.jpg" alt>
                    <div class="carousel-caption">
                        <h4><em>Alouatta pigra</em></h4>
						<h4>(Nombres comunes: mono aullador negro, saraguato negro)</h4>
						<font color="white">
						En el pasado, <em>A. pigra</em> era considerada una subespecie de <em>A. palliata</em> (Hall y Kelson, 1959; Leopold, 1959). 
						Hoy en día se le reconoce como una especie distinta dadas sus características genéticas, conductuales y anatómicas 
						(Cortés-Ortiz et al., 2003).
						<a class="btn btn-info btn-mini" href='{site_url('leer_mas_c/texto2')}' >Leer más</a> 
						</font>
                    </div>
                </div>
                <div class="item">
                    <img style="width:100%"  src="{base_url()}themes/default/img/mono3.jpg" alt>
                    <div class="carousel-caption">
                        <h4><em>Ateles geoffroyi ssp. vellerosus</em></h4>
						<h4><em>Ateles geoffroyi ssp. yucatanensis</em></h4>
						<h4>(Nombres comunes: mono araña, chango)</h4>
                        <font color="white">
                        <em>Ateles</em> es considerado como uno de los primates más grandes del Nuevo Mundo, 
                        con un rango de peso que oscila entre cuatro y siete kilogramos, dependiendo de la especie (Hershkovitz, 1972). 
                        A diferencia de otros atelinos, <em>Ateles geoffroyi</em> posee un cuerpo largo y delgado con un aspecto muy peculiar.
                        <a class="btn btn-info btn-mini" href='{site_url('leer_mas_c/texto3')}' >Leer más</a>
                        </font>
                     </div>
                </div>
            </div>
            <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
            <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
        </div>
    </div>
	{if !($this->ion_auth->logged_in())}
	<div class="span3 offset1" align = "center">
	
	<img src='{base_url()}themes/default/img/publicidad.png'>
	 
	<a class="btn btn-success" href='{site_url('auth/create_user')}' >Registrate Ahora</a>
	
	</div>
	{/if}
	
	<div class="span3 offset1">
		<div class="thumbnail" style="margin-bottom: 0px">
			<div class="row-fluid">
				<div class="span10 offset1">
					
					<p class="text-center">
					<script type="text/javascript" src="http://jf.revolvermaps.com/2/1.js?i=5i4pfadzzu9&amp;s=220&amp;m=0&amp;v=true&amp;r=false&amp;b=000000&amp;n=true&amp;c=ff0000" async="async"></script>
					</p>
					{*
					<p class="text-center">
					<strong>
					Visitas: {$visitas_analitycs}
					</strong>
					</p>
                    *}               
				</div>
			</div>
		</div>
	
	<br>
	{if $comentarios_sitio}
		<div id="Comentarios2">
			<h4 class="text-success">Comentarios recientes al sitio:</h4>        	                         
			<div id="myCarousel2" class="carousel slide">
				<ol class="carousel-indicators">
					<li data-target="#myCarousel2" data-slide-to="0" class="active"></li>
					<li data-target="#myCarousel2" data-slide-to="1"></li>
					<li data-target="#myCarousel2" data-slide-to="2"></li>
					<li data-target="#myCarousel2" data-slide-to="3"></li>
					<li data-target="#myCarousel2" data-slide-to="4"></li>
				</ol>	
            <div class="carousel-inner">
            {foreach from=$comentarios_sitio item=comsit name=foo}
                {if $smarty.foreach.foo.index < 5}
                	{if $smarty.foreach.foo.index == 0}
                		<div class="active item">
                	{else}
                		<div class="item">
                	{/if}
                 <img style="width:100%"  src="{base_url()}themes/default/img/comentarios.png" alt>
                 <br>
                   <center><h3>{$comsit.com_comentario}</h3> </Center>
                   {$nombre = ucwords($comsit.username)}
                   <Strong>Autor: </strong><em>{$nombre}</em>
			 	</div>
			 {/if}	
   			{/foreach}
   			
   			</div>
            <a class="carousel-control left"  href="#myCarousel2" data-slide="prev">&lsaquo;</a>
            <a class="carousel-control right" href="#myCarousel2" data-slide="next">&rsaquo;</a>
        </div>
        
        <center>
        <a class="btn btn-success" href='{site_url('about_c/comentarios_sitio')}' >   Ver todos los comentarios </a> 
        </center>
        {else}
        <!-- <strong><center>No hay comentarios por mostrar.</center></strong> -->
        {/if}
   
   </div>
	{/if}
</div>
