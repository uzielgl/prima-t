                        <div class="tab-pane active" id="especies">
                            <ul class="thumbnails">
                                <li class="span12">
                                    <div class="thumbnail">
                                        <div class="row-fluid">
                                            <div class="row-fluid">
                                                <div class="span8 offset2" style=" margin-bottom: 10px">
                                                    <a id="nuevaEspecie" type="button" class="btn btn-success pull-right"><i class="icon-plus icon-white"></i> Nueva Especie</a>
                                                </div>
                                            </div>
                                            <div class="row-fluid">
                                                <div class="span8 offset2" style="overflow: scroll; max-height: 250px">
                                                    <table class="table table-hover table-bordered" style="position: static">
                                                        <thead >
                                                          <tr >
                                                            <th>Nombre Científico <a class="pull-right" href="#"><i class="icon-tasks"></i> </a></th>
                                                            <th>Nombre Común <a class="pull-right" href="#"><i class="icon-tasks"></i> </a></th>
                                                            <th>Selección</th>
                                                            <th>Edición</th>
                                                          </tr>
                                                        </thead>
                                                        <tr class="success" >
                                                            <td>{$nombre_cientifico}</td>
                                                            <td>{$nombre_comun}</td>
                                                            <td style="text-align: center;" >
                                                                <input style="margin-left: auto; margin-right: auto" type="checkbox">
                                                            </td>
                                                            <td style="text-align: center;" >
                                                                <a style="margin-left: auto; margin-right: auto" href="#"><i  class="icon-pencil"></i> </a>
                                                            </td>
                                                        </tr>
                                                        
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="row-fluid">
                                                <div class="span2 offset8" >
                                                    <button type="submit" class="btn btn-danger pull-right"><i class="icon-remove icon-white"></i> Eliminar</button>
                                                </div>
                                            </div>
                                        </div>
                                        <ul id="datosEspecie" class="thumbnails" style="margin-top: 10px; display: none">
                                            <li class="span8 offset2">
                                                <div class="thumbnail">
                                                    <div class="row-fluid">
                                                        <div class="row-fluid">
                                                            <div class="span12" style="margin-top: -13px">
                                                                <span class="label  pull-left">Datos de especie</span>  
                                                            </div>
                                                        </div>
                                                        <div class="row-fluid">
                                                            <div class="span4" style="text-align: right; margin-right: 20px">
                                                                Nombre científico:
                                                            </div>
                                                            <div class="span6" style="text-align: left; margin-top: -5px">
                                                                <input class="input-xlarge" type="text" placeholder="Nombre científico">
                                                            </div>
                                                        </div>
                                                        <div class="row-fluid">
                                                            <div class="span4" style="text-align: right; margin-right: 20px">
                                                                Nombre común:
                                                            </div>
                                                            <div class="span6" style="text-align: left; margin-top: -5px">
                                                                <input class="input-xlarge" type="text" placeholder="Nombre común">
                                                            </div>
                                                        </div>
                                                        <div class="row-fluid">
                                                            <div class="span4" style="text-align: right; margin-right: 20px">
                                                                Hábitat:
                                                            </div>
                                                            <div class="span6" style="text-align: left; margin-top: -5px">
                                                                <input class="input-xlarge" type="text" placeholder="Hábitat">
                                                            </div>
                                                        </div>
                                                        <div class="span2 offset5" style="margin-top: 15px">
                                                            <button type="submit" class="btn btn-success"><i class="icon-ok icon-white"></i> Guardar</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                          </div>
                        