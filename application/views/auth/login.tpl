<div class="btn-group pull-right">
	<a data-toggle="dropdown" role="button" class="btn btn-success"><i class="icon-user icon-white"></i> Iniciar sesión</a>
	<a data-toggle="dropdown" role="button" class="btn btn-success dropdown-toggle"><span class="caret"></span></a>
	<ul class="dropdown-menu">
		<form style="margin: 15px;" method="post" class="frm-login">
			<div class="control-group">
		      <div class="controls">
					<div class="input-prepend">
						<span class="add-on"><i class="icon icon-user"></i></span>
						<input class="input-medium" id="prependedInput" name="identity" type="text" placeholder="Usuario@dominio">
					</div>
		      </div>
		    </div>
		    <div class="control-group">
		      <div class="controls">
					<div class="input-prepend">
						<span class="add-on"><i class="icon icon-key"></i></span>
						<input class="input-medium" id="prependedInput" name="password" type="password" placeholder="Contraseña">
					</div>
					<label class="checkbox">
		                <input type="checkbox" name="remember"> Recordar
		            </label>
		      </div>
		    </div>
		    <div class="control-group">
		        <div class="controls">
					
		            <button class="btn btn-block" type="submit"><i class="icon-play icon"></i> Iniciar Sesión</button>
		        </div>
		    </div>
		</form>
		<li><a href="{site_url("auth/create_user")}"><i class="icon-plus"></i> Registrarse</a>
		<a href="forgot_password"><i class="icon-refresh"></i> Recuperar Contraseña</a>
		</li>
		                                 
	</ul>
</div>