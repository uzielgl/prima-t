<h3 class="text-success">{lang('create_user_heading')}</h1>
    
<!-- <p>{lang('create_user_subheading')}</p> -->

<div id="infoMessage">{$message}</div>

{form_open("auth/create_user")}

    
    <div class="thumbnail form-horizontal">
        <div style="margin-top: -13px" class="span12">
            <span class="label pull-left">Datos de Acceso</span>  
        </div>
        
        <div class="row-fluid">
            
            <!-- Columna 1 -->
            <div class="span6">
				<div class="clearfix" ></div>
                <!-- Text input-->
                <div class="control-group {cls_error("email")}">
                  <label class="control-label" for="textinput">Correo electrónico</label>
                  <div class="controls">
                    {form_input($email)}
                    {help_inline(form_error_msg("email", " "))}
                  </div>
                </div>
                
                
                <div class="control-group {cls_error("password")}">
                  <label class="control-label" for="textinput">Contraseña</label>
                  <div class="controls">
                    {form_input($password)}
                    {help_inline(form_error_msg("password", " "))}
                  </div>
                </div>
                
                
                    
            </div>
            
            
            <!-- Columna 2 -->
            <div class="span6">
                               
                  <!-- Text input-->
                <div class="control-group {cls_error("email_confirm")}">
                  <label class="control-label" for="textinput">Confirmar correo</label>
                  <div class="controls">
                    {form_input($email_confirm)}
                    {help_inline(form_error_msg("email_confirm", " "))}
                  </div>
                </div>
                
                <div class="control-group {cls_error("password_confirm")}">
                  <label class="control-label" for="textinput">Confirmar contraseña</label>
                  <div class="controls">
                    {form_input($password_confirm)}
                    {help_inline(form_error_msg("password_confirm", " "))}
                  </div>
                </div>
                
                
                
            </div>
            
        </div>
    </div> 
    
    
   <div class="clearfix" style="margin:20px; "></div>
   

    
    <div class="thumbnail form-horizontal">
        <div style="margin-top: -13px" class="span12">
            <span class="label pull-left">Datos Personales</span>  
        </div>
        
        <div class="row-fluid">
            
            <!-- Columna 1 -->
            <div class="span6">
				<div class="clearfix" ></div>
                <!-- Text input-->
                <div class="control-group {cls_error("first_name")}">
                  <label class="control-label" for="textinput">Nombre(s)</label>
                  <div class="controls">
                    {form_input($first_name)}
                    {help_inline(form_error_msg("first_name", " "))}
                  </div>
                </div>
                
                
                <div class="control-group {cls_error("grado_academico")}">
                  <label class="control-label" for="textinput">Grado Académico</label>
                  <div class="controls">
                    {form_dropdown($grado_academico[0], $grado_academico[1], $grado_academico[2])}
                    {help_inline(form_error_msg("grado_academico", " "))}
                  </div>
                </div>
                <div class="control-group {cls_error("fecha_nacimiento")}">
                  <label class="control-label" for="textinput">Fecha de nacimiento</label>
                  <div class="controls">
                    {form_input($fecha_nacimiento)}
                    {help_inline(form_error_msg("fecha_nacimiento", " "))}
                  </div>
                </div>
                <div class="control-group {cls_error("pais")}">
                  <label class="control-label" for="textinput">País</label>
                  <div class="controls">
                    {form_dropdown($pais[0], $pais[1], $pais[2])}
                    {help_inline(form_error_msg("pais", " "))}
                  </div>
                </div>
                <div class="control-group {cls_error("facebook")}">
                  <label class="control-label" for="textinput">Facebook</label>
                  <div class="controls">
                    {form_input($facebook)}
                    {help_inline(form_error_msg("facebook", " "))}
                  </div>
                </div>
                
                
                    
            </div>
            
            
            <!-- Columna 2 -->
            <div class="span6">
                               
                  <!-- Text input-->
                <div class="control-group {cls_error("last_name")}">
                  <label class="control-label" for="textinput">Apellidos</label>
                  <div class="controls">
                    {form_input($last_name)}
                    {help_inline(form_error_msg("last_name", " "))}
                  </div>
                </div>
                
                <div class="control-group {cls_error("ocupacion")}">
                  <label class="control-label" for="textinput">Ocupación</label>
                  <div class="controls">
                    {form_input($ocupacion)}
                    {help_inline(form_error_msg("ocupacion", " "))}
                  </div>
                </div>
                 <div class="control-group {cls_error("genero")}">
                  <label class="control-label" for="textinput">Género</label>
                  <div class="controls">
                    {form_dropdown($genero[0], $genero[1], $genero[2])}
                    {help_inline(form_error_msg("genero", " "))}
                  </div>
                </div>
                <div class="control-group {cls_error("ciudad")}">
                  <label class="control-label" for="textinput">Ciudad</label>
                  <div class="controls">
                    {form_input($ciudad)}
                    {help_inline(form_error_msg("ciudad", " "))}
                  </div>
                </div>
                
                <div class="control-group {cls_error("ciudad")}">
                  <label class="control-label" for="textinput"></label>
                  <div class="controls">
					<button type="submit" class="btn btn-success"><i class="icon-ok icon-white"></i> Registrarme</button>
                  </div>
                </div>
                
                
                
            </div>
            
        </div>
    </div> 
     
      <!--  
    

      <p>
            {lang('create_user_fname_label', 'first_name')} <br />
            {form_input($first_name)}
      </p>

      <p>
            {lang('create_user_lname_label', 'first_name')} <br />
            {form_input($last_name)}
      </p>

      <p>
            {lang('create_user_company_label', 'company')} <br />
            {form_input($company)}
      </p>

      <p>
            {lang('create_user_email_label', 'email')} <br />
            {form_input($email)}
      </p>

      <p>
            {lang('create_user_phone_label', 'phone')} <br />
            {form_input($phone)}
      </p>

      <p>
            {lang('create_user_password_label', 'password')} <br />
            {form_input($password)}
      </p>

      <p>
            {lang('create_user_password_confirm_label', 'password_confirm')} <br />
            {form_input($password_confirm)}
      </p>

       -->

	
{form_close()}
