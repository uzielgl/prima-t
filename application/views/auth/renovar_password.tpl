<div class="container">
	<div class="row">
		<div class="span offset3">
			{form_open("auth/reset_password/$code")}
						
				<div class="control-group">
					<div class="controls">
						<h2>Nueva contraseña</h2>
					</div>
				</div>
				
				
				<div class="control-group  {cls_error("new")}">
		      		<div class="controls">
						<div class="input-prepend">
							<span class="add-on"><i class="icon icon-key"></i></span>
							<input class="input-medium" id="prependedInput" name="new" type="password" placeholder="Nueva contraseña:">
						</div>
						{help_inline(form_error_msg("new", " "))}
		      		</div>
		    	</div>
				    	
				<div class="control-group  {cls_error("new_confirm")}">
		      		<div class="controls">
						<div class="input-prepend">
							<span class="add-on"><i class="icon icon-key"></i></span>
							<input class="input-medium" id="prependedInput" name="new_confirm" type="password" placeholder="Confirmar contraseña">
						</div>
						{help_inline(form_error_msg("new_confirm", " "))}
					</div>
		    	</div>
		    	{form_input($user_id)}
				{form_hidden($csrf)}
				
				{if !validation_errors() && $message}
					<div class="control-group error">
						<div class="controls">
							{help_inline($message)}
						</div>
					</div>
				{/if}
				
				<div class="control-group">
		        <div class="controls">
					
		            <button class="btn" type="submit"><i class="icon-ok icon"></i> Aceptar</button>
		        </div>
		    </div>
			</form>
		</div>
	</div>
</div>