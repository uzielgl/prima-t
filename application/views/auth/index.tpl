<h3 class="text-success">{lang('index_heading')}</h3>

{if $users}
	
	
	
	
	<div id="infoMessage">{$message}</div>
	
	<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover">
		<tr>
			<th>{lang('index_fname_th')}</th>
			<th>{lang('index_lname_th')}</th>
			<th>{lang('index_email_th')}</th>
			<!-- <th>{lang('index_groups_th')}</th> -->
			<th>{lang('index_status_th')}</th>
			<th>Registro</th>
			<th>{lang('index_action_th')}</th>
			<!-- <th>{lang('index_action_th')}</th> --> 
		</tr>
		{foreach $users as $user}
			<tr>
				<td>{$user->first_name}</td>
				<td>{$user->last_name}</td>
				<td>{$user->email}</td>
				<!--
				<td>
					{foreach $user->groups as $group}
						{anchor("auth/edit_group/"|cat:$group->id, $group->name) }<br />
	                {/foreach}
				</td>-->
				<td>{ter( ($user->active), lang('index_active_link'), lang('index_inactive_link'))}</td>
				<td>{date('d/m/Y H-i:s', $user->created_on)}</td>
				<td>{ter( ($user->active), anchor("auth/deactivate/"|cat:$user->id, "Desactivar"), anchor("auth/activate/"|cat: $user->id, "Activar"))}</td>
				<!-- <td>{anchor("auth/edit_user/"|cat:$user->id, 'Edit') }</td> -->
			</tr>
		{/foreach}
	</table>
	
	<!-- <p>{anchor('auth/create_user', lang('index_create_user_link'))} | {anchor('auth/create_group', lang('index_create_group_link'))}</p> -->
	
{else}

	<h5><center>No se han encontrado usuarios</center></h5>
{/if}
