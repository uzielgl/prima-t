
<div class="container">

	<div class="row">
		
		<div class="span offset2">
			
			<form class="form-horizontal" method="post">
				
				<div class="control-group">
					<div class="controls">
						<h2>Iniciar sesión</h2>
					</div>
				</div>
				
				    	
				<div class="control-group  {cls_error("identity")}">
					<label class="control-label" for="inputEmail">Email</label>
					<div class="controls">
						<div class="input-prepend">
							<span class="add-on"><i class="icon icon-user"></i></span>
							<input class="input-medium" id="prependedInput" value="{set_value("identity")}" name="identity" type="text" placeholder="Usuario@dominio">
						</div>
						{help_inline( form_error("identity") )}
					</div>
				</div>
				
				<div class="control-group {cls_error("password")}">
					<label class="control-label" for="inputPassword">Password</label>
					<div class="controls">
						<div class="input-prepend">
							<span class="add-on"><i class="icon icon-key"></i></span>
							<input class="input-medium" id="prependedInput" name="password" type="password" placeholder="Contraseña">
						</div>
						{help_inline( form_error("password") )}
					</div>
				</div>
				
				
				{if !validation_errors() && $message}
					<div class="control-group info">
						<div class="controls">
							{help_inline($message)}
						</div>
					</div>
				{/if}
				
				<div class="control-group">
					<div class="controls">
						<label class="checkbox">
							<input type="checkbox" name="remember"> Recordar
						</label>
						<button class="btn " type="submit"><i class="icon-play icon"></i> Iniciar Sesión</button>
					</div>
				</div>
				
				<div class="control-group">
				    <div class="controls">
					    <a href="forgot_password"><i class="icon-refresh"></i> Recuperar Contraseña</a> <br>
					    <a href="{site_url("auth/create_user")}"><i class="icon-plus"></i> Registrarse</a>
				    </div>
				</div>
			
			
			</form>
		
		</div>
	
	</div>

</div>