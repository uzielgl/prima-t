


<div class="container">

	<div class="row">
		
		<div class="span offset2">
			
			<form class="form-horizontal" method="post">
				
				<div class="control-group">
					<div class="controls">
						<h2>Recuperar contraseña</h2>
					</div>
				</div>
				
				    	
				<div class="control-group  {cls_error("identity")}">
					<label class="control-label" for="inputEmail">Correo electrónico</label>
					<div class="controls">
						<div class="input-prepend">
							<span class="add-on"><i class="icon icon-user"></i></span>
							<input class="input-medium" id="prependedInput" value="{set_value("email")}" name="email" type="text" placeholder="Usuario@dominio">
						</div>
						{help_inline( form_error("email") )}
					</div>
				</div>
				
				{if !validation_errors() && $message}
					<div class="control-group error">
						<div class="controls">
							{help_inline($message)}
						</div>
					</div>
				{/if}
				
				<div class="control-group">
		        <div class="controls">
					
		            <button class="btn" type="submit"><i class="icon-refresh icon"></i> Recuperar</button>
		        </div>
		    </div>
				
			</form>
		
		</div>
	
	</div>

</div>