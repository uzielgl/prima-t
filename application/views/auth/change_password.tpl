
<div class="container">

	<div class="row">
		
		<div class="span offset3">

			{form_open("auth/change_password")}
			
				{if !validation_errors() && $message}
		          <div class="control-group error">
		            <div class="controls">
		              {help_inline($message)}
		            </div>
		          </div>
		        {/if}
			
				<div class="control-group">
					<label class="control-label" for="inputEmail">	<h2>{lang('change_password_heading')}</h2></label>
				</div>
			
				<div class="control-group  {cls_error("old")}">
					<label class="control-label" for="inputEmail">{lang('change_password_old_password_label', 'old_password')}</label>
					<div class="controls">
						<div class="input-prepend">
							<span class="add-on"><i class="icon icon-key"></i></span>
							{form_input($old_password)}
						</div>
						{help_inline( form_error("old") )}
					</div>
				</div>
				
				
				
				<div class="control-group  {cls_error("new")}">
					<label class="control-label" for="inputEmail">{sprintf(lang('change_password_new_password_label'), $min_password_length)}</label>
					<div class="controls">
						<div class="input-prepend">
							<span class="add-on"><i class="icon icon-key"></i></span>
							{form_input($new_password)}
						</div>
						{help_inline( form_error("new") ) }
					</div>
				</div>
				
				
				<div class="control-group  {cls_error("new_confirm")}">
					<label class="control-label" for="new_confirm">{lang('change_password_new_password_confirm_label', 'new_password_confirm')}</label>
					<div class="controls">
						<div class="input-prepend">
							<span class="add-on"><i class="icon icon-key"></i></span>
							{form_input($new_password_confirm)}
						</div>
						{help_inline( form_error("new_confirm") ) }
					</div>
				</div>
			
			
			
			      
			      {form_input($user_id)}
			      
			      <button class="btn btn-success">
			      	<i class="icon-white icon-ok"> </i>
			      	 Cambiar
			      </button>
			
			{form_close()}
			
		</div>
	</div>
</div>