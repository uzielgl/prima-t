{literal}
<style>
	.sub td{padding-right:0px;}
</style>
{/literal}

<div class="tab-pane active" id="disciplinas">
    <ul class="thumbnails">                    
        <li class="span12" style="margin-bottom: 0px">
            <div class="thumbnail">
                <div class="row-fluid">
                    <div class="span12" style="overflow-y: auto;">
                    <a id="nueva_disciplina" style="margin:5px 10px 10px 0px" href="#modal_agregar" data-toggle="modal" role="button" class="btn btn-success" > <i class="icon-plus icon-white"></i> Disciplina</a>
                        <table id="tabla_catalogos" class="table" style="position: static">
                            <thead>
                              <tr>
                                <th data-field="dis_estudio_nombre" data-type>
                                </th>
                                <th style="width: 100px" > </th> <!-- Agregar -->
                                <th style="width: 59px" > </th> <!-- Editar -->
                                <th style="width: 67px" > </th><!-- Eliminar -->
                              </tr>
                            </thead>
                            <tbody>
                            {foreach from=$disciplinas item=disciplina name=foo}
                                <tr id={$disciplina.dis_id_disciplina_estudio|string_format:"tr_%d"} class="success" >
                                    <td id={$disciplina.dis_id_disciplina_estudio|string_format:"dis_estudio_nombre_%d"} >
                                        <strong>{$disciplina.dis_estudio_nombre}</strong>
                                    </td>
                                    <td id={$disciplina.dis_id_disciplina_estudio|string_format:"td_agregar_subcatalogo_%d"} style="text-align: center;" >
                                        <a id={$disciplina.dis_id_disciplina_estudio|string_format:"a_agregar_subcatalogo_%d"} class="btn btn-success btn-mini" href="#modal_agregar_subdisciplinas" ><i class="icon-plus icon-white"></i> Subdisciplinas</a>
                                    </td>
                                    <td id={$disciplina.dis_id_disciplina_estudio|string_format:"td_editar_%d"} style="text-align: center;" >
                                        <a id={$disciplina.dis_id_disciplina_estudio|string_format:"a_editar_%d"} class="btn btn-warning btn-mini" href="#modal_editar" ><i class="icon-pencil icon-white"></i> Editar</a>
                                    </td>
                                    <td id={$disciplina.dis_id_disciplina_estudio|string_format:"td_eliminar_%d"} style="text-align: center;" >
                                        <a id={$disciplina.dis_id_disciplina_estudio|string_format:"a_eliminar_%d"} class="btn btn-danger btn-mini" href="#modal_eliminar" ><i class="icon-remove icon-white"></i> Eliminar</a>
                                    </td>
                                </tr>
                                <tr id={$disciplina.dis_id_disciplina_estudio|string_format:"tr_subcatalogos_%d"} >
                                    <td id={$disciplina.dis_id_disciplina_estudio|string_format:"td_subcatalogos_%d"} colspan="4" >
                                        <table id="tabla_subcatalogos_{$disciplina.dis_id_disciplina_estudio}" class="table table-hover tablesorter" style="position: static">
                                            
                                            <tbody>
                                            {foreach from=$disciplina.subdisciplinas item=subdisciplina name=foo2}
                                                <tr class="sub" id={$subdisciplina.sub_id_subdisciplina|string_format:"tr_subcatalogo_%d"}>
                                                    <td id={$subdisciplina.sub_id_subdisciplina|string_format:"sub_nombre_%d"} >
                                                        {$subdisciplina.sub_nombre}
                                                    </td>
                                                    <td id={$subdisciplina.sub_id_subdisciplina|string_format:"td_subcatalogo_editar_%d"} style="text-align: center;" width="70px" >
                                                        <a id={$subdisciplina.sub_id_subdisciplina|string_format:"a_editar_subcatalogo_%d"} class="btn btn-warning btn-mini" href="#modal_editar_subdisciplinas" ><i class="icon-pencil icon-white"></i> Editar</a>
                                                    </td>
                                                    <td id={$subdisciplina.sub_id_subdisciplina|string_format:"td_subcatalogo_eliminar_%d"} style="text-align: center;" width="70px" >
                                                        <a id={$subdisciplina.sub_id_subdisciplina|string_format:"a_eliminar_subcatalogo_%d"} class="btn btn-danger btn-mini" href="#modal_eliminar_subdisciplinas" ><i class="icon-remove icon-white"></i> Eliminar</a>
                                                    </td>
                                                </tr>
                                            {/foreach}
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            {/foreach}
                            </tbody>
                        </table>
                    </div>
                    {$paginacion}
                </div>
            </div>
        </li>
    </ul>
</div>