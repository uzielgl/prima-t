<div class="container-fluid" >
    <div class="row-fluid">
        <h3 class="text-success" style="text-align: left" >Administración de Catálogos</h3>
        <div class="row-fluid">
            <div class="span12">
                {if isset($vista)}
                <div class="tabbable"> <!-- Only required for left/right tabs -->
                    <!--<ul class="nav nav-tabs" style="margin-bottom: 0px">
                        {if $vista === "especies_c"}
                            <li class="active">
                                <a href="{base_url()}index.php/catalogos/especies_c" >Especies</a>
                            </li>
                        {else}
                            <li>
                                <a href="{base_url()}index.php/catalogos/especies_c" >Especies</a>
                            </li>
                        {/if}
                        {if $vista === "disciplinas_c"}
                            <li class="active">
                                <a href="{base_url()}index.php/catalogos/disciplina_estudio_c" >Disciplinas</a>
                            </li>
                        {else}
                            <li>
                                <a href="{base_url()}index.php/catalogos/disciplina_estudio_c" >Disciplinas</a>
                            </li>
                        {/if}
                        {if $vista === "instituciones_c"}
                            <li class="active">
                                <a href="{base_url()}index.php/catalogos/instituciones_c" >Instituciones</a>
                            </li>
                        {else}
                            <li>
                                <a href="{base_url()}index.php/catalogos/instituciones_c" >Instituciones</a>
                            </li>
                        {/if}

                        {if $vista === "condiciones_c"}
                            <li class="active">
                                <a href="{base_url()}index.php/catalogos/condiciones_sitio_c" >Condiciones de estudio</a>
                            </li>
                        {else}
                            <li>
                                <a href="{base_url()}index.php/catalogos/condiciones_sitio_c" >Condiciones de estudio</a>
                            </li>
                        {/if}
                    </ul>-->
                    <div class="tab-content">
                        {if $vista === "especies_c"}
                            {include file="modals/modal_agregar_subespecies_c.tpl"}
                            {include file="modals/modal_editar_subespecies_c.tpl"}
                            {include file="modals/modal_eliminar_subespecies_c.tpl"}
                            {include file="modals/modal_agregar_especies_c.tpl"}
                            {include file="modals/modal_editar_especies_c.tpl"}
                        {elseif $vista === "disciplinas_c"}
                            {include file="modals/modal_agregar_disciplina_c.tpl"}
                            {include file="modals/modal_editar_disciplina_c.tpl"}
                            {include file="modals/modal_agregar_subdisciplinas_c.tpl"}
                            {include file="modals/modal_editar_subdisciplinas_c.tpl"}
                            {include file="modals/modal_eliminar_subdisciplinas_c.tpl"}
                        {elseif $vista === "instituciones_c"}
                            {include file="modals/modal_agregar_instituciones_c.tpl"}
                            {include file="modals/modal_editar_instituciones_c.tpl"}
                        {elseif $vista === "condiciones_c"}
                            {include file="modals/modal_agregar_condiciones_c.tpl"}
                            {include file="modals/modal_editar_condiciones_c.tpl"}
                        {/if}
                        {include file="modals/modal_catalogos.tpl"}
                        {include file="$vista.tpl"}
                    </div>
                </div>
                {/if}
            </div>
        </div>
    </div>
</div>
               
        
