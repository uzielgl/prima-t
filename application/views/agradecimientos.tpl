<div class="row-fluid">
	<br>
	<div class="span9">
	<h3 class="text-success" style="margin-bottom: 30px">Agradecimientos a colaboradores</h3>		
	</div>
	
	<div class="span10" >
		
		<div class="span6">
			<img src='{base_url()}themes/default/img/norma alicia.jpg'> </img>
		</div> 
		
		<div class="span6">
	  		<p class="lead" align="center">
		 		<font face="times">
		 	
		 	<br>Se agradece la colaboración y apoyo decidido de la <br>
		 	</p>
		 	<p class="lead" align="center"><strong>Biol. Norma Alicia Hernández Hernández</strong><br> 
		 	<em>(Instituto de Ecología AC)</em></center>
		 	</p> 
		 	<p class="lead" align="center">
		 	quien trabajó arduamente en la clasificación y 
		 	selección de material para digitalizar durante las primeras etapas para la materialización de esta idea.
		 	</p> 
		 	<br><p class="lead" align="center"><strong> Muchas gracias Norma.</strong> </p>
			</font>
	  		</p>
  		</div>
	</div>

</div>

<div class="row-fluid">
	<br>
	
	<div class="span10" > 
	
	<div class="span6">
		<img style="height: 370px; float:left; margin:0 110px;" src='{base_url()}themes/default/img/colaboradores/john.jpg'> </img>		
	</div>
	
	<div class="span6">
		
	  		<p class="lead" align="center">
		 		<font face="times">
		 	
		 	<br>Se agradece la colaboración del <br>
		 	</p>
		 	<p class="lead" align="center"><strong>MC John Aristizabal</strong><br> 
		 	<em>(Instituto de Ecología AC)</em></center>
		 	</p> 
		 	<p class="lead" align="center">
		 	quien con mucho inter&eacute;s y gran empeño apoyó en la recta final de selección  de archivos y montaje de los mismos, así como en la redacción cuidadosa de numerosas secciones dentro de la p&aacute;gina.
		 	</p> 
		 	<br><p class="lead" align="center"><strong> Mil gracias John!!</strong> </p>
			</font>
	  		</p>
		
	</div>
	
	</div>
	
	

</div>

<div class="row-fluid">
	<br>
	
	<div class="span10" > 
		
		<div class="span6">
			<img src='{base_url()}themes/default/img/colaboradores/uziel.jpg'> </img>
		</div>
		<div class="span6">
			
			<p class="lead" align="center">
	 		<font face="times">
	 	
		 	<br>Se agradece de manera importante la participación del <br>
		 	</p>
		 	<p class="lead" align="center"><strong><a href="mailto:uzielgl@gmail.com?subject=Desarrollo">MCA Uziel Garc&iacute;a L&oacute;pez</a></strong><br> 
		 	<em>(Laboratorio Nacional de Informática Avanzada)</em></center>
		 	</p> 
		 	<p class="lead" align="center">
			quien con enorme talento y mayor eficacia,  apoyó a resolver los detalles finales de este portal que será de gran utilidad a todos los interesados en la primatología.
		 	</p> 
		 	<br><p class="lead" align="center"><strong> Muchas gracias Uziel!</strong> </p>
			</font>
	  		</p>
		</div>
		
  		
	</div>

</div>