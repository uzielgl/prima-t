{literal}
<style>
	.sub td{padding-right:0px;}
</style>
{/literal}

<div class="tab-pane active" id="especies">
    <ul class="thumbnails">                    
        <li class="span12" style="margin-bottom: 0px">
            <div class="thumbnail">
                <div class="row-fluid">
                    <div class="span12" style="overflow-y: auto; ">
                        <a id="nueva_especie" style="margin:5px 10px 10px 0px" href="#modal_agregar" data-toggle="modal" role="button" class="btn btn-success" > <i class="icon-plus icon-white"></i> Especie</a>
                        <table id="tabla_catalogos" class="table tablesorter" style="position: static">
                            <thead>
                              <tr>
                                <th data-field="esp_nombre" data-type>
                                    <div class="filter">
                                        
                                    </div>
                                </th>
                                <th style="width: 100px" > </th> <!-- Agregar -->
                                <th style="width: 59px" > </th> <!-- Editar -->
                                <th style="width: 67px" > </th><!-- Eliminar -->
                              </tr>
                            </thead>
                            <tbody>
                            {foreach from=$especies item=especie name=foo}
                                <tr id={$especie.esp_id_especie|string_format:"tr_%d"} class="success" >
                                    <td id={$especie.esp_id_especie|string_format:"esp_nombre_%d"} >
                                        <strong>{$especie.esp_nombre}</strong>
                                    </td>
                                    <td id={$especie.esp_id_especie|string_format:"td_agregar_subcatalogo_%d"} style="text-align: center;min-width:0px;max-width: 70px">
                                        <a id={$especie.esp_id_especie|string_format:"a_agregar_subcatalogo_%d"} class="btn btn-success btn-mini" href="#modal_agregar_subespecies" ><i class="icon-plus icon-white"></i> Subespecies</a>
                                    </td>
                                    <td id={$especie.esp_id_especie|string_format:"td_editar_%d"} style="text-align: center;min-width:0px;max-width: 70px" >  
                                        <a id={$especie.esp_id_especie|string_format:"a_editar_%d"} class="btn btn-warning btn-mini" href="#modal_editar" ><i class="icon-pencil icon-white"></i> Editar</a>
                                    </td>
                                    <td id={$especie.esp_id_especie|string_format:"td_eliminar_%d"} style="text-align: center;min-width:0px;max-width: 70px" >
                                        <a id={$especie.esp_id_especie|string_format:"a_eliminar_%d"} class="btn btn-danger btn-mini" href="#modal_eliminar" ><i class="icon-remove icon-white"></i> Eliminar</a>
                                    </td>
                                </tr>
                                <tr id={$especie.esp_id_especie|string_format:"tr_subcatalogos_%d"} >
                                    <td id={$especie.esp_id_especie|string_format:"td_subcatalogos_%d"} colspan="4" >
                                        <table id="tabla_subcatalogos_{$especie.esp_id_especie}" class="table table-hover " style="position: static">
                                                                                        
                                            <tbody>
                                            {foreach from=$especie.subespecies item=subespecie name=foo2}
                                                <tr class="sub" id={$subespecie.sue_id_subespecie|string_format:"tr_subcatalogo_%d"}>
                                                    <td class="nombre" id={$subespecie.sue_id_subespecie|string_format:"sue_nombre_%d"} >
                                                        {$subespecie.sue_nombre}
                                                    </td>
                                                    <td id={$subespecie.sue_id_subespecie|string_format:"td_subcatalogo_editar_%d"} style="text-align: center;" width="70px" >
                                                        <a id={$subespecie.sue_id_subespecie|string_format:"a_editar_subcatalogo_%d"} class="btn btn-warning btn-mini" href="#modal_editar_subespecies" ><i class="icon-pencil icon-white"></i> Editar</a>
                                                    </td>
                                                    <td id={$subespecie.sue_id_subespecie|string_format:"td_subespecie_eliminar_%d"} style="text-align: center;" width="70px" >
                                                        <a id={$subespecie.sue_id_subespecie|string_format:"a_eliminar_subcatalogo_%d"} class="btn btn-danger btn-mini" href="#modal_eliminar_subespecies" ><i class="icon-remove icon-white"></i> Eliminar</a>
                                                    </td>
                                                </tr>
                                            {/foreach}
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            {/foreach}
                            </tbody>
                        </table>
                    </div>
                    {$paginacion}
                </div>
            </div>
        </li>
    </ul>
</div>