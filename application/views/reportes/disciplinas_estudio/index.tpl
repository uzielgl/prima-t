<h3 style="text-align: left" class="pull-left text-success">Reporte de tesis por disciplinas y subdisciplinas</h3>
<div class="pull-right">
	<a href="{site_url('reportes/disciplinas_estudio/csv')}" class="btn"> <i class="icon-share"> </i> Exportar a CSV</a>	 
</div>
<div class="span8">
	<p class="container lead">
		Este reporte muestra un listado de las diferentes disciplinas de estudio y la cantidad de tesis registradas. Además de un desglose de las subdisciplinas correspondientes.
	</p>	
</div>
<div class="clearfix"></div>
{foreach $disciplinas as $dis}
	<table align="center" style="max-width: 90% ;min-width:20%; alignment-baseline:center" class="table table-striped table-bordered table-hover table-condensed container">
		<thead>
			<tr>
				<th style="text-align: center; background-color: #DFF0D8; color: #A9302A">DISCIPLINA / SUBDISCIPLINA</th>
				<th style="background-color: #DFF0D8; color: #A9302A">CANTIDAD DE TESIS</th>
			</tr>
			<tr>
				<th>{$dis->dis_estudio_nombre}</td>
				<th>{$dis->cantidad}</td>
			</tr>			
		</thead>

		<tbody>
			{foreach $this->_get_subdisciplinas_con_cantidad( $dis->dis_id_disciplina_estudio) as $sub}
				<tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;{$sub->subcategoria}
					</td>
					<td>{$sub->cantidad}
					</td>
				</tr>
			{/foreach}
		</tbody>
		
	</table>
{/foreach}
