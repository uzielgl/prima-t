<h3 style="text-align: left" class="pull-left text-success">Reporte de descargas por tesis</h3>
<form method="post" class="form-inline">
	<div class="pull-right">
		<!-- <a href="{site_url('reportes/tesis_c/descargas_por_tesis/2')}" class="btn"> <i class="icon-share"> </i> Exportar a CSV</a> -->
		<button  class="btn" name='accion' value="2"> <i class="icon-share"> </i> Exportar a CSV</button>
	</div>
	
	<div class="clearfix"></div>
	
	
	Desde: <input id="from" type="text" value="{set_value('from')}" class="input-small" name='from'> Hasta: <input id="to" type="text" class="input-small" name="to" value="{set_value('to')}">
	<button name="accion" value="1" type="submit" class="btn"><i class="icon icon-filter"> </i>Filtrar</button>
	<button type="button" id='reset' class="btn"><i class="icon icon-trash"> </i> Limpiar</button>
</form>



	<table class="table table-striped table-bordered table-hover table-condensed">
		<thead>
			<tr>
				<th>TITULO</th>
				<th>AUTORES</th>
				<th>DIRECTORES</th>
				<th>GRADOS</th>
				<th>DESCARGAS</th>
				
			</tr>			
		</thead>

		<tbody>
			{foreach $aTesis as $tesis}
				<tr>
					<td>{$tesis["TITULO"]}</td>
					<td>{$tesis["AUTORES"]|replace:",":",<br/><br/>"}</td>
					<td>{$tesis["DIRECTORES"]|replace:",":",<br/><br/>"}</td>
					<td>{$tesis["GRADOS"]}</td>
					<td>{$tesis["DESCARGAS"]}</td>
				</tr>
			{/foreach}
		</tbody>
		
	</table>

