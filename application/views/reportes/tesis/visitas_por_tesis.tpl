

<div class="span10"> 
	<p align="justify">
		<h3 style="text-align: left" class="text-success">Reporte de visitas por tesis</h3>
	</p>
	<p align="justify">
		<font face="MS Sans Serif" color="Green"> 
			<br>
			<h4>Los reportes de visitas del sitio se encuentran en Google Analytics.</h4>
			<ul>
				<li>
					Para acceder, por favor ingresar a la cuenta del sitio en <a target="_blank" href="http://www.google.com/analytics/"> Google Analytics </a>
					con un usuario de google que tenga permisos de seguimiento del sitio.  
				</li>
				<li>Posteriormente deberá dar click en la opción de Todos los datos del sitio web dentro de la carpeta de Inecol->primates. 
					menú "Herramientas y Análisis" y elegir la opción "Google Analytics".
				</li>
				<li>
					En la parte izquierda aparecerá la opción de "Público" con sus principales opciones, y "Visión general" para un resumén de la actividad del sitio.
				</li>
			</ul> 				
		</font>
	</p>
</div>



{*

<h3 style="text-align: left" class="pull-left text-success">Reporte de visitas por tesis</h3>
<div class="pull-right">
	<a href="{site_url('reportes/tesis_c/visitas_por_tesis/2')}" class="btn"> <i class="icon-share"> </i> Exportar a CSV</a>	 
</div>


<div class="clearfix"></div>

	<table align="center" style="max-width: 95% ;min-width:20%; alignment-baseline:center" class="table table-striped table-bordered table-hover table-condensed">
		<thead>
			<tr>
				<th style="background-color: #DFF0D8; color: #A9302A">TITULO</th>
				<th style="background-color: #DFF0D8; color: #A9302A">AUTORES</th>
				<th style="background-color: #DFF0D8; color: #A9302A">DIRECTORES</th>
				<th style="background-color: #DFF0D8; color: #A9302A">GRADOS</th>
				<th style="background-color: #DFF0D8; color: #A9302A">DESCARGAS</th>
				<th style="background-color: #DFF0D8; color: #A9302A">VISITAS</th>
			</tr>			
		</thead>

		<tbody>
			{foreach $aTesis as $tesis}
				<tr>
					<td>{$tesis["TITULO"]}</td>
					<td>{$tesis["AUTORES"]|replace:",":",<br/><br/>"}</td>
					<td>{$tesis["DIRECTORES"]|replace:",":",<br/><br/>"}</td>
					<td>{$tesis["GRADOS"]}</td>
					<td>{$tesis["DESCARGAS"]}</td>
					<td>{$tesis["VISITAS"]}</td>
				</tr>
			{/foreach}
		</tbody>
		
	</table>

*}