<p>
<h3 style="text-align: left" class="pull-left text-success">Reporte de tesis por especies y subespecies</h3>
</p>
<div class="pull-right">
	<a href="{site_url('reportes/tesis_c/especiesSubespecies_por_tesis/2')}" class="btn"> <i class="icon-share"> </i> Exportar a CSV</a>	 
</div>
<div class="clearfix"></div>
{foreach $especies as $esp}
	<table align="center" style="max-width: 90% ;min-width:20%; alignment-baseline:center" class="table table-striped table-bordered table-hover table-condensed container">
		<thead>
			<tr>
				<th style="text-align: center; background-color: #DFF0D8; color: #A9302A">ESPECIE / SUBESPECIE</th>
				<th style="background-color: #DFF0D8; color: #A9302A">CANTIDAD DE TESIS</th>
			</tr>
			<tr>
				<th>{$esp->esp_nombre}</td>
				<th>{$esp->cantidad}</td>
			</tr>			
		</thead>

		<tbody>
			{foreach $this->getSubespeciesPorTesis( $esp->esp_id_especie) as $sub}
				<tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;{$sub->subespecie}
					</td>
					<td>{$sub->cantidad}
					</td>
				</tr>
			{/foreach}
		</tbody>
		
	</table>
{/foreach}
