<h3 style="text-align: left" class="pull-left text-success">Reporte de grado acádemico y género por usuarios</h3>
<div class="pull-right">
	<a href="{site_url('reportes/usuarios_c/grado_academico_genero/2')}" class="btn"> <i class="icon-share"> </i> Exportar a CSV</a>	 
</div>


<div class="clearfix"></div>

	<table class="table table-striped table-bordered table-hover table-condensed">
		<thead>
			<tr>
				<th>NOMBRE</th>
				<th>USUARIO</th>
				<th>GENERO</th>
				<th>NACIONALIDAD</th>
				<th>GRADO ACÁDEMICO</th>
			
			</tr>			
		</thead>

		<tbody>
			{foreach $aUsuarios as $usuario}
				<tr>
					<td>{$usuario["NOMBRE"]}</td>
					<td>{$usuario["APELLIDOS"]}</td>
					<td>{$usuario["GENERO"]}</td>
					<td>{$usuario["NACIONALIDAD"]}</td>
					<td>{$usuario["GRADO_ACADEMICO"]}</td>
					
				</tr>
			{/foreach}
		</tbody>
		
	</table>

