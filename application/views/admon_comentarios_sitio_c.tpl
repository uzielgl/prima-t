{if {$comentarios_pendientes > 0}}
		<br>
  		{if {$comentarios_pendientes == 1}}
  			<center><font size=4 > *<font size=4 color="red"> {$comentarios_pendientes} </font> comentario pendiente por revisar. </font></center>
  		{else}
  			<center><font size=4 > *<font size=4 color="red"> {$comentarios_pendientes} </font> comentarios pendientes por revisar. </font></center>
  		{/if}
		<br>
{/if}    
<div class="tab-pane active" id="admon_comentarios_sitio">
    <ul class="thumbnails">                    
        <li class="span12">
            <div class="thumbnail">
                <div class="row-fluid">
                    <div class="row-fluid">
                        <div class="span12" style="overflow-y: auto;">
                            <table id="tabla_comentarios_sitio" class="table table-hover table-bordered" >
                                <thead> 
                                  <tr id={$comsit.com_id_comentario_sitio|string_format:"tr_%d"}>
                                    <th> Usuario </th>
                                    <th> Comentario  </th>
                                    <th style="width:80px"> Fecha </th>
                                    <th style="width:80px; text-align: center;"> </th>
                                    <th style="width:80px; text-align: center;"> </th>
                                  </tr>
                                  </thead> 
                                   {foreach from=$comentarios_sitio item=comsit name=foo}
                                   {if $smarty.foreach.foo.index % 2 == 0}
                                        <tr id={$comsit.com_id_comentario_sitio|string_format:"tr_%d"} class="success" >
                                   {else}
                                        <tr id={$comsit.com_id_comentario_sitio|string_format:"tr_%d"}>
                                   {/if}     
                                        
                                   <td id={$comsit.com_id_comentario_sitio|string_format:"username_%d"} >
                                            {$nombre = ucwords($comsit.username)}
                                            {$nombre}
                                   </td>
                                   <td id={$comsit.com_id_comentario_sitio|string_format:"com_comentario_%d"} >
                                            {$comsit.com_comentario}
                                   </td>
                                   <td id={$comsit.com_id_comentario_sitio|string_format:"com_fecha_creacion_%d"} >
                                            {$comsit.com_fecha_creacion}
                                   </td>          
                                                                   
                                     <td id={$comsit.com_id_comentario_sitio|string_format:"td_eliminar_%d"} style="text-align: center;" >
                                         <a id_c={$comsit.com_id_comentario_sitio|string_format:"%d"} comentario_sitio ="{$comsit.com_comentario}" class="btn btn-success btn-mini" href="#modal_aprobar_sitio" ><i class="icon-ok icon-white"></i> Aprobar</a>
                                     </td>
                                     
                                     <td id={$comsit.com_id_comentario_sitio|string_format:"td_eliminar_%d"} style="text-align: center;" >
                                         <a id_c={$comsit.com_id_comentario_sitio|string_format:"%d"} comentario_sitio ="{$comsit.com_comentario}" class="btn btn-danger btn-mini" href="#modal_eliminar_sitio" ><i class="icon-remove icon-white"></i> Eliminar</a>
                                     </td>
                                     
                                     
                                  </tr>
                               {/foreach}
                             </table>
                        </div>
                        {$paginacion}
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>
