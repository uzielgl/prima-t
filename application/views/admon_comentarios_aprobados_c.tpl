<div class="tab-pane active" id="admon_comentarios">
    <ul class="thumbnails">                    
        <li class="span12">
            <div class="thumbnail">
                <div class="row-fluid">
                    <div class="row-fluid">
                        <div class="span12" style="overflow-y: auto;">
                            <table id="tabla_comentarios_aprobados" class="table table-hover table-bordered" >
                                <thead> 
                                  <tr id={"$comtes.tes_id_tesis_users"|string_format:"tr_%d"}>
                                    <th> Tesis </th>
                                    <th> Usuario </th>
                                    <th> Comentario  </th>
                                    <th style="width:80px"> Fecha </th>
                                    <th style="width:80px; text-align: center;"> </th>
                                  </tr>
                                  </thead>
                                  {foreach from=$comentarios_aprobados item=comtes name=foo}
                                   {if $smarty.foreach.foo.index % 2 == 0}
                                        <tr id={$comtes.tes_id_tesis_users|string_format:"tr_%d"} class="success" >
                                   {else}
                                        <tr id={$comtes.tes_id_tesis_users|string_format:"tr_%d"}>
                                   {/if}     
                                        
                                        
                                   <td id={$comtes.tes_id_tesis_users|string_format:"tes_nombre_%d"} >
                                            {$comtes.tes_nombre}
                                   </td>
                                   
                                   <td id={$comtes.tes_id_tesis_users|string_format:"username_%d"} >
                                           {$nombre = ucwords($comtes.username)}
                                            {$nombre}
                                   </td>
                                   <td id={$comtes.tes_id_tesis_users|string_format:"tes_comentario_%d"} >
                                            {$comtes.tes_comentario}
                                   </td>
                                   <td id={$comtes.tes_id_tesis_users|string_format:"tes_fecha_creacion_%d"} >
                                            {$comtes.tes_fecha_creacion}
                                   </td>          
                                     
                                     <td id={$comtes.tes_id_tesis_users|string_format:"td_eliminar_%d"} style="text-align: center;" >
                                         <a id_c={$comtes.tes_id_tesis_users|string_format:"%d"} comentario_tesis ="{$comtes.tes_comentario}" class="btn btn-danger btn-mini" href="#modal_eliminar" ><i class="icon-remove icon-white"></i> Eliminar</a>
                                     </td>
                                  </tr>
                               {/foreach}

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>