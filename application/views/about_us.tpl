<div class="row-fluid">

  <div class="span5 offset3">
   	<a class="brand" href='{base_url()}index.php/home' style="padding: 0; float:center">
                <img src='{base_url()}themes/default/img/logo-carta-primat.png'>
        </a>
  </div>

<div class="row">
	<div class="span5 offset1">
	<h3 class="text-success" style="margin-bottom: 30px">Bienvenido</h3>
	</div>
</div>    

 

<div class="row">
<div class="span7 offset1"> 
  <p class="lead" align="justify">
	 
	<font face="times">
	 	Aun cuando <strong> en México la Primatología es una disciplina relativamente joven </strong>(pues sus primeros estudios aparecen hace solo cuatro décadas), 
 en los últimos años se ha convertido en un campo con creciente desarrollo. Un claro ejemplo de ello es que <strong>hoy en día se encuentran notables 
 aportaciones académicas relacionadas con los primates</strong> que se estudian en nuestro país, especialmente dentro de áreas como la ecología, comportamiento, 
 conservación, manejo, psicología, neurofisiología, genética, medicina, antropología, entre otras muchas. Sin embargo, quizás algo de lo más importante de 
 todo este gran esfuerzo, es que ahora el estudio de los primates se desarrolla con más y mejores grupos de trabajo, y sobre todo que varios de ellos 
 contribuyen con la constante formación de recursos humanos dedicados a trabajar con monos en nuestro país. Es inclusive notorio que en años recientes 
 en México estamos recibiendo estudiantes de otros países para ser entrenados y capacitados por los experimentados primatólogos mexicanos.
	</font>
  </p>
  
</div>
	
	<div class="span4 ">
		<p class='lead text-center'>
			<img src='{base_url()}themes/default/img/texto1.png'></center>
		</p>
	</div>
</div>
  
<div class="row">

<div class="span4 offset1">
		<p class='lead text-center'>
			<img src='{base_url()}themes/default/img/AULLADOR.jpg'></center>
		</p>
	</div>
	
<div class="span7" > 
 <p class="lead" align="justify">
	<font face="times">

Sin duda, la gran cantidad de información expresada en publicaciones científicas internacionales, han hecho que la primatología realizada en México 
sea reconocida a nivel mundial. Pero aun con todo este prestigio, se debe reconocer que durante muchos años -y aun en nuestros días- existe una 
gran cantidad de información que se queda archivada exclusivamente en documentos de Tesis, y que no alcanza la deseada divulgación de un artículo 
científico. Muchas veces, el final de todo ese esfuerzo académico queda arrumbado en una biblioteca entre miles de libros de todos los temas,
 donde pocas veces puede ser consultada a pesar de tener información extremadamente valiosa para los estudiantes que inician en la primatología 
 e inclusive para investigadores experimentados.
 	</font>
  </p>
</div>
</div>
<div class="row">
 
<div class="span7 offset1"> 
  <p class="lead" align="justify">
	 <font face="times">
Con base en lo anterior, es que surge esta iniciativa de ofrecerles esta plataforma de consulta <strong>“Prima-T”</strong> desarrollada por los colegas del 
<strong>Laboratorio Nacional de Informática Avanzada AC (LANIA)</strong>. Esta nueva ventana a la literatura especializada en formato de tesis pone a su 
disposición los documentos de cualquier grado (Licenciatura, Maestría, Doctorado, Especialidad, etc.) que se hubieran obtenido desde el origen 
de la primatología Mexicana hasta la fecha. Por supuesto esta “biblioteca virtual” será cada día mejor conforme los autores de años previos y actuales de estas tesis, contribuyan para que este acervo continúe complementándose. Gracias a la total disposición y apoyo de mi Institución (Instituto de Ecología AC – INECOL-) se mantiene esta plataforma de información en sus servidores con gran seguridad para todos los usuarios.

<br>
Queda así, esta nueva opción de consulta para todos ustedes que esperamos tenga gran aceptación por todos ustedes 
y quizás en un futuro pueda crecer hacia otras metas mayores.
</p>
   </font>
  </div>
	
	<div class="span4 ">
		<p class='lead text-center'>
			<img src='{base_url()}themes/default/img/Alouatta_pigra.jpg'></center>
		</p>
	</div>

</div>
<div class="span5 offset3" > 
  <p class="lead text-center">
	 <font face="times">
<em><strong>Mil gracias a todos por su apoyo y paciencia para lograr materializar este proyecto.</strong></em>
     </font>
  </p>
</div>


<div class="row">

 <div class="span2 offset4">
  <p class="lead text-center">
<img src='{base_url()}themes/default/img/chango humano.jpg'>
  </p>
</div>

<div class="span4">    
  <p class="lead text-left">
	 <font face="times">
<strong>Dr. Juan Carlos Serio Silva</Strong><br>
<em>Administrador “Prima-T”</em><br>
Investigador Titular “B” <br>
Instituto de Ecología AC <br>
Xalapa, Veracruz, México <br>	 					
Agosto 2013 <br> 
     </font>
  </p>

</div>
</div>
</div>