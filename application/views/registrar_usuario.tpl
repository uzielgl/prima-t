<form class="form-horizontal" method="post">
<fieldset>

<!-- Form Name -->
<legend>Registro de Usuario</legend>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="usu_nombre">Nombre:</label>
  <div class="controls">
    <input id="usu_nombre" name="usu_nombre" type="text" placeholder="Escriba su nombre y apellido(s)" class="input-xxlarge">
  </div>
</div>

<!-- Select Basic -->
<div class="control-group {cls_error("usu_fecha_nacimiento")}">
  <label class="control-label" for="usu_fecha_nacimiento">Fecha de Nacimiento:</label>
  <div class="controls">
    {$campos.usu_fecha_nacimiento}
    {help_inline(form_error_msg("usu_fecha_nacimiento", " "))}
    </select>
  </div>
</div>

<!-- Select Basic -->
<div class="control-group {cls_error("usu_sexo")}">
  <label class="control-label" for="usu_sexo">Sexo:</label>
  <div class="controls">
    {$campos.usu_sexo}
    {help_inline(form_error_msg("usu_sexo", " "))}
    </select>
  </div>
</div>

<div class="control-group {cls_error("usu_pais")}">
  <label class="control-label" for="usu_pais">País de donde nos visita:</label>
  <div class="controls">
    {form_input($campos.usu_pais)}
    {help_inline(form_error_msg("usu_pais", " "))}
  </div>
</div>

<!-- Select Basic -->
<div class="control-group {cls_error("usu_ocupacion")}">
  <label class="control-label" for="usu_ocupacion">Ocupación:</label>
  <div class="controls">
    {$campos.usu_ocupacion}
    {help_inline(form_error_msg("usu_ocupacion", " "))}
    </select>
  </div>
</div>



<!-- Select Basic -->
<div class="control-group {cls_error("gra2_id_grado_academico")}">
  <label class="control-label" for="gra2_id_grado_academico">Grado Académico:</label>
  <div class="controls">
    {$campos.gra_id_grado_academico}
    {help_inline(form_error_msg("gra2_id_grado_academico", " "))}
    </select>
  </div>
</div>
		
<!-- Select Basic -->
<div class="control-group {cls_error("usu_cuenta_facebook")}">
  <label class="control-label" for="usu_cuenta_facebook">Cuenta de Facebook:</label>
  <div class="controls">
    {$campos.usu_cuenta_facebook}
    {help_inline(form_error_msg("usu_cuenta_facebook", " "))}
    </select>
  </div>
</div>
<!-- Select Basic -->
<div class="control-group {cls_error("usu_correo_electronico")}">
  <label class="control-label" for="usu_correo_electronico">Correo eléctronico:</label>
  <div class="controls">
    {$campos.usu_correo_electronico}
    {help_inline(form_error_msg("usu_correo_electronico", " "))}
    </select>
  </div>
</div>

!-- Select Basic -->
<div class="control-group {cls_error("usu_contraseña")}">
  <label class="control-label" for="usu_contraseña">Correo eléctronico:</label>
  <div class="controls">
    {$campos.usu_contraseña}
    {help_inline(form_error_msg("usu_contraseña", " "))}
    </select>
  </div>
</div>



</fieldset>

<button type="submit" class="btn btn-success"><i class="icon-ok icon-white"></i> Registar</button>
</form>