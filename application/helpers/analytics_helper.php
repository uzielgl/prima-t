<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Codeigniter Google Analytics Helper
 *
 * @package	DASC
 * @subpackage	helpers
 * @category	hepers
 * @author	Wilbur Suero
 * @link	http://retrorock.info/code/google_analytics_helper
 */


/**
 * Google analytics
 *
 * Prints the Gogole Analytics tracking code after reading if this is wanted
 * in the configuration file
 */
if (!function_exists('google_analytics')) {
    function google_analytics($code=null) {
	$CI = get_instance();
	if ($CI->config->item('analytics_active') == TRUE) {
	    if ($code) {
		$UA = $code;
	    } else if (($CI->config->item('tracking_code'))) {
		$UA = $CI->config->item('tracking_code');
	    }
	} else {
	    return false;
	}
	
	if (isset($UA))	{
	    $output = "<script>
  			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			
  			ga('create', '".$UA."', 'ocharan.org');
  			ga('send', 'pageview');
			
			</script>";
	    echo $output;
	}
    }
}