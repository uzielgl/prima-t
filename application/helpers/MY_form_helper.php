<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Form Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/helpers/form_helper.html
 */

// ------------------------------------------------------------------------

if ( ! function_exists('form_error_msg'))
{
	function form_error_msg($field = '', $prefix = '', $suffix = '')
	{
		if (FALSE === ($OBJ =& _get_validation_object()))
		{
			return '';
		}
		return $OBJ->error($field, " ", " ");
//		return $OBJ->error($field, $prefix, $suffix);
	}
}

/*
if ( ! function_exists('form_error_cls'))
{
	function form_error_cls($field = '')
	{
		if (FALSE === ($OBJ =& _get_validation_object()))
		{
			return '';
		}
		if( $OBJ->error($field, " ", " ") )
			return "error";
		else
			return "";
	}
}*/

if ( ! function_exists('cls_error'))
{
    function cls_error($field = '')
    {
		
        if (FALSE === ($OBJ =& _get_validation_object()))
        {
            return '';
        }
		
        if( $OBJ->error($field, " ", " ") )
            return "error";
        else
            return "";
    }
}


if ( ! function_exists('input_attr'))
{
	function input_attr($field, $class="", $val="")
	{
		$CI =& get_instance();
		//Aqu� deber�a de usar el generador de inputs de html_helper
		$input = sprintf("<input  
						title='%s' 
						class='%s $class' 
						value='%s'  
						type='text' 
						name='$field'
						id='name'
					/>",
					form_error_msg($field),
					form_error_cls($field),
					$val ? $val : (trim( set_value($field) ) ? set_value($field) : $CI->input->get_post($field) )
					);
		return $input;
	}
}

//Por aca falta la de input_select_attr



if( ! function_exists("my_validation_errors" ) ){
	function my_validation_errors(){
		if ( $ve = validation_errors("<li>", "</li>") ){
			$t = "<div class='errors' ><ul>%s</ul></div>";
			return sprintf($t, $ve);
		}
		return "";
	}
}

if ( !function_exists("class_error") ){
	function class_error($field){
		if (FALSE === ($OBJ =& _get_validation_object()))
		{
			return '';
		}
		return $OBJ->error($field, " ", " ") ? "input-error" : "";
	}
}


/**For form validation*/

function form_validation($validation, $p1){
	$CI =& get_instance();
	$CI->load->library("form_validation");
	return $CI->form_validation->$validation( $p1 );
}


/**For form validation*/ 
/* Version mejorada, hay que ver como registrar objetos en smarty
function form_validation(){
	$CI =& get_instance();
	$CI->load->library("form_validation");
	return $CI->form_validation;
	
}
 * */














