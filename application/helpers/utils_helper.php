<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Form Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/helpers/form_helper.html
 */

// ------------------------------------------------------------------------


/** 
	* Recibe la fecha que viene en d/m/Y y la transforma a formato mysql (Y-m-d)
	* 
*/

/* Para documentos */
if ( ! function_exists('flog'))
{
	function flog($p1, $p2= NULL){
		global $fp;
		$fp->log($p1, $p2);
	}
}

/*Operador ternario para usar en los tpl*/
function ter($condicion, $v1=NULL, $v2=''){
	if( $condicion && $v1 == NULL){
		if( $condicion ) return $condicion;
		else return "";  
	}
	
    if( $condicion ) return $v1; 
    else return $v2;
}

function highlight($needle, $haystack, $f='<strong class="highlight">', $l='</strong>')
{
    $first = stripos( $haystack, $needle );   
    if( $first !== FALSE )
        $original = substr($haystack, $first, strlen($needle) );
    
    return str_ireplace($needle, $f . $original .$l , $haystack);
}
