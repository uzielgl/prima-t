<?php  
if( !function_exists("help_inline") ){
    function help_inline($text){
        return "<span class='help-inline'>$text</span>";
    }
}



function alert_error_msg( $in ){
	return '<div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    ' . $in . '
                </div>';
}



function btn_order( $direction="up-down" ){
	if($direction == "up-down") return '<button class="btn btn-mini"><i class="icon-resize-vertical"></i></button>'; 
	return '<button class="btn btn-mini"><i class="icon-chevron-' . $direction . '"></i></button>';
}


function set_order_type( $field ){
	if( set_value( "order_by" ) == $field ){
		if( $o = set_value( "order_type" ) ){
			if( $o == "asc") return btn_order("up");
			elseif($o == "des") return btn_order("down");
		}else
			return btn_order();
	}else
		return btn_order();
	
}

function set_cls_order( $field ){
	if( set_value( "order_by" ) == $field ){
		if( $o = set_value( "order_type" ) ){
			if( $o == "asc") return "filter-asc";
			elseif($o == "des") return "filter-desc";
		}else
			return "";
	}else
		return "";
}
