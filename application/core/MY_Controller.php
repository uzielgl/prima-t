<?php

class MY_Controller extends CI_Controller{
    
    //Variables de control para el tag_title
    var $tag_title = "";
    var $tag_fix_title = "Prima-T";
	var $tag_fix_pos = "";  // Valores suffix|prefix|none;
	
	
	var $jsons = array();
    
    //  no_private = array() //Estos no serán considerados en el is_private.

	public function __construct($logued_required=TRUE){
		parent::__construct();

		$from_url = "";
		if ($this->agent->is_referral())
		{
		    $from_url = $this->agent->referrer();
		}
		$this->session->set_userdata("from_url", $from_url);
		
		$this->data = array();
        $this->offset = $this->data["offset"] = $this->input->get_post("offset")? $this->input->get_post("offset") : $this->config->item("offset");
        $this->limit = $this->data["limit"] = $this->input->get_post("limit")? $this->input->get_post("offset") : $this->config->item("limit");
        
        if( $this->uri->rsegment(1) !== "admin" || true ){ //Siempre cae aca
            //Elementos css y js generales para el front
            $this->data['js'] = array( 
                "base" => array(
                    "jquery-1.9.1.min.js", "bootstrap/bootstrap.min.js", "modernizr-2.6.2-respond-1.1.0.min.js"),
                "plugs" => array("nod.min.js", "jquery.noty.min.js"),
                "globals" => array("extend.js", "init.js"),
                "page" => array()
            );
            
            $this->data["css"] = array( 
                "base" => array(
                   "bootstrap/bootstrap.min.css", "bootstrap/bootstrap-responsive.min.css"),
                "plugs" => array("jquery.noty.css", "noty_theme_default.css"),
                "globals" => array("base.css"),
                "page" => array()
            ); 
        }else{
             //Elementos css y js generales para el admin
            $this->data['js'] = array( 
                "base" => array(
                    "jquery-1.9.1.min.js", "bootstrap/bootstrap.js", "modernizr-2.6.2-respond-1.1.0.min.js",  ),
                "plugs" => array(),
                "globals" => array("extend.js", "init.js"),
                "page" => array()
            );
            
            $this->data["css"] = array( 
                "base" => array(
                   "bootstrap/bootstrap.min.css", "bootstrap/bootstrap-responsive.min.css"),
                "plugs" => array(),
                "globals" => array("base.css"),
                "page" => array()
            ); 
        }
        
        $this->data["prefix_title"] = " ";

		$this->data['data'] = array();
				$this->load->library('ion_auth');
		$this->load->library('form_validation');
		$this->load->helper('url');

		// Load MongoDB library instead of native db driver if required
		$this->config->item('use_mongodb', 'ion_auth') ?
		$this->load->library('mongo_db') :

		$this->load->database();

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
		$this->load->helper('language');
	}

    /** Estable el tag "title" del sitio web
     * @param title El título del sitio web
     * @param fix Define si se pone o no el prefijo o sufijo de "Prima-T"  
     * @param type si va a ser un sufijo o un prefijo, opciones: sufix:prefix
     * @author uzielgl@hotmail.com
     */
    public function setTitle( $title, $fix = 'suffix' ){
        $this->tag_title = $title;
		if( $fix ){
			$this->tag_fix_pos = $fix;
		}else{
			$this->$tag_fix_title = '';
		}
    }
    
    /** Generá el título del sitio web en base a lo establecido en setTitle
     * @return String El título que se pondrá en el sitio
     * @author uzielgl@hotmail.com
     * */
    private function _getTitle(){
    	if( $this->tag_fix_pos ){
    		if($this->tag_fix_pos == 'suffix') return trim("{$this->tag_title} - {$this->tag_fix_title}");
			elseif($this->tag_fix_pos == 'preffix') return trim("{$this->tag_fix_title} - {$this->tag_title}");
    	}else{
    		return trim($this->tag_fix_title);
    	}
    }
    
    
    /** Agrega un archivo JS al sitio web
     * @param $file La ubicación del archivo a partir del tema seleccionado
     * @param $file Establece donde se pondrá el archivo, posibles opciones last|first|N
     * */
     /*
    public function addJs($file, $position="last"){
        if( is_array($file) )
            $this->data["js"] = array_merge( $this->data["js"], $file );
        else
            $this->data["js"][] = $file;
    }*/
    
    public function addJs($file, $section="page", $position="last"){
        if( is_array($file) ){
            if( $position == "last" ){
                $this->data["js"][$section] = array_merge(
                    $this->data["js"][$section], 
                    $file 
                );
            }elseif( $position == "first" ){
                $this->data["js"][$section] = array_merge(
                    $file, 
                    $this->data["js"][$section]
                );
            }
        }else{
            if( $position == "last"){
                $this->data["js"][$section][] = $file;
            }elseif( $position == "first" ){
            }
        }
    }
    
    /** Agrega un archivo CSS al sitio web
     * @param $file La ubicación del archivo a partir del tema seleccionado
     * @param $file Establece donde se pondrá el archivo, posibles opciones last|first|N
     * */
    public function addCss($file, $section="page", $position="last"){
        if( is_array($file) ){
            if( $position == "last" ){
                $this->data["css"][$section] = array_merge(
                    $this->data["css"][$section], 
                    $file 
                );
            }elseif( $position == "first" ){
                $this->data["css"][$section] = array_merge(
                    $file, 
                    $this->data["js"][$section]
                );
            }
        }else{
            if( $position == "last"){
                $this->data["css"][$section][] = $file;
            }elseif( $position == "first" ){
            }
        }
    }
	
	public function addJson( $name, $json){
		$this->jsons[$name] = $json;
	}


	public function sresponse($p1=null, $p2=null) { //Success response
		$r = array('success' => TRUE);
		if ($p1 and $p2){ //means that p1 is msg and p2 is extra data
			$r['msg'] = $p1;
			$r= array_merge($r, $p2);
		}elseif($p1 and !$p2)//means that only there are one parameter
			if(is_string($p1)) // If is a string set in msg
				$r['msg'] = $p1;
			elseif(is_array($p1)) // if is a array merge
				$r = array_merge($r, $p1);
		return json_encode($r);

	}

	public function fresponse($p1=null, $p2=null, $p3=null){ // Failure response
		$r = array('success' => FALSE);
		if(is_array($p1)){ //Means that p1 is ERRORS
			$tmp = array();
    		foreach($p1 as $id => $msg) $tmp[] = array('id' => $id, 'msg' => $msg);
			$r['errors'] = $tmp;
			if(is_string($p2)) {
				$r['msg'] = $p2;//Means that p2 is MSG
				if(is_array($p3)) $r = array_merge($r, p3); // The p3 parameter is DATA that will be merge with the response
			}elseif(is_array($p2)) $r = array_merge($r, $p2); //Means that p2 is more data for merge with the response
		}elseif(is_string($p1)){//Means that p1 is MSG
			$r['msg'] = $p1;
			if (is_array($p2)) $r = array_merge($r, $p2);//The p2 parameter is DATA that will be merge with the response
		}
		return json_encode($r);
	}

	

	public function tresponse($data){//Tree Response
		return json_encode($data);
	}

	

	  /**
     * Permite el acceso al tipo de usuario especificado, si no se especifica ninguno
     * por lo menos el usuario debe de estar logueado
     * @param Los ids de los grupos de usuarios que se permitirá acceso
     * @return void
     * @author UzielGL  
     */
    public function is_private( ){
        $id_groups = func_get_args();
        
        if( $this->ion_auth->logged_in() ){
            
            //Si no se le pasa ningún parámetro, sólo se verifica que esté logueado            
            if( !$id_groups ) return;

            $user_groups = $this->ion_auth->get_users_groups()->result();
            
            function filter( $obj ){ return $obj->id; }
            $id_user_groups = array_map( "filter", $user_groups  );
            
            $allow = FALSE;
            foreach( $id_user_groups as $i){
                if( in_array($i, $id_groups) ){
                    $allow = TRUE;
                    break;
                }
            }
            
            if( !$allow ){
                $msg = "Lo sentimos, no cuenta con los permisos necesarios.";
                if( IS_AJAX ){
                    echo $this->fresponse( $msg );
                }else{
                    $data = array( "content" => "<h3 style='margin:100px 0;'><center>$msg</center><h3>");
                    echo $this->view(NULL, $data, TRUE);
                }
                exit;
            }
        }else{
            $this->session->set_userdata("to", current_url() );
            if( IS_AJAX ){
                echo $this->fresponse("Para continuar debes de estar logueado.");
            }else{
                redirect("auth/login");
            }
        } 
    }

      
      
    /** Abrirá una función en un ámbito (front|end) */
    public function load_view($template = "", $subview=NULL, $data=array(), $return=FALSE){
        $this->data["tag_title"] = $this->_getTitle();
        
         $this->data["js"] = array_merge( 
            $this->data["js"]["base"],
            $this->data["js"]["plugs"],
            $this->data["js"]["globals"],
            $this->data["js"]["page"]
        );
        
        $this->data["css"] = array_merge( 
            $this->data["css"]["base"],
            $this->data["css"]["plugs"],
            $this->data["css"]["globals"],
            $this->data["css"]["page"]
        );

		$this->data["jsons"] = $this->jsons;
        
        $this->data["cls_section"] = "" . $this->uri->rsegment(1);
        $this->data["cls_subsection"] = $this->uri->rsegment(1) . "-" . $this->uri->rsegment(1) ? $this->uri->rsegment(1) . "-" . $this->uri->rsegment(2) : ""; 
        
        $data['subview'] = $subview;
        $new_data = array_merge($this->data, $data);
        if( $return )
            return $this->parser->parse($template, $new_data, true);
        else
            $this->parser->parse($template, $new_data);
    }

	
    /** Abre una vista dada a partir de la carpeta view y la carga en el template
     * @param $subview
     * */
	public function view($subview=NULL, $data=array(), $return=FALSE  ){  //Muestra la página de frontend, NO LA DE ADMINISTRADOR
        if( $return )
            return $this->load_view("tpl/page.tpl", $subview, $data, TRUE );
        else
            $this->load_view("tpl/page.tpl", $subview, $data, $return );        
	}


	/** Muestra la página del administrador, NO LA DEL FRONTEND
	 *
	 */
	public function aview($subview='', $data=array()  ){
        $subview = $subview ? "admin/" . $subview : "";
	    $this->parser->set_theme("admin");  
		$this->load_view("admin/tpl/page.tpl", $subview, $data);        
	}		

	

	public function show_message( $msg , $fs = "<h4>", $last = "</h4>"){
		$this->data["content"] = $fs . $msg . $fs;
		$this->aview();
	}

}
