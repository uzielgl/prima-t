<?php
class MY_Model extends CI_Model{
/*	var $tb = ""; //tabla
	var $pre = ""; //prefix
	var $pk = "";
	*/
	
	public function __construct(){
		parent::__construct();
	}
	
	public function setLimit($limit, $offset = 0){
		$this->limit = $limit;
		$this->offset = $offset;
		$this->db->limit($limit, $offset);
	}
	
	public function get(){
		$rs = $this->db->get($this->tb . " " . $this->alias);
		return $rs->result_array();
	}
	
	public function get_by_id($id){
		$this->db->where_in("{$this->pk}", $id);
		$rs = $this->get();
		if( count($rs) > 0 )
			return $rs[0];
		else
			return false;
	}
	
	
	public function count_all_results(){
		$this->db->from($this->tb);
		$rs = $this->db->count_all_results();
		return $rs;
	}
	
	public function get_count(){
		$rs = $this->db->query($this->count_all_query);
		print_r($rs->row_array());
	}
	
	public function upd($id, $data= array() ){
		try{
			$this->db->where($this->pk, $id);
			$this->db->update($this->tb, $data);
			return TRUE;
		}catch( Exception $e){ 
			return FALSE;
		}
	}
	
	public function add(&$data, $returnAllData = FALSE){
		try{
			$tmp = explode(" ", $this->tb);
			if( count($tmp) == 2) list($tb, $alias) = $tmp;
			else {
				$tb = $this->tb;
				$alias = $this->tb;
			}
		}catch(Exception $e){
			$tb = $this->tb;
			$alias = $this->tb;
		}
		
		try{
			$this->db->insert($tb, $data);
		}catch( Exception $e){
			return FALSE;
		}
		$data[$this->pk] = $this->db->insert_id();
		if( $returnAllData )
			return $this->get_by_id( $this->db->insert_id() );
		return true;
	}
	
	public function del($id){
		try{
			$tmp = explode(" ", $this->tb);
			if( count($tmp) == 2) list($tb, $alias) = $tmp;
			else {
				$tb = $this->tb;
				$alias = $this->tb;
			}
		}catch(Exception $e){
			$tb = $this->tb;
			$alias = $this->tb;
		}
		
		$this->db->where_in($this->pk, $id);
		$this->db->delete($tb); 
	}

}
