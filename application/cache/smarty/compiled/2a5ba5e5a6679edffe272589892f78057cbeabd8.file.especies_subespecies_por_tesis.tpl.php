<?php /* Smarty version Smarty-3.1.12, created on 2014-01-22 09:57:48
         compiled from "/var/www/primat_trunk/application/views/reportes/tesis/especies_subespecies_por_tesis.tpl" */ ?>
<?php /*%%SmartyHeaderCode:57018511252dfea7c97cf69-96103697%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2a5ba5e5a6679edffe272589892f78057cbeabd8' => 
    array (
      0 => '/var/www/primat_trunk/application/views/reportes/tesis/especies_subespecies_por_tesis.tpl',
      1 => 1377355956,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '57018511252dfea7c97cf69-96103697',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'especies' => 0,
    'esp' => 0,
    'this' => 0,
    'sub' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_52dfea7ca327e3_55440210',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52dfea7ca327e3_55440210')) {function content_52dfea7ca327e3_55440210($_smarty_tpl) {?><p>
<h3 style="text-align: left" class="pull-left text-success">Reporte de tesis por especies y subespecies</h3>
</p>
<div class="pull-right">
	<a href="<?php echo site_url('reportes/tesis_c/especiesSubespecies_por_tesis/2');?>
" class="btn"> <i class="icon-share"> </i> Exportar a CSV</a>	 
</div>
<div class="clearfix"></div>
<?php  $_smarty_tpl->tpl_vars['esp'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['esp']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['especies']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['esp']->key => $_smarty_tpl->tpl_vars['esp']->value){
$_smarty_tpl->tpl_vars['esp']->_loop = true;
?>
	<table align="center" style="max-width: 90% ;min-width:20%; alignment-baseline:center" class="table table-striped table-bordered table-hover table-condensed container">
		<thead>
			<tr>
				<th style="text-align: center; background-color: #DFF0D8; color: #A9302A">ESPECIE / SUBESPECIE</th>
				<th style="background-color: #DFF0D8; color: #A9302A">CANTIDAD DE TESIS</th>
			</tr>
			<tr>
				<th><?php echo $_smarty_tpl->tpl_vars['esp']->value->esp_nombre;?>
</td>
				<th><?php echo $_smarty_tpl->tpl_vars['esp']->value->cantidad;?>
</td>
			</tr>			
		</thead>

		<tbody>
			<?php  $_smarty_tpl->tpl_vars['sub'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sub']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['this']->value->getSubespeciesPorTesis($_smarty_tpl->tpl_vars['esp']->value->esp_id_especie); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['sub']->key => $_smarty_tpl->tpl_vars['sub']->value){
$_smarty_tpl->tpl_vars['sub']->_loop = true;
?>
				<tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $_smarty_tpl->tpl_vars['sub']->value->subespecie;?>

					</td>
					<td><?php echo $_smarty_tpl->tpl_vars['sub']->value->cantidad;?>

					</td>
				</tr>
			<?php } ?>
		</tbody>
		
	</table>
<?php } ?>
<?php }} ?>