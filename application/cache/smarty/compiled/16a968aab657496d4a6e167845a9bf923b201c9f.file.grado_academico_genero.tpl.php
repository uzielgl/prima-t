<?php /* Smarty version Smarty-3.1.12, created on 2013-09-17 10:28:21
         compiled from "/var/www/primat_trunk/application/views/reportes/usuarios/grado_academico_genero.tpl" */ ?>
<?php /*%%SmartyHeaderCode:95175995523875150a5839-17988601%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '16a968aab657496d4a6e167845a9bf923b201c9f' => 
    array (
      0 => '/var/www/primat_trunk/application/views/reportes/usuarios/grado_academico_genero.tpl',
      1 => 1377355956,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '95175995523875150a5839-17988601',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'aUsuarios' => 0,
    'usuario' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_523875150edaa9_00608888',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_523875150edaa9_00608888')) {function content_523875150edaa9_00608888($_smarty_tpl) {?><h3 style="text-align: left" class="pull-left text-success">Reporte de grado acádemico y género por usuarios</h3>
<div class="pull-right">
	<a href="<?php echo site_url('reportes/usuarios_c/grado_academico_genero/2');?>
" class="btn"> <i class="icon-share"> </i> Exportar a CSV</a>	 
</div>


<div class="clearfix"></div>

	<table class="table table-striped table-bordered table-hover table-condensed">
		<thead>
			<tr>
				<th>NOMBRE</th>
				<th>USUARIO</th>
				<th>GENERO</th>
				<th>NACIONALIDAD</th>
				<th>GRADO ACÁDEMICO</th>
			
			</tr>			
		</thead>

		<tbody>
			<?php  $_smarty_tpl->tpl_vars['usuario'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['usuario']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aUsuarios']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['usuario']->key => $_smarty_tpl->tpl_vars['usuario']->value){
$_smarty_tpl->tpl_vars['usuario']->_loop = true;
?>
				<tr>
					<td><?php echo $_smarty_tpl->tpl_vars['usuario']->value["NOMBRE"];?>
</td>
					<td><?php echo $_smarty_tpl->tpl_vars['usuario']->value["APELLIDOS"];?>
</td>
					<td><?php echo $_smarty_tpl->tpl_vars['usuario']->value["GENERO"];?>
</td>
					<td><?php echo $_smarty_tpl->tpl_vars['usuario']->value["NACIONALIDAD"];?>
</td>
					<td><?php echo $_smarty_tpl->tpl_vars['usuario']->value["GRADO_ACADEMICO"];?>
</td>
					
				</tr>
			<?php } ?>
		</tbody>
		
	</table>

<?php }} ?>