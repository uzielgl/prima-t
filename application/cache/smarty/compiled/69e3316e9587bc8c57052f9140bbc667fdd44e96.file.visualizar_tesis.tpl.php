<?php /* Smarty version Smarty-3.1.12, created on 2013-12-03 05:54:02
         compiled from "/var/www/primat_trunk/application/views/visualizar_tesis.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1117834180529dc65a3d99a7-50356782%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '69e3316e9587bc8c57052f9140bbc667fdd44e96' => 
    array (
      0 => '/var/www/primat_trunk/application/views/visualizar_tesis.tpl',
      1 => 1377355956,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1117834180529dc65a3d99a7-50356782',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'tes_ruta_tesis' => 0,
    'comentarios' => 0,
    'comentario' => 0,
    'id_tesis' => 0,
    'tes_nombre' => 0,
    'tes_nombre_autor' => 0,
    'ins_nombre' => 0,
    'tes_anio_titulacion' => 0,
    'gra_descripcion' => 0,
    'est_nombre' => 0,
    'tes_num_descargas' => 0,
    'tes_num_visitas' => 0,
    'cantidad_imagenes' => 0,
    'cantidad_videos' => 0,
    'cantidad_documentos' => 0,
    'cantidad_audios' => 0,
    'prom_cal' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_529dc65a4c8804_97237138',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_529dc65a4c8804_97237138')) {function content_529dc65a4c8804_97237138($_smarty_tpl) {?><html lang='es'>
  <head>  
    
  </head>
<body>
    <style type="text/css">
      #videoDiv { 
        margin-right: 3px;
      }
      #videoInfo {
        margin-left: 3px;
      }
    </style>
    <script src="//www.google.com/jsapi" type="text/javascript"></script>
    <script type="text/javascript">
      google.load("swfobject", "2.1");
    </script>
    
    
    
 <div class="container-fluid" >
     
     
     
     <?php echo $_smarty_tpl->getSubTemplate ("modals/modal_tesis_videos.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

     <?php echo $_smarty_tpl->getSubTemplate ("modals/modal_tesis_documentos.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

     <?php echo $_smarty_tpl->getSubTemplate ("modals/modal_tesis_imagenes.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
 
     <?php echo $_smarty_tpl->getSubTemplate ("modals/modal_tesis_audios.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
 
     <div class="row-fluid">
       <h3 class="text-success" style="margin-bottom: 30px">Visualizar Tesis</h3>
           <div class="row-fluid">
                <div class="span12">
                     <div class="span9">
                          <div class="row-fluid" style="margin-bottom: 20px">
                               <div class="span12" >
                                 <iframe style="width:98%; height:650px" src=<?php echo base_url();?>
<?php echo sprintf("themes/default/pdf/%s",$_smarty_tpl->tpl_vars['tes_ruta_tesis']->value);?>
 ></iframe>
                             
                       <!--      <iframe style="width:98%; height:650px" src=<?php echo site_url();?>
<?php echo "themes/default/pdf/%s";?>
 ></iframe> -->
                                </div>
                          </div>
                           <div class="row-fluid" >
                                <ul class="thumbnails" >
                                    <li  class="span12" style="margin-bottom: 0px" >
                                        <div class="thumbnail">
                                            <div class="row-fluid" >
                                                <div class="span12" style="margin-top: -13px">
                                                  <span class="label  pull-left"><?php if (count($_smarty_tpl->tpl_vars['comentarios']->value)>0){?><?php echo count($_smarty_tpl->tpl_vars['comentarios']->value);?>
<?php }?>  Comentarios </span>  
                                                </div>
                                            </div>
                                        <div style="overflow: scroll; max-height: 180px">
                                        		<?php if (count($_smarty_tpl->tpl_vars['comentarios']->value)>0){?> 
                                        			<?php  $_smarty_tpl->tpl_vars['comentario'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['comentario']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['comentarios']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['comentario']->key => $_smarty_tpl->tpl_vars['comentario']->value){
$_smarty_tpl->tpl_vars['comentario']->_loop = true;
?>
																<div class="thumbnail" style="margin-bottom: 5px" >
                                                    			<div class="span12" style="margin-top: -6px">
                                                        			<label class="label label-success" ><?php echo $_smarty_tpl->tpl_vars['comentario']->value['username'];?>
</label>
                                                    			</div>
                                                    			<p align="justify">
                                                        			<?php echo $_smarty_tpl->tpl_vars['comentario']->value['tes_comentario'];?>

                                                    			</p>                                        
                                                			</div>
                                        			<?php } ?>
                                        			<?php }else{ ?>
                                        			<div class="thumbnail" style="margin-bottom: 5px" >
                                                    	<div class="span12" style="margin-top: -6px">
                                                        		<!--label class="label label-success" ><?php echo $_smarty_tpl->tpl_vars['comentario']->value['username'];?>
</label-->
                                                    		</div>
                                                    			<p>
																	<center><h6>No se ha comentado ésta tesis.</h6></center>
                                                    			</p>                                        
                                                		</div>                          
                                        			<?php }?>
                                            </div>
                                        </div>
										<form id="formComentario" >
											<input type="hidden" name="id_tesis" value="<?php echo $_smarty_tpl->tpl_vars['id_tesis']->value;?>
" />
											<div class="row-fluid">
													<div style="overflow: scroll: auto; max-height: 180px"> 
													<label class="label label-success" for="tes_comentario">Escriba su comentario:</label>
													<textarea placeholder="Escriba su comentario aquí" id="txtacomentario"  name="comentario" style="width:100%" maxlength="500"></textarea>
												</div>												
											</div>

										</form>
										<button id="agregarComentario" OURL="<?php echo site_url();?>
/test/ver_tesis/<?php echo $_smarty_tpl->tpl_vars['id_tesis']->value;?>
" URL="<?php echo site_url();?>
/test/agregarComentario" type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i> Comentario</button>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="span3" >
                            <ul class="thumbnails">
                                <li  class="span12" >
                                    <div class="thumbnail" style="margin-bottom: 20px">
                                        <div class="row-fluid" >
                                            <div class="span12" style="margin-top: -15px">
                                                <span class="label  pull-left">Ficha tecnica</span>  
                                            </div>
                                            <div class="span12" >
                                            	<p> <?php echo sprintf("<h5>Titulo:</h5> %s",$_smarty_tpl->tpl_vars['tes_nombre']->value);?>
</p>
                                                <p> <?php echo sprintf("<h5>Autor:</h5> <h7 >%s</h7 >",$_smarty_tpl->tpl_vars['tes_nombre_autor']->value);?>
</p>
                                                <p> <?php echo sprintf("<h5>Institución:</h5> <h7>%s</h7>",$_smarty_tpl->tpl_vars['ins_nombre']->value);?>
 </p>
                                                <p> <?php echo sprintf("<h5>Año:</h5><h7>%s</h7>",$_smarty_tpl->tpl_vars['tes_anio_titulacion']->value);?>
 </p>
                                                <p> <?php echo sprintf("<h5>Grado:</h5> <h7>%s</h7>",$_smarty_tpl->tpl_vars['gra_descripcion']->value);?>
 </p>
                                                <p> <?php echo sprintf("<h5>Lugar:</h5> <h7>%s</h7>",$_smarty_tpl->tpl_vars['est_nombre']->value);?>
 </p>
                           
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="thumbnail" style="margin-bottom: 20px">
                                        <div class="row-fluid" >
                                            <div class="span12" style="margin-top: -15px">
                                                <span class="label  pull-left">Estadísticas</span>  
                                            </div>
                                            <div class="span10 offset1">
                                                <div class="label label-success">
                                                	Descargas: <span class="descargas-totales"><?php echo $_smarty_tpl->tpl_vars['tes_num_descargas']->value;?>
</span>
                                                </div>
                                                <div class="label label-success">
                                                    <?php echo sprintf("Visitas: %d",$_smarty_tpl->tpl_vars['tes_num_visitas']->value);?>

                                                </div>
                                                                                      
                                            </div>
                                          </div>
                                                                                
                                    </div>
                                    
                                    <div class="thumbnail" style="margin-bottom: 20px">
                                        <div class="row-fluid" >
                                            <div class="span12" style="margin-top: -15px">
                                                <span class="label  pull-left">Material Extra</span>  
                                            </div>
                                            <div class="span12 text-left">
                                                <a id_tesis="<?php echo $_smarty_tpl->tpl_vars['id_tesis']->value;?>
" class="btn btn-info" onclick="modalMostrarImagenes(this);"> Imágenes (<?php echo $_smarty_tpl->tpl_vars['cantidad_imagenes']->value;?>
)</a></br>
                                                </br>
                                                <a id_tesis="<?php echo $_smarty_tpl->tpl_vars['id_tesis']->value;?>
" class="btn btn-info" onclick="modalMostrarVideos(this);"> Videos (<?php echo $_smarty_tpl->tpl_vars['cantidad_videos']->value;?>
)</a></br>
                                                </br>
                                                <a id_tesis="<?php echo $_smarty_tpl->tpl_vars['id_tesis']->value;?>
" class="btn btn-info" onclick="modalMostrarDocumentos(this);"> Documentos (<?php echo $_smarty_tpl->tpl_vars['cantidad_documentos']->value;?>
)</a></br>
                                                </br>
                                                <a id_tesis="<?php echo $_smarty_tpl->tpl_vars['id_tesis']->value;?>
" class="btn btn-info" onclick="modalMostrarAudios(this);"> Audios (<?php echo $_smarty_tpl->tpl_vars['cantidad_audios']->value;?>
)</a></br>
                                            </div>
                                          </div>
                                                                                
                                    </div>
                                    <div class="thumbnail" style="margin-bottom: 20px">
                                        <div class="row-fluid"  >
                                            <div class="span12" style="margin-top: -15px">
                                                <span class="label  pull-left">Descargar</span>  
                                            </div>
                                            <div class="span10 offset1">
                                            	<center><a class="btn-descargar" href="<?php echo site_url("tesis/descargar/".((string)$_smarty_tpl->tpl_vars['id_tesis']->value).".pdf");?>
"><?php echo image('download.png');?>
</a></center>
                                            	<center><a class="btn-descargar" href="<?php echo site_url("tesis/descargar/".((string)$_smarty_tpl->tpl_vars['id_tesis']->value).".pdf");?>
">Descargar tesis</a></center>
                                            </div>
                                          </div>
                                                                                
                                    </div>
                    					
                    					
                    					<div class="thumbnail" style="margin-bottom: 20px">
										<div class="row-fluid">
										<div class="span12" style="margin-top: -15px">
										
										</div>
										<div class="span10 offset1">
										<div class="basic" id="cal" data-average="<?php echo $_smarty_tpl->tpl_vars['prom_cal']->value;?>
" url="<?php echo base_url();?>
" id="cal" data-id="<?php echo $_smarty_tpl->tpl_vars['id_tesis']->value;?>
"></div>
										<p id="mensaje" class="text-success"></p>	
										<p id="mensajeError" class="text-error"></p>
											</div>
											</div>
										</div>	
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
      </div>
             
  </body>
</html>


<?php }} ?>