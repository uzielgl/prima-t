<?php /* Smarty version Smarty-3.1.12, created on 2013-08-25 14:18:27
         compiled from "/var/www/primat_trunk/application/views/catalogos/tesis/tabla.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1120646612521a588315fcc6-96770727%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cad39c47d416a2d465c0eca28f97e18a68e1dc49' => 
    array (
      0 => '/var/www/primat_trunk/application/views/catalogos/tesis/tabla.tpl',
      1 => 1377355956,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1120646612521a588315fcc6-96770727',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'tesis' => 0,
    'i' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_521a58831f9a37_36337617',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_521a58831f9a37_36337617')) {function content_521a58831f9a37_36337617($_smarty_tpl) {?><table class="table table-striped tesis ">
	<thead>
		<tr class="order">
			<!--
			<th data-field="tes_nombre" data-type="<?php echo ter(set_value("order_by")=="tes_nombre",set_value("order_type"));?>
" >
				Título<?php echo set_order_type("tes_nombre");?>

			</th>
			-->
			<th data-field="tes_nombre" data-type="<?php echo ter(set_value("order_by")=="tes_nombre",set_value("order_type"));?>
" >
				<div class="filter <?php echo set_cls_order("tes_nombre");?>
">
					<span>Título</span>
				</div>
			</th>
			<th data-field="autores" data-type="<?php echo ter(set_value("order_by")=="autores",set_value("order_type"));?>
">
				<div class="filter <?php echo set_cls_order("autores");?>
">
					<span>Autores</span>
				</div>
			</th>
			<th data-field="directores" data-type="<?php echo ter(set_value("order_by")=="directores",set_value("order_type"));?>
">
                <div class="filter <?php echo set_cls_order("directores");?>
">
                    <span>Directores</span>
                </div>
            </th>
			<th data-field="tes_id_grado_obtenido" data-type="<?php echo ter(set_value("order_by")=="tes_id_grado_obtenido",set_value("order_type"));?>
">
				<div class="filter <?php echo set_cls_order("tes_id_grado_obtenido");?>
">
					<span>Grado</span>
				</div>
			</th>
			<th data-field="descargas" data-type="<?php echo ter(set_value("order_by")=="descargas",set_value("order_type"));?>
">
				<div class="filter <?php echo set_cls_order("descargas");?>
">
					<span>Descargas</span>
				</div>
			</th>
			<th data-field="visitas" data-type="<?php echo ter(set_value("order_by")=="visitas",set_value("order_type"));?>
">
				<div class="filter <?php echo set_cls_order("visitas");?>
">
					<span>Visitas</span>
				</div>
			</th>
			<th> </th>
			<th> </th>
			<th> </th>
			<th> </th>
		</tr>
	</thead>
	<tbody>
	    <?php  $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['i']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tesis']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['i']->key => $_smarty_tpl->tpl_vars['i']->value){
$_smarty_tpl->tpl_vars['i']->_loop = true;
?>
    		<tr>
    			<td >
    				<?php echo $_smarty_tpl->tpl_vars['i']->value['tes_nombre'];?>

    			</td>
    			<td>
    				<?php echo substr($_smarty_tpl->tpl_vars['i']->value['autores'],0,strlen($_smarty_tpl->tpl_vars['i']->value['autores'])-2);?>

    			</td>
    			<td>
                    <?php echo $_smarty_tpl->tpl_vars['i']->value['directores'];?>

                </td>
    			<td>
    				<?php echo $_smarty_tpl->tpl_vars['i']->value['gra_descripcion'];?>

    			</td>
    			<td >
    				<div class="text-right">
    					<?php echo $_smarty_tpl->tpl_vars['i']->value['descargas'];?>

    				</div>
    			</td>
    			<td class="separation">
    				<div class="text-right">
    					<?php echo $_smarty_tpl->tpl_vars['i']->value['visitas'];?>

    				</div>
    			</td>
    			<td class="btn-container">
    				<a id="btn-ver-tesis-<?php echo $_smarty_tpl->tpl_vars['i']->value['tes_id_tesis'];?>
" href="<?php echo site_url(("catalogos/tesis/ver_tesis_admin/").($_smarty_tpl->tpl_vars['i']->value['tes_id_tesis']));?>
"  class="btn btn-mini btn-action"><i class="icon-black icon-eye-open"></i> Ver</a>
    			
    	<!--		<a id="btn-ver-tesis-<?php echo $_smarty_tpl->tpl_vars['i']->value['tes_id_tesis'];?>
"  href="href='<?php echo base_url();?>
index.php/visualizar_tesis_administrador"  class="btn btn-mini btn-action"><i class="icon-black icon-eye-open"></i> Ver</a>
    		-->		
    			</td>
    			<td class="btn-container" >
                    <a style="width:82px" href="<?php echo site_url(("catalogos/tesis/material_extra/").($_smarty_tpl->tpl_vars['i']->value['tes_id_tesis']));?>
" class="btn btn-mini"><i class="icon-pencil"></i> Material Extra</a>
                </td>
    			<td class="btn-container">
    				<a href="<?php echo site_url(("catalogos/tesis/modificar/").($_smarty_tpl->tpl_vars['i']->value['tes_id_tesis']));?>
" class="btn btn-mini btn-warning"><i class="icon-white icon-pencil"></i> Editar</a>
    			</td>
    			<td class="btn-container">
    				<a id="btn-eliminar-tesis-<?php echo $_smarty_tpl->tpl_vars['i']->value['tes_id_tesis'];?>
" href="<?php echo site_url(("catalogos/tesis/eliminar/").($_smarty_tpl->tpl_vars['i']->value['tes_id_tesis']));?>
" class="btn btn-mini btn-danger"><i class="icon-white icon-remove"></i> Eliminar</a>
    			</td>
    		</tr>
		<?php } ?>
	</tbody>
</table><?php }} ?>