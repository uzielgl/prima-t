<?php /* Smarty version Smarty-3.1.12, created on 2013-08-26 17:16:52
         compiled from "/var/www/primat_trunk/application/views/auth/recuperar_password.tpl" */ ?>
<?php /*%%SmartyHeaderCode:170292023521bd3d48fac51-35565761%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '65dd14c9ba7c540efd499bc7c1285a1e9665e45e' => 
    array (
      0 => '/var/www/primat_trunk/application/views/auth/recuperar_password.tpl',
      1 => 1377355956,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '170292023521bd3d48fac51-35565761',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'message' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_521bd3d493fc30_93521578',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_521bd3d493fc30_93521578')) {function content_521bd3d493fc30_93521578($_smarty_tpl) {?>


<div class="container">

	<div class="row">
		
		<div class="span offset2">
			
			<form class="form-horizontal" method="post">
				
				<div class="control-group">
					<div class="controls">
						<h2>Recuperar contraseña</h2>
					</div>
				</div>
				
				    	
				<div class="control-group  <?php echo cls_error("identity");?>
">
					<label class="control-label" for="inputEmail">Correo electrónico</label>
					<div class="controls">
						<div class="input-prepend">
							<span class="add-on"><i class="icon icon-user"></i></span>
							<input class="input-medium" id="prependedInput" value="<?php echo set_value("email");?>
" name="email" type="text" placeholder="Usuario@dominio">
						</div>
						<?php echo help_inline(form_error("email"));?>

					</div>
				</div>
				
				<?php if (!validation_errors()&&$_smarty_tpl->tpl_vars['message']->value){?>
					<div class="control-group error">
						<div class="controls">
							<?php echo help_inline($_smarty_tpl->tpl_vars['message']->value);?>

						</div>
					</div>
				<?php }?>
				
				<div class="control-group">
		        <div class="controls">
					
		            <button class="btn" type="submit"><i class="icon-refresh icon"></i> Recuperar</button>
		        </div>
		    </div>
				
			</form>
		
		</div>
	
	</div>

</div><?php }} ?>