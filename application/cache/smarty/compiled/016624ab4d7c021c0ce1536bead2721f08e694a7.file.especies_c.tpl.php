<?php /* Smarty version Smarty-3.1.12, created on 2013-08-28 06:51:55
         compiled from "/var/www/primat_trunk/application/views/especies_c.tpl" */ ?>
<?php /*%%SmartyHeaderCode:155899214521de45b6b8172-35579170%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '016624ab4d7c021c0ce1536bead2721f08e694a7' => 
    array (
      0 => '/var/www/primat_trunk/application/views/especies_c.tpl',
      1 => 1377355956,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '155899214521de45b6b8172-35579170',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'especies' => 0,
    'especie' => 0,
    'subespecie' => 0,
    'paginacion' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_521de45b732c36_52214685',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_521de45b732c36_52214685')) {function content_521de45b732c36_52214685($_smarty_tpl) {?>
<style>
	.sub td{padding-right:0px;}
</style>


<div class="tab-pane active" id="especies">
    <ul class="thumbnails">                    
        <li class="span12" style="margin-bottom: 0px">
            <div class="thumbnail">
                <div class="row-fluid">
                    <div class="span12" style="overflow-y: auto; ">
                        <a id="nueva_especie" style="margin:5px 10px 10px 0px" href="#modal_agregar" data-toggle="modal" role="button" class="btn btn-success" > <i class="icon-plus icon-white"></i> Especie</a>
                        <table id="tabla_catalogos" class="table tablesorter" style="position: static">
                            <thead>
                              <tr>
                                <th data-field="esp_nombre" data-type>
                                    <div class="filter">
                                        
                                    </div>
                                </th>
                                <th style="width: 100px" > </th> <!-- Agregar -->
                                <th style="width: 59px" > </th> <!-- Editar -->
                                <th style="width: 67px" > </th><!-- Eliminar -->
                              </tr>
                            </thead>
                            <tbody>
                            <?php  $_smarty_tpl->tpl_vars['especie'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['especie']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['especies']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['especie']->key => $_smarty_tpl->tpl_vars['especie']->value){
$_smarty_tpl->tpl_vars['especie']->_loop = true;
?>
                                <tr id=<?php echo sprintf("tr_%d",$_smarty_tpl->tpl_vars['especie']->value['esp_id_especie']);?>
 class="success" >
                                    <td id=<?php echo sprintf("esp_nombre_%d",$_smarty_tpl->tpl_vars['especie']->value['esp_id_especie']);?>
 >
                                        <strong><?php echo $_smarty_tpl->tpl_vars['especie']->value['esp_nombre'];?>
</strong>
                                    </td>
                                    <td id=<?php echo sprintf("td_agregar_subcatalogo_%d",$_smarty_tpl->tpl_vars['especie']->value['esp_id_especie']);?>
 style="text-align: center;min-width:0px;max-width: 70px">
                                        <a id=<?php echo sprintf("a_agregar_subcatalogo_%d",$_smarty_tpl->tpl_vars['especie']->value['esp_id_especie']);?>
 class="btn btn-success btn-mini" href="#modal_agregar_subespecies" ><i class="icon-plus icon-white"></i> Subespecies</a>
                                    </td>
                                    <td id=<?php echo sprintf("td_editar_%d",$_smarty_tpl->tpl_vars['especie']->value['esp_id_especie']);?>
 style="text-align: center;min-width:0px;max-width: 70px" >  
                                        <a id=<?php echo sprintf("a_editar_%d",$_smarty_tpl->tpl_vars['especie']->value['esp_id_especie']);?>
 class="btn btn-warning btn-mini" href="#modal_editar" ><i class="icon-pencil icon-white"></i> Editar</a>
                                    </td>
                                    <td id=<?php echo sprintf("td_eliminar_%d",$_smarty_tpl->tpl_vars['especie']->value['esp_id_especie']);?>
 style="text-align: center;min-width:0px;max-width: 70px" >
                                        <a id=<?php echo sprintf("a_eliminar_%d",$_smarty_tpl->tpl_vars['especie']->value['esp_id_especie']);?>
 class="btn btn-danger btn-mini" href="#modal_eliminar" ><i class="icon-remove icon-white"></i> Eliminar</a>
                                    </td>
                                </tr>
                                <tr id=<?php echo sprintf("tr_subcatalogos_%d",$_smarty_tpl->tpl_vars['especie']->value['esp_id_especie']);?>
 >
                                    <td id=<?php echo sprintf("td_subcatalogos_%d",$_smarty_tpl->tpl_vars['especie']->value['esp_id_especie']);?>
 colspan="4" >
                                        <table id="tabla_subcatalogos_<?php echo $_smarty_tpl->tpl_vars['especie']->value['esp_id_especie'];?>
" class="table table-hover " style="position: static">
                                                                                        
                                            <tbody>
                                            <?php  $_smarty_tpl->tpl_vars['subespecie'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['subespecie']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['especie']->value['subespecies']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['subespecie']->key => $_smarty_tpl->tpl_vars['subespecie']->value){
$_smarty_tpl->tpl_vars['subespecie']->_loop = true;
?>
                                                <tr class="sub" id=<?php echo sprintf("tr_subcatalogo_%d",$_smarty_tpl->tpl_vars['subespecie']->value['sue_id_subespecie']);?>
>
                                                    <td class="nombre" id=<?php echo sprintf("sue_nombre_%d",$_smarty_tpl->tpl_vars['subespecie']->value['sue_id_subespecie']);?>
 >
                                                        <?php echo $_smarty_tpl->tpl_vars['subespecie']->value['sue_nombre'];?>

                                                    </td>
                                                    <td id=<?php echo sprintf("td_subcatalogo_editar_%d",$_smarty_tpl->tpl_vars['subespecie']->value['sue_id_subespecie']);?>
 style="text-align: center;" width="70px" >
                                                        <a id=<?php echo sprintf("a_editar_subcatalogo_%d",$_smarty_tpl->tpl_vars['subespecie']->value['sue_id_subespecie']);?>
 class="btn btn-warning btn-mini" href="#modal_editar_subespecies" ><i class="icon-pencil icon-white"></i> Editar</a>
                                                    </td>
                                                    <td id=<?php echo sprintf("td_subespecie_eliminar_%d",$_smarty_tpl->tpl_vars['subespecie']->value['sue_id_subespecie']);?>
 style="text-align: center;" width="70px" >
                                                        <a id=<?php echo sprintf("a_eliminar_subcatalogo_%d",$_smarty_tpl->tpl_vars['subespecie']->value['sue_id_subespecie']);?>
 class="btn btn-danger btn-mini" href="#modal_eliminar_subespecies" ><i class="icon-remove icon-white"></i> Eliminar</a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <?php echo $_smarty_tpl->tpl_vars['paginacion']->value;?>

                </div>
            </div>
        </li>
    </ul>
</div><?php }} ?>