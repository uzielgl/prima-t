<?php /* Smarty version Smarty-3.1.12, created on 2013-08-24 07:57:17
         compiled from "/home/primat_user/primat_trunk/application/views/busqueda_avanzada/filtros.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6442989565218c9cda05436-36637988%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '208da7c97439df0e9ff6875ac3d5253c1a23ba09' => 
    array (
      0 => '/home/primat_user/primat_trunk/application/views/busqueda_avanzada/filtros.tpl',
      1 => 1377355956,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6442989565218c9cda05436-36637988',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'campos' => 0,
    'this' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5218c9cda791f0_68293367',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5218c9cda791f0_68293367')) {function content_5218c9cda791f0_68293367($_smarty_tpl) {?><!-- 
    Seccion de busqueda de tesis para el usuario
-->

<div class="filtros">
    
<form class="form-horizontal" method="post" action="<?php echo site_url("busqueda_avanzada_c/index");?>
">
	<input type="hidden" value="<?php echo set_value("order_by");?>
" name="order_by" id="order_by">
	<input type="hidden" value="<?php echo set_value("order_type");?>
" name="order_type" id="order_type">
	
    
    <!-- Text input-->
        <div class="control-group">
          <label class="control-label" for="palabra_clave">Palabra clave</label>
          <div class="controls">
            <div class="input-append">
                <?php echo form_input($_smarty_tpl->tpl_vars['campos']->value['palabra_clave']);?>

                
                <button class="btn btn-primary" type="submit"><i class="icon-white icon-search"> </i> Buscar</button>
                <button  class="btn" id="mostrar-filtros" type="button"><i class="icon-cog"> </i> Opciones avanzadas</button>
            </div>
            <a href="<?php echo site_url("busqueda_avanzada_c");?>
" class="btn"><i class="icon-eye-open"> </i> Mostrar todo</a>
          </div>
        </div>
    
    
<fieldset >

<!-- Form Name -->

<div class="row-fluid <?php echo ter($_smarty_tpl->tpl_vars['this']->value->input->post("mostrar"),'',"hide");?>
" id="fls-filtros" >
    
    <div class="span3"> <!-- Columna 1 -->
        <input id="mostrar" name="mostrar" value="<?php echo ter($_smarty_tpl->tpl_vars['this']->value->input->post("mostrar"),"1","0");?>
" type="hidden" />

        <!-- Select Basic -->
        <div class="control-group">
          <label class="control-label" for="tes_id_institucion">Institución</label>
          <div class="controls">
            <?php echo $_smarty_tpl->tpl_vars['campos']->value['tes_id_institucion'];?>

          </div>
        </div>
        
         <!-- Select Basic -->
        <div class="control-group">
          <label class="control-label" for="tes_id_grado_obtenido">Grado obtenido</label>
          <div class="controls">
            <?php echo $_smarty_tpl->tpl_vars['campos']->value['tes_id_grado_obtenido'];?>

          </div>
        </div>
        
       <!-- Select Basic -->
        <div class="control-group">
          <label class="control-label" for="tes_id_especie">Especie</label>
          <div class="controls">
            <?php echo $_smarty_tpl->tpl_vars['campos']->value['tes_id_especie'];?>

          </div>
        </div>
           
    </div>
    
    <div class="span3"><!-- Columna 2 -->

        
         <div class="control-group">
          <label class="control-label" for="tes_id_disciplina_estudio">Disciplina</label>
          <div class="controls">
            <?php echo $_smarty_tpl->tpl_vars['campos']->value['tes_id_disciplina_estudio'];?>

          </div>
        </div>
           
                <!-- Select Basic -->
         <div class="control-group">
          <label class="control-label" for="tes_id_subdisciplina">Subdisciplina</label>
          <div class="controls">
            <?php echo $_smarty_tpl->tpl_vars['campos']->value['tes_id_subdisciplina'];?>

          </div>
        </div>        
        
    </div>

    <div class="span3"> <!-- Columna 3 -->
        
	   <!-- Select Basic -->
		<div class="control-group <?php echo cls_error("tes_id_subdisciplina");?>
">
		  <label class="control-label" for="tes_id_subdisciplina">Director</label>
		  <div class="controls">
		    <?php echo $_smarty_tpl->tpl_vars['campos']->value['tes_id_tesis_director'];?>

<!--		    <?php echo help_inline(form_error_msg("tes_id_subdisciplina"," "));?>

                </select>-->
		  </div>
		</div>
        
        <!-- Busqueda por autor -->
        <div class="control-group">
          <label class="control-label" for="tes_nombre_autor">
              Autor
          </label>
          <div class="controls">
            <?php echo $_smarty_tpl->tpl_vars['campos']->value['tes_id_tesis_autores'];?>
 
          </div>
        </div>
        
       <!-- Button -->
         <div class="control-group">
          <label class="control-label no-padding" for="">
            <button type="button" style="width:100px;" id="btn-clean" name="" class="btn  "><i class="icon-filter icon"></i> Limpiar</button>  
          </label>
          <div class="controls">
            <button style="width:100px;" id="singlebutton" name="singlebutton" class="btn-block btn btn-primary "><i class="icon-search icon-white"></i> Buscar</button> 
          </div>
        </div>

        
    </div>
       
</div>

</fieldset>
</form>

    
</div>
<?php }} ?>