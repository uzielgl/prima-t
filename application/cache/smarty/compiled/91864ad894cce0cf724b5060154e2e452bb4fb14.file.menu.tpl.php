<?php /* Smarty version Smarty-3.1.12, created on 2013-08-24 03:54:58
         compiled from "/var/www/primat_trunk/application/views/tpl/menu.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1818478195521874e2bf41b3-58615331%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '91864ad894cce0cf724b5060154e2e452bb4fb14' => 
    array (
      0 => '/var/www/primat_trunk/application/views/tpl/menu.tpl',
      1 => 1377355956,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1818478195521874e2bf41b3-58615331',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'this' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_521874e2c602b0_99168952',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_521874e2c602b0_99168952')) {function content_521874e2c602b0_99168952($_smarty_tpl) {?><div class='navbar navbar-inverse navbar-fixed-top'>
    <div class='navbar-inner'>
        <div class="container-fluid">
            <a class="brand" href='<?php echo site_url('home');?>
' style="padding: 0; float:left">
                <img src='<?php echo base_url();?>
themes/default/img/HeaderPrima.png'>
            </a>
            <button id="b" type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <i class='icon-list icon-white'> </i>  
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            
            <div class="nav-collapse collapse navbar-responsive-collapse">
                <ul class="nav">
                    <li>
                        <a href='<?php echo base_url();?>
index.php/busqueda_avanzada_c'>
                            <i class='icon-search icon-white'> </i> Tesis
                        </a>
                    </li>
                    <?php if (($_smarty_tpl->tpl_vars['this']->value->ion_auth->logged_in())){?>
                    <li>
                        <a id='Comentario' href='#'>
                            <i class='icon-comment icon-white'> </i> Comentario
                        </a>
                    </li>
                    <?php }?>
                    <li>
                        <a href='<?php echo site_url("contacto_c");?>
'>
                            <i class='icon-envelope icon-white'> </i> Contacto
                        </a>
                    </li>
                    <li>
                        <form class='navbar-search' action='<?php echo base_url();?>
index.php/busqueda_avanzada_c' method='post'>
                            <input id='buscar' name='palabra_clave' type='search' class='search-query' placeholder='Buscar'>
                        </form>
                    </li>
                   
                </ul>
            </div>
            
            <?php if ($_smarty_tpl->tpl_vars['this']->value->ion_auth->is_admin()){?>
                <div class="nav-collapse collapse navbar-responsive-collapse">
                    <ul class="nav">
                        <li class='dropdown'>
                            <a class='dropdown-toggle' href="#" data-toggle='dropdown'>
                                <i class='icon-wrench icon-white'> </i> Administración 
                                <span class='caret' > </span>
                            </a>
                            
                            <ul class="dropdown-menu">
                                <li>
                                    <a href='<?php echo site_url("catalogos/tesis");?>
'><i class='icon-lock'></i> Tesis</a>
                                </li>
                                <li>
                                    <a href='<?php echo site_url('admon_comentarios_c/comen_sitio');?>
'><i class='icon-inbox'></i> Comentarios</a>
                                </li>
                                <li>
                                    <a href='<?php echo site_url('catalogos/especies_c');?>
'><i class='icon-list-alt'></i> Especies</a>
                                </li>
                                <li>
                                
                                    <a href='<?php echo site_url('catalogos/disciplina_estudio_c');?>
'><i class='icon-list-alt'></i> Disciplinas de estudio</a>
                                </li>
                                <li>
                                    <a href='<?php echo site_url('catalogos/instituciones_c');?>
'><i class='icon-list-alt'></i> Instituciones</a>
                                </li>
                                <li>
                                    <a href='<?php echo base_url();?>
index.php/catalogos/condiciones_sitio_c'><i class='icon-list-alt'></i> Condiciones de sitio</a>
                                </li>
								<li class="dropdown-submenu">
									
									<a href='<?php echo site_url('reportes');?>
'></i> Reportes</a>
									<ul class="dropdown-menu">
										<li>
											<a href='<?php echo site_url('reportes/disciplinas_estudio');?>
'><i class='icon-list-alt'></i> Disciplinas de estudio</a>
										</li>
										<li>
											<a href='<?php echo site_url('reportes/tesis_c/especiesSubespecies_por_tesis/1');?>
'><i class='icon-list-alt'></i> Especies y subespecies </a>
										</li>
										<li>
											<a href='<?php echo site_url('reportes/tesis_c/visitas_por_tesis/1');?>
'><i class='icon-list-alt'></i> Visitas por tesis</a>
										</li>
										<li>
											<a href='<?php echo site_url('reportes/tesis_c/descargas_por_tesis/1');?>
'><i class='icon-list-alt'></i> Descargas por tesis</a>
										</li>

										<li>
											<a href='<?php echo site_url('reportes/usuarios_c/');?>
'><i class='icon-list-alt'></i> Grado académico y género de usuarios </a>
										</li>
									</ul>
								</li>                                
                                <li>
                                    <a href='<?php echo site_url('auth/index');?>
'><i class='icon-user'></i> Usuarios</a>
                                </li>                                                         
                         
                            
                            </ul>
                        </li>
                    </ul>
                </div>
            <?php }?>
                
            <div class="nav-collapse collapse navbar-responsive-collapse">
                <ul class="nav pull-right" style="margin-right: 10px">
                    <li class='dropdown'>
                        <a class='dropdown-toggle' href="#" data-toggle='dropdown'>
                            <i class='icon-user icon-white'> </i>
                            <?php if ($_smarty_tpl->tpl_vars['this']->value->ion_auth->logged_in()){?>
                                <?php echo $_smarty_tpl->tpl_vars['this']->value->ion_auth->user()->row()->first_name;?>
 <?php echo $_smarty_tpl->tpl_vars['this']->value->ion_auth->user()->row()->last_name;?>

                            <?php }else{ ?>
                                Iniciar sesión
                            <?php }?>
                             <b class="caret"></b>
                        </a>
                        <?php if (!$_smarty_tpl->tpl_vars['this']->value->ion_auth->logged_in()){?>
                        <ul class="dropdown-menu">
                            <form style="margin: 15px;" method="post" class="frm-login" action="<?php echo site_url("auth/login");?>
">
                                <div class="control-group">
                                  <div class="controls">
                                        <div class="input-prepend">
                                            <span class="add-on"><i class="icon icon-user"></i></span>
                                            <input class="input-medium" id="prependedInput" name="identity" type="text" placeholder="Usuario@dominio">
                                        </div>
                                  </div>
                                </div>
                                <div class="control-group">
                                  <div class="controls">
                                        <div class="input-prepend">
                                            <span class="add-on"><i class="icon icon-key"></i></span>
                                            <input class="input-medium" id="prependedInput" name="password" type="password" placeholder="Contraseña">
                                        </div>
                                        <label class="checkbox">
                                            <input type="checkbox" name="remember"> Recordar
                                        </label>
                                  </div>
                                </div>
                                <div class="control-group">
                                    <div class="controls">
                                        
                                        <button class="btn btn-block" type="submit"><i class="icon-play icon"></i> Iniciar sesión</button>
                                    </div>
                                </div>
                            </form>
                            <li><a href="<?php echo site_url("auth/forgot_password");?>
"><i class="icon-refresh"></i> Recuperar contraseña</a></li>
                            <li><a href="<?php echo site_url("auth/create_user");?>
"><i class="icon-plus"></i> Registrarse</a></li>                                                         
                        </ul>
                        <?php }else{ ?>
                        <ul class='dropdown-menu'>
                            <li>
                                <a href="<?php echo site_url("auth/change_password");?>
">
                                    <i class="icon-pencil"> </i> Cambiar contraseña
                                </a>
                            </li>
                            <li class="divider"> </li>
                            <li>
                                <a href="<?php echo site_url("auth/logout");?>
">
                                    <i class="icon-off"> </i> Cerrar sesión
                                </a>
                            </li>
                            
                            
                        </ul>
                        <?php }?>
                    </li>
                     <li>
                      	  <a href='http://www.inecol.edu.mx/' style="padding: 0"> <img style='max-height: 40px' src='<?php echo base_url();?>
themes/default/img/logo_inecol.png'></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>    
</div>
<?php echo $_smarty_tpl->getSubTemplate ("modals/modal_comentar_sitio_c.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>