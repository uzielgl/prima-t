<?php /* Smarty version Smarty-3.1.12, created on 2013-08-28 21:08:54
         compiled from "/var/www/primat_trunk/application/views/modals/modal_editar_instituciones_c.tpl" */ ?>
<?php /*%%SmartyHeaderCode:720510840521ead361ebf34-42074654%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9aedf95765cc0a3bdfeb4f058f85eeef9e76d677' => 
    array (
      0 => '/var/www/primat_trunk/application/views/modals/modal_editar_instituciones_c.tpl',
      1 => 1377355956,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '720510840521ead361ebf34-42074654',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_521ead361edde8_89610691',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_521ead361edde8_89610691')) {function content_521ead361edde8_89610691($_smarty_tpl) {?>﻿<div id="modal_editar" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Editar</h3>
    </div>
    <div class="modal-body">
        <div class="thumbnail row-fluid">
            <div class="row-fluid">
                <div class="span12" style="margin-top: -13px">
                    <span class="label  pull-left">Datos de institución</span>  
                </div>
            </div>
            <div class="row-fluid">
                <div class="span4" style="text-align: right; margin-right: 20px">
                    Nombre:
                </div>
                <div class="span6" style="text-align: left; margin-top: -5px">
                    <input id="ins_nombre_editar" class="input-xlarge" type="text" disabled>
                </div>
            </div>
             <div class="row-fluid">
                <div class="span4" style="text-align: right; margin-right: 20px">
                    Siglas:
                </div>
                <div class="span6" style="text-align: left; margin-top: -5px">
                    <input id="ins_siglas_editar" class="input-xlarge" type="text" placeholder="Siglas de institución">
                </div>
            </div>
			   <div class="row-fluid">
                <div class="span4" style="text-align: right; margin-right: 20px">
                    Dependencia:
                </div>
                <div class="span6" style="text-align: left; margin-top: -5px">
                    <input id="ins_dependencia_editar" class="input-xlarge" type="text" placeholder="Nombre de la dependencia">
                </div>
            </div>
			   <div class="row-fluid">
                <div class="span4" style="text-align: right; margin-right: 20px">
                    Sede:
                </div>
                <div class="span6" style="text-align: left; margin-top: -5px">
                    <input id="ins_sede_editar" class="input-xlarge" type="text" placeholder="Nombre de la sede">
                </div>
            </div>
      
            
            
        </div>
    </div>
    <div class="modal-footer">
        <a id="btn_editar"  editar-id="0" class="btn btn-success">Editar</a>
        <a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</a>
    </div>
</div><?php }} ?>