<?php /* Smarty version Smarty-3.1.12, created on 2013-08-26 16:46:49
         compiled from "/var/www/primat_trunk/application/views/leer_mas3.tpl" */ ?>
<?php /*%%SmartyHeaderCode:581943738521bccc9f189d6-20192196%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '75631b60889d5c44f5379bd9e06ff79d6f566ab5' => 
    array (
      0 => '/var/www/primat_trunk/application/views/leer_mas3.tpl',
      1 => 1377355956,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '581943738521bccc9f189d6-20192196',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_521bccca006849_67751584',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_521bccca006849_67751584')) {function content_521bccca006849_67751584($_smarty_tpl) {?><div class="row-fluid">
	<br>
	<div class="span7" align="center">
	<h3 class="text-success" style="margin-bottom: 0px"><em>Ateles geoffroyi ssp. vellerosus</em></h3>
	<h3 class="text-success" style="margin-bottom: 20px"><em>Ateles geoffroyi ssp. yucatanensis</em></h3>
	<h3 class="text-success" style="margin-bottom: 20px">(Nombres comunes: mono araña, chango)</h3>
	</div>
	<div class="span3 offset1" align="center">
	 	<img style="width:100%"  src="<?php echo base_url();?>
themes/default/img/mono3.jpg" alt>
	</div>
	<br>
	<div class="span10" >
	<font face="times">
	<p class="lead" align="justify">
	<em>Ateles</em> es considerado como uno de los primates más grandes del Nuevo Mundo, con un rango de peso 
	que oscila entre cuatro y siete kilogramos, dependiendo de la especie (Hershkovitz, 1972). A diferencia 
	de otros atelinos, <em>Ateles geoffroyi</em> posee un cuerpo largo y delgado con un aspecto muy peculiar. A partir 
	de un tronco globular surgen los miembros esbeltos, los brazos ligeramente más largos que las piernas y 
	una cola muy larga, por lo que se le conoce vulgarmente como “mono araña”. La longitud de su cuerpo varía entre 
	38 y 65 centímetros y la cola entre 60 y 80 centímetros. Las manos de los monos araña aparentemente carecen de 
	dedo pulgar debido a que este es vestigial. El pelaje suele ser largo, tiene una cabeza pequeña y hocico prominente, 
	con órbitas oculares hacia delante y fosas nasales hacia los lados separadas por un cojinete internasal; las orejas 
	son desnudas y poco prominentes (Vaughan, 1988). Poseen incisivos alargados, una mandíbula reducida y miembros 
	anteriores largos que permiten una eficiente locomoción en la búsqueda de árboles con frutos (Rosenberger y Strier, 1989). 
	Al igual que la de <em>Alouatta</em>, la cola del mono araña es larga, desprovista de pelo en el último tercio inferior y la más prensil de 
	todos los primates neotropicales con longitudes que pueden llegar a medir hasta 84 centímetros en las hembras y 82 centímetros en 
	los machos (Hershkovitz, 1972). La cola desempeña, además, un papel muy importante en la locomoción y postura del mono y desde 
	el punto de vista social es utilizada para mantener contacto con otros individuos (Klein y Klein, 1971). No se observa dimorfismo 
	sexual evidente, machos y hembras tienen un peso y masa corporal similar, sin embargo, los caninos de los machos son más grandes 
	que los de las hembras. De manera distintiva, las hembras poseen un clítoris largo en forma de péndulo. Es posible que esta estructura 
	sirva para depositar orina y secreciones vaginales en las ramas de los árboles y anunciar, así, su presencia y estado reproductivo 
	(Pastor-Nieto, 2000). <em>Ateles</em> habita en selva alta (con dos variantes: perennifolia y subperennifolia); selva mediana 
	(con tres variantes: subperennifolia, subcaducifolia y caducifolia); selva baja caducifolia, bosque mesófilo de montaña y manglar 
	(Estrada y Coates-Estrada, 1988; Watts y Rico-Gray, 1987, 1988; Serio-Silva et al., 2006; Ortiz-Martínez y Rico-Gray, 2007). 
	El mono araña emplea gran parte de su tiempo en los estratos más altos de la cobertura vegetal y rara vez se mueve por el suelo 
	(Campbell et al., 2007). Es un animal de hábitos diurnos y preponderantemente frugívoros, aunque también incluye hojas y flores 
	en su dieta ya que se les ha observado consumiendo hojas, especialmente por las tardes antes de pernoctar (Van Roosmalen y Klein, 1988).
	<br>
	</p>
	<p class="lead" align="justify">
	<strong>Texto tomado de:
	SEMARNAT / CONANP. 2012. Programa de acción para la conservación de las especies: Primates, Mono Araña (<em>Ateles geoffroyi</em>) y 
	Monos Aulladores (<em>Alouatta palliata, Alouatta pigra</em>). Oropeza-Hernández, P.; Rendón-Hernández, E. (Eds.). Comisión Nacional de Áreas
	 Naturales Protegidas, Primera Edición. 53 Pp.
	 <strong>
	 </p>
	</font>
	</div>
</div><?php }} ?>