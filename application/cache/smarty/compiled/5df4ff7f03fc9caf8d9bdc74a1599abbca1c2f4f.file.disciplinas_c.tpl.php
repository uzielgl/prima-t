<?php /* Smarty version Smarty-3.1.12, created on 2013-08-25 14:59:51
         compiled from "/var/www/primat_trunk/application/views/disciplinas_c.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2020006784521a62375d17d9-94895991%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5df4ff7f03fc9caf8d9bdc74a1599abbca1c2f4f' => 
    array (
      0 => '/var/www/primat_trunk/application/views/disciplinas_c.tpl',
      1 => 1377355956,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2020006784521a62375d17d9-94895991',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'disciplinas' => 0,
    'disciplina' => 0,
    'subdisciplina' => 0,
    'paginacion' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_521a6237646630_58910434',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_521a6237646630_58910434')) {function content_521a6237646630_58910434($_smarty_tpl) {?>
<style>
	.sub td{padding-right:0px;}
</style>


<div class="tab-pane active" id="disciplinas">
    <ul class="thumbnails">                    
        <li class="span12" style="margin-bottom: 0px">
            <div class="thumbnail">
                <div class="row-fluid">
                    <div class="span12" style="overflow-y: auto;">
                    <a id="nueva_disciplina" style="margin:5px 10px 10px 0px" href="#modal_agregar" data-toggle="modal" role="button" class="btn btn-success" > <i class="icon-plus icon-white"></i> Disciplina</a>
                        <table id="tabla_catalogos" class="table" style="position: static">
                            <thead>
                              <tr>
                                <th data-field="dis_estudio_nombre" data-type>
                                </th>
                                <th style="width: 100px" > </th> <!-- Agregar -->
                                <th style="width: 59px" > </th> <!-- Editar -->
                                <th style="width: 67px" > </th><!-- Eliminar -->
                              </tr>
                            </thead>
                            <tbody>
                            <?php  $_smarty_tpl->tpl_vars['disciplina'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['disciplina']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['disciplinas']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['disciplina']->key => $_smarty_tpl->tpl_vars['disciplina']->value){
$_smarty_tpl->tpl_vars['disciplina']->_loop = true;
?>
                                <tr id=<?php echo sprintf("tr_%d",$_smarty_tpl->tpl_vars['disciplina']->value['dis_id_disciplina_estudio']);?>
 class="success" >
                                    <td id=<?php echo sprintf("dis_estudio_nombre_%d",$_smarty_tpl->tpl_vars['disciplina']->value['dis_id_disciplina_estudio']);?>
 >
                                        <strong><?php echo $_smarty_tpl->tpl_vars['disciplina']->value['dis_estudio_nombre'];?>
</strong>
                                    </td>
                                    <td id=<?php echo sprintf("td_agregar_subcatalogo_%d",$_smarty_tpl->tpl_vars['disciplina']->value['dis_id_disciplina_estudio']);?>
 style="text-align: center;" >
                                        <a id=<?php echo sprintf("a_agregar_subcatalogo_%d",$_smarty_tpl->tpl_vars['disciplina']->value['dis_id_disciplina_estudio']);?>
 class="btn btn-success btn-mini" href="#modal_agregar_subdisciplinas" ><i class="icon-plus icon-white"></i> Subdisciplinas</a>
                                    </td>
                                    <td id=<?php echo sprintf("td_editar_%d",$_smarty_tpl->tpl_vars['disciplina']->value['dis_id_disciplina_estudio']);?>
 style="text-align: center;" >
                                        <a id=<?php echo sprintf("a_editar_%d",$_smarty_tpl->tpl_vars['disciplina']->value['dis_id_disciplina_estudio']);?>
 class="btn btn-warning btn-mini" href="#modal_editar" ><i class="icon-pencil icon-white"></i> Editar</a>
                                    </td>
                                    <td id=<?php echo sprintf("td_eliminar_%d",$_smarty_tpl->tpl_vars['disciplina']->value['dis_id_disciplina_estudio']);?>
 style="text-align: center;" >
                                        <a id=<?php echo sprintf("a_eliminar_%d",$_smarty_tpl->tpl_vars['disciplina']->value['dis_id_disciplina_estudio']);?>
 class="btn btn-danger btn-mini" href="#modal_eliminar" ><i class="icon-remove icon-white"></i> Eliminar</a>
                                    </td>
                                </tr>
                                <tr id=<?php echo sprintf("tr_subcatalogos_%d",$_smarty_tpl->tpl_vars['disciplina']->value['dis_id_disciplina_estudio']);?>
 >
                                    <td id=<?php echo sprintf("td_subcatalogos_%d",$_smarty_tpl->tpl_vars['disciplina']->value['dis_id_disciplina_estudio']);?>
 colspan="4" >
                                        <table id="tabla_subcatalogos_<?php echo $_smarty_tpl->tpl_vars['disciplina']->value['dis_id_disciplina_estudio'];?>
" class="table table-hover tablesorter" style="position: static">
                                            
                                            <tbody>
                                            <?php  $_smarty_tpl->tpl_vars['subdisciplina'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['subdisciplina']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['disciplina']->value['subdisciplinas']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['subdisciplina']->key => $_smarty_tpl->tpl_vars['subdisciplina']->value){
$_smarty_tpl->tpl_vars['subdisciplina']->_loop = true;
?>
                                                <tr class="sub" id=<?php echo sprintf("tr_subcatalogo_%d",$_smarty_tpl->tpl_vars['subdisciplina']->value['sub_id_subdisciplina']);?>
>
                                                    <td id=<?php echo sprintf("sub_nombre_%d",$_smarty_tpl->tpl_vars['subdisciplina']->value['sub_id_subdisciplina']);?>
 >
                                                        <?php echo $_smarty_tpl->tpl_vars['subdisciplina']->value['sub_nombre'];?>

                                                    </td>
                                                    <td id=<?php echo sprintf("td_subcatalogo_editar_%d",$_smarty_tpl->tpl_vars['subdisciplina']->value['sub_id_subdisciplina']);?>
 style="text-align: center;" width="70px" >
                                                        <a id=<?php echo sprintf("a_editar_subcatalogo_%d",$_smarty_tpl->tpl_vars['subdisciplina']->value['sub_id_subdisciplina']);?>
 class="btn btn-warning btn-mini" href="#modal_editar_subdisciplinas" ><i class="icon-pencil icon-white"></i> Editar</a>
                                                    </td>
                                                    <td id=<?php echo sprintf("td_subcatalogo_eliminar_%d",$_smarty_tpl->tpl_vars['subdisciplina']->value['sub_id_subdisciplina']);?>
 style="text-align: center;" width="70px" >
                                                        <a id=<?php echo sprintf("a_eliminar_subcatalogo_%d",$_smarty_tpl->tpl_vars['subdisciplina']->value['sub_id_subdisciplina']);?>
 class="btn btn-danger btn-mini" href="#modal_eliminar_subdisciplinas" ><i class="icon-remove icon-white"></i> Eliminar</a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <?php echo $_smarty_tpl->tpl_vars['paginacion']->value;?>

                </div>
            </div>
        </li>
    </ul>
</div><?php }} ?>