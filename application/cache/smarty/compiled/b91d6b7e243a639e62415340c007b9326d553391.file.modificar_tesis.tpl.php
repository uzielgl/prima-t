<?php /* Smarty version Smarty-3.1.12, created on 2013-12-05 08:20:00
         compiled from "/var/www/primat_trunk/application/views/catalogos/tesis/modificar_tesis.tpl" */ ?>
<?php /*%%SmartyHeaderCode:99544274552a08b903343c9-60723335%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b91d6b7e243a639e62415340c007b9326d553391' => 
    array (
      0 => '/var/www/primat_trunk/application/views/catalogos/tesis/modificar_tesis.tpl',
      1 => 1377355956,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '99544274552a08b903343c9-60723335',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'tesis' => 0,
    'campos' => 0,
    'this' => 0,
    'autores_post' => 0,
    'i' => 0,
    'in' => 0,
    'directores_post' => 0,
    'grados_post' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_52a08b90572991_69018338',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52a08b90572991_69018338')) {function content_52a08b90572991_69018338($_smarty_tpl) {?><form class="form-horizontal"  id="formModificarTesis" action="" method="post" enctype="multipart/form-data" >
<fieldset>

<!-- Form Name -->
<Legend><h3 class="text-success">Modificar Tesis</h3></Legend>
<input type="hidden" id="id_tesis" name="id_tesis" value="<?php echo $_smarty_tpl->tpl_vars['tesis']->value['tes_id_tesis'];?>
" />
<!-- Text input-->
<div class="control-group">
  
  <label class="control-label" for="tes_palabras_clave">Palabras clave:</label>
  <div class="controls">
    <?php echo form_input($_smarty_tpl->tpl_vars['campos']->value['tes_palabras_clave']);?>

    <?php echo help_inline(form_error_msg("tes_palabras_clave"," "));?>

  	<span class="muted">Opcional </span>
  </div>
</div>

<div class="control-group <?php echo cls_error("tes_nombre");?>
">
  <label class="control-label" for="tes_nombre">Titulo: </label>
  <div class="controls">
    <?php echo form_input($_smarty_tpl->tpl_vars['campos']->value['tes_nombre']);?>

    <?php echo help_inline(form_error_msg("tes_nombre"," "));?>

  </div>
</div>


<?php if ($_smarty_tpl->tpl_vars['this']->value->input->post("tes_nombre_autor")){?>
	<?php $_smarty_tpl->tpl_vars['autores_post'] = new Smarty_variable($_smarty_tpl->tpl_vars['this']->value->input->post("tes_nombre_autor"), null, 0);?>
	
	<!-- Text input-->
	<div class="control-group <?php echo cls_error("tes_nombre_autor[".((string)$_smarty_tpl->tpl_vars['i']->value)."]");?>
">
	  <label class="control-label" for="tes_nombre_autor">Autor:</label>
	  <div class="controls" id="camposAutores">
	    <div id="autor1">
		<?php $_smarty_tpl->createLocalArrayVariable('campos', null, 0);
$_smarty_tpl->tpl_vars['campos']->value['tes_nombre_autor']['value'] = $_smarty_tpl->tpl_vars['autores_post']->value[0];?>
	    <?php echo form_input($_smarty_tpl->tpl_vars['campos']->value['tes_nombre_autor']);?>

	    <a id="agregar_autor" name="agregar_autor" class="btn btn-default" onClick="AgregarCamposAutor(this);">+</a>
		<?php echo help_inline(form_error_msg((("tes_nombre_autor[").($_smarty_tpl->tpl_vars['autores_post']->value[0])).("]")," "));?>

	    </div>
	    
	    <?php  $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['i']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['autores_post']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['i']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['i']->key => $_smarty_tpl->tpl_vars['i']->value){
$_smarty_tpl->tpl_vars['i']->_loop = true;
 $_smarty_tpl->tpl_vars['i']->index++;
?>
	    	<?php if ($_smarty_tpl->tpl_vars['i']->index!=0){?>
	    		<div id="autor<?php echo $_smarty_tpl->tpl_vars['i']->index+1;?>
">
				<?php $_smarty_tpl->createLocalArrayVariable('campos', null, 0);
$_smarty_tpl->tpl_vars['campos']->value['tes_nombre_autor']['value'] = $_smarty_tpl->tpl_vars['i']->value;?>
			    <?php echo form_input($_smarty_tpl->tpl_vars['campos']->value['tes_nombre_autor']);?>

				<a onclick="QuitarCampoAutor('autor<?php echo $_smarty_tpl->tpl_vars['i']->index+1;?>
');" class="btn btn-default" name="agregar_autor" id="agregar_autor">-</a>			    
				<?php echo help_inline(form_error_msg("tes_nombre_autor[".((string)$_smarty_tpl->tpl_vars['i']->value)."]"," "));?>

			    </div>
	    	<?php }?>
	    <?php } ?>
	    
	  </div>
	</div>
	
<?php }else{ ?>
	
	<!-- Text input-->
	<div class="control-group <?php echo cls_error("tes_nombre_autor[]");?>
">
	  <label class="control-label" for="tes_nombre_autor">Autor:</label>
	  <div class="controls" id="camposAutores">
	    <div id="autor1">
	    <?php echo form_input($_smarty_tpl->tpl_vars['campos']->value['tes_nombre_autor']);?>

	    <a id="agregar_autor" name="agregar_autor" class="btn btn-default" onClick="AgregarCamposAutor(this);">+</a>
		<?php echo help_inline(form_error_msg("tes_nombre_autor[]"," "));?>

	    </div>
	  </div>
	</div>

<?php }?>






	<div class="control-group">
		 <label class="control-label" for="tes_resumen">Resumen:</label>
 	 	<div class="controls">                     
    	<?php echo form_textarea($_smarty_tpl->tpl_vars['campos']->value['tes_resumen']);?>

    	<?php echo help_inline(form_error_msg("tes_resumen"," "));?>

  		</div>
	</div>

<!-- Select Basic -->
<div class="control-group <?php echo cls_error("gra_id_grado_academico");?>
">
  <label class="control-label" for="gra_id_grado_academico">Grado obtenido:</label>
  <div class="controls">
    <?php echo $_smarty_tpl->tpl_vars['campos']->value['gra_id_grado_academico'];?>

    <?php echo help_inline(form_error_msg("gra_id_grado_academico"," "));?>

    </select>
  </div>
</div>
		

<!-- Text input-->
<div class="control-group <?php echo cls_error("tes_anio_titulacion");?>
">
  <label class="control-label" for="tes_anio_titulacion">Año de titulación:</label>
  <div class="controls">
    <?php echo form_input($_smarty_tpl->tpl_vars['campos']->value['tes_anio_titulacion']);?>

    <?php echo help_inline(form_error_msg("tes_anio_titulacion"," "));?>

    
  </div>
</div>


<!-- Select Basic -->
<div class="control-group <?php echo cls_error("tes_id_institucion");?>
">
  <label class="control-label" for="tes_id_institucion">Institución de adscripción:</label>
  <div class="controls">
    <?php echo $_smarty_tpl->tpl_vars['campos']->value['tes_id_institucion'];?>

    <?php echo help_inline(form_error_msg("tes_id_institucion"," "));?>

    </select>
  </div>
</div>


<div id="camposDirectores">
	
	<?php if ($_smarty_tpl->tpl_vars['this']->value->input->post("tes_nombre_director")||$_smarty_tpl->tpl_vars['this']->value->input->post("tes_id_grado_academico")){?>
		<?php $_smarty_tpl->tpl_vars['directores_post'] = new Smarty_variable($_smarty_tpl->tpl_vars['this']->value->input->post("tes_nombre_director"), null, 0);?>
		<?php $_smarty_tpl->tpl_vars['grados_post'] = new Smarty_variable($_smarty_tpl->tpl_vars['this']->value->input->post("tes_id_grado_academico"), null, 0);?>
		
		<?php  $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['i']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['this']->value->input->post("tes_nombre_director"); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['i']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['i']->key => $_smarty_tpl->tpl_vars['i']->value){
$_smarty_tpl->tpl_vars['i']->_loop = true;
 $_smarty_tpl->tpl_vars['i']->index++;
?>
			<?php $_smarty_tpl->tpl_vars['in'] = new Smarty_variable($_smarty_tpl->tpl_vars['i']->index, null, 0);?>
			
			<div id="director<?php echo $_smarty_tpl->tpl_vars['i']->index+1;?>
">
				<!-- Text input-->
				<div class="control-group <?php echo cls_error("tes_nombre_director[".((string)$_smarty_tpl->tpl_vars['i']->value)."]");?>
">
		  			<label class="control-label" for="tes_nombre_director">Directores de tesis:</label>
		  
		  			<div class="controls">
		  				<?php $_smarty_tpl->createLocalArrayVariable('campos', null, 0);
$_smarty_tpl->tpl_vars['campos']->value['tes_nombre_director']['value'] = $_smarty_tpl->tpl_vars['directores_post']->value[$_smarty_tpl->tpl_vars['in']->value];?>
		    			<?php echo form_input($_smarty_tpl->tpl_vars['campos']->value['tes_nombre_director']);?>

		    			
		    			<?php if ($_smarty_tpl->tpl_vars['i']->index==0){?>
		    				<a id="agregar_director" name="agregar_director" class="btn btn-default" onClick="AgregarCamposDirector(this);">+</a>
		    			<?php }else{ ?>
		    				<a onclick="QuitarCampoDirector('director<?php echo $_smarty_tpl->tpl_vars['i']->index+1;?>
');" class="btn btn-default" name="agregar_director" id="agregar_director">-</a>
		    			<?php }?>
		    			
		    			<?php echo help_inline(form_error_msg("tes_nombre_director[".((string)$_smarty_tpl->tpl_vars['i']->value)."]"," "));?>

		   			</div >
		  		</div>
		  		
		  		<!-- Select Basic -->
		  		<div class="control-group <?php echo cls_error((("tes_id_grado_academico[").($_smarty_tpl->tpl_vars['grados_post']->value[$_smarty_tpl->tpl_vars['in']->value])).("]"));?>
">
		  			<label class="control-label" for="tes_id_grado_academico">Grado académico del Director:</label>
		  			<div class="controls">
		  				<?php echo form_dropdown("tes_id_grado_academico[]",$_smarty_tpl->tpl_vars['campos']->value['tes_id_grado_academico'][1],$_smarty_tpl->tpl_vars['grados_post']->value[$_smarty_tpl->tpl_vars['in']->value],$_smarty_tpl->tpl_vars['campos']->value['tes_id_grado_academico'][3]);?>

		    	    	<?php echo help_inline(form_error_msg((("tes_id_grado_academico[").($_smarty_tpl->tpl_vars['grados_post']->value[$_smarty_tpl->tpl_vars['in']->value])).("]")," "));?>

				    	</select>
		  			</div>
				</div>
		  	</div>
		  	
  			<?php } ?>
		<?php }else{ ?>
	
		<div id="director1">
			<!-- Text input-->
			<div class="control-group <?php echo cls_error("tes_nombre_director[]");?>
">
	  			<label class="control-label" for="tes_nombre_director">Directores de tesis:</label>
	  
	  			<div class="controls">
	    			<?php echo form_input($_smarty_tpl->tpl_vars['campos']->value['tes_nombre_director']);?>

	    			<a id="agregar_director" name="agregar_director" class="btn btn-default" onClick="AgregarCamposDirector(this);">+</a>
	    			<?php echo help_inline(form_error_msg("tes_nombre_director[]"," "));?>

	   			</div >
	  		</div>
	  		
			<!-- Select Basic -->
			<div class="control-group <?php echo cls_error("tes_id_grado_academico[]");?>
">
	  			<label class="control-label" for="tes_id_grado_academico">Grado académico del Director:</label>
	  			<div class="controls">
	    	    	<?php echo form_dropdown($_smarty_tpl->tpl_vars['campos']->value['tes_id_grado_academico'][0],$_smarty_tpl->tpl_vars['campos']->value['tes_id_grado_academico'][1],$_smarty_tpl->tpl_vars['campos']->value['tes_id_grado_academico'][2],$_smarty_tpl->tpl_vars['campos']->value['tes_id_grado_academico'][3]);?>

	    	    	<?php echo help_inline(form_error_msg("tes_id_grado_academico[]"," "));?>

			    	</select>
	  			</div>
			</div>
		</div>
	
	<?php }?>
	
	
		
	</div>
<?php echo help_inline(form_error_msg("tes_nombre_director[]"," "));?>

	<?php echo help_inline(form_error_msg("tes_id_grado_academico[]"," "));?>





<!-- Select Basic -->
<div class="control-group <?php echo cls_error("vista_subespecies");?>
">
  <label class="control-label" for="vista_subespecies">Subespecies:</label>
  <div class="controls">
    <?php echo $_smarty_tpl->tpl_vars['campos']->value['vista_subespecies'];?>

    </select>
    <span class="muted">Opcional </span>
  </div>
</div>



<!-- Select Basic -->
<div class="control-group <?php echo cls_error("con_id_condicion_sitio");?>
">
  <label class="control-label" for="con_id_condicion_sitio">Condición del sitio de estudio:</label>
  <div class="controls">
    <?php echo $_smarty_tpl->tpl_vars['campos']->value['con_id_condicion_sitio'];?>

    <?php echo help_inline(form_error_msg("con_id_condicion_sitio"," "));?>

    </select>
  </div>
</div>

<!-- Select Basic -->
<div class="control-group <?php echo cls_error("est_id_zona_estudio");?>
">
  <label class="control-label" for="est_id_zona_estudio">Zona de estudio:</label>
  <div class="controls">
 	<?php echo $_smarty_tpl->tpl_vars['campos']->value['est_id_zona_estudio'];?>

    <?php echo help_inline(form_error_msg("est_id_zona_estudio"," "));?>

  </select>
  </div>
</div>


<!-- Select Basic -->
<div class="control-group <?php echo cls_error("vista_subdisciplinas");?>
">
  <label class="control-label" for="vista_subdisciplinas">Subdisciplina de estudio:</label>
  <div class="controls">
    <?php echo $_smarty_tpl->tpl_vars['campos']->value['vista_subdisciplinas'];?>

    <?php echo help_inline(form_error_msg("vista_subdisciplinas"," "));?>

    </select>
    <span class="muted">Opcional </span>
  </div>
</div>


<!-- File Button --> 


<div>
	<label class="control-label" for="tes_ruta_tesis">Archivo PDF de la tesis:</label>
	<div class="controls">
		<button type="button" URL="<?php echo site_url();?>
/catalogos/tesis/editarTesisPDF/<?php echo $_smarty_tpl->tpl_vars['tesis']->value['tes_id_tesis'];?>
" class="btn btn-warning btn-mini" id="modal_editar_tesis" ><i class="icon-pencil icon-white"></i> Editar PDF</a>		
	</div>
</div>

<!-- Button -->
<!--div class="control-group">
  <label class="control-label" for="subir_archivo">  </label>
  <div class="controls">
    <button id="subir_archivo" name="subir_archivo" class="btn btn-primary"> + Adjuntar Archivo</button>
    
  </div>
</div-->




<!-- File Button --> 
<!--div class="control-group">
  <label class="control-label" for="subir_material_extra">Material extra:</label>
  <div class="controls">
    <input id="subir_material_extra" name="subir_material_extra" class="input-file" type="file">
    
  </div>
</div-->

<!-- Textarea -->
<!--div class="control-group">
  <label class="control-label" for="textarea"> </label>
  <div class="controls">                     
    <textarea id="textarea" name="textarea">Listado de material extra</textarea>
    <button id="agregar_material" name="agregar_material" class="btn btn-default">+</button>
    <button id="quitar_material" name="quitar_material" class="btn btn-default">- </button>
  </div>
</div-->


</fieldset>
<br>
<button id="editarTesis" URL="<?php echo site_url();?>
/catalogos/tesis/modificarTesis" type="button" class="btn btn-warning"><i class="icon-pencil icon-white"></i> Guardar</button>
</form>
<?php }} ?>