<?php /* Smarty version Smarty-3.1.12, created on 2013-09-25 16:48:23
         compiled from "/var/www/primat_trunk/application/views/catalogos/tesis/registrar_tesis.tpl" */ ?>
<?php /*%%SmartyHeaderCode:141553738352435a271cfd20-55533684%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '284cf47ad8faea43e002c85b41cdabf56638e207' => 
    array (
      0 => '/var/www/primat_trunk/application/views/catalogos/tesis/registrar_tesis.tpl',
      1 => 1377355956,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '141553738352435a271cfd20-55533684',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'campos' => 0,
    'this' => 0,
    'autores_post' => 0,
    'i' => 0,
    'directores_post' => 0,
    'grados_post' => 0,
    'in' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_52435a2736d358_89693338',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52435a2736d358_89693338')) {function content_52435a2736d358_89693338($_smarty_tpl) {?>﻿<form id="form_agregar" class="form-horizontal" method="post" ENCTYPE="multipart/form-data">
<fieldset>

<!-- Form Name -->
<legend>Registro de Tesis</legend>

<!-- Text input-->

 <ul class="thumbnails">

 <li class="span12">
              <div class="thumbnail">
                                  <div class="span12" style="margin-top: -13px">
                                    <span class="label  pull-left">Datos Generales:</span>  
                                  </div>
                                  <div class="control-group">


  <label class="control-label" for="tes_palabras_clave">Palabras claves:</label>
  <div class="controls">
    <input id="tes_palabras_clave" name="tes_palabras_clave" type="text" placeholder="Escriba las palabras claves separadas por coma" class="input-xxlarge">
    <span class="muted">Opcional </span>
  </div>
</div>


<div class="control-group <?php echo cls_error("tes_nombre");?>
">
  <label class="control-label" for="tes_nombre">Título:</label>
  <div class="controls">
    <?php echo form_input($_smarty_tpl->tpl_vars['campos']->value['tes_nombre']);?>

    <?php echo help_inline(form_error_msg("tes_nombre"," "));?>

  </div>
</div>
<!-- Textarea -->
<div class="control-group <?php echo cls_error("tes_resumen");?>
">
  <label class="control-label" for="tes_resumen">Resumen:</label>
  <div class="controls">                     
    <?php echo form_textarea($_smarty_tpl->tpl_vars['campos']->value['tes_resumen']);?>

    <?php echo help_inline(form_error_msg("tes_resumen"," "));?>

  </div>
</div>

<!-- Select Basic -->
<div class="control-group <?php echo cls_error("gra_id_grado_academico");?>
">
  <label class="control-label" for="gra_id_grado_academico">Grado obtenido:</label>
  <div class="controls">
    <?php echo $_smarty_tpl->tpl_vars['campos']->value['gra_id_grado_academico'];?>

    <?php echo help_inline(form_error_msg("gra_id_grado_academico"," "));?>

    </select>
  </div>
</div>
		

<!-- Text input-->
<div class="control-group <?php echo cls_error("tes_anio_titulacion");?>
">
  <label class="control-label" for="tes_anio_titulacion">Año de titulación:</label>
  <div class="controls">
    <?php echo form_input($_smarty_tpl->tpl_vars['campos']->value['tes_anio_titulacion']);?>

    <?php echo help_inline(form_error_msg("tes_anio_titulacion"," "));?>

    
  </div>
</div>


<!-- Select Basic -->
<div class="control-group <?php echo cls_error("tes_id_institucion");?>
">
  <label class="control-label" for="tes_id_institucion">Institución de adscripción:</label>
  <div class="controls">
    <?php echo $_smarty_tpl->tpl_vars['campos']->value['tes_id_institucion'];?>

    <?php echo help_inline(form_error_msg("tes_id_institucion"," "));?>

    </select>
  </div>
</div>






                               
                              </div>


 </li> 	

 <li class="span12">
                              <div class="thumbnail">
                                  <div class="span12" style="margin-top: -13px">
                                    <span class="label  pull-left">Autor(es):</span>  
                                  </div>
                                  <?php if ($_smarty_tpl->tpl_vars['this']->value->input->post("tes_nombre_autor")){?>
	<?php $_smarty_tpl->tpl_vars['autores_post'] = new Smarty_variable($_smarty_tpl->tpl_vars['this']->value->input->post("tes_nombre_autor"), null, 0);?>
	
	<!-- Text input-->
	<div class="control-group <?php echo cls_error("tes_nombre_autor[]");?>
">
	  
	  <label class="control-label" for="tes_nombre_autor">Autor:</label>
	  <div class="controls" id="camposAutores">
	    <div id="autor1">
		<?php $_smarty_tpl->createLocalArrayVariable('campos', null, 0);
$_smarty_tpl->tpl_vars['campos']->value['tes_nombre_autor']['value'] = $_smarty_tpl->tpl_vars['autores_post']->value[0];?>
	    <?php echo form_input($_smarty_tpl->tpl_vars['campos']->value['tes_nombre_autor']);?>

	    <a id="agregar_autor" name="agregar_autor" class="btn btn-default" onClick="AgregarCamposAutor(this);">+</a>
		
	    </div>
	    
	    <?php  $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['i']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['autores_post']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['i']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['i']->key => $_smarty_tpl->tpl_vars['i']->value){
$_smarty_tpl->tpl_vars['i']->_loop = true;
 $_smarty_tpl->tpl_vars['i']->index++;
?>
	    	<?php if ($_smarty_tpl->tpl_vars['i']->index!=0){?>
	    		<div id="autor<?php echo $_smarty_tpl->tpl_vars['i']->index+1;?>
">
				<?php $_smarty_tpl->createLocalArrayVariable('campos', null, 0);
$_smarty_tpl->tpl_vars['campos']->value['tes_nombre_autor']['value'] = $_smarty_tpl->tpl_vars['i']->value;?>
			    <?php echo form_input($_smarty_tpl->tpl_vars['campos']->value['tes_nombre_autor']);?>

				<a onclick="QuitarCampoAutor('autor<?php echo $_smarty_tpl->tpl_vars['i']->index+1;?>
');" class="btn btn-default" name="agregar_autor" id="agregar_autor">-</a>			    
				
			    </div>
	    	<?php }?>
	    <?php } ?>
	    
	  </div>
	  <?php echo help_inline(form_error_msg("tes_nombre_autor[]"," "));?>

	</div>
	
<?php }else{ ?>
	
	<!-- Text input-->
	<div class="control-group <?php echo cls_error("tes_nombre_autor[]");?>
">
	  <label class="control-label" for="tes_nombre_autor">Autor:</label>
	  <div class="controls" id="camposAutores">
	    <div id="autor1">
	    <?php echo form_input($_smarty_tpl->tpl_vars['campos']->value['tes_nombre_autor']);?>

	    <a id="agregar_autor" name="agregar_autor" class="btn btn-default" onClick="AgregarCamposAutor(this);">+</a>
		<?php echo help_inline(form_error_msg("tes_nombre_autor[]"," "));?>

	    </div>
	  </div>
	</div>

<?php }?>

                               
                              </div>
                            </li>








 <li class="span12">
                              <div class="thumbnail">
                                  <div class="span12" style="margin-top: -13px">
                                    <span class="label  pull-left">Director(es):</span>  
                                  </div>
                                  <div class="control-group <?php echo cls_error("tes_id_grado_academico[]");?>
">
	<div class="control-group <?php echo cls_error("tes_nombre_director[]");?>
">
<div id="camposDirectores">
	
	<?php if ($_smarty_tpl->tpl_vars['this']->value->input->post("tes_nombre_director")||$_smarty_tpl->tpl_vars['this']->value->input->post("tes_id_grado_academico")){?>
		<?php $_smarty_tpl->tpl_vars['directores_post'] = new Smarty_variable($_smarty_tpl->tpl_vars['this']->value->input->post("tes_nombre_director"), null, 0);?>
		<?php $_smarty_tpl->tpl_vars['grados_post'] = new Smarty_variable($_smarty_tpl->tpl_vars['this']->value->input->post("tes_id_grado_academico"), null, 0);?>
		
		<div id="director1">
				<!-- Text input-->
				<div class="control-group <?php echo cls_error("tes_nombre_director[]");?>
">
		  			<label class="control-label" for="tes_nombre_director">Director de tesis:</label>
		  
		  			<div class="controls">
		  				<?php $_smarty_tpl->createLocalArrayVariable('campos', null, 0);
$_smarty_tpl->tpl_vars['campos']->value['tes_nombre_director']['value'] = $_smarty_tpl->tpl_vars['directores_post']->value[0];?>
		    			<?php echo form_input($_smarty_tpl->tpl_vars['campos']->value['tes_nombre_director']);?>

		    			 


	    				<a id="agregar_director" name="agregar_director" class="btn btn-default" onClick="AgregarCamposDirector(this);">+</a>
		    			
		   			</div >
		  		</div>
		
				<!-- Select Basic -->
				<div class="control-group <?php echo cls_error("tes_id_grado_academico[]");?>
">
		  			<label class="control-label" for="tes_id_grado_academico">Grado académico del Director:</label>
		  			<div class="controls">
		  				<?php echo form_dropdown("tes_id_grado_academico[]",$_smarty_tpl->tpl_vars['campos']->value['tes_id_grado_academico'][1],$_smarty_tpl->tpl_vars['grados_post']->value[0],$_smarty_tpl->tpl_vars['campos']->value['tes_id_grado_academico'][3]);?>

		    	    	
				    	</select>
		  			</div>
				</div>
			</div>
		
		<?php  $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['i']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['this']->value->input->post("tes_nombre_director"); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['i']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['i']->key => $_smarty_tpl->tpl_vars['i']->value){
$_smarty_tpl->tpl_vars['i']->_loop = true;
 $_smarty_tpl->tpl_vars['i']->index++;
?>
		<?php if ($_smarty_tpl->tpl_vars['i']->index!=0){?>
			<?php $_smarty_tpl->tpl_vars['in'] = new Smarty_variable($_smarty_tpl->tpl_vars['i']->index, null, 0);?>
			
			<div id="director<?php echo $_smarty_tpl->tpl_vars['i']->index+1;?>
">
				<!-- Text input-->
				<div class="control-group <?php echo cls_error("tes_nombre_director[]");?>
">
		  			<label class="control-label" for="tes_nombre_director">Director de tesis:</label>
		  
		  			<div class="controls">
		  				<?php $_smarty_tpl->createLocalArrayVariable('campos', null, 0);
$_smarty_tpl->tpl_vars['campos']->value['tes_nombre_director']['value'] = $_smarty_tpl->tpl_vars['directores_post']->value[$_smarty_tpl->tpl_vars['in']->value];?>
		    			<?php echo form_input($_smarty_tpl->tpl_vars['campos']->value['tes_nombre_director']);?>

		    			<a onclick="QuitarCampoDirector('director<?php echo $_smarty_tpl->tpl_vars['i']->index+1;?>
');" class="btn btn-default" name="agregar_director" id="agregar_director">-</a>
		    			
		    		</div >
		  		</div>
		
				<!-- Select Basic -->
				<div class="control-group <?php echo cls_error("tes_id_grado_academico[]");?>
">
		  			<label class="control-label" for="tes_id_grado_academico">Grado académico del Director:</label>
		  			<div class="controls">
		  				<?php echo form_dropdown("tes_id_grado_academico[]",$_smarty_tpl->tpl_vars['campos']->value['tes_id_grado_academico'][1],$_smarty_tpl->tpl_vars['grados_post']->value[$_smarty_tpl->tpl_vars['in']->value],$_smarty_tpl->tpl_vars['campos']->value['tes_id_grado_academico'][3]);?>

		    	    	</select>
		  			</div>
				</div>
			</div>
				<?php }?>
		<?php } ?>
	<?php }else{ ?>
	
		<div id="director1">
			<!-- Text input-->
			<div class="control-group <?php echo cls_error("tes_nombre_director[]");?>
">
	  			<label class="control-label" for="tes_nombre_director">Directores de tesis:</label>
	  
	  			<div class="controls">
	    			<?php echo form_input($_smarty_tpl->tpl_vars['campos']->value['tes_nombre_director']);?>

	    			<a id="agregar_director" name="agregar_director" class="btn btn-default" onClick="AgregarCamposDirector(this);">+</a>
	    			<?php echo help_inline(form_error_msg("tes_nombre_director[]"," "));?>

	   			</div >
	  		</div>
	
			<!-- Select Basic -->
			<div class="control-group <?php echo cls_error("tes_id_grado_academico[]");?>
">
	  			<label class="control-label" for="tes_id_grado_academico">Grado académico del Director:</label>
	  			<div class="controls">
	    	    	<?php echo form_dropdown($_smarty_tpl->tpl_vars['campos']->value['tes_id_grado_academico'][0],$_smarty_tpl->tpl_vars['campos']->value['tes_id_grado_academico'][1],$_smarty_tpl->tpl_vars['campos']->value['tes_id_grado_academico'][2],$_smarty_tpl->tpl_vars['campos']->value['tes_id_grado_academico'][3]);?>

	    	    	<?php echo help_inline(form_error_msg("tes_id_grado_academico[]"," "));?>

			    	</select>
	  			</div>
			</div>
		</div>
		
	<?php }?>

	
	
	
</div>
   <?php echo help_inline(form_error_msg("tes_nombre_director[]"," "));?>

	<?php echo help_inline(form_error_msg("tes_id_grado_academico[]"," "));?>

	</div>
	</div>
                               
                              </div>
                            </li>
 <li class="span12">
                              <div class="thumbnail">
                                  <div class="span12" style="margin-top: -13px">
                                    <span class="label  pull-left">Datos de estudio:</span>  
                                  </div>
                                  <!-- Select Basic -->
<div class="control-group <?php echo cls_error("vista_subespecies");?>
">
  <label class="control-label" for="vista_subespecies">Especies:</label>
  <div class="controls">
    <?php echo $_smarty_tpl->tpl_vars['campos']->value['vista_subespecies'];?>

    </select>
    <span class="muted">Opcional </span>
  </div>
</div>


   
<!-- Select Basic -->
<div class="control-group <?php echo cls_error("con_id_condicion_sitio");?>
">
  <label class="control-label" for="con_id_condicion_sitio">Condición del sitio de estudio:</label>
  <div class="controls">
    <?php echo $_smarty_tpl->tpl_vars['campos']->value['con_id_condicion_sitio'];?>

    <?php echo help_inline(form_error_msg("con_id_condicion_sitio"," "));?>

    </select>
  </div>
</div>

<!-- Select Basic -->
<div class="control-group <?php echo cls_error("est_id_zona_estudio");?>
">
  <label class="control-label" for="est_id_zona_estudio">Zona de estudio:</label>
  <div class="controls">
 	<?php echo $_smarty_tpl->tpl_vars['campos']->value['est_id_zona_estudio'];?>

    <?php echo help_inline(form_error_msg("est_id_zona_estudio"," "));?>

  </select>
  </div>
</div>



<!-- Select Basic -->
<div class="control-group <?php echo cls_error("id_vista_subdisciplinas");?>
">
  <label class="control-label" for="vista_subdisciplinas">Disciplina de estudio:</label>
  <div class="controls">
    <?php echo $_smarty_tpl->tpl_vars['campos']->value['vista_subdisciplinas'];?>

    <?php echo help_inline(form_error_msg("id_vista_subdisciplinas"," "));?>

    </select>
    <span class="muted">Opcional </span>
  </div>
</div>


                               
                              </div>
                            </li>



 <li class="span12">
                              <div class="thumbnail">
                                  <div class="span12" style="margin-top: -13px">
                                    <span class="label  pull-left">Archivos</span>  
                                  </div>
                                  <!-- File Button --> 
<div class="control-group <?php echo cls_error("tes_ruta_tesis");?>
">
  <label class="control-label" for="tes_ruta_tesis">Archivo PDF de la tesis</label>
  <div class="controls">
    <!--input id="tes_ruta_tesis" name="tes_ruta_tesis" class="input-file" type="file" accept="application/pdf">-->
    <?php echo form_input($_smarty_tpl->tpl_vars['campos']->value['tes_ruta_tesis']);?>

    </br>
    <?php echo help_inline(form_error_msg("tes_ruta_tesis"," "));?>

  </div>
</div>
                               
                              </div>
                            </li>
                            <li class="span12" >
                                <div class="thumbnail">    
                                    <div class="span12" style="margin-top: -13px">
                                        <span class="label  pull-left">Material Extra:</span>  
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="subir_material_extra">Imágenes, videos y documentos:</label>
                                        <div class="controls">
                                            <button id="material_extra" name="material_extra" type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i> Material Extra</button>
                                            <span class="muted"> Opcional </span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="span12">
                                <div class="thumbnail">
                                    <button id="agregar" name="agregar" type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i> Guardar</button>
                                </div>
                            </li>
                     </ul>
    </fieldset>
</form><?php }} ?>