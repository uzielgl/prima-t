<?php /* Smarty version Smarty-3.1.12, created on 2013-08-24 04:31:48
         compiled from "/var/www/primat_trunk/application/views/busqueda_avanzada/tabla.tpl" */ ?>
<?php /*%%SmartyHeaderCode:56937368452187d84106612-41118495%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '66feb7bf4f89ab2326effac33547cb79ad044d58' => 
    array (
      0 => '/var/www/primat_trunk/application/views/busqueda_avanzada/tabla.tpl',
      1 => 1377355956,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '56937368452187d84106612-41118495',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'tesis' => 0,
    'i' => 0,
    'this' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_52187d84149944_24506814',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52187d84149944_24506814')) {function content_52187d84149944_24506814($_smarty_tpl) {?><table class="table table-striped table-bordered">
	<thead>
		<tr class="order">
			<th data-field="tes_nombre" data-type="<?php echo ter(set_value("order_by")=="tes_nombre",set_value("order_type"));?>
" >
				<div class="filter <?php echo set_cls_order("tes_nombre");?>
">
					<span>Resultados de búsqueda</span> 
				</div>
			</th>
		</tr>
	</thead>
	<tbody>
	    <?php  $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['i']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tesis']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['i']->key => $_smarty_tpl->tpl_vars['i']->value){
$_smarty_tpl->tpl_vars['i']->_loop = true;
?>
    		<tr>
    			<td >
    				<strong><?php echo $_smarty_tpl->tpl_vars['i']->value['tes_nombre'];?>
</strong> <br/>
    				Autores:<?php echo substr($_smarty_tpl->tpl_vars['i']->value['autores'],0,strlen($_smarty_tpl->tpl_vars['i']->value['autores'])-2);?>
 <br/>
    				Directores:<?php echo $_smarty_tpl->tpl_vars['i']->value['directores'];?>
 <br/>
    				Grado académico: <?php echo $_smarty_tpl->tpl_vars['i']->value['gra_descripcion'];?>
 <br/>
    				<a href="#btn_abstract" id_tesis="<?php echo $_smarty_tpl->tpl_vars['i']->value['tes_id_tesis'];?>
" class="btn btn-mini btn-action"><i class="icon-black icon-eye-open"></i> Resumen</a> 
    				<?php if (($_smarty_tpl->tpl_vars['this']->value->ion_auth->logged_in())){?>
    				<a class="btn btn-mini btn-action" href= "<?php echo site_url();?>
/test/ver_tesis/<?php echo $_smarty_tpl->tpl_vars['i']->value['tes_id_tesis'];?>
" ><i class="icon-black icon-eye-open"></i> Tesis completa</a>
 					<?php }else{ ?>
 					<a class="btn btn-mini btn-action" href="#modal_editar"  ><i class="icon-black icon-eye-open"></i> Tesis completa</a>
 					<?php }?>
 					
 					
 					<p style="display:none" id="abstract_<?php echo $_smarty_tpl->tpl_vars['i']->value['tes_id_tesis'];?>
"><?php echo sprintf("Resumen: %s",$_smarty_tpl->tpl_vars['i']->value['tes_resumen']);?>
</p>
 					 			
    			</td>
    		</tr>
		<?php } ?>
	</tbody>
</table><?php }} ?>