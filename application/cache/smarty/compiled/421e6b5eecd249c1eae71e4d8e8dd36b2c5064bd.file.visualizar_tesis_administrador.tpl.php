<?php /* Smarty version Smarty-3.1.12, created on 2013-12-03 05:53:11
         compiled from "/var/www/primat_trunk/application/views/catalogos/tesis/visualizar_tesis_administrador.tpl" */ ?>
<?php /*%%SmartyHeaderCode:324471785529dc627c2cb25-29572903%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '421e6b5eecd249c1eae71e4d8e8dd36b2c5064bd' => 
    array (
      0 => '/var/www/primat_trunk/application/views/catalogos/tesis/visualizar_tesis_administrador.tpl',
      1 => 1377355956,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '324471785529dc627c2cb25-29572903',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'tes_ruta_tesis' => 0,
    'tes_nombre' => 0,
    'tes_resumen' => 0,
    'tes_palabras_clave' => 0,
    'tes_autores' => 0,
    'autor' => 0,
    'tes_grado_obtenido' => 0,
    'tes_anio_titulacion' => 0,
    'tes_ins_nombre' => 0,
    'tes_directores' => 0,
    'director' => 0,
    'tes_esp_sub_especie' => 0,
    'sub_especie' => 0,
    'tes_condicion_sitio' => 0,
    'condicion' => 0,
    'tes_zonas_estudio' => 0,
    'zona' => 0,
    'tes_sub_disciplina' => 0,
    'sub_disciplina' => 0,
    'id_tesis' => 0,
    'tes_fecha_creacion' => 0,
    'tes_usuario_crea' => 0,
    'tes_fecha_modificacion' => 0,
    'tes_usuario_mod' => 0,
    'tes_num_visitas' => 0,
    'tes_num_descargas' => 0,
    'prom_cal' => 0,
    'cantidad_imagenes' => 0,
    'cantidad_videos' => 0,
    'cantidad_documentos' => 0,
    'cantidad_audios' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_529dc627d4c317_20710453',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_529dc627d4c317_20710453')) {function content_529dc627d4c317_20710453($_smarty_tpl) {?>﻿       
  <html lang='es'>
  <head>  
    
  </head>
<body>             
 <div class="container-fluid" >
 <?php echo $_smarty_tpl->getSubTemplate ("modals/modal_tesis_videos.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

     <?php echo $_smarty_tpl->getSubTemplate ("modals/modal_tesis_documentos.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

     <?php echo $_smarty_tpl->getSubTemplate ("modals/modal_tesis_audios.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

     <?php echo $_smarty_tpl->getSubTemplate ("modals/modal_tesis_imagenes.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

     <div class="row-fluid">
       <h3 class="text-success" style="margin-bottom: 30px">Visualizar Tesis Administrador</h3>
           <div class="row-fluid">
                <div class="span12">
                     <div class="span9">
                       <!--   <div class="row-fluid" style="margin-bottom: 20px">
                               <div class="span12" >
                                    <iframe style="width:98%; height:650px" src=<?php echo site_url();?>
<?php echo sprintf("themes/default/pdf/%s",$_smarty_tpl->tpl_vars['tes_ruta_tesis']->value);?>
 ></iframe>
                                </div>
                          </div>
                          -->
                           <div class="row-fluid" >
                                <ul class="thumbnails" >
                                    <li  class="span12" style="margin-bottom: 0px" >
                                        <div class="thumbnail">
                                            <div class="row-fluid" >
                                                <div class="span12" style="margin-top: -13px">
                                                  <span class="label  pull-left">Datos Generales</span>    
                                                </div>
                                                <div class="span12" >
                                                 <p> <h5>Titulo:</h5></p>
                                                 <div class="span11" >
                                             		<p> <?php echo sprintf("<h7>%s</h7>",$_smarty_tpl->tpl_vars['tes_nombre']->value);?>
 </p>
                                            	 </div>
                                                <p> <h5>Resumen:</h5></p>
                                                 <div class="span11" >
                                             		<p> <?php echo sprintf("<h7>%s</h7>",$_smarty_tpl->tpl_vars['tes_resumen']->value);?>
 </p>
                                            	 </div>
                                                 </div>
                                                 
												<div class="span12" >
                                           <!-- 	<p> <?php echo sprintf("<h5>Titulo:</h5> %s",$_smarty_tpl->tpl_vars['tes_nombre']->value);?>
</p> -->
                                             	<p> <?php echo sprintf("<h5>Palabras claves:</h5> %s",$_smarty_tpl->tpl_vars['tes_palabras_clave']->value);?>
</p>
                                             	<p> <h5>Autor(es):</h5></p>
                                        		 <?php  $_smarty_tpl->tpl_vars['autor'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['autor']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tes_autores']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['autor']->key => $_smarty_tpl->tpl_vars['autor']->value){
$_smarty_tpl->tpl_vars['autor']->_loop = true;
?>
                                            	<p> <?php echo sprintf(" <h7 >%s</h7 >",$_smarty_tpl->tpl_vars['autor']->value);?>
</p>
                                            	<?php } ?>
                                          <!--  <p> <h5>Resumen:</h5></p>
                                             <div class="span11" >
                                             <p> <?php echo sprintf("<h7>%s</h7>",$_smarty_tpl->tpl_vars['tes_resumen']->value);?>
 </p>
                                             </div>
                                        -->     
                                               <p> <?php echo sprintf("<h5>Grado:</h5> <h7>%s</h7>",$_smarty_tpl->tpl_vars['tes_grado_obtenido']->value);?>
 </p>
                                               <p> <?php echo sprintf("<h5>Año:</h5><h7>%s</h7>",$_smarty_tpl->tpl_vars['tes_anio_titulacion']->value);?>
 </p>
                                            	<p> <?php echo sprintf("<h5>Institución:</h5> <h7>%s</h7>",$_smarty_tpl->tpl_vars['tes_ins_nombre']->value);?>
 </p>
                                                                                                                     
												</div>
												
									     </div>
										</div>
										
										
										<div class="thumbnail">
                                            <div class="row-fluid" >
                                                <div class="span12" style="margin-top: -13px">
                                                  <span class="label  pull-left">Director(es)</span>    
                                                </div>
												<div class="span12" >
                                            	<p> <h5>Director(es):</h5></p>
                                            	 <?php  $_smarty_tpl->tpl_vars['director'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['director']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tes_directores']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['director']->key => $_smarty_tpl->tpl_vars['director']->value){
$_smarty_tpl->tpl_vars['director']->_loop = true;
?>
                                                <p> <?php echo sprintf(" <h7 >%s</h7 >",$_smarty_tpl->tpl_vars['director']->value);?>
</p>
                                                 <?php } ?>                          
												</div>
																						
									     </div>
										</div>
										
										<div class="thumbnail">
                                            <div class="row-fluid" >
                                                <div class="span12" style="margin-top: -13px">
                                                  <span class="label  pull-left">Datos de Estudio</span>    
                                                </div>
												<div class="span12" >
                                            	<p> <h5>Subespecies:</h5></p>
                                            	 <?php  $_smarty_tpl->tpl_vars['sub_especie'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sub_especie']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tes_esp_sub_especie']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['sub_especie']->key => $_smarty_tpl->tpl_vars['sub_especie']->value){
$_smarty_tpl->tpl_vars['sub_especie']->_loop = true;
?>
                                                <p> <?php echo sprintf(" <h7 >%s</h7 >",$_smarty_tpl->tpl_vars['sub_especie']->value);?>
</p>
                                                 <?php } ?>  
                                                 <p> <h5>Condición del sitio de estudio:</h5></p>
                                            	 <?php  $_smarty_tpl->tpl_vars['condicion'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['condicion']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tes_condicion_sitio']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['condicion']->key => $_smarty_tpl->tpl_vars['condicion']->value){
$_smarty_tpl->tpl_vars['condicion']->_loop = true;
?>
                                                <p> <?php echo sprintf(" <h7 >%s</h7 >",$_smarty_tpl->tpl_vars['condicion']->value);?>
</p>
                                                 <?php } ?>
                                                 <p> <h5>Zona de estudio:</h5></p>
                                            	 <?php  $_smarty_tpl->tpl_vars['zona'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['zona']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tes_zonas_estudio']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['zona']->key => $_smarty_tpl->tpl_vars['zona']->value){
$_smarty_tpl->tpl_vars['zona']->_loop = true;
?>
                                                <p> <?php echo sprintf(" <h7 >%s</h7 >",$_smarty_tpl->tpl_vars['zona']->value);?>
</p>
                                                 <?php } ?>  
                                                  <p> <h5>Subdisciplinas de estudios:</h5></p>
                                            	 <?php  $_smarty_tpl->tpl_vars['sub_disciplina'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sub_disciplina']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tes_sub_disciplina']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['sub_disciplina']->key => $_smarty_tpl->tpl_vars['sub_disciplina']->value){
$_smarty_tpl->tpl_vars['sub_disciplina']->_loop = true;
?>
                                                <p> <?php echo sprintf(" <h7 >%s</h7 >",$_smarty_tpl->tpl_vars['sub_disciplina']->value);?>
</p>
                                                 <?php } ?>   
                                                                                                                        
												</div>
												
									     </div>
										</div>
										
										
										
										
										
                                    </li>
                                </ul>
                            </div>
                            
                            <div class="row-fluid" style="margin-bottom: 20px">
                               <div class="span12" >
                                    <iframe style="width:98%; height:650px" src=<?php echo base_url();?>
<?php echo sprintf("themes/default/pdf/%s",$_smarty_tpl->tpl_vars['tes_ruta_tesis']->value);?>
 ></iframe>
                                </div>
                          </div>
                           
                        </div>
                        
                        
                        <div class="span3" >
                            <ul class="thumbnails">
                                <li  class="span12" >
                                    <div class="thumbnail" style="margin-bottom: 20px">
                                        <div class="row-fluid" >
                                            <div class="span12" style="margin-top: -15px">
                                                <span class="label  pull-left">Otros datos</span>  
                                            </div>
                                            <div class="span12" >
                                            	 <p> <?php echo sprintf("<h5>Identificador de la tesis:</h5> <h7>%s</h7>",$_smarty_tpl->tpl_vars['id_tesis']->value);?>
 </p>
                                            	<p> <?php echo sprintf("<h5>Fecha de creación:</h5> %s",$_smarty_tpl->tpl_vars['tes_fecha_creacion']->value);?>
</p>
                                            	<p> <?php echo sprintf("<h5>Usuario de creación:</h5> <h7>%s</h7>",$_smarty_tpl->tpl_vars['tes_usuario_crea']->value);?>
 </p>
                                                <p> <?php echo sprintf("<h5>Ultima modificación:</h5> <h7 >%s</h7 >",$_smarty_tpl->tpl_vars['tes_fecha_modificacion']->value);?>
</p>
                                                <p> <?php echo sprintf("<h5>Usuario de modificación:</h5><h7>%s</h7>",$_smarty_tpl->tpl_vars['tes_usuario_mod']->value);?>
 </p>
                                               
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="thumbnail" style="margin-bottom: 20px">
                                        <div class="row-fluid" >
                                            <div class="span12" style="margin-top: -15px">
                                                <span class="label  pull-left">Estadísticas</span>  
                                            </div>
                                             <p> <?php echo sprintf("<h5>Número de visitas:</h5> %s",$_smarty_tpl->tpl_vars['tes_num_visitas']->value);?>
</p>
                                            <!-- <p> <?php echo sprintf("<h5>Número de descargas:</h5> %s",$_smarty_tpl->tpl_vars['tes_num_descargas']->value);?>
</p>-->
                                             <p> <?php echo sprintf("<h5>Promedio de calificación:</h5> <h7>%s</h7>",$_smarty_tpl->tpl_vars['prom_cal']->value);?>
 </p>
											
                                          </div>
                                                          
                                    </div>
                                  <div class="thumbnail" style="margin-bottom: 20px">
                                        <div class="row-fluid" >
                                            <div class="span12" style="margin-top: -15px">
                                                <span class="label  pull-left">Material extra</span>  
                                            </div>
                                             <div class="span12 text-left">
                                                <a id_tesis="<?php echo $_smarty_tpl->tpl_vars['id_tesis']->value;?>
" class="btn btn-info" onclick="modalMostrarImagenes(this);"> Imágenes (<?php echo $_smarty_tpl->tpl_vars['cantidad_imagenes']->value;?>
)</a></br>
                                                </br>
                                                <a id_tesis="<?php echo $_smarty_tpl->tpl_vars['id_tesis']->value;?>
" class="btn btn-info" onclick="modalMostrarVideos(this);"> Videos (<?php echo $_smarty_tpl->tpl_vars['cantidad_videos']->value;?>
)</a></br>
                                                </br>
                                                <a id_tesis="<?php echo $_smarty_tpl->tpl_vars['id_tesis']->value;?>
" class="btn btn-info" onclick="modalMostrarDocumentos(this);"> Documentos (<?php echo $_smarty_tpl->tpl_vars['cantidad_documentos']->value;?>
)</a></br>
                                                </br>
                                                <a id_tesis="<?php echo $_smarty_tpl->tpl_vars['id_tesis']->value;?>
" class="btn btn-info" onclick="modalMostrarAudios(this);"> Audios (<?php echo $_smarty_tpl->tpl_vars['cantidad_audios']->value;?>
)</a></br>
                                            </div>
                                          </div>
                                                          
                                    </div>
                                    
                                    
									
                                </li>
                            </ul>
                        </div>
                        
                        
                        

					
						
                    </div>
                </div>
            </div>
      </div>
        
  </body>
</html><?php }} ?>