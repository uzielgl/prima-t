<?php /* Smarty version Smarty-3.1.12, created on 2013-08-24 07:57:01
         compiled from "/home/primat_user/primat_trunk/application/views/tpl/page.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16707164245218c9bd150562-63054894%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '693e4b68303d8e47aafcd7257871daabb504ea3d' => 
    array (
      0 => '/home/primat_user/primat_trunk/application/views/tpl/page.tpl',
      1 => 1377355956,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '16707164245218c9bd150562-63054894',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'tag_title' => 0,
    'css' => 0,
    'i' => 0,
    'analytics' => 0,
    'subview' => 0,
    'js' => 0,
    'this' => 0,
    'msg_ok' => 0,
    'msg_error' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5218c9bd22c4c7_69835825',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5218c9bd22c4c7_69835825')) {function content_5218c9bd22c4c7_69835825($_smarty_tpl) {?><!DOCTYPE html>
<html lang="es">
	<head>
	    <meta charset='utf-8'>
        <title><?php echo $_smarty_tpl->tpl_vars['tag_title']->value;?>
</title>
        
		<meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<script type="text/javascript">
			//Global variables
			var b_url = "<?php echo base_url();?>
";
			var s_url = "<?php echo site_url();?>
";
		</script>
		
		<?php  $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['i']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['css']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['i']->key => $_smarty_tpl->tpl_vars['i']->value){
$_smarty_tpl->tpl_vars['i']->_loop = true;
?>
		<?php echo css($_smarty_tpl->tpl_vars['i']->value);?>

		<?php } ?>

		
		<!--[if lt IE 9]><?php echo css("ielt9.css");?>
<![endif]-->
		
		<link rel="icon" href="<?php echo base_url();?>
themes/default/img/favicon.png" type="image/gif">
		
		<?php if ($_smarty_tpl->tpl_vars['analytics']->value==true){?>
			<?php echo google_analytics();?>

		<?php }?>
	</head>
	<body >
	    <div class="container-fluid"> 
		    <?php echo $_smarty_tpl->getSubTemplate ("tpl/top.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

		    <div id="body" >
			<?php if (isset($_smarty_tpl->tpl_vars['subview']->value)){?>
			    <?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['subview']->value).".tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

			<?php }?>
			</div>
    		<?php echo $_smarty_tpl->getSubTemplate ("tpl/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    		
    		<?php  $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['i']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['js']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['i']->key => $_smarty_tpl->tpl_vars['i']->value){
$_smarty_tpl->tpl_vars['i']->_loop = true;
?>
            <?php echo js($_smarty_tpl->tpl_vars['i']->value);?>

            <?php } ?>
        
        
    		<script type="text/javascript">
    		    $(function(){
    		       <?php if ($_smarty_tpl->tpl_vars['this']->value->session->flashdata("msg.ok")){?>
    		          msg.ok("<?php echo $_smarty_tpl->tpl_vars['this']->value->session->flashdata("msg.ok");?>
");
    		       <?php }?>
    		       <?php if ($_smarty_tpl->tpl_vars['this']->value->session->flashdata("msg.error")){?>
                      msg.error("<?php echo $_smarty_tpl->tpl_vars['this']->value->session->flashdata("msg.error");?>
");
                   <?php }?>
                   
                   
                   <?php if (isset($_smarty_tpl->tpl_vars['msg_ok']->value)){?>
                      msg.ok("<?php echo $_smarty_tpl->tpl_vars['msg_ok']->value;?>
");
                   <?php }?>
                   <?php if (isset($_smarty_tpl->tpl_vars['msg_error']->value)){?>
                      msg.error("<?php echo $_smarty_tpl->tpl_vars['msg_error']->value;?>
");
                   <?php }?>
                   
    		    });
    		</script>
		</div>
	</body>
</html><?php }} ?>