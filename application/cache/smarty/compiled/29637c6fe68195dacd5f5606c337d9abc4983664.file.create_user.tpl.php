<?php /* Smarty version Smarty-3.1.12, created on 2013-08-24 05:24:31
         compiled from "/var/www/primat_trunk/application/views/auth/create_user.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2042639713521889df02bf60-77111480%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '29637c6fe68195dacd5f5606c337d9abc4983664' => 
    array (
      0 => '/var/www/primat_trunk/application/views/auth/create_user.tpl',
      1 => 1377355956,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2042639713521889df02bf60-77111480',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'message' => 0,
    'email' => 0,
    'password' => 0,
    'email_confirm' => 0,
    'password_confirm' => 0,
    'first_name' => 0,
    'grado_academico' => 0,
    'fecha_nacimiento' => 0,
    'pais' => 0,
    'facebook' => 0,
    'last_name' => 0,
    'ocupacion' => 0,
    'genero' => 0,
    'ciudad' => 0,
    'company' => 0,
    'phone' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_521889df14d8d8_58189420',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_521889df14d8d8_58189420')) {function content_521889df14d8d8_58189420($_smarty_tpl) {?><h3 class="text-success"><?php echo lang('create_user_heading');?>
</h1>
    
<!-- <p><?php echo lang('create_user_subheading');?>
</p> -->

<div id="infoMessage"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</div>

<?php echo form_open("auth/create_user");?>


    
    <div class="thumbnail form-horizontal">
        <div style="margin-top: -13px" class="span12">
            <span class="label pull-left">Datos de Acceso</span>  
        </div>
        
        <div class="row-fluid">
            
            <!-- Columna 1 -->
            <div class="span6">
				<div class="clearfix" ></div>
                <!-- Text input-->
                <div class="control-group <?php echo cls_error("email");?>
">
                  <label class="control-label" for="textinput">Correo electrónico</label>
                  <div class="controls">
                    <?php echo form_input($_smarty_tpl->tpl_vars['email']->value);?>

                    <?php echo help_inline(form_error_msg("email"," "));?>

                  </div>
                </div>
                
                
                <div class="control-group <?php echo cls_error("password");?>
">
                  <label class="control-label" for="textinput">Contraseña</label>
                  <div class="controls">
                    <?php echo form_input($_smarty_tpl->tpl_vars['password']->value);?>

                    <?php echo help_inline(form_error_msg("password"," "));?>

                  </div>
                </div>
                
                
                    
            </div>
            
            
            <!-- Columna 2 -->
            <div class="span6">
                               
                  <!-- Text input-->
                <div class="control-group <?php echo cls_error("email_confirm");?>
">
                  <label class="control-label" for="textinput">Confirmar correo</label>
                  <div class="controls">
                    <?php echo form_input($_smarty_tpl->tpl_vars['email_confirm']->value);?>

                    <?php echo help_inline(form_error_msg("email_confirm"," "));?>

                  </div>
                </div>
                
                <div class="control-group <?php echo cls_error("password_confirm");?>
">
                  <label class="control-label" for="textinput">Confirmar contraseña</label>
                  <div class="controls">
                    <?php echo form_input($_smarty_tpl->tpl_vars['password_confirm']->value);?>

                    <?php echo help_inline(form_error_msg("password_confirm"," "));?>

                  </div>
                </div>
                
                
                
            </div>
            
        </div>
    </div> 
    
    
   <div class="clearfix" style="margin:20px; "></div>
   

    
    <div class="thumbnail form-horizontal">
        <div style="margin-top: -13px" class="span12">
            <span class="label pull-left">Datos Personales</span>  
        </div>
        
        <div class="row-fluid">
            
            <!-- Columna 1 -->
            <div class="span6">
				<div class="clearfix" ></div>
                <!-- Text input-->
                <div class="control-group <?php echo cls_error("first_name");?>
">
                  <label class="control-label" for="textinput">Nombre(s)</label>
                  <div class="controls">
                    <?php echo form_input($_smarty_tpl->tpl_vars['first_name']->value);?>

                    <?php echo help_inline(form_error_msg("first_name"," "));?>

                  </div>
                </div>
                
                
                <div class="control-group <?php echo cls_error("grado_academico");?>
">
                  <label class="control-label" for="textinput">Grado Académico</label>
                  <div class="controls">
                    <?php echo form_dropdown($_smarty_tpl->tpl_vars['grado_academico']->value[0],$_smarty_tpl->tpl_vars['grado_academico']->value[1],$_smarty_tpl->tpl_vars['grado_academico']->value[2]);?>

                    <?php echo help_inline(form_error_msg("grado_academico"," "));?>

                  </div>
                </div>
                <div class="control-group <?php echo cls_error("fecha_nacimiento");?>
">
                  <label class="control-label" for="textinput">Fecha de nacimiento</label>
                  <div class="controls">
                    <?php echo form_input($_smarty_tpl->tpl_vars['fecha_nacimiento']->value);?>

                    <?php echo help_inline(form_error_msg("fecha_nacimiento"," "));?>

                  </div>
                </div>
                <div class="control-group <?php echo cls_error("pais");?>
">
                  <label class="control-label" for="textinput">País</label>
                  <div class="controls">
                    <?php echo form_dropdown($_smarty_tpl->tpl_vars['pais']->value[0],$_smarty_tpl->tpl_vars['pais']->value[1],$_smarty_tpl->tpl_vars['pais']->value[2]);?>

                    <?php echo help_inline(form_error_msg("pais"," "));?>

                  </div>
                </div>
                <div class="control-group <?php echo cls_error("facebook");?>
">
                  <label class="control-label" for="textinput">Facebook</label>
                  <div class="controls">
                    <?php echo form_input($_smarty_tpl->tpl_vars['facebook']->value);?>

                    <?php echo help_inline(form_error_msg("facebook"," "));?>

                  </div>
                </div>
                
                
                    
            </div>
            
            
            <!-- Columna 2 -->
            <div class="span6">
                               
                  <!-- Text input-->
                <div class="control-group <?php echo cls_error("last_name");?>
">
                  <label class="control-label" for="textinput">Apellidos</label>
                  <div class="controls">
                    <?php echo form_input($_smarty_tpl->tpl_vars['last_name']->value);?>

                    <?php echo help_inline(form_error_msg("last_name"," "));?>

                  </div>
                </div>
                
                <div class="control-group <?php echo cls_error("ocupacion");?>
">
                  <label class="control-label" for="textinput">Ocupación</label>
                  <div class="controls">
                    <?php echo form_input($_smarty_tpl->tpl_vars['ocupacion']->value);?>

                    <?php echo help_inline(form_error_msg("ocupacion"," "));?>

                  </div>
                </div>
                 <div class="control-group <?php echo cls_error("genero");?>
">
                  <label class="control-label" for="textinput">Género</label>
                  <div class="controls">
                    <?php echo form_dropdown($_smarty_tpl->tpl_vars['genero']->value[0],$_smarty_tpl->tpl_vars['genero']->value[1],$_smarty_tpl->tpl_vars['genero']->value[2]);?>

                    <?php echo help_inline(form_error_msg("genero"," "));?>

                  </div>
                </div>
                <div class="control-group <?php echo cls_error("ciudad");?>
">
                  <label class="control-label" for="textinput">Ciudad</label>
                  <div class="controls">
                    <?php echo form_input($_smarty_tpl->tpl_vars['ciudad']->value);?>

                    <?php echo help_inline(form_error_msg("ciudad"," "));?>

                  </div>
                </div>
                
                <div class="control-group <?php echo cls_error("ciudad");?>
">
                  <label class="control-label" for="textinput"></label>
                  <div class="controls">
					<button type="submit" class="btn btn-success"><i class="icon-ok icon-white"></i> Registrarme</button>
                  </div>
                </div>
                
                
                
            </div>
            
        </div>
    </div> 
     
      <!--  
    

      <p>
            <?php echo lang('create_user_fname_label','first_name');?>
 <br />
            <?php echo form_input($_smarty_tpl->tpl_vars['first_name']->value);?>

      </p>

      <p>
            <?php echo lang('create_user_lname_label','first_name');?>
 <br />
            <?php echo form_input($_smarty_tpl->tpl_vars['last_name']->value);?>

      </p>

      <p>
            <?php echo lang('create_user_company_label','company');?>
 <br />
            <?php echo form_input($_smarty_tpl->tpl_vars['company']->value);?>

      </p>

      <p>
            <?php echo lang('create_user_email_label','email');?>
 <br />
            <?php echo form_input($_smarty_tpl->tpl_vars['email']->value);?>

      </p>

      <p>
            <?php echo lang('create_user_phone_label','phone');?>
 <br />
            <?php echo form_input($_smarty_tpl->tpl_vars['phone']->value);?>

      </p>

      <p>
            <?php echo lang('create_user_password_label','password');?>
 <br />
            <?php echo form_input($_smarty_tpl->tpl_vars['password']->value);?>

      </p>

      <p>
            <?php echo lang('create_user_password_confirm_label','password_confirm');?>
 <br />
            <?php echo form_input($_smarty_tpl->tpl_vars['password_confirm']->value);?>

      </p>

       -->

	
<?php echo form_close();?>

<?php }} ?>