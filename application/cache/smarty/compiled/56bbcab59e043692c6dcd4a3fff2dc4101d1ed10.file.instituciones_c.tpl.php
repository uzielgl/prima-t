<?php /* Smarty version Smarty-3.1.12, created on 2013-08-28 21:08:54
         compiled from "/var/www/primat_trunk/application/views/instituciones_c.tpl" */ ?>
<?php /*%%SmartyHeaderCode:509678020521ead361f1e17-94642051%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '56bbcab59e043692c6dcd4a3fff2dc4101d1ed10' => 
    array (
      0 => '/var/www/primat_trunk/application/views/instituciones_c.tpl',
      1 => 1377355956,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '509678020521ead361f1e17-94642051',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'instituciones' => 0,
    'inst' => 0,
    'paginacion' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_521ead36250902_82831866',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_521ead36250902_82831866')) {function content_521ead36250902_82831866($_smarty_tpl) {?> 	
<div class="tab-pane active" id="instituciones">
    <ul class="thumbnails">                    
        <li class="span12">
            <div class="thumbnail">
                <div class="row-fluid">
                    <div class="span12" style="overflow-y: auto;">
                        <a id="nueva_institucion" style="margin:5px 10px 10px 0px" href="#modal_agregar" data-toggle="modal" role="button" class="btn btn-success" > <i class="icon-plus icon-white"></i> Institución</a>
                        <table id="tabla_catalogos" class="table table-hover table-striped" style="position: static">
                            <thead>
                              <tr>
                                <th data-field="ins_nombre" data-type>
                                    <div class="filter">
                                        <span>Nombre Institución</span>
                                    </div>
                                </th>
                                <th data-field="ins_siglas" data-type>
                                    <div class="filter">
                                        <span>Siglas</span>
                                    </div>
                                </th>
                              <th data-field="ins_dependencia" data-type>
                                    <div class="filter">
                                        <span>Dependencia</span>
                                    </div>
                                </th>
								 <th data-field="ins_sede" data-type>
                                    <div class="filter">
                                        <span>Sede</span>
                                    </div>
                                </th>
                                <th style="width: 59px" > </th> <!-- Editar -->
                                <th style="width: 67px" > </th><!-- Eliminar -->
                              </tr>
                            </thead>
                            <tbody>
                            <?php  $_smarty_tpl->tpl_vars['inst'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['inst']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['instituciones']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['foo2']['index']=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['inst']->key => $_smarty_tpl->tpl_vars['inst']->value){
$_smarty_tpl->tpl_vars['inst']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['foo2']['index']++;
?>
                                <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['foo2']['index']%2==0){?>
                                    <tr id=<?php echo sprintf("tr_%d",$_smarty_tpl->tpl_vars['inst']->value['ins_id_institucion']);?>
 class="success">
                                <?php }else{ ?>
                                    <tr id=<?php echo sprintf("tr_%d",$_smarty_tpl->tpl_vars['inst']->value['ins_id_institucion']);?>
 >
                                <?php }?>
                                    <td id=<?php echo sprintf("ins_nombre_%d",$_smarty_tpl->tpl_vars['inst']->value['ins_id_institucion']);?>
 >
                                        <?php echo $_smarty_tpl->tpl_vars['inst']->value['ins_nombre'];?>

                                    </td>
                                    <td id=<?php echo sprintf("ins_siglas_%d",$_smarty_tpl->tpl_vars['inst']->value['ins_id_institucion']);?>
 >
                                        <?php echo $_smarty_tpl->tpl_vars['inst']->value['ins_siglas'];?>

                                    </td>
									 <td id=<?php echo sprintf("ins_dependencia_%d",$_smarty_tpl->tpl_vars['inst']->value['ins_id_institucion']);?>
 >
                                        <?php echo $_smarty_tpl->tpl_vars['inst']->value['ins_dependencia'];?>

                                    </td>
									 <td id=<?php echo sprintf("ins_sede_%d",$_smarty_tpl->tpl_vars['inst']->value['ins_id_institucion']);?>
 >
                                        <?php echo $_smarty_tpl->tpl_vars['inst']->value['ins_sede'];?>

                                    </td>
                                    <td id=<?php echo sprintf("td_editar_%d",$_smarty_tpl->tpl_vars['inst']->value['ins_id_institucion']);?>
 style="text-align: center;" >
                                        <a id=<?php echo sprintf("a_editar_%d",$_smarty_tpl->tpl_vars['inst']->value['ins_id_institucion']);?>
 class="btn btn-warning btn-mini" href="#modal_editar" ><i class="icon-pencil icon-white"></i> Editar</a>
                                    </td>
                                    <td id=<?php echo sprintf("td_eliminar_%d",$_smarty_tpl->tpl_vars['inst']->value['ins_id_institucion']);?>
 style="text-align: center;" >
                                        <a id=<?php echo sprintf("a_eliminar_%d",$_smarty_tpl->tpl_vars['inst']->value['ins_id_institucion']);?>
 class="btn btn-danger btn-mini" href="#modal_eliminar" ><i class="icon-remove icon-white"></i> Eliminar</a>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <?php echo $_smarty_tpl->tpl_vars['paginacion']->value;?>

                </div>
            </div>
        </li>
    </ul>
</div><?php }} ?>