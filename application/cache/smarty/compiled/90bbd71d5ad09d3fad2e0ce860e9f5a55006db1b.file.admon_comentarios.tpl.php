<?php /* Smarty version Smarty-3.1.12, created on 2013-09-17 10:29:24
         compiled from "/var/www/primat_trunk/application/views/admon_comentarios.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1917414815523875549f5dd1-75477135%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '90bbd71d5ad09d3fad2e0ce860e9f5a55006db1b' => 
    array (
      0 => '/var/www/primat_trunk/application/views/admon_comentarios.tpl',
      1 => 1377355956,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1917414815523875549f5dd1-75477135',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'vista' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_52387554a6d3b3_56476779',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52387554a6d3b3_56476779')) {function content_52387554a6d3b3_56476779($_smarty_tpl) {?><div class="container-fluid" >
    <div class="row-fluid">
        <h3 class="text-success" style="text-align: left" >Administración de Comentarios</h3>
        <div class="row-fluid">
            <div class="span12">
                <?php if (isset($_smarty_tpl->tpl_vars['vista']->value)){?>
                <div class="tabbable"> <!-- Only required for left/right tabs -->
                    <ul class="nav nav-tabs" style="margin-bottom: 0px">
                        <?php if ($_smarty_tpl->tpl_vars['vista']->value==="sitio"){?>
                            <li class="active">
                                <a href="<?php echo base_url();?>
index.php/admon_comentarios_c/comen_sitio" >Sitio</a>
                            </li>
                        <?php }else{ ?>
                            <li>
                                <a href="<?php echo base_url();?>
index.php/admon_comentarios_c/comen_sitio" >Sitio</a>
                            </li>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['vista']->value==="tesis"){?>
                            <li class="active">
                                <a href="<?php echo base_url();?>
index.php/admon_comentarios_c/comen_tesis" >Tesis</a>
                            </li>
                        <?php }else{ ?>
                            <li>
                                <a href="<?php echo base_url();?>
index.php/admon_comentarios_c/comen_tesis" >Tesis</a>
                            </li>
                        <?php }?>
                        
                        <?php if ($_smarty_tpl->tpl_vars['vista']->value==="aprobados"){?>
                            <li class="active">
                                <a href="<?php echo base_url();?>
index.php/admon_comentarios_c/comen_aprobados" >Aprobados</a>
                            </li>
                        <?php }else{ ?>
                            <li>
                                <a href="<?php echo base_url();?>
index.php/admon_comentarios_c/comen_aprobados" >Aprobados</a>
                                
                            </li>
                        <?php }?>

                    </ul>
                    <div class="tab-content">
                        <?php if ($_smarty_tpl->tpl_vars['vista']->value==="sitio"){?>
                            <?php echo $_smarty_tpl->getSubTemplate ("modals/modal_aprobar_comentario_sitio.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

                            <?php echo $_smarty_tpl->getSubTemplate ("modals/modal_eliminar_comentario_sitio.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

                            <?php echo $_smarty_tpl->getSubTemplate ("admon_comentarios_sitio_c.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

                        <?php }elseif($_smarty_tpl->tpl_vars['vista']->value==="tesis"){?>
                            <?php echo $_smarty_tpl->getSubTemplate ("modals/modal_aprobar_comentario_tesis.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

                            <?php echo $_smarty_tpl->getSubTemplate ("modals/modal_eliminar_comentario_tesis.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

                            <?php echo $_smarty_tpl->getSubTemplate ("admon_comentarios_tesis_c.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

                        <?php }elseif($_smarty_tpl->tpl_vars['vista']->value==="aprobados"){?>
                        	<?php echo $_smarty_tpl->getSubTemplate ("modals/modal_eliminar_comentario_tesis.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

                        	<?php echo $_smarty_tpl->getSubTemplate ("admon_comentarios_aprobados_c.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

                        <?php }?>
                        
                    </div>
                </div>
                <?php }?>
            </div>
        </div>
    </div>
</div>
               <?php }} ?>