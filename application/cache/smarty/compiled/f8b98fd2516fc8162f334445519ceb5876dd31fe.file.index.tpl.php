<?php /* Smarty version Smarty-3.1.12, created on 2013-08-28 06:52:03
         compiled from "/var/www/primat_trunk/application/views/auth/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1586326858521de463bf4996-16883800%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f8b98fd2516fc8162f334445519ceb5876dd31fe' => 
    array (
      0 => '/var/www/primat_trunk/application/views/auth/index.tpl',
      1 => 1377355956,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1586326858521de463bf4996-16883800',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'users' => 0,
    'message' => 0,
    'user' => 0,
    'group' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_521de463c8de20_74317154',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_521de463c8de20_74317154')) {function content_521de463c8de20_74317154($_smarty_tpl) {?><h3 class="text-success"><?php echo lang('index_heading');?>
</h3>

<?php if ($_smarty_tpl->tpl_vars['users']->value){?>
	
	
	
	
	<div id="infoMessage"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</div>
	
	<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover">
		<tr>
			<th><?php echo lang('index_fname_th');?>
</th>
			<th><?php echo lang('index_lname_th');?>
</th>
			<th><?php echo lang('index_email_th');?>
</th>
			<!-- <th><?php echo lang('index_groups_th');?>
</th> -->
			<th><?php echo lang('index_status_th');?>
</th>
			<th><?php echo lang('index_action_th');?>
</th>
			<!-- <th><?php echo lang('index_action_th');?>
</th> --> 
		</tr>
		<?php  $_smarty_tpl->tpl_vars['user'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['user']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['users']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['user']->key => $_smarty_tpl->tpl_vars['user']->value){
$_smarty_tpl->tpl_vars['user']->_loop = true;
?>
			<tr>
				<td><?php echo $_smarty_tpl->tpl_vars['user']->value->first_name;?>
</td>
				<td><?php echo $_smarty_tpl->tpl_vars['user']->value->last_name;?>
</td>
				<td><?php echo $_smarty_tpl->tpl_vars['user']->value->email;?>
</td>
				<!--
				<td>
					<?php  $_smarty_tpl->tpl_vars['group'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['group']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['user']->value->groups; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['group']->key => $_smarty_tpl->tpl_vars['group']->value){
$_smarty_tpl->tpl_vars['group']->_loop = true;
?>
						<?php echo anchor(("auth/edit_group/").($_smarty_tpl->tpl_vars['group']->value->id),$_smarty_tpl->tpl_vars['group']->value->name);?>
<br />
	                <?php } ?>
				</td>-->
				<td><?php echo ter(($_smarty_tpl->tpl_vars['user']->value->active),lang('index_active_link'),lang('index_inactive_link'));?>
</td>
				<td><?php echo ter(($_smarty_tpl->tpl_vars['user']->value->active),anchor(("auth/deactivate/").($_smarty_tpl->tpl_vars['user']->value->id),"Desactivar"),anchor(("auth/activate/").($_smarty_tpl->tpl_vars['user']->value->id),"Activar"));?>
</td>
				<!-- <td><?php echo anchor(("auth/edit_user/").($_smarty_tpl->tpl_vars['user']->value->id),'Edit');?>
</td> -->
			</tr>
		<?php } ?>
	</table>
	
	<!-- <p><?php echo anchor('auth/create_user',lang('index_create_user_link'));?>
 | <?php echo anchor('auth/create_group',lang('index_create_group_link'));?>
</p> -->
	
<?php }else{ ?>

	<h5><center>No se han encontrado usuarios</center></h5>
<?php }?>
<?php }} ?>