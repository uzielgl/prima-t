<?php /* Smarty version Smarty-3.1.12, created on 2013-08-25 14:59:56
         compiled from "/var/www/primat_trunk/application/views/reportes/disciplinas_estudio/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:72067909521a623cab8fe2-17090588%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ea8e33751521edcbc569bbe8ca58edcf638fb4cc' => 
    array (
      0 => '/var/www/primat_trunk/application/views/reportes/disciplinas_estudio/index.tpl',
      1 => 1377355956,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '72067909521a623cab8fe2-17090588',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'disciplinas' => 0,
    'dis' => 0,
    'this' => 0,
    'sub' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_521a623cb01e52_29423760',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_521a623cb01e52_29423760')) {function content_521a623cb01e52_29423760($_smarty_tpl) {?><h3 style="text-align: left" class="pull-left text-success">Reporte de tesis por disciplinas y subdisciplinas</h3>
<div class="pull-right">
	<a href="<?php echo site_url('reportes/disciplinas_estudio/csv');?>
" class="btn"> <i class="icon-share"> </i> Exportar a CSV</a>	 
</div>
<div class="span8">
	<p class="container lead">
		Este reporte muestra un listado de las diferentes disciplinas de estudio y la cantidad de tesis registradas. Además de un desglose de las subdisciplinas correspondientes.
	</p>	
</div>
<div class="clearfix"></div>
<?php  $_smarty_tpl->tpl_vars['dis'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['dis']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['disciplinas']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['dis']->key => $_smarty_tpl->tpl_vars['dis']->value){
$_smarty_tpl->tpl_vars['dis']->_loop = true;
?>
	<table align="center" style="max-width: 90% ;min-width:20%; alignment-baseline:center" class="table table-striped table-bordered table-hover table-condensed container">
		<thead>
			<tr>
				<th style="text-align: center; background-color: #DFF0D8; color: #A9302A">DISCIPLINA / SUBDISCIPLINA</th>
				<th style="background-color: #DFF0D8; color: #A9302A">CANTIDAD DE TESIS</th>
			</tr>
			<tr>
				<th><?php echo $_smarty_tpl->tpl_vars['dis']->value->dis_estudio_nombre;?>
</td>
				<th><?php echo $_smarty_tpl->tpl_vars['dis']->value->cantidad;?>
</td>
			</tr>			
		</thead>

		<tbody>
			<?php  $_smarty_tpl->tpl_vars['sub'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sub']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['this']->value->_get_subdisciplinas_con_cantidad($_smarty_tpl->tpl_vars['dis']->value->dis_id_disciplina_estudio); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['sub']->key => $_smarty_tpl->tpl_vars['sub']->value){
$_smarty_tpl->tpl_vars['sub']->_loop = true;
?>
				<tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $_smarty_tpl->tpl_vars['sub']->value->subcategoria;?>

					</td>
					<td><?php echo $_smarty_tpl->tpl_vars['sub']->value->cantidad;?>

					</td>
				</tr>
			<?php } ?>
		</tbody>
		
	</table>
<?php } ?>
<?php }} ?>