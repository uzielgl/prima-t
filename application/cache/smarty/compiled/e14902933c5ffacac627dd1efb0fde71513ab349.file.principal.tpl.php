<?php /* Smarty version Smarty-3.1.12, created on 2013-08-25 14:18:27
         compiled from "/var/www/primat_trunk/application/views/catalogos/tesis/principal.tpl" */ ?>
<?php /*%%SmartyHeaderCode:920120235521a58830d1c89-13110614%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e14902933c5ffacac627dd1efb0fde71513ab349' => 
    array (
      0 => '/var/www/primat_trunk/application/views/catalogos/tesis/principal.tpl',
      1 => 1377355956,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '920120235521a58830d1c89-13110614',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'total_tesis' => 0,
    'paginador' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_521a5883114521_74509196',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_521a5883114521_74509196')) {function content_521a5883114521_74509196($_smarty_tpl) {?>

<h3 class="text-success">Administración de tesis</h3>
        <?php echo $_smarty_tpl->getSubTemplate ("catalogos/tesis/filtros.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="pull-left">
    <a class="btn btn-success" href="<?php echo site_url("catalogos/tesis/registrar");?>
"><i class="icon-white icon-plus"></i> Tesis</a>
</div>

<h4 class="pull-left"  style="margin-left:20px;" >Se han encontrado <span class="total-tesis"><?php echo $_smarty_tpl->tpl_vars['total_tesis']->value;?>
</span> tesis.</h4>
<!-- 
<div class="pagination" style="margin-left:10px;">
    <ul>
        <li class="active"><a>Se han encontrado <?php echo $_smarty_tpl->tpl_vars['total_tesis']->value;?>
 tesis.</a></li>
    </ul>
</div>-->
    
<?php echo $_smarty_tpl->getSubTemplate ("catalogos/tesis/tabla.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<div class="pagination">
	<center><?php echo $_smarty_tpl->tpl_vars['paginador']->value;?>
</center>
</div>


<div class="clearfix"></div>




<!-- Para los modales -->

<!-- Modal de confirmación-->
<div id="mdl-eliminar-tesis" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Eliminar tesis</h3>
  </div>
  <div class="modal-body">
    <p>¿Realmente deseas eliminar la tesis <span class="nombre-tesis"></span>?</p>
  </div>
  <div class="modal-footer">
    <button id='btn-eliminar-tesis' class="btn btn-danger"><i class="icon-white icon-warning-sign"> </i> Eliminar</button>
    <button class="btn" data-dismiss="modal" aria-hidden="true"><i class="icon icon-remove"> </i> Cancelar</button>
  </div>
</div>


<div id='mdl-espera-eliminar-tesis' class="modal hide fade">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3>Eliminando...</h3>
  </div>
  <div class="modal-body">
    <p>
    	Eliminando... <?php echo image('loading.gif');?>

    </p>
  </div>
</div>


<div id="modal_info" class="modal hide fade">
    <div id="modal_info_header" class="modal-header">
        
    </div>
    <div id="modal_info_body" class="modal-body">
    
    </div>
</div>


<?php }} ?>