<?php /* Smarty version Smarty-3.1.12, created on 2013-08-25 14:59:51
         compiled from "/var/www/primat_trunk/application/views/admon_catalogos.tpl" */ ?>
<?php /*%%SmartyHeaderCode:232976394521a623751ccb8-35957868%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6db746b76a332156a592b87ded2857b5ca39bf44' => 
    array (
      0 => '/var/www/primat_trunk/application/views/admon_catalogos.tpl',
      1 => 1377355956,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '232976394521a623751ccb8-35957868',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'vista' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_521a62375b8be7_25423902',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_521a62375b8be7_25423902')) {function content_521a62375b8be7_25423902($_smarty_tpl) {?><div class="container-fluid" >
    <div class="row-fluid">
        <h3 class="text-success" style="text-align: left" >Administración de Catálogos</h3>
        <div class="row-fluid">
            <div class="span12">
                <?php if (isset($_smarty_tpl->tpl_vars['vista']->value)){?>
                <div class="tabbable"> <!-- Only required for left/right tabs -->
                    <!--<ul class="nav nav-tabs" style="margin-bottom: 0px">
                        <?php if ($_smarty_tpl->tpl_vars['vista']->value==="especies_c"){?>
                            <li class="active">
                                <a href="<?php echo base_url();?>
index.php/catalogos/especies_c" >Especies</a>
                            </li>
                        <?php }else{ ?>
                            <li>
                                <a href="<?php echo base_url();?>
index.php/catalogos/especies_c" >Especies</a>
                            </li>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['vista']->value==="disciplinas_c"){?>
                            <li class="active">
                                <a href="<?php echo base_url();?>
index.php/catalogos/disciplina_estudio_c" >Disciplinas</a>
                            </li>
                        <?php }else{ ?>
                            <li>
                                <a href="<?php echo base_url();?>
index.php/catalogos/disciplina_estudio_c" >Disciplinas</a>
                            </li>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['vista']->value==="instituciones_c"){?>
                            <li class="active">
                                <a href="<?php echo base_url();?>
index.php/catalogos/instituciones_c" >Instituciones</a>
                            </li>
                        <?php }else{ ?>
                            <li>
                                <a href="<?php echo base_url();?>
index.php/catalogos/instituciones_c" >Instituciones</a>
                            </li>
                        <?php }?>

                        <?php if ($_smarty_tpl->tpl_vars['vista']->value==="condiciones_c"){?>
                            <li class="active">
                                <a href="<?php echo base_url();?>
index.php/catalogos/condiciones_sitio_c" >Condiciones de estudio</a>
                            </li>
                        <?php }else{ ?>
                            <li>
                                <a href="<?php echo base_url();?>
index.php/catalogos/condiciones_sitio_c" >Condiciones de estudio</a>
                            </li>
                        <?php }?>
                    </ul>-->
                    <div class="tab-content">
                        <?php if ($_smarty_tpl->tpl_vars['vista']->value==="especies_c"){?>
                            <?php echo $_smarty_tpl->getSubTemplate ("modals/modal_agregar_subespecies_c.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

                            <?php echo $_smarty_tpl->getSubTemplate ("modals/modal_editar_subespecies_c.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

                            <?php echo $_smarty_tpl->getSubTemplate ("modals/modal_eliminar_subespecies_c.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

                            <?php echo $_smarty_tpl->getSubTemplate ("modals/modal_agregar_especies_c.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

                            <?php echo $_smarty_tpl->getSubTemplate ("modals/modal_editar_especies_c.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

                        <?php }elseif($_smarty_tpl->tpl_vars['vista']->value==="disciplinas_c"){?>
                            <?php echo $_smarty_tpl->getSubTemplate ("modals/modal_agregar_disciplina_c.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

                            <?php echo $_smarty_tpl->getSubTemplate ("modals/modal_editar_disciplina_c.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

                            <?php echo $_smarty_tpl->getSubTemplate ("modals/modal_agregar_subdisciplinas_c.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

                            <?php echo $_smarty_tpl->getSubTemplate ("modals/modal_editar_subdisciplinas_c.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

                            <?php echo $_smarty_tpl->getSubTemplate ("modals/modal_eliminar_subdisciplinas_c.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

                        <?php }elseif($_smarty_tpl->tpl_vars['vista']->value==="instituciones_c"){?>
                            <?php echo $_smarty_tpl->getSubTemplate ("modals/modal_agregar_instituciones_c.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

                            <?php echo $_smarty_tpl->getSubTemplate ("modals/modal_editar_instituciones_c.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

                        <?php }elseif($_smarty_tpl->tpl_vars['vista']->value==="condiciones_c"){?>
                            <?php echo $_smarty_tpl->getSubTemplate ("modals/modal_agregar_condiciones_c.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

                            <?php echo $_smarty_tpl->getSubTemplate ("modals/modal_editar_condiciones_c.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

                        <?php }?>
                        <?php echo $_smarty_tpl->getSubTemplate ("modals/modal_catalogos.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

                        <?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['vista']->value).".tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

                    </div>
                </div>
                <?php }?>
            </div>
        </div>
    </div>
</div>
               
        
<?php }} ?>