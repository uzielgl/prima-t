<?php /* Smarty version Smarty-3.1.12, created on 2013-08-26 15:49:00
         compiled from "/var/www/primat_trunk/application/views/leer_mas2.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1965282270521bbf3c40c794-92272187%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e7cbdb3c29f495e0aef02667728de0999622ba7f' => 
    array (
      0 => '/var/www/primat_trunk/application/views/leer_mas2.tpl',
      1 => 1377355956,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1965282270521bbf3c40c794-92272187',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_521bbf3c43c1d5_63741309',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_521bbf3c43c1d5_63741309')) {function content_521bbf3c43c1d5_63741309($_smarty_tpl) {?><div class="row-fluid">
	<br>
	<div class="span7" align="center">
	<h3 class="text-success" style="margin-bottom: 0px"><em>Alouatta pigra</em></h3>
	<h3 class="text-success" style="margin-bottom: 20px">(Nombres comunes: mono aullador negro, saraguato negro)</h3>
	
	</div>
	<div class="span3 offset1" align="center">
	 	<img style="width:100%"  src="<?php echo base_url();?>
themes/default/img/mono2.jpg" alt>
	</div>
	<br>
	<div class="span10" >
	<font face="times">
	<p class="lead" align="justify">
	En el pasado, <em>A. pigra</em> era considerada una subespecie de A. palliata (Hall y Kelson, 1959; Leopold, 1959). Hoy en día se le reconoce 
	como una especie distinta dadas sus características genéticas, conductuales y anatómicas (Cortés-Ortiz et al., 2003). Cabe destacar 
	que Smith (1970) estudió la variación geográfica e individual de los monos aulladores de México y encontró una gran divergencia entre 
	especímenes en peso, talla, morfología dental (particularmente de los molares), tamaño del hueso hioides e inclusive forma y tamaño 
	craneal. Recientemente se encontraron diferencias significativas entre <em>A. palliata</em> y <em>A. pigra</em> a nivel de ADN mitocondrial (en 5.7 por 
	ciento) lo que confirma las diferencias entre las dos especies descritas por Smith en 1970 (Cortés-Ortiz, 2003). La morfología de <em>A. 
	pigra</em> corresponde a una especie más robusta y pesada que <em>A. palliata</em>. También se aprecian diferencias sustanciales en cuanto al color 
	del pelaje pues<em> A. pigra</em> posee un pelo mucho más denso, suave y de color negro homogéneo. El dimorfismo sexual de <em>A. pigra</em> es mucho 
	más marcado que en <em>A. palliata</em> ya que los machos son más grandes y pesados que las hembras en un grado mayor. Otra característica 
	distintiva de <em>A. pigra</em> es la presencia de testículos en escroto desde infantes, mientras en A. palliata los testículos son retenidos 
	en el canal inguinal hasta que alcanzan la madurez sexual (Crockett y Eisenberg, 1987). Los monos aulladores negros son también animales 
	arbóreos de hábitos diurnos. Al igual que <em>A. palliata</em>, tienden a preferir la cobertura vegetal primaria, aunque también se les observa 
	en variedad de hábitats, incluyendo selvas tropicales medianas subperenifolias y subcaducifolias, desde 250 metros sobre el nivel del 
	mar (Rodríguez-Luna et al., 1996 a.). Aunque al igual que otras especies de <em>Alouatta</em> se le ha considerado como folívoro, existen 
	evidencias que sugieren que presentándose la oportunidad, la dieta de <em>A. pigra</em> puede estar constituida por una alta proporción de fruta 
	(Pavelka y Knopff, 2004). En condiciones extremas de fragmentación, se ha observado que los animales se alimentan en huertos de mango 
	(Mangnifera indica), tamarindo (Tamarindos indica), ciruela (Pronus Spp.), guayaba (Psidium Spp.) y plántulas de papaya (Carica papaya) 
	(Pozo-Montuy, 2006; Pozo-Montuy y Serio-Silva, 2007). De igual manera, se han registrado monos aulladores desplazándose por el suelo y 
	alimentándose de plántulas de algunas cucurbitáceas y fabáceas, e inclusive a monos aulladores que han logrado subsistir en plantaciones 
	forestales comerciales como el Eucalipto (Eucalypthus grandis). Sin embargo, se desconocen aún las implicaciones nutricionales y 
	sociales de éstas en los aulladores negros (Serio-Silva et al., 2006).
	<br>
	</p>
	<p class="lead" align="justify">
	<strong>Texto tomado de:
	SEMARNAT / CONANP. 2012. Programa de acción para la conservación de las especies: Primates, Mono Araña (<em>Ateles geoffroyi</em>) y Monos 
	Aulladores (<em>Alouatta palliata, Alouatta pigra</em>). Oropeza-Hernández, P.; Rendón-Hernández, E. (Eds.). Comisión Nacional de Áreas Naturales
	 Protegidas, Primera Edición. 53 Pp. 
	 <strong>
	 </p>
	</font>
	</div><?php }} ?>