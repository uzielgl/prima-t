<?php /* Smarty version Smarty-3.1.12, created on 2013-08-24 04:12:07
         compiled from "/var/www/primat_trunk/application/views/auth/logueo.tpl" */ ?>
<?php /*%%SmartyHeaderCode:836455129521878e702a570-50081881%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'eca3b813eedb47bba7170c95a7c657bd1a73faa9' => 
    array (
      0 => '/var/www/primat_trunk/application/views/auth/logueo.tpl',
      1 => 1377355956,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '836455129521878e702a570-50081881',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'message' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_521878e7077090_92102708',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_521878e7077090_92102708')) {function content_521878e7077090_92102708($_smarty_tpl) {?>
<div class="container">

	<div class="row">
		
		<div class="span offset2">
			
			<form class="form-horizontal" method="post">
				
				<div class="control-group">
					<div class="controls">
						<h2>Iniciar sesión</h2>
					</div>
				</div>
				
				    	
				<div class="control-group  <?php echo cls_error("identity");?>
">
					<label class="control-label" for="inputEmail">Email</label>
					<div class="controls">
						<div class="input-prepend">
							<span class="add-on"><i class="icon icon-user"></i></span>
							<input class="input-medium" id="prependedInput" value="<?php echo set_value("identity");?>
" name="identity" type="text" placeholder="Usuario@dominio">
						</div>
						<?php echo help_inline(form_error("identity"));?>

					</div>
				</div>
				
				<div class="control-group <?php echo cls_error("password");?>
">
					<label class="control-label" for="inputPassword">Password</label>
					<div class="controls">
						<div class="input-prepend">
							<span class="add-on"><i class="icon icon-key"></i></span>
							<input class="input-medium" id="prependedInput" name="password" type="password" placeholder="Contraseña">
						</div>
						<?php echo help_inline(form_error("password"));?>

					</div>
				</div>
				
				
				<?php if (!validation_errors()&&$_smarty_tpl->tpl_vars['message']->value){?>
					<div class="control-group info">
						<div class="controls">
							<?php echo help_inline($_smarty_tpl->tpl_vars['message']->value);?>

						</div>
					</div>
				<?php }?>
				
				<div class="control-group">
					<div class="controls">
						<label class="checkbox">
							<input type="checkbox" name="remember"> Recordar
						</label>
						<button class="btn " type="submit"><i class="icon-play icon"></i> Iniciar Sesión</button>
					</div>
				</div>
				
				<div class="control-group">
				    <div class="controls">
					    <a href="forgot_password"><i class="icon-refresh"></i> Recuperar Contraseña</a> <br>
					    <a href="<?php echo site_url("auth/create_user");?>
"><i class="icon-plus"></i> Registrarse</a>
				    </div>
				</div>
			
			
			</form>
		
		</div>
	
	</div>

</div><?php }} ?>