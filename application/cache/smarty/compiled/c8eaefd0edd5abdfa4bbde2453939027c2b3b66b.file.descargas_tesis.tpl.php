<?php /* Smarty version Smarty-3.1.12, created on 2013-08-25 15:00:05
         compiled from "/var/www/primat_trunk/application/views/reportes/tesis/descargas_tesis.tpl" */ ?>
<?php /*%%SmartyHeaderCode:808399307521a6245e17976-62736830%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c8eaefd0edd5abdfa4bbde2453939027c2b3b66b' => 
    array (
      0 => '/var/www/primat_trunk/application/views/reportes/tesis/descargas_tesis.tpl',
      1 => 1377355956,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '808399307521a6245e17976-62736830',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'aTesis' => 0,
    'tesis' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_521a6245e6cc75_43881996',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_521a6245e6cc75_43881996')) {function content_521a6245e6cc75_43881996($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/var/www/primat_trunk/application/third_party/Smarty/plugins/modifier.replace.php';
?><h3 style="text-align: left" class="pull-left text-success">Reporte de descargas por tesis</h3>
<form method="post" class="form-inline">
	<div class="pull-right">
		<!-- <a href="<?php echo site_url('reportes/tesis_c/descargas_por_tesis/2');?>
" class="btn"> <i class="icon-share"> </i> Exportar a CSV</a> -->
		<button  class="btn" name='accion' value="2"> <i class="icon-share"> </i> Exportar a CSV</button>
	</div>
	
	<div class="clearfix"></div>
	
	
	Desde: <input id="from" type="text" value="<?php echo set_value('from');?>
" class="input-small" name='from'> Hasta: <input id="to" type="text" class="input-small" name="to" value="<?php echo set_value('to');?>
">
	<button name="accion" value="1" type="submit" class="btn"><i class="icon icon-filter"> </i>Filtrar</button>
	<button type="button" id='reset' class="btn"><i class="icon icon-trash"> </i> Limpiar</button>
</form>



	<table class="table table-striped table-bordered table-hover table-condensed">
		<thead>
			<tr>
				<th>TITULO</th>
				<th>AUTORES</th>
				<th>DIRECTORES</th>
				<th>GRADOS</th>
				<th>DESCARGAS</th>
				
			</tr>			
		</thead>

		<tbody>
			<?php  $_smarty_tpl->tpl_vars['tesis'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tesis']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['aTesis']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tesis']->key => $_smarty_tpl->tpl_vars['tesis']->value){
$_smarty_tpl->tpl_vars['tesis']->_loop = true;
?>
				<tr>
					<td><?php echo $_smarty_tpl->tpl_vars['tesis']->value["TITULO"];?>
</td>
					<td><?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['tesis']->value["AUTORES"],",",",<br/><br/>");?>
</td>
					<td><?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['tesis']->value["DIRECTORES"],",",",<br/><br/>");?>
</td>
					<td><?php echo $_smarty_tpl->tpl_vars['tesis']->value["GRADOS"];?>
</td>
					<td><?php echo $_smarty_tpl->tpl_vars['tesis']->value["DESCARGAS"];?>
</td>
				</tr>
			<?php } ?>
		</tbody>
		
	</table>

<?php }} ?>