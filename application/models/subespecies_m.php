<?php
/**
 * undocumented class
 *
 * @package default
 * @author  yairguz
 */
class Subespecies_m extends MY_Model {
	
	//Establecemos con que tabla va a trabajar normalmente y la primary key.
	//Estableciendo esto, ya tenemos métodos en MY_Model como add, upd, del.
	//De hecho, con esto, no es necesario el método agregar, pues ya está add en MY_Model, 
	//pero a modo de ejemplo se ha implementado.
	var $tb = "cat_subespecies";
	var $pk = "sue_id_subespecie";
	var $nombre = "sue_nombre";
	var $alias = "sue";
	
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	
	public function agregar($data){
		//Insertamos, es parte de la clase de Database, este insert
		//nos genera una instrucción sql, para este caso, que nada más viene el esp_nombre => "TAL NOMBRE" algo como 
		//INSERT INTO especies (esp_nombre) values ('TAL_NOMBRE') y la ejecuta
		//$this->db->insert( $this->tb, $data );
		//echo $data;
		$user=$this->ion_auth->user()->row();
		$data['sue_id_user_mod']=$user->id;
		$data['sue_id_user_crea']=$user->id;
		$data['sue_fecha_modificacion']=date("Y-m-d H:i:s");
		$data['sue_fecha_creacion']=date("Y-m-d H:i:s");
		//exit();
		
		return $this->add( $data, true );
	}
	
	public function modificar($id,$data){
		$data['sue_fecha_modificacion']=date("Y-m-d H:i:s");
		$this->upd($id,$data);
	}
	
	public function eliminar($sue_id_subespecie){
		$this->del($sue_id_subespecie);
	}
	
	public function buscarTodos(){
		return $this->get();
	}
	
	public function buscarPorId($id){
		return $this->get_by_id($id);
	}
	public function buscarPorIdEspecie($id){
		$this->db->where("sue_id_especie", $id);
		$rs = $this->get();
		if( count($rs) > 0 )
			return $rs;
		else
			return false;
	}
    
     public function getCantidadPorTesis( $id_especie ) {
        $columnas="";
        $where="";
        if($id_especie==-1){
            $columnas="
                sd.especie 
                , sd.sue_nombre as subespecie
                , count(*) as cantidad";
        }else{
            $columnas="count(*) as cantidad
                , t.tes_id_sub_especie
                ,  sd.especie 
                , sd.sue_nombre as subespecie";
                
            $where="where sue_id_especie=".$id_especie;            
        }
         
        $sql='
        select '.$columnas.' 
        from tesis_sub_especies t 
            inner join 
                ( select s.sue_id_subespecie
                    , s.sue_nombre
                    , s.sue_id_especie
                    , d.esp_nombre as especie
                  from cat_subespecies s 
                    right join cat_especies d 
                        on (s.sue_id_especie = d.esp_id_especie)
                 ) sd 
                        on (t.tes_id_sub_especie = sd.sue_id_subespecie)
        '.$where.' 
        group by t.tes_id_sub_especie 
        order by cantidad desc;';
        if($id_especie==-1){
            return $this->db->query($sql);    
        }else{
            return $this->db->query($sql)->result();
        }
        
        
    }
     
     
     
    
} // END