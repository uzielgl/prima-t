<?php
/**
 * undocumented class
 *
 * @package default
 * @author  Sofía, Elyar
 */
class instituciones_m extends MY_Model {
	
	//Establecemos con que tabla va a trabajar normalmente y la primary key.
	//Estableciendo esto, ya tenemos métodos en MY_Model como add, upd, del.
	//De hecho, con esto, no es necesario el método agregar, pues ya está add en MY_Model, 
	//pero a modo de ejemplo se ha implementado.
	var $tb = "cat_instituciones";
	var $pk = "ins_id_institucion";
    var $alias="ins";
	
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	
	public function agregar($data){
		//Insertamos, es parte de la clase de Database, este insert
		//nos genera una instrucción sql, para este caso, que nada más viene el esp_nombre => "TAL NOMBRE" algo como 
		//INSERT INTO especies (esp_nombre) values ('TAL_NOMBRE') y la ejecuta
		//$this->db->insert( $this->tb, $data );
		//echo $data;
		// El nombre y las siglas ya las envía el controlador
		$user=$this->ion_auth->user()->row();
		$data['ins_fecha_modificacion']=date("Y-m-d H:i:s");
		$data['ins_fecha_creacion']=date("Y-m-d H:i:s");
		$data['ins_id_user_mod']=$user->id;
		$data['ins_id_user_crea']=$user->id;
		//exit();
		
		return $this->add( $data, true );
	}
	
	// El nombre y las siglas ya vienen desde el controlador
	public function modificar($id,$data){
		$data['ins_fecha_modificacion']=date("Y-m-d H:i:s");
		$this->upd($id,$data);
	}
	
	public function eliminar($ins_id){
		$this->del($ins_id);
	}
	
	public function buscarTodos(){
		return $this->get();
	}
	
	public function buscarPorId($id){
		return $this->get_by_id($id);
	}
	
} // END