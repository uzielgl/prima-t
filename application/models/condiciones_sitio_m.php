<?php

class Condiciones_sitio_m extends MY_Model{
    var $tb = "cat_condiciones_sitio";
    var $pk = "con_id_condicion_sitio";
    var $nombre = "con_nombre";
    var $alias = "con";
    
    public function _construct() {
        parent::__construct();
        $this->load->database();
    }
    public function buscarTodos(){
        return $this->get();
    }
    public function agregar($data){
        $user=$this->ion_auth->user()->row();
        $data['con_id_user_mod']=$user->id;
        $data['con_id_user_crea']=$user->id;
        $data['con_fecha_modificacion']=date("Y-m-d H:i:s");
        $data['con_fecha_creacion']=date("Y-m-d H:i:s");
        //exit();
        return $this->add( $data, true );
    }
    public function modificar($id,$data){
        $data['con_fecha_modificacion']=date("Y-m-d H:i:s");
        $this->upd($id,$data);
    }
    public function eliminar($con_id_condicion_sitio){
        $this->del($con_id_condicion_sitio);
    }
}
