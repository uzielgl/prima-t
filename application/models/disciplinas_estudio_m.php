<?php
/**
 * @author FerGR  
 */
 
class Disciplinas_estudio_m extends MY_Model {
	
	var $tb = "cat_disciplinas_estudio";
	var $pk = "dis_id_disciplina_estudio";
	var $nombre = "dis_estudio_nombre";
	var $alias = "dis";
			
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	public function agregar($data){
			
		$user=$this->ion_auth->user()->row();
		
		$data['dis_fecha_modificacion']=date("Y-m-d H:i:s");
		$data['dis_fecha_creacion']=date("Y-m-d H:i:s");
	    $data['dis_id_user_mod']=$user->id;
		$data['dis_id_user_crea']=$user->id;
		
	    return $this->add( $data, true );
	}
	
	public function modificar($id,$data){
		$data['dis_fecha_modificacion']=date("Y-m-d H:i:s");	
		$this->upd($id,$data);
	}
	
	public function eliminar($id){
		$this->del($id);
	}
	
	public function buscarTodos(){
		return $this->get();
	}
	
	public function buscarPorId($id){
		$this->get_by_id($id);
	}
	
	//Para lops reportes
	/**
	 * Obtiene las disciplinas con la cantidad de tesis asociadas que tiene esta disciplinas
	 *
	 * @return void
	 * @author  
	 */
	function get_con_cantidad() 
	{
		return $this->db->select('dis_id_disciplina_estudio, dis_estudio_nombre, count(*) as cantidad')
			->from('cat_disciplinas_estudio')
			->join('cat_subdisciplinas', 'dis_id_disciplina_estudio = sub_id_disciplina_estudio', 'INNER')
			->join('tesis_subdisciplinas', 'tes_id_subdisciplina = sub_id_subdisciplina', 'INNER')
			->group_by('dis_id_disciplina_estudio')
			->order_by('cantidad desc')
			->having('cantidad >', '0')->get()->result();
	}
	
} // END