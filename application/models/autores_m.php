<?php
class Autores_m extends MY_Model
{
    var $tb = 'tesis_autores';
    var $pk = 'tes_id_tesis';
    
    public function get_names()
    {
        $sql = "
            select TRIM( tes_nombre_autor ) as name
            from tesis_autores
            group by trim( tes_nombre_autor )";
        $rs = $this->db->query($sql);
        return $rs->result_array();
    }
}
