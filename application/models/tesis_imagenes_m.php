<?php
/**
 * undocumented class
 *
 * @package default
 * @author  kyoo
 */
class Tesis_imagenes_m extends MY_Model {
	var $tb = "tesis_imagenes";
	var $pk = "tes_id_tesis_imagenes";
	var $nombre = "tes_ruta_imagen";
	var $alias = "tes";
	
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	public function agregar($data){
		return $this->add( $data, true );
	}
	
	public function modificar($id,$data){
		$this->upd($id,$data);
	}
	
	public function eliminar($id){
		$this->del($id);
	}
	
	public function buscarTodos(){
		return $this->get();
	}
	
	public function buscarPorId($id){
		return $this->get_by_id($id);
	}
	
	public function del_of( $id_tesis ){
		$this->db->where( 'tes_id_tesis', $id_tesis );
		$items = $this->get();
		foreach( $items as $i ){
			unlink( './themes/default/img/' . $i['tes_ruta_imagen'] );
		}
		//Eliminamos de la BD
		$this->db->where( 'tes_id_tesis', $id_tesis );
		$this->db->delete( $this->tb );		
	}
}