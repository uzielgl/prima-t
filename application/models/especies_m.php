<?php
/**
 * undocumented class
 *
 * @package default
 * @author  yairguz
 */
class Especies_m extends MY_Model {
	
	//Establecemos con que tabla va a trabajar normalmente y la primary key.
	//Estableciendo esto, ya tenemos métodos en MY_Model como add, upd, del.
	//De hecho, con esto, no es necesario el método agregar, pues ya está add en MY_Model, 
	//pero a modo de ejemplo se ha implementado.
	var $tb = "cat_especies";
	var $pk = "esp_id_especie";
	var $nombre = "esp_nombre";
	var $alias = "esp";
	
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	
	public function agregar($data){
		//Insertamos, es parte de la clase de Database, este insert
		//nos genera una instrucción sql, para este caso, que nada más viene el esp_nombre => "TAL NOMBRE" algo como 
		//INSERT INTO especies (esp_nombre) values ('TAL_NOMBRE') y la ejecuta
		//$this->db->insert( $this->tb, $data );
		//echo $data;
		$user=$this->ion_auth->user()->row();
		$data['esp_id_user_mod']=$user->id;
		$data['esp_id_user_crea']=$user->id;
		$data['esp_fecha_modificacion']=date("Y-m-d H:i:s");
		$data['esp_fecha_creacion']=date("Y-m-d H:i:s");
		//exit();
		
		return $this->add( $data, true );
	}
	
	public function modificar($id,$data){
		$data['esp_fecha_modificacion']=date("Y-m-d H:i:s");
		$this->upd($id,$data);
	}
	
	public function eliminar($esp_id_especie){
		$this->del($esp_id_especie);
	}
	
	public function buscarTodos(){
		return $this->get();
	}
	
	public function buscarPorId($id){
		return $this->get_by_id($id);
	}
    
    function getCantidadPorTesis() 
    {
        return $this->db->select('esp_id_especie, esp_nombre, count(*) as cantidad')
            ->from('cat_especies')
            ->join('cat_subespecies', 'esp_id_especie = sue_id_especie', 'LEFT')
            ->join('tesis_sub_especies', 'tes_id_sub_especie = sue_id_subespecie', 'INNER')
            ->group_by('esp_id_especie')
            ->order_by('cantidad desc')->get()->result();
    }
    
	
} // END