<?php
/**
 * undocumented class
 *
 * @package default
 * @author UzielGL
 */
class Tesis_m extends MY_Model {
    var $tb = "tesis";
    var $pk = "tes_id_tesis";
    var $alias = "tes";

    public function construct() {
        parent::__construct();
    }

    /**
     * Establecer los filtros para la búsqueda
     *
     * @return void
     * @author
     */
    function set_filtros($filters) {
        $this->db->join("tesis_sub_especies", "tes.tes_id_tesis = tesis_sub_especies.tes_id_tesis", "LEFT");
        $this->db->join("cat_subespecies", "tesis_sub_especies.tes_id_sub_especie = cat_subespecies.sue_id_subespecie", "LEFT");
        $this->db->join("cat_especies", "cat_subespecies.sue_id_especie = cat_especies.esp_id_especie", "LEFT");
        $this->db->join("tesis_autores as t_a", "tes.tes_id_tesis = t_a.tes_id_tesis", "LEFT");
        $this->db->join("tesis_directores", "tes.tes_id_tesis = tesis_directores.tes_id_tesis", "LEFT");
                            
        foreach ($filters as $key => $value) {
            if ($value) {
                if ($key == "tes_palabra_clave") 
                {
                    #$this -> db -> where(" ( tes_palabras_clave like '%{$value}%' or tes_nombre like '%{$value}%' ) ", NULL, FALSE);
                    $this->db->where("concat(tes_nombre, tes_palabras_clave, tes_resumen, gra_descripcion, t_a.tes_nombre_autor, tesis_directores.tes_nombre_director) like '%{$value}%' ", NULL, FALSE);
                }
                elseif ( $key == "tes_id_disciplina_estudio") 
                {
                    //$this -> db -> join("tesis_subdisciplinas", "tesis_subdisciplinas.tes_id_tesis = tes.tes_id_tesis", "RIGHT");
                    $this -> db -> where("dis_id_disciplina_estudio", $value);
                }
                elseif( $key == "id_subdisciplina")
                {
                	$this->db->where("tes_id_subdisciplina", $value);
                }
                elseif ($key == "id_especie") 
                {
                    $this -> db -> where("esp_id_especie", $value);
				}
				elseif( $key == "tes_nombre_autor" )
				{
					//$this->db->join("tesis_autores", "tes.tes_id_tesis = tesis_autores.tes_id_tesis", "LEFT");
					$this -> db -> where(" ( t_a.tes_nombre_autor like '%{$value}%' ) ", NULL, FALSE);
				} 
				elseif( $key == "tes_id_tesis_autores" )
				{
					//$this->db->join("tesis_autores", "tes.tes_id_tesis = tesis_autores.tes_id_tesis", "LEFT");
					$this -> db -> where("t_a.tes_id_tesis_autores", $value);
				} 
				elseif( $key == "tes_id_tesis_director" )
				{
					//$this->db->join("tesis_autores", "tes.tes_id_tesis = tesis_autores.tes_id_tesis", "LEFT");
					$this -> db -> where("tesis_directores.tes_id_tesis_director", $value);
                } 
                elseif( $key == 'tes_nombre_director')
                {
                    $this->db->where("tesis_directores.tes_nombre_director like '%{$value}%'");
                } 
                else {
                    $this -> db -> where($key, $value);
                }
                
            }
        }
    }

    /**
     * Establecerá el campo de acuerdo a alguna llava que se le pase, si no se le pasa nada
     * no hará nada, y será el orden por default
     *
     * @return void
     * @author
     */
    function set_orden($field = "", $type = "asc") {
        if ($type == "des")
            $type = "desc";
        if ($field) {
            if ($field == "autores") {
                $this -> db -> order_by("autores", $type);
            } elseif ($field == "tes_id_grado_obtenido") {
                $this -> db -> order_by("gra_descripcion", $type);
            } else {
                $this -> db -> order_by($field, $type);
            }
        }
    }

    /**
     * Obtiene las tesis con más información, como autores, grados, etc
     *
     * @return void
     * @author
     */
    function get_all() {
        $this -> db -> select("SQL_CALC_FOUND_ROWS tes.*,
        grados_academicos.*,
        tesis_autores.tes_nombre_autor,
        t_d.tes_nombre_director, t_d.tes_id_grado_academico,
        tesis_subdisciplinas.tes_id_subdisciplina, 
        GROUP_CONCAT(distinct(tesis_autores.tes_nombre_autor), ' ,') as autores,
        GROUP_CONCAT(distinct(t_d.tes_nombre_director), ' ') as directores,
        ( select count(*) from descargas where des_id_tesis = tes.tes_id_tesis  ) as descargas,
        if( tes_num_visitas is null, 0, tes_num_visitas) as visitas
        ", FALSE);
        $this -> db -> join("grados_academicos", "gra_id_grado_academico = tes_id_grado_obtenido", "LEFT");
        $this -> db -> join("tesis_autores", " tesis_autores.tes_id_tesis = tes.tes_id_tesis ", "LEFT");
		$this -> db -> join("tesis_directores as t_d", " t_d.tes_id_tesis = tes.tes_id_tesis ", "LEFT");
		$this->db->join("tesis_subdisciplinas", "tesis_subdisciplinas.tes_id_tesis = tes.tes_id_tesis", "LEFT");
		$this->db->join("cat_subdisciplinas", "cat_subdisciplinas.sub_id_subdisciplina = tesis_subdisciplinas.tes_id_subdisciplina", "LEFT");
		$this->db->join("cat_disciplinas_estudio", "cat_subdisciplinas.sub_id_disciplina_estudio = cat_disciplinas_estudio.dis_id_disciplina_estudio", "LEFT");
        $this -> db -> group_by(1);

        $rs = parent::get();
	//echo $this->db->last_query();
        return $rs;
    }

    function get_total_rows() {
        $res = $this -> db -> query("SELECT FOUND_ROWS() as rows") -> row_array();
        return $res["rows"];
    }

    public function agregar($data) {
        $user = $this -> ion_auth -> user() -> row();
        $data['tes_id_user_mod'] = $user -> id;
        $data['tes_id_user_crea'] = $user -> id;
        $data['tes_fecha_modificacion'] = date("Y-m-d H:i:s");
        $data['tes_fecha_creacion'] = date("Y-m-d H:i:s");
        //exit();
        return $this -> add($data, true);
    }

    public function agregar2($data) {
        $user = $this -> ion_auth -> user() -> row();
        $data['tes_id_user_mod'] = $user -> id;
        $data['tes_id_user_crea'] = $user -> id;
        $data['tes_fecha_modificacion'] = date("Y-m-d H:i:s");
        $data['tes_fecha_creacion'] = date("Y-m-d H:i:s");
        $this -> db -> insert('tesis', $data);
        $cid = $this -> db -> insert_id();

        return $cid;
    }


    //Consulta que genera todos los datos referente a un tesis. Para la vista a detalle del lado del administrador.
    public function visualizar_tesis_administrador($id){
        $query=$this->db->query("select tes.tes_id_tesis,tes.tes_nombre,tes.tes_resumen,tes.tes_anio_titulacion,tes.tes_ruta_tesis,
        tes.tes_palabras_clave,tes.tes_fecha_creacion,tes.tes_fecha_modificacion,
        ifnull(tes.tes_num_visitas,0) tes_num_visitas,tes.tes_cantidad_de_calificaciones, tes.tes_total_calificaciones,
        concat(usrc.first_name,' ',usrc.last_name) AS 'usuario_crea', 
        concat(usrm.first_name,' ',usrm.last_name) AS 'usuario_mod',  
        GROUP_CONCAT(distinct vsub_especie.nombre_esp_subesp) as esp_sub_especie, 
        GROUP_CONCAT(distinct vsub_dis.nombre_dis_subdis) as sub_disciplina, 
        GROUP_CONCAT(DISTINCT td.tes_nombre_director) as directores, 
        GROUP_CONCAT(DISTINCT ta.tes_nombre_autor) as autores,gra.gra_descripcion as grado_obtenido,
        cins.ins_nombre as nombre_institucion,cins.ins_dependencia,cins.ins_sede,
        GROUP_CONCAT(distinct ccon.con_nombre) as condicion_sitio, 
        GROUP_CONCAT(DISTINCT cze.est_nombre) as zonas_estudio from tesis as tes 
        left join tesis_directores td on td.tes_id_tesis=tes.tes_id_tesis 
        left join tesis_autores ta on ta.tes_id_tesis=tes.tes_id_tesis 
        left join grados_academicos gra on gra.gra_id_grado_academico=tes.tes_id_grado_obtenido 
        left join cat_instituciones cins on cins.ins_id_institucion=tes.tes_id_institucion 
        left join users usrc on usrc.id=tes.tes_id_user_crea 
        left join users usrm on usrm.id=tes.tes_id_user_mod 
        left join tesis_condiciones_sitio tcon on tcon.tes_id_tesis=tes.tes_id_tesis 
        left join cat_condiciones_sitio ccon on ccon.con_id_condicion_sitio=tcon.tes_id_condicion_sitio 
        left join tesis_sub_especies tsub_esp on tsub_esp.tes_id_tesis=tes.tes_id_tesis 
        left join vista_especie_subespecie vsub_especie on vsub_especie.sue_id_subespecie=tsub_esp.tes_id_sub_especie 
        left join tesis_subdisciplinas tsub_dis on tsub_dis.tes_id_tesis=tes.tes_id_tesis 
        left join vista_disciplina_subdisciplina vsub_dis on vsub_dis.sub_id_subdisciplina=tsub_dis.tes_id_subdisciplina 
        left join tesis_zona_estudio tze on tze.tes_id_tesis=tes.tes_id_tesis 
        left join cat_zonas_estudio cze on tze.tes_id_zona_estudio=cze.est_id_zona_estudio where tes.tes_id_tesis='$id'");
        return $query;
        //Fin
    }

	public function reporte_visitas(){
	    $this -> db -> select("tes.tes_nombre TITULO
               ,( select group_concat( tes_nombre_autor separator ', ') from tesis_autores ta where ta.tes_id_tesis=tes.tes_id_tesis ) AUTORES
               ,(select   group_concat( tes_nombre_director separator ', ')  from tesis_directores td where td.tes_id_tesis=tes.tes_id_tesis ) DIRECTORES
               ,gra_descripcion GRADOS 
               ,ifnull(tes_num_descargas,0) DESCARGAS
               ,ifnull(tes_num_visitas,0) VISITAS
        ", FALSE);
        $this -> db -> join("grados_academicos", "gra_id_grado_academico = tes_id_grado_obtenido", "LEFT");
        $this -> db -> order_by("tes_num_visitas", "desc");
        //$rs = parent::get();
        $rs = $this->db->get("tesis tes");	      
	    return $rs;  	    
	}
	
	
	public function reporte_descargas( $filtros = array() ){
		$from = '';
		$to= '';
		
		//Formamos el sql para el from to
		if ( isset($filtros['from']) && $filtros['from']){
			$from = date_to_mysql($filtros['from']);
			$from =  "cast( des_fecha as date) >= date '{$from}'";
		}
		if ( isset($filtros['to']) && $filtros['to']){
			$to = date_to_mysql($filtros['to']);
			$to =  "cast( des_fecha as date) <= date '{$to}'";
		}

		$from_to_sql = implode(' and ', array_filter( array($from, $to) ) );

		$from_to_sql = $from_to_sql ? ' and ' . $from_to_sql : ''; 
		
	    $this -> db -> select("tes.tes_nombre TITULO
               ,( select group_concat( tes_nombre_autor separator ', ') from tesis_autores ta where ta.tes_id_tesis=tes.tes_id_tesis ) AUTORES
               ,(select   group_concat( tes_nombre_director separator ', ')  from tesis_directores td where td.tes_id_tesis=tes.tes_id_tesis ) DIRECTORES
               ,gra_descripcion GRADOS 
               ,(
                    SELECT
                        count(*)
                    FROM
                        descargas
                    WHERE
                        des_id_tesis = tes.tes_id_tesis
                        {$from_to_sql}
                )  DESCARGAS,
                tes_id_tesis
        ", FALSE);
        $this -> db -> join("grados_academicos", "gra_id_grado_academico = tes_id_grado_obtenido", "LEFT");
        $this -> db -> order_by("DESCARGAS", "desc");
        $this->db->having('descargas >', 0);
        //$rs = parent::get();
        $rs = $this->db->get("tesis tes");      
	    return $rs;  	    
	}
	
	public function modificar($id,$data){
	    $user = $this -> ion_auth -> user() -> row();
        $data['tes_id_user_mod'] = $user -> id;
        $data['tes_fecha_modificacion'] = date("Y-m-d H:i:s");
		return $this->upd($id,$data);
     }
	
	public function modificarCalificacion($id,$data){
       return $this->upd($id,$data);
     }
    
	public function eliminar($tes_id_tesis){
		try{
			$this->load->model("descargas_m");
			$this->descargas_m->del($tes_id_tesis);
			
			//Borramos todos los documentos, audios, videos e imágenes
			$this->load->model( array('tesis_audios_m', 'tesis_imagenes_m', 'tesis_documentos_m', 'tesis_videos_m') );
			$this->tesis_audios_m->del_of( $tes_id_tesis );
			$this->tesis_imagenes_m->del_of( $tes_id_tesis );
			$this->tesis_documentos_m->del_of( $tes_id_tesis );
			$this->tesis_videos_m->del_of( $tes_id_tesis );
			
			$tesis = $this->get_by_id( $tes_id_tesis );
			unlink('./themes/default/pdf/' . $tesis['tes_ruta_tesis'] );
			
			$this->del($tes_id_tesis);
			return TRUE;
		}
		catch(Exception $e){
			return FALSE;
		}
	}
	public function eliminarRegistrosCruzados($nombreTbl, $data){
	    $this->db->delete($nombreTbl,$data);
	}	
	public function modificarTablasCruzadas($nombreTbl,$data){
	    //$this->eliminarRegistrosCruzados($nombreTbl,$data);    
	    $this->insertarTablasCruzadas($nombreTbl,$data);
	}
    public function insertarTablasCruzadas($nombreTbl, $data){
           $this->db->insert($nombreTbl, $data);
               
   }
   public function buscarPorId($id){
        return $this->get_by_id($id); // array("tes_titulo" => "valor")
       
   }
   
   public function get_autores($id){
      $this-> db-> select("tes_nombre_autor, tes_id_tesis_autores");
      $this-> db-> join("tesis_autores","tesis_autores.tes_id_tesis=tes.tes_id_tesis");
      $this-> db-> where("tes.tes_id_tesis",$id);
      $rs = parent::get();
      //array( array("aut_nombre" => "tal nombre", "aut_apell"), array("aut_nombre" => "tal nombre 2") )  
      return $rs;
   }
   public function get_directores($id){
      $this-> db-> select("tes_nombre_director, tes_id_grado_academico");
      $this-> db-> join("tesis_directores","tesis_directores.tes_id_tesis=tes.tes_id_tesis");
      $this-> db-> where("tes.tes_id_tesis",$id);
      $rs = parent::get();
      
      return $rs;
   }
   public function get_grado_academico($id){
      $this->load->model("directores_grados_m");
      return $this->directores_grados_m->get_grado($id);
   }
   public function get_subespecies($id){
      $this-> db-> select("tes_id_sub_especie");
      $this-> db-> join("tesis_sub_especies","tesis_sub_especies.tes_id_tesis=tes.tes_id_tesis");
      $this-> db-> where("tes.tes_id_tesis",$id);
      $rs = parent::get();
      return $rs;
   }
   public function get_zonas_estudio($id){
      $this-> db-> select("tes_id_zona_estudio");
      $this-> db-> join("tesis_zona_estudio","tesis_zona_estudio.tes_id_tesis=tes.tes_id_tesis");
      $this-> db-> where("tes.tes_id_tesis",$id);
      $rs = parent::get();
      return $rs;       
   }
   public function get_subdisciplinas($id){
      $this-> db-> select("tes_id_subdisciplina");
      $this-> db-> join("tesis_subdisciplinas","tesis_subdisciplinas.tes_id_tesis=tes.tes_id_tesis");
      $this-> db-> where("tes.tes_id_tesis",$id);
      $rs = parent::get();
      return $rs;  
   }
   public function get_condicion_sitio($id){
      $this-> db-> select("tes_id_condicion_sitio");
      $this-> db-> join("tesis_condiciones_sitio","tesis_condiciones_sitio.tes_id_tesis=tes.tes_id_tesis");
      $this-> db-> where("tes.tes_id_tesis",$id);
      $rs = parent::get();
      return $rs;       
   }
   public function agregarComentario($comentario){
       $this->load->model("tesis_comentarios_m");
       $this->tesis_comentarios_m->agregar($comentario);
   }
   public function get_imagenes($id){
      $this-> db-> select("tes_id_tesis_imagenes, tes_ruta_imagen");
      $this-> db-> join("tesis_imagenes","tesis_imagenes.tes_id_tesis=tes.tes_id_tesis");
      $this-> db-> where("tes.tes_id_tesis",$id);
      $rs = parent::get();
      return $rs;  
   }
   public function get_documentos($id){
      $this-> db-> select("tes_id_tesis_documentos, tes_ruta_documento");
      $this-> db-> join("tesis_documentos","tesis_documentos.tes_id_tesis=tes.tes_id_tesis");
      $this-> db-> where("tes.tes_id_tesis",$id);
      $rs = parent::get();
      return $rs;  
   }
   public function get_videos($id){
      $this-> db-> select("tes_id_tesis_videos, tes_ruta_video");
      $this-> db-> join("tesis_videos","tesis_videos.tes_id_tesis=tes.tes_id_tesis");
      $this-> db-> where("tes.tes_id_tesis",$id);
      $rs = parent::get();
      return $rs;  
   }
   public function get_audios($id){
      $this-> db-> select("tes_id_tesis_audios, tes_ruta_audio");
      $this-> db-> join("tesis_audios","tesis_audios.tes_id_tesis=tes.tes_id_tesis");
      $this-> db-> where("tes.tes_id_tesis",$id);
      $rs = parent::get();
      return $rs;  
   }
   	/**
    * Incrementa el número de visitas en la tabla de tesis
    *
    * @return boolean
    * @author  Uziel García <uzielgl@hotmail.com>
    */
	function incrementar_visitas($id_tesis, $numero_visitas = 1 ) {
		$this->db->where("tes_id_tesis", $id_tesis);
		$this->db->set("tes_num_visitas", " if(tes_num_visitas, tes_num_visitas, 0) + $numero_visitas ", FALSE );
		$this->db->update("tesis");
		return TRUE;
	}
	
	
 function obtener_directores($q){
    $this->db->select('tes_nombre_director');
     $this->db->distinct('tes_nombre_director');
    $this->db->like('tes_nombre_director', $q);
    $query = $this->db->get('tesis_directores');
    if($query->num_rows > 0){
      foreach ($query->result_array() as $row){
        //$row_set[] = htmlentities(stripslashes($row['tes_nombre_director'])); //build an array
        $row_set[] = stripslashes($row['tes_nombre_director']);
      }
      echo json_encode($row_set); //format the array into json data
    }
  }
	
	function cantidad_material_extra($id){
		$cantidad_imagenes=$this->db->query("SELECT COUNT( tes_id_tesis ) AS num FROM tesis_imagenes WHERE tes_id_tesis = '$id'");
		$cantidad_documentos=$this->db->query("SELECT COUNT( tes_id_tesis ) AS num FROM tesis_documentos WHERE tes_id_tesis = '$id'");
		$cantidad_videos=$this->db->query("SELECT COUNT( tes_id_tesis ) AS num FROM tesis_videos WHERE tes_id_tesis = '$id'");
		$cantidad_audios=$this->db->query("SELECT COUNT( tes_id_tesis ) AS num FROM tesis_audios WHERE tes_id_tesis = '$id'");
		$cantidades = array("cantidad_imagenes" => $cantidad_imagenes, "cantidad_documentos" => $cantidad_documentos, "cantidad_videos" => $cantidad_videos, "cantidad_audios" => $cantidad_audios);
		return $cantidades;
	}
	
	public function contarTodasTesis(){
	  $this-> db-> from("tesis");
      $rs = $this->db->count_all_results();
	  return $rs;
	}

}
