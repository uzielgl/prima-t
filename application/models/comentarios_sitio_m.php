<?php
/**
 * undocumented class
 *
 * @package default
 * @author  yairguz
 */
class Comentarios_sitio_m extends MY_Model {
	
	//Establecemos con que tabla va a trabajar normalmente y la primary key.
	//Estableciendo esto, ya tenemos métodos en MY_Model como add, upd, del.
	//De hecho, con esto, no es necesario el método agregar, pues ya está add en MY_Model, 
	//pero a modo de ejemplo se ha implementado.
	var $tb = "comentarios_sitio";
	var $pk = "com_id_comentario_sitio";
	//var $nombre = "esp_nombre";
	var $alias = "cos";
	
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	
	public function agregar($data){
		//Insertamos, es parte de la clase de Database, este insert
		//nos genera una instrucción sql, para este caso, que nada más viene el esp_nombre => "TAL NOMBRE" algo como 
		//INSERT INTO especies (esp_nombre) values ('TAL_NOMBRE') y la ejecuta
		//$this->db->insert( $this->tb, $data );
		$user=$this->ion_auth->user()->row();
        $data['com_id_user']=$user->id;
		$data['com_fecha_creacion']=date("Y-m-d H:i:s");
        $data['com_estado_comentario']=0;
		
		return $this->add( $data, true );
	}
	
	public function modificar($id,$data){
		$data['com_fecha_modificacion']=date("Y-m-d H:i:s");
        $data['com_id_user_mod']=$user->id;
		$this->upd($id,$data);
	}
	
	public function aprobar($id){
		$data['com_estado_comentario']=1;	
		$this->upd($id,$data);
	}
	
	
	
	public function eliminar($id){
		$this->del($id);
	}
	
	public function buscarTodos(){
	  $this-> db-> select("com_id_comentario_sitio, username, com_comentario, com_estado_comentario, cos.com_fecha_creacion");
      $this-> db-> join("users","users.id=cos.com_id_user");
	  $this-> db-> where("cos.com_estado_comentario",0);
      $rs = parent::get();
      
      return $rs;
	}
	
	public function contarNoAprobados(){
      $this->db->from($this->tb);
	  $this-> db-> where("com_estado_comentario",0);
	  $rs = $this->db->count_all_results();
	  return $rs;
	}
	
	public function buscarAprobados(){
	  $this-> db-> select("com_comentario, username, com_estado_comentario,cos.com_fecha_creacion");
      $this-> db-> join("users","users.id=cos.com_id_user");
      $this-> db-> where("cos.com_estado_comentario",1);
	  $this-> db-> order_by("cos.com_fecha_creacion","desc");
      $rs = parent::get();
	  return $rs;
	}
	
	
	
	public function buscarPorId($id){
		return $this->get_by_id($id);
	}
	
	
	public function contarTodos(){
	  $this-> db-> from("comentarios_sitio");
      $this-> db-> where("com_estado_comentario",0);
      $rs = $this->db->count_all_results();
	  return $rs;
	}
} // END