<?php
/**
 * undocumented class
 *
 * @package default
 * @author  kyoo
 */
class tesis_audios_m extends MY_Model {
	var $tb = "tesis_audios";
	var $pk = "tes_id_tesis_audios";
	var $nombre = "tes_ruta_audio";
	var $alias = "tes";
	
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	public function agregar($data){
		return $this->add( $data, true );
	}
	
	public function modificar($id,$data){
		$this->upd($id,$data);
	}
	
	public function eliminar($id){
		$this->del($id);
	}
	
	public function buscarTodos(){
		return $this->get();
	}
	
	public function buscarPorId($id){
		return $this->get_by_id($id);
	}
	
	public function del_of( $id_tesis ){
		$this->db->where( 'tes_id_tesis', $id_tesis );
		$items = $this->get();
		foreach( $items as $i ){
			unlink( './themes/default/aud/' . $i['tes_ruta_audio'] );
		}
		//Eliminamos de la BD
		$this->db->where( 'tes_id_tesis', $id_tesis );
		$this->db->delete( $this->tb );		
	}
}