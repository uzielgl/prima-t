<?php 
class Descargas_m extends MY_Model{
    
    var $tb = 'descargas';
    var $pk = 'des_id_descargas';
	var $alias = 'des';
    
    public function get_total( $id_tesis ){
        $this->db->select('count(*) as total');
        $this->db->from( $this->tb );
        $this->db->where('des_id_tesis', $id_tesis);
        $row = $this->db->get()->row();
        return $row->total;
    }
    
    /**
    * Agrega una  descargas en la tabla de descargas
    *
    * @return boolean
    * @author  Uziel García <uzielgl@hotmail.com>
    */
    public function add($id_tesis ) {
        $data = array(
            "des_fecha" => date('Y-m-d H:i:s'),
            "des_id_user" => $this->ion_auth->user()->row()->id,
            "des_id_tesis" => $id_tesis,
            "des_ip" => $this->input->ip_address(),
        );
        $this->db->insert('descargas', $data);
        
        return $this->db->insert_id();
    }   
	
	public function descargasPorTesis(){
        	
        $this->db->select("COUNT(des_id_tesis) as CANTIDAD,des_id_tesis, tes_nombre");
		$this-> db-> join("tesis","tesis.tes_id_tesis=des_id_tesis");
		$this->db->where("month(des_fecha)=month(now()) and  year(des_fecha)=year(now())");
		$this->db->group_by("des_id_tesis");
		$this->db->order_by("CANTIDAD","desc");
		$this->db->limit(5,0);
	    $rs = parent::get();
        return $rs;
	}
	
	/* Borra todas las descargas respecto a alguna tesis
	 * */
	public function del( $id_tesis ){
		$this->db->where("des_id_tesis", $id_tesis);
		$this->db->delete( $this->tb );
		return TRUE;
	}
    
} 