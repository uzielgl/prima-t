<?php
class Tesis_comentarios_m extends MY_MODEL{
	
    var $tb = "tesis_users_comentario";
    var $pk = "tes_id_tesis_users";
    var $alias = "tes";
	
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
    public function agregar($data){
        $user=$this->ion_auth->user()->row();
        $data['tes_id_user']=$user->id;
        $data['tes_fecha_creacion']=date("Y-m-d H:i:s");
        $this->add( $data, true );
    }
	public function cargarComentarios($id){
      $this-> db-> select("username, tes_comentario, tes_estado_comentario");
      $this-> db-> join("users","users.id=tes.tes_id_user");
      $this-> db-> where("tes.tes_id_tesis",$id);
      $this-> db-> where("tes.tes_estado_comentario",1);
      $this-> db-> order_by("tes.tes_fecha_creacion","desc");
      $rs = parent::get();
      
      return $rs;
    }
	public function buscarTodos(){
	  $this-> db-> select("tes_id_tesis_users, username, tes_nombre, tes_comentario, tes_estado_comentario,tes.tes_fecha_creacion");
      $this-> db-> join("users","users.id=tes.tes_id_user");
	  $this-> db-> join("tesis","tesis.tes_id_tesis=tes.tes_id_tesis");
	  $this-> db-> where("tes.tes_estado_comentario",0);
      $rs = parent::get();
      
      return $rs;
	}
	
	public function buscarAprobados(){
	  $this-> db-> select("tes_id_tesis_users, username, tes_nombre, tes_comentario, tes_estado_comentario,tes.tes_fecha_creacion");
      $this-> db-> join("users","users.id=tes.tes_id_user");
	  $this-> db-> join("tesis","tesis.tes_id_tesis=tes.tes_id_tesis");
	  $this-> db-> where("tes.tes_estado_comentario",1);
      $rs = parent::get();
      
      return $rs;
	}
	
	public function contarTodos(){
	  $this-> db-> from("tesis_users_comentario");
      $this-> db-> where("tes_estado_comentario",0);
      $rs = $this->db->count_all_results();
	  return $rs;
	}
	
	public function eliminar($tes_id_tesis_users){
		$this->del($tes_id_tesis_users);
	}
	
	public function aprobar($id){
		$data['tes_estado_comentario']=1;	
		$this->upd($id,$data);
	}
	
}