<?php
/**
 * undocumented class
 *
 * @package default
 * @author  yairguz
 */
class Subdisciplinas_m extends MY_Model {
	
	//Establecemos con que tabla va a trabajar normalmente y la primary key.
	//Estableciendo esto, ya tenemos métodos en MY_Model como add, upd, del.
	//De hecho, con esto, no es necesario el método agregar, pues ya está add en MY_Model, 
	//pero a modo de ejemplo se ha implementado.
	var $tb = "cat_subdisciplinas";
	var $pk = "sub_id_subdisciplina";
	var $nombre = "sub_nombre";
	var $alias = "sbd";

	
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	
	public function agregar($data){
		//Insertamos, es parte de la clase de Database, este insert
		//nos genera una instrucción sql, para este caso, que nada más viene el esp_nombre => "TAL NOMBRE" algo como 
		//INSERT INTO especies (esp_nombre) values ('TAL_NOMBRE') y la ejecuta
		//$this->db->insert( $this->tb, $data );
		//echo $data;
		$user=$this->ion_auth->user()->row();
		$data['sub_id_user_mod']=$user->id;
		$data['sub_id_user_crea']=$user->id;
		$data['sub_fecha_modificacion']=date("Y-m-d H:i:s");
		$data['sub_fecha_creacion']=date("Y-m-d H:i:s");
		//exit();
		
		return $this->add( $data, true );
	}
	
	public function modificar($id,$data){
		$data['sub_fecha_modificacion']=date("Y-m-d H:i:s");
		$this->upd($id,$data);
	}
	
	public function eliminar($sub_id_subdisciplina){
		$this->del($sub_id_subdisciplina);
	}
	
	public function buscarTodos(){
		return $this->get();
	}
	
	public function buscarPorId($id){
		return $this->get_by_id($id);
	}
	
	public function get_subdisciplinas( $id_disciplina ){
		$this->db->where("sub_id_disciplina_estudio", $id_disciplina);
		return parent::get();
	}
	public function buscarPorIdDisciplina($id){
		$this->db->where("sub_id_disciplina_estudio", $id);
		$rs = $this->get();
		if( count($rs) > 0 )
			return $rs;
		else
			return false;
	}
	
	/**
	 * Obtiene las subdisciplinas de una disciplina con sus totales de relaciones a las tesis
	 *
	 * @return array
	 * @author  
	 */
	public function get_subdisciplinas_con_cantidad( $id_disciplina ) {
		$rs = $this->db->query('
		select count(*) as cantidad, t.tes_id_subdisciplina,  sd.disciplina , sd.sub_nombre as subcategoria 
		from tesis_subdisciplinas t inner join 
		(select s.sub_id_subdisciplina, s.sub_nombre, s.sub_id_disciplina_estudio, d.dis_estudio_nombre as disciplina
		from cat_subdisciplinas s right join cat_disciplinas_estudio d 
		on (s.sub_id_disciplina_estudio = d.dis_id_disciplina_estudio)) sd 
		on (t.tes_id_subdisciplina = sd.sub_id_subdisciplina) 
		where sub_id_disciplina_estudio = ' . $this->db->escape( $id_disciplina ) . '
		group by t.tes_id_subdisciplina 
		order by cantidad desc;')->result();
		return $rs;
	}
	
	/**
	 * OBtiene todas las disciplinas y el número de tesis asociadas a esta subdisciplina
	 *
	 * @return void
	 * @author  
	 */
	function get_all_subdisciplinas_con_cantidad() {
		return $this->db->query('
			select sd.disciplina , sd.sub_nombre as subcategoria, count(*) as cantidad 
			from tesis_subdisciplinas t inner join 
			(select s.sub_id_subdisciplina, s.sub_nombre, s.sub_id_disciplina_estudio, d.dis_estudio_nombre as disciplina
			from cat_subdisciplinas s right join cat_disciplinas_estudio d 
			on (s.sub_id_disciplina_estudio = d.dis_id_disciplina_estudio)) sd 
			on (t.tes_id_subdisciplina = sd.sub_id_subdisciplina) 
			group by t.tes_id_subdisciplina 
			
			order by cantidad desc;');
	}
} // END

