<?php
class Directores_grados_m extends MY_MODEL{
    var $tb = "tesis_directores";
    var $pk = "tes_id_tesis_director";
    var $alias = "tes";
 public function get_grado($id){
      $director=$this->buscarPorId($id);
      //print_r(tes.tes_id_grado_academico);exit();
      $this-> db-> select("gra_descripcion, gra_id_grado_academico");
      $this-> db-> join("grados_academicos","grados_academicos.gra_id_grado_academico=tes.tes_id_grado_academico");
      $this-> db-> where("tes.tes_id_tesis_director",$id);
      $rs = parent::get();
      
      return $rs;
 }
 public function buscarPorId($id){
     return $this->get_by_id($id);
 }   
 
    public function get_names()
    {
        $rs = $this->db->query("
            select TRIM( tes_nombre_director ) name
            from tesis_directores
            group by trim( tes_nombre_director )");
    
        return $rs->result_array();
        
    }
}
