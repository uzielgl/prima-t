<?php
/**
 * undocumented class
 *
 * @package default
 * @author  kyoo
 */
class Tesis_videos_m extends MY_Model {
	var $tb = "tesis_videos";
	var $pk = "tes_id_tesis_videos";
	var $nombre = "tes_ruta_video";
	var $alias = "tes";
	
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	public function agregar($data){
		return $this->add( $data, true );
	}
	
	public function modificar($id,$data){
		$this->upd($id,$data);
	}
	
	public function eliminar($id){
		$this->del($id);
	}
	
	public function buscarTodos(){
		return $this->get();
	}
	
	public function buscarPorId($id){
		return $this->get_by_id($id);
	}
	
	public function del_of( $id_tesis ){
		//Eliminamos de la BD
		$this->db->where( 'tes_id_tesis', $id_tesis );
		$this->db->delete( $this->tb );		
	}
}