<?php

$lang['required'] = 'El campo %s es obligatorio.';
$lang['isset'] = 'El campo %s debe contener un valor.';
$lang['valid_email'] = 'El campo %s debe contener una dirección.';
$lang['valid_emails'] = 'El campo %s debe contener todas las direcciones de email válidas.';
$lang['valid_url'] = 'El campo %s debe contener una URL válido.';
$lang['valid_ip'] = 'El campo %s debe contener una IP válido.';
$lang['min_length'] = 'El campo %s debe tener al menos %s caracteres de longitud.';
$lang['max_length'] = 'El campo %s no puede exceder los %s caracteres de longitud.';
$lang['exact_length'] = 'El campo %s debe tener exactamente %s caracteres de longitud.';
$lang['alpha'] = 'El campo %s No acepta números ni caracteres raros';
$lang['alpha_numeric'] = 'El campo %s debe contener sólo números alfanuméricos.';
$lang['alpha_dash'] = 'El campo %s debe contener sólo números alfanuméricos y guión bajo.';
$lang['numeric'] = 'El campo %s debe ser un número.';
$lang['is_numeric'] = 'El campo %s debe contener sólo números.';
$lang['integer'] = 'El campo %s debe contener un número entero.';
$lang['matches'] = 'El campo %s no coincide con el campo %s.';
$lang['is_natural'] = 'El campo %s debe contener sólo números naturales.';
$lang['is_natural_no_zero'] = 'El campo %s debe contener un número natural diferente de ceroo.';
$lang['is_unique']='El campo %s debe ser único.';
$lang['regex_match']='El campo %s no debe contener caracteres extraños.';
?>