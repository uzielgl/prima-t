<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Estados extends CI_Migration {

	public function up()
	{
        $data = array(
            /* Ya están
            array('est_id_zona_estudio' => 1, 'est_nombre' => 'Campeche'),
            array('est_id_zona_estudio' => 2, 'est_nombre' => 'Chiapas'),
            array('est_id_zona_estudio' => 3, 'est_nombre' => 'Oaxaca'),
            array('est_id_zona_estudio' => 4, 'est_nombre' => 'Quintana Roo'),
            array('est_id_zona_estudio' => 5, 'est_nombre' => 'Tabasco'),
            array('est_id_zona_estudio' => 6, 'est_nombre' => 'Veracruz'),
            array('est_id_zona_estudio' => 7, 'est_nombre' => 'Yucatan'),
             * 
             */
            array('est_id_zona_estudio' => 8, 'est_nombre' => 'Aguascalientes'),
            array('est_id_zona_estudio' => 9, 'est_nombre' => 'Baja California'),
            array('est_id_zona_estudio' => 10, 'est_nombre' => 'Baja California Sur'), 
            array('est_id_zona_estudio' => 11, 'est_nombre' => 'Chihuahua'),
            array('est_id_zona_estudio' => 12, 'est_nombre' => 'Coahuila'),
            array('est_id_zona_estudio' => 13, 'est_nombre' => 'Colima'),
            array('est_id_zona_estudio' => 14, 'est_nombre' => 'Distrito Federal'),
            array('est_id_zona_estudio' => 15, 'est_nombre' => 'Durango'),
            array('est_id_zona_estudio' => 16, 'est_nombre' => 'Guanajuato'),
            array('est_id_zona_estudio' => 17, 'est_nombre' => 'Guerrero'),
            array('est_id_zona_estudio' => 18, 'est_nombre' => 'Hidalgo'),
            array('est_id_zona_estudio' => 19, 'est_nombre' => 'Jalisco'),
            array('est_id_zona_estudio' => 20, 'est_nombre' => 'Mexico'),
            array('est_id_zona_estudio' => 21, 'est_nombre' => 'Michoacan'),
            array('est_id_zona_estudio' => 22, 'est_nombre' => 'Morelos'),
            array('est_id_zona_estudio' => 23, 'est_nombre' => 'Nayarit'),
            array('est_id_zona_estudio' => 24, 'est_nombre' => 'Nuevo Leon'),
            array('est_id_zona_estudio' => 25, 'est_nombre' => 'Puebla'),
            array('est_id_zona_estudio' => 26, 'est_nombre' => 'Queretaro'),
            array('est_id_zona_estudio' => 27, 'est_nombre' => 'San Luis Potosi'),
            array('est_id_zona_estudio' => 28, 'est_nombre' => 'Sinaloa'),
            array('est_id_zona_estudio' => 29, 'est_nombre' => 'Sonora'),
            array('est_id_zona_estudio' => 30, 'est_nombre' => 'Tamaulipas'),
            array('est_id_zona_estudio' => 31, 'est_nombre' => 'Tlaxcala'),
            array('est_id_zona_estudio' => 32, 'est_nombre' => 'Zacatecas')
       );
       
       $this->db->insert_batch('cat_zonas_estudio', $data);
	}

	public function down()
	{
	    # No se puede borrar pues debe de estar con llaves forarenas en otros lados
	}
}
