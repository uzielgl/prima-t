Alias /prima-t/ "C:\BitNami\wampstack-5.4.14-0/apps/prima-t_trunk/htdocs/"
Alias /prima-t "C:\BitNami\wampstack-5.4.14-0/apps/prima-t_trunk/htdocs"
 
<Directory "C:\BitNami\wampstack-5.4.14-0/apps/prima-t_trunk/htdocs">
    Options +MultiViews
    AllowOverride None
    <IfVersion < 2.3 >
    Order allow,deny
    Allow from all
    </IfVersion>
    <IfVersion >= 2.3>
    Require all granted
    </IfVersion>
</Directory>
 
# Uncomment the following lines to see your application in the root
# of your URL. This is not compatible with more than one application.
#RewriteEngine On
#RewriteRule ^/$ /prima-t/ [PT]
