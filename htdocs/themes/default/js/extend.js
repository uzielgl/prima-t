//Global functions
function base_url( uri ){
    uri = uri || "";
    return b_url + uri;
}
function site_url( uri ){
    uri = uri || "";
    return s_url + "/" + uri;
}


/*Obtiene el id en formato asdfasdf-algo-id*/
function get_id(cad, separator){
	separator = separator || "-";
	var t = cad.split( separator );
	return t[ t.length - 1 ];	
}


function relocate(page, params){
	var body = document.body;
	var form = document.createElement('form');
	form.method = 'POST';
	form.action = page;
	form.name = 'jsform';
	for (index in params){
		var input = document.createElement('input');
		input.type='hidden';
		input.name=index;
		input.id=index;
		input.value=params[index];
		form.appendChild(input);
	}
	body.appendChild(form);
	//form.onsubmit = function(){ }
	form.submit();
	
}

/** El administrador de mensajes*/

msg = {
	ok : function( msg ){
		return $.noty({
			"text": msg,
			"layout":"topRight",
			"type":"success"
		});
	},
	error : function( msg ){
		return $.noty({
			"text": msg,
			"layout":"topRight",
			"type":"error"
		});
	},
	information : function( msg ){
		return $.noty({
			"text": msg,
			"layout":"topRight",
			"type":"information"
		});
	}
	
}