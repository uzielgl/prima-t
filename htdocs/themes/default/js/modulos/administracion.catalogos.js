$( document ).ready(function() {
    $('#ordernar_tabla').click(function(){
        $("#tabla_catalogos").tablesorter();         
    }); 
});
function modalInfo(titulo,contenido,temporal,retardo){
    if(retardo>0){
        setTimeout(function(){
           modalInfo(titulo,contenido,temporal);
        },retardo);
        return;
    }
    var btn_cerrar='<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
    $('#modal_info_header').html(btn_cerrar);
    $('#modal_info_header').append("<h5>"+titulo+"</h5>");
    $('#modal_info_body').html(contenido);
    $('#modal_info').modal('show');
    if(temporal)
        setTimeout(function(){
           $('#modal_info').modal('hide'); 
        },temporal);
}
function asignar_valores_modal_agregar(e,atributo_id,nombre_modal){
    var id=$(e).attr('id').substring($(e).attr('id').lastIndexOf('_')+1);
    if(nombre_modal){
        $('#modal_agregar_'+nombre_modal).modal('show');
        if($("#"+atributo_id+"_agregar").length>0)
            $("#"+atributo_id+"_agregar").val(id);
        else{
            var input_id=$('<input>').attr('type','hidden').attr('id',atributo_id+"_agregar").val(id);
            $('#modal_agregar_'+nombre_modal).append(input_id);
        }
    }
    else{
        $('#btn_agregar').attr(atributo, id);
        $('#modal_agregar').modal('show');
        if($("#"+atributo_id+"_agregar").length>0)
            $("#"+atributo_id+"_agregar").val(id);
        else{
            var input_id=$('<input>').attr('type','hidden').attr('id',atributo_id+"_agregar").val(id);
            $('#modal_agregar').append(input_id);
        }
    }
}
function asignar_valores_modal_eliminar(e,atributos,nombre_modal){
    var id=$(e).attr('id').substring($(e).attr('id').lastIndexOf('_')+1);
    var valor=$('#'+atributos[1]+"_"+id).text();
    var mensaje='¿Realmente deseas eliminar el elemento seleccionado? : "'+$.trim(valor) +'"';
    if(nombre_modal){
        $('#btn_eliminar_'+nombre_modal).attr('eliminar-id', id);
        $('#modal_eliminar_'+nombre_modal+'-body').text(mensaje);
        $('#modal_eliminar_'+nombre_modal).modal('show');
    }
    else{
        $('#btn_eliminar').attr('eliminar-id', id);
        $('#modal_eliminar-body').text(mensaje);
        $('#modal_eliminar').modal('show');
    }
    $('#btn_cancelar').focus()
}
function asignar_valores_modal_editar(e,atributos,nombre_modal){
    var id=$(e).attr('id').substring($(e).attr('id').lastIndexOf('_')+1);
    $('#btn_editar').attr('editar-id', id);
    $.each(atributos, function(i,atributo){
        var valor=$('#'+atributo+"_"+id).text();
        $('#'+atributo+'_editar').val( $.trim(valor) );
    });
    if(nombre_modal)
        $('#modal_editar_'+nombre_modal).modal('show');
    else
        $('#modal_editar').modal('show');
}
function agregarForm(url,atributos,subcatalogo,subfila){
    var atributos_valores={};
    for(var i=1;i<atributos.length;i++){
        var atributo=atributos[i];
        if($("#"+atributo+"_agregar").length>0){
            var valor=$("#"+atributo+"_agregar").val();
            atributos_valores[atributo]=valor;
        }
    }
    if(subfila)
        $('#modal_agregar_'+subcatalogo).modal('hide');
    else
        $('#modal_agregar').modal('hide');
    modalInfo("Agregar"," Agregando... <img src='"+b_url+"themes/default/img/loading.gif' alt>",false);
    var campus="";
    if($("#ins_campus_agregar").length >0 && $("#ins_campus_agregar option:selected").text()!= "Seleccionar"){
        campus=$("#ins_campus_agregar option:selected").text();
    }
    var institucion="";
    if($("#ins_nombre_agregar").length >0 ){
        institucion=$("#ins_nombre_agregar").val();
    }
    for(var i=1;i<atributos.length;i++){
        var atributo=atributos[i];
        if($("#"+atributo+"_agregar").length>0)
            $("#"+atributo+"_agregar").val("");
    }
    
    $.ajax({
        type: "POST",
        url: b_url+url,
        data: atributos_valores,
        success: function(datos){
            var data_agregar= JSON.parse(datos);
            if(data_agregar.success){
                      
                if(data_agregar.redirect_to)
                    window.location = data_agregar.redirect_to
                                  
                modalInfo("Agregar","<span class='label label-success'>Los datos fueron agregados correctamente.</span>",5000,750);
                
                var id=data_agregar.datos[atributos[0]];
                if(institucion.length>0){
                    var nueva_opcion="<option value='"+id+"' selected='selected'>"+institucion+"</option>";
                    $("#ins_campus_agregar").append(nueva_opcion);
                    $("#ins_campus_editar").append(nueva_opcion);
                }
                //cambiar el id de la tabla por tabla_catalogos
                //cambiar los id de editar y elimar por td_editar_id td_eliminar_id
                var fila="";
                if(subfila)
                    fila+="<tr id='tr_subcatalogo_"+id+"'>";
                else
                    fila+="<tr id='tr_"+id+"' class='success' >";
                if(!subfila){
                    for(var i=1;i<atributos.length;i++){
                        var atributo=atributos[i];
                        
                        if(atributo == "ins_campus")
                            fila+="<td id='"+atributo+"_"+id+"' >"+campus+"</td>"
                        else
                            fila+="<td id='"+atributo+"_"+id+"' >"+atributos_valores[atributo]+"</td>"
                    }
                    if(subcatalogo){
                        fila+="<td id='td_agregar_subcatalogo_"+id+"'} style='text-align: center;' >";
                        fila+="<a id='a_agregar_subcatalogo_"+id+"' class='btn btn-success btn-mini' href='#modal_agregar_"+subcatalogo+"' ><i class='icon-plus icon-white'></i> "+subcatalogo+"</a>";
                        fila+="</td>";
                    }
                    fila+="<td id='td_editar_"+id+"' style='text-align: center' >"
                    fila+="<a id='a_editar_"+id+"'  class='btn btn-warning btn-mini' href='#modal_editar'>";
                    fila+="<i class='icon-pencil icon-white'></i> Editar</a>";
                    fila+="</td>";
                    fila+="<td id='td_eliminar_"+id+"' style='text-align: center' >";
                    fila+="<a id='a_eliminar_"+id+"' class='btn btn-danger btn-mini' href='#modal_eliminar'>";
                    fila+="<i class='icon-remove icon-white'></i> Eliminar</a>";
                    fila+="</td>";
                    fila+="</tr>";
                    if(subcatalogo){
                        fila+="<tr id='tr_subcatalogos_"+id+"'} >";
                        fila+="<td id='td_subcatalogos_"+id+"'} colspan='4' >";
                        fila+="<table id='tabla_subcatalogos_"+id+"' class='table table-hover table-bordered tablesorter' style='position: static'>";
                        fila+="<tbody></tbody>";
                        fila+="</table>";
                        fila+="</td>";
                        fila+="</tr>";
                    }
                    $("#tabla_catalogos>tbody").append(fila);
                    $("#a_eliminar_"+id).on("click",function(){
                        asignar_valores_modal_eliminar(this,atributos); 
                    });
                    $("#a_editar_"+id).on("click",function(){
                        asignar_valores_modal_editar(this,atributos); 
                    });
                    if(subcatalogo){
                        if(subcatalogo.indexOf('especie')>=0)
                            $("#a_agregar_subcatalogo_"+id).on("click",function(){
                                asignar_valores_modal_agregar(this,'sue_id_especie',subcatalogo);
                            });
                        else
                            $("#a_agregar_subcatalogo_"+id).on("click",function(){
                                asignar_valores_modal_agregar(this,'sub_id_disciplina_estudio',subcatalogo);
                            });
                    }
                }
                else{
                    for(var i=1;i<atributos.length-1;i++){
                        var atributo=atributos[i];
                        fila+="<td id='"+atributo+"_"+id+"' >"+atributos_valores[atributo]+"</td>"
                    }
                    fila+="<td id='td_subcatalogo_editar_"+id+"' style='width:70px; text-align: center' >"
                    fila+="<a id='a_editar_subcatalogo_"+id+"'  class='btn btn-warning btn-mini' href='#modal_editar_"+subcatalogo+"'>";
                    fila+="<i class='icon-pencil icon-white'></i> Editar</a>";
                    fila+="</td>";
                    fila+="<td id='td_subcatalogo_eliminar_"+id+"' style='width:70px; text-align: center' >";
                    fila+="<a id='a_eliminar_subcatalogo_"+id+"' class='btn btn-danger btn-mini' href='#modal_eliminar_"+subcatalogo+"'>";
                    fila+="<i class='icon-remove icon-white'></i> Eliminar</a>";
                    fila+="</td>";
                    fila+="</tr>";
                    var id_esp=atributos_valores[atributos[atributos.length-1]];
                    $("#tabla_subcatalogos_"+id_esp+">tbody").append(fila);
                    $("#a_eliminar_subcatalogo_"+id).on("click",function(){
                        asignar_valores_modal_eliminar(this,atributos,subcatalogo); 
                    });
                    $("#a_editar_subcatalogo_"+id).on("click",function(){
                        asignar_valores_modal_editar(this,atributos,subcatalogo); 
                    });
                }   
            }
            else
                modalInfo("Agregar","<span class='label label-important'>Los datos no se pudieron agregar:</span>"+data_agregar.msg,false);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            modalInfo("Agregar","<span class='label label-important'>Hubo un error al agregar los datos.</span>",false);
        }
    });   
}
function editarForm(url,atributos,subcatalogo,atributos_valores){
    var atributos_valores={};
    var id = $('#btn_editar').attr('editar-id');
    atributos_valores[atributos[0]] =id;
    for(var i=1;i<atributos.length;i++){
        var atributo=atributos[i];
        var valor=$("#"+atributo+"_editar").val();
        atributos_valores[atributo]=valor;
    }
    var campus="";
    if($("#ins_campus_editar").length >0 && $("#ins_campus_editar option:selected").text()!= "Seleccionar"){
        campus=$("#ins_campus_editar option:selected").text();
    }
    if(subcatalogo)
        $('#modal_editar_'+subcatalogo).modal('hide');
    else
        $('#modal_editar').modal('hide');
    modalInfo("Editar"," Editando...  <img src='"+b_url+"themes/default/img/loading.gif' alt>",false);
    $.ajax({
        type: "POST",
        url: b_url+url,
        data: atributos_valores,
        success: function(datos){
            var data_agregar= JSON.parse(datos);
            if(data_agregar.success){
                modalInfo("Editar","<span class='label label-success'>Los datos fueron editados correctamente.</span>",5000,750);
                var fila="<tr id='tr_"+id+"' >";
                for(var i=1;i<atributos.length;i++){
                    var atributo=atributos[i];
                    if(atributo == "ins_campus")
                        $("#"+atributo+"_"+id).html(campus);
                    else
                        $("#"+atributo+"_"+id).html(atributos_valores[atributo]);
                }
            }
            else
                modalInfo("Editar","<span class='label label-warning'>Los datos no se pudieron editar.</span>"+data_agregar.msg,false);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            modalInfo("Editar","<span class='label label-important'>Hubo un error al editar los datos.</span>",false);
        }
    });
}

function eliminarForm(url,atributos, subcatalogo){
    var atributos_valores={};
    var id = 0;
    if(subcatalogo)
        id=$('#btn_eliminar_'+subcatalogo).attr('eliminar-id');
    else
        id=$('#btn_eliminar').attr('eliminar-id');
    atributos_valores[atributos[0]] = id;
    if(subcatalogo)
        $('#modal_eliminar_'+subcatalogo).modal('hide');
    else
        $('#modal_eliminar').modal('hide');
    modalInfo("Eliminar","Eliminando...  <img src='"+b_url+"themes/default/img/loading.gif' alt>",false);
    $.ajax({
        type: "POST",
        url: b_url+url,
        data: atributos_valores,
        success: function(datos){
            var data_agregar= JSON.parse(datos);
            if(data_agregar.success){
                modalInfo("Eliminar","<span class='label label-success'>Los datos fueron eliminados correctamente.</span>",5000,750);
                if(subcatalogo)
                    $("#tr_subcatalogo_"+id).remove();
                else{
                    $("#tr_"+id).remove();
                    $("#tr_subcatalogos_"+id).remove();
                }
            }
            else
                modalInfo("Eliminar","<span class='label label-warning'>Los datos no se pudieron eliminar.</span>"+data_agregar.msg,false);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            modalInfo("Eliminar","<span class='label label-important'>Hubo un error al eliminar los datos.</span>",false);
        }
    });
}