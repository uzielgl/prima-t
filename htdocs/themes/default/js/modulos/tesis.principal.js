$( document ).ready(function(){
    
    //$("#tes_id_tesis_autores").select2();
    //$("#tes_id_tesis_director").select2();
    
    $("#tes_nombre_director, #tes_nombre_autor").select2();
    
});



$(function(){
	$("#mostrar-filtros").click(function(){
		$(this).button('toggle');
		$("#fls-filtros").slideToggle();
		
		if( $("#mostrar").val() == 1 ){
			$("#mostrar").val("0");
			$("#fls-filtros input, #fls-filtros select").val("");
		}else $("#mostrar").val(1);
	});
	
	//Ponemos el toggle de opciones avanzadas
	if( $("#mostrar").val() == 1 ) $("#mostrar-filtros").button("toggle");
	
	//Cuando den click a un paginador, enviamos con todo y formulario
	$(".pagination a").click(function(e){
		e.preventDefault();
		e.stopPropagation();
		
		if( $(this).prop("href") ){
			var values  = {};
			//formamos el objeto
			$.each($(".filtros input, .filtros select"), function(){
				values[$(this).attr("name")] = $(this).val();
			});
	
			relocate( $(this).prop("href"), values );
		}
	});
	
	$(".filtros input:first").focus();
	
	//Para el ordenamiento en las columnas, cuando dan click en alguna de ellas
	$(".order th .filter").click(function(e){
		var value = $(this).parents("th").data("field");
		var type = $(this).parents("th").data("type");
		
		if( !type ) type = "asc";
		else if( type == "asc" ) type = "des";
		else if( type == "des" ) type = "asc"
		
		
		$("#order_by").val( value );
		$("#order_type").val( type );
		
		
		var values  = {};
		//formamos el objeto
		$.each($(".filtros input, .filtros select"), function(){
			values[$(this).attr("name")] = $(this).val();
		});
		relocate( window.location.href, values );
		
	});
	
	//Para el combo de Disciplina y ligarlo con subdisciplina
	$("select[name=tes_id_disciplina_estudio]").change(function( e ){
		$cmb = $(this);
		$.ajax({
			beforeSend : function( ){
				this.idMessage = msg.ok("Descargando subdisciplinas...");
			},
			complete : function(){
				$.noty.close( this.idMessage );
			},
			type: "POST",
			url : site_url() + "catalogos/subdisciplinas_c/get_combobox_subdisciplinas/" + $cmb.val(),
			success: function( data ){
				$("#tes_id_subdisciplina").replaceWith( data );
			}
			
		})
	});
	
	
	//Cuando dan click al botón de limpiar
	$('#btn-clean').click(function(e){
		e.stopPropagation();
		e.preventDefault();
		
		$("#tes_nombre_director").select2('val', '');
		$("#tes_nombre_autor").select2('val', '');
		
		var $form = $(".filtros > form");
		$form.find('select, input').val('')
		
	});
	
	el = new EliminarTesis();
});





function EliminarTesis(){
	var me = this;
	
	//Click en eliminar de cada tesis
	$("table.tesis").on("click", ".btn-danger", function(e){
		e.preventDefault();
		e.stopPropagation();
		
		//Buscamos información
		me.title = $(this).parents("tr").find("td:first-child").html();
		me.id_tesis = get_id( $(this).prop("id") );
		me.mostrarConfirmacion();
	});
	
	//Click en Eliminar en el modal de confirmación de eliminar tesis
	$("#btn-eliminar-tesis").click(function(e){
		e.preventDefault();
		e.stopPropagation();
		$("#mdl-eliminar-tesis").modal("hide");
		me.mostrarCarga();
		//hacemos la petición
		$.ajax({
			type: "POST",
			url : site_url( "catalogos/tesis/eliminar/" + me.id_tesis ),
			success : function( data ){
				var res = JSON.parse( data );
				if( res.success ){
					modalInfo("Eliminar","<span class='label label-success'>Los datos fueron eliminados correctamente.</span>",5000,750);
					$("#btn-eliminar-tesis-" + me.id_tesis).parents( "tr" ).fadeOut(function(e){
						$('.total-tesis').html( $('.total-tesis').html() - 1 );
					});
				}else{
					modalInfo("Eliminar","<span class='label label-important'>" + res.msg + "</span>", false);
				}
			}, 
			complete : function(){
				$("#mdl-espera-eliminar-tesis").modal("hide");
			}
		});
	});
	
}

EliminarTesis.prototype = {
	mostrarConfirmacion : function(){
		//Establecemos los valores
		$(".nombre-tesis").html( "\"<strong>" + $.trim( this.title ) + "</strong>\"" );
		$("#mdl-eliminar-tesis").modal("show");
	},
	mostrarCarga : function(){
		$("#mdl-espera-eliminar-tesis").modal("show");
	}
}



function modalInfo(titulo,contenido,temporal,retardo){
    if(retardo>0){
        setTimeout(function(){
           modalInfo(titulo,contenido,temporal);
        },retardo);
        return;
    }
    var btn_cerrar='<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
    $('#modal_info_header').html(btn_cerrar);
    $('#modal_info_header').append("<h5>"+titulo+"</h5>");
    $('#modal_info_body').html(contenido);
    $('#modal_info').modal('show');
    if(temporal)
        setTimeout(function(){
           $('#modal_info').modal('hide'); 
        },temporal);
}