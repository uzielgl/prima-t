$( document ).ready(function() {
	$('a[href="#btn_abstract"]').click(function() {
		var id_abstract="#abstract_"+$(this).attr("id_tesis");
		$(id_abstract).toggle('slow');
    });
    
    //Cuando dan click en el botón de descargar tesis, aumentos en uno el contador de descargas
    $('.btn-descargar').click(function(){
    	$('.descargas-totales').html( +$('.descargas-totales').html() + 1 );
    });
    
    //Inicializamos el visualizador de imágenes
    if( $.prettyPhoto )
    	$("a[rel^='prettyPhoto']").prettyPhoto();
});

function modalMostrarImagenes(e){
    var id=$(e).attr('id_tesis');
    $('#modal_tesis_imagenes').modal('show');
    $('#modal_tesis_imagenes_body').html(
    '<img src="'+b_url+'themes/default/img/loading.gif" >');
    $.ajax({
        type: "POST",
        url: s_url+'/test/materialExtraImagenes/'+id,
        success: function(datos){
            var imagenes= JSON.parse(datos);
            var numImagenes=Object.keys(imagenes).length-1;
            $('#modal_tesis_imagenes_body').html('<h6>Existentes ('+numImagenes+')</h6>');
            
            for(var datos in imagenes){
                if(datos!='success'){
                    var nombre_imagen=imagenes[datos].tes_ruta_imagen;
                    var m_extra=$('<a href="'+b_url+'themes/default/img/'+nombre_imagen+'" rel="prettyPhoto[pp_gal]" title="Banana" data-gallery="gallery">'+
                    '<img src="'+b_url+'themes/default/img/' + nombre_imagen + '" /> '+
                    ' </a>');
                    
                    $('#modal_tesis_imagenes_body').append(m_extra);                    
                }
            }
            
            //Lanzamos la librería de imágenes
            $("a[rel^='prettyPhoto']").prettyPhoto();
            
        },
        error: function (xhr, ajaxOptions, thrownError) {
            
        }
    });
}
function modalMostrarVideos(e){
    var id=$(e).attr('id_tesis');
    $('#modal_tesis_videos').modal('show');
    $('#modal_tesis_videos_body').html(
    '<img src="'+b_url+'themes/default/img/loading.gif" >');
    $.ajax({
        type: "POST",
        url: s_url+'/test/materialExtraVideos/'+id,
        success: function(datos){
            var videos= JSON.parse(datos);
            var numVideos=Object.keys(videos).length-1;
            $('#modal_tesis_videos_body').html('<h6>Existentes ('+numVideos+')</h6>');
            var i=1;
            for(var datos in videos){
                if(datos!='success'){
                    var url=videos[datos].tes_ruta_video;
                    var m_extra=$('<div id="videoDiv'+i+'">Cargando...</div>');
                    $('#modal_tesis_videos_body').append(m_extra);
                    loadPlayer(i,url);
                }
                i++;
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr+" "+ajaxOptions+" "+thrownError);
            $('#modal_tesis_videos').modal('hide');
        }
    }); 
}
function modalMostrarDocumentos(e){
    var id=$(e).attr('id_tesis');
    $('#modal_tesis_documentos').modal('show');
    $('#modal_tesis_documentos_body').html(
    '<img src="'+b_url+'themes/default/img/loading.gif" >');
    $.ajax({
        type: "POST",
        url: s_url+'/test/materialExtraDocumentos/'+id,
        success: function(datos){
            var documentos= JSON.parse(datos);
            var numDocumentos=Object.keys(documentos).length-1;
            $('#modal_tesis_documentos_body').html('<h6>Existentes ('+numDocumentos+')</h6>');
            for(var datos in documentos){
                if(datos!='success'){
                    var nombre_documento=documentos[datos].tes_ruta_documento;
                    var m_extra=$('<a href="'+b_url+'themes/default/pdf/'+nombre_documento+'"/>').html(nombre_documento);
                    
                    $('#modal_tesis_documentos_body').append($('<div/>').append(m_extra));
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {}
    });        
}

function modalMostrarAudios(e){
    var id=$(e).attr('id_tesis');
    $('#modal_tesis_audios').modal('show');
    $('#modal_tesis_audios_body').html(
    '<img src="'+b_url+'themes/default/img/loading.gif" >');
    $.ajax({
        type: "POST",
        url: s_url+'/test/materialExtraAudios/'+id,
        success: function(datos){
            var audios= JSON.parse(datos);
            var numAudios=Object.keys(audios).length-1;
            $('#modal_tesis_audios_body').html('<h6>Existentes ('+numAudios+')</h6>');
            for(var datos in audios){
                if(datos!='success'){
                    var nombre_audio=audios[datos].tes_ruta_audio;
                    var m_extra=$('<a href="'+b_url+'themes/default/aud/'+nombre_audio+'"/>').html(nombre_audio);
                    
                    $('#modal_tesis_audios_body').append($('<div/>').append(m_extra));
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {}
    });        
}

// Update a particular HTML element with a new value
      function updateHTML(elmId, value) {
        document.getElementById(elmId).innerHTML = value;
      }
      
      // This function is called when an error is thrown by the player
      function onPlayerError(errorCode) {
        alert("An error occured of type:" + errorCode);
      }
      
      // This function is called when the player changes state
      function onPlayerStateChange(newState) {
        updateHTML("playerState", newState);
      }
      
      // Display information about the current state of the player
      function updatePlayerInfo() {
        // Also check that at least one function exists since when IE unloads the
        // page, it will destroy the SWF before clearing the interval.
        if(ytplayer && ytplayer.getDuration) {
          updateHTML("videoDuration", ytplayer.getDuration());
          updateHTML("videoCurrentTime", ytplayer.getCurrentTime());
          updateHTML("bytesTotal", ytplayer.getVideoBytesTotal());
          updateHTML("startBytes", ytplayer.getVideoStartBytes());
          updateHTML("bytesLoaded", ytplayer.getVideoBytesLoaded());
        }
      }
      
      // This function is automatically called by the player once it loads
      function onYouTubePlayerReady(playerId) {
        ytplayer = document.getElementById("ytPlayer");
        // This causes the updatePlayerInfo function to be called every 250ms to
        // get fresh data from the player
        setInterval(updatePlayerInfo, 250);
        updatePlayerInfo();
        ytplayer.addEventListener("onStateChange", "onPlayerStateChange");
        ytplayer.addEventListener("onError", "onPlayerError");
      }
      
      // The "main method" of this sample. Called when someone clicks "Run".
      function loadPlayer(i,url) {
        // Lets Flash from another domain call JavaScript
        var videoID = getVideoID(url);
        // Lets Flash from another domain call JavaScript
        var params = { allowScriptAccess: "always" };
        // The element id of the Flash embed
        var atts = { id: "ytPlayer"+i };
        // All of the magic handled by SWFObject (http://code.google.com/p/swfobject/)
        swfobject.embedSWF("http://www.youtube.com/v/" + videoID + 
                           "?version=3&enablejsapi=1&playerapiid=player1", 
                           "videoDiv"+i, "480", "295", "9", null, null, params, atts)
      }
      function getVideoID(url){
      	url = url || "";
        var st=url;
        if(url.indexOf('v=')>=0)
            st=url.substring(url.indexOf('v=')+2);
        if(url.indexOf('v/')>=0)
            st=url.substring(url.indexOf('v/')+2);
        if(url.indexOf('http://youtu.be/')>=0)
            st=url.substring(url.indexOf('http://youtu.be/')+16);
        if(st.indexOf('&')>=0)
            st=st.substring(0,st.indexOf('&'));
        return st;
      }
      function _run() {
        loadPlayer();
      }
      google.setOnLoadCallback(_run);