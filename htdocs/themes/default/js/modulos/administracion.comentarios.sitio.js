$( document ).ready(function() {
        
    //Eventos de los botones de la pagina para personalizar modals
    $('a[href="#modal_aprobar_sitio"]').click(function(){
    	var miid=$(this).attr("id_c");
    	var comentario=$(this).attr("comentario_sitio");
		$("#id_coment_aprobar_sitio").val(miid);
		$("#comentario_para_aprobar_sitio").html(comentario);
		$('#modal_aprobar_sitio').modal('show');
    });
    
    
    $('a[href="#modal_eliminar_sitio"]').click(function(){
    	var miid=$(this).attr("id_c");
    	var comentario=$(this).attr("comentario_sitio");
		$("#id_coment_eliminar_sitio").val(miid);
		$("#comentario_para_eliminar_sitio").html(comentario);
		$('#modal_eliminar_sitio').modal('show');
		
    });
    
    //Eventos de los botones de los modals
    $('#btn_aprobar_sitio').click(function() {
          $.ajax({type:'POST', dataType: 'json', url:'../admon_comentarios_c/aprobarComentSitio',data:$('#formAprobar_sitio').serialize(),success:function(data){
                if(data.success==true){
                  location.reload();
                }else{
                    msg.error(data.msg);
                }
          }});
    });
    
    $('#btn_eliminar_sitio').click(function() {
        
         $.ajax({type:'POST', dataType: 'json', url:'../admon_comentarios_c/eliminarComentSitio',data:$('#formEliminar_sitio').serialize(),success:function(data){
                if(data.success==true){
                  location.reload();
                }else{
                    msg.error(data.msg);
                }
          }});
    });
});