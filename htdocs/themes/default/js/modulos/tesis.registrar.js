var inputsAutores=1;

function AgregarCamposAutor(btn){
	inputsAutores++;
	var campo= $(btn).parents("#autor1").clone();
	campo.find("input").val("");
	campo.prop("id","autor"+inputsAutores);
	campo.find("[name=agregar_autor]").attr("onclick", 'QuitarCampoAutor(\'autor'+inputsAutores+'\');');
	campo.find("[name=agregar_autor]").html("-");
	$("#camposAutores").append(campo);
	
}

function QuitarCampoAutor(elemento){
    $("#"+elemento).remove();
    inputsAutores--;
}

var inputsDirectores=1;
function AgregarCamposDirector(btn){
    inputsDirectores++;

    var campo = $(btn).parents("#director1").clone();
    campo.find("input, select").val("");
    campo.prop("id", "director" + inputsDirectores );
    campo.find("[name=agregar_director]").attr("onclick", 'QuitarCampoDirector(\'director'+inputsDirectores+'\');');
    campo.find("[name=agregar_director]").html("-");
    $("#camposDirectores").append(campo);
	campo.find("input").autocomplete({ source: "obtener_directores" });
	
}

function QuitarCampoDirector(elemento){
    $("#"+elemento).remove();
    inputsDirectores--;
}

$(document).ready(function() {
    $("#e1").select2();
});
$(document).ready(function() {
    $("#est_id_zona_estudio").select2();
});

$(document).ready(function() {
    $("#con_id_condicion_sitio").select2();
});
$(document).ready(function() {
    $("#id_vista_subespecies").select2();

});
$(document).ready(function() {
    $("#id_vista_subdisciplinas").select2();
});

$( document ).ready(function() {
    $("#agregar").click(function() {

        var r=$("#id_vista_subespecies").select2("val");
        var input = $("<input>").attr("type", "hidden").attr("name", "id_vista_subespecies2[]").val(r);
        $('#form_agregar').append($(input));
        
         var r1=$("#est_id_zona_estudio").select2("val");
        var input1 = $("<input>").attr("type", "hidden").attr("name", "est_id_zona_estudio2[]").val(r1);
        $('#form_agregar').append($(input1));
        
         var r2=$("#con_id_condicion_sitio").select2("val");
        var input2 = $("<input>").attr("type", "hidden").attr("name", "con_id_condicion_sitio2[]").val(r2);
        $('#form_agregar').append($(input2));
        
         var r3=$("#id_vista_subdisciplinas").select2("val");
        var input3 = $("<input>").attr("type", "hidden").attr("name", "id_vista_subdisciplinas2[]").val(r3);
        $('#form_agregar').append($(input3));
    });
});

$(function(){
  $("#tes_nombre_director").autocomplete({
    source: "obtener_directores" // path to the get_birds method
   //  source: [ "c++", "java", "php", "coldfusion", "javascript", "asp", "ruby" ]
    //minLength: 2
  });
});
 
  


