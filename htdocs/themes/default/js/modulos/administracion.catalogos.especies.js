$( document ).ready(function() {
    
    //Eventos de los botones de la pagina para personalizar modals
    $('a[href="#modal_agregar_subespecies"]').click(function(){
        asignar_valores_modal_agregar(this,'sue_id_especie','subespecies');
    });
    $('a[href="#modal_editar"]').click(function(){
        var nombre_atributos=['esp_id_especie','esp_nombre'];
        asignar_valores_modal_editar(this,nombre_atributos);
    });
    $('a[href="#modal_eliminar"]').click(function(){
        var nombre_atributos=['esp_id_especie','esp_nombre'];
        asignar_valores_modal_eliminar(this,nombre_atributos); 
    });
    $('a[href="#modal_editar_subespecies"]').click(function(){
        var nombre_atributos=['sue_id_subespecie','sue_nombre'];
        asignar_valores_modal_editar(this,nombre_atributos,'subespecies');
    });
    $('a[href="#modal_eliminar_subespecies"]').click(function(){
        var nombre_atributos=['sue_id_subespecie','sue_nombre'];
        asignar_valores_modal_eliminar(this,nombre_atributos,'subespecies'); 
    });
    //Eventos de los botones de los modals
    $('#btn_editar').click(function() {
        var nombre_atributos=['esp_id_especie','esp_nombre'];
        var url_controlador='index.php/catalogos/especies_c/modificar';
        editarForm(url_controlador,nombre_atributos);
    });
    $('#btn_agregar').click(function() {
        var nombre_atributos=['esp_id_especie','esp_nombre'];
        var url_controlador='index.php/catalogos/especies_c/agregar';
        agregarForm(url_controlador,nombre_atributos,"subespecies");
    });
    $('#btn_eliminar').click(function() {
        var nombre_atributos=['esp_id_especie','esp_nombre'];
        var url_controlador='index.php/catalogos/especies_c/eliminar';
        eliminarForm(url_controlador,nombre_atributos);
    });
    $('#btn_agregar_subespecies').click(function() {
        var nombre_atributos=['sue_id_subespecie','sue_nombre','sue_id_especie'];
        var url_controlador='index.php/catalogos/especies_c/agregarSubespecie';
        agregarForm(url_controlador,nombre_atributos,"subespecies",true);
    });
    $('#btn_editar_subespecies').click(function() {
        var nombre_atributos=['sue_id_subespecie','sue_nombre'];
        var url_controlador='index.php/catalogos/especies_c/modificarSubespecie';
        editarForm(url_controlador,nombre_atributos,"subespecies");
    });
    $('#btn_eliminar_subespecies').click(function() {
        var nombre_atributos=['sue_id_subespecie','sue_nombre'];
        var url_controlador='index.php/catalogos/especies_c/eliminarSubespecie';
        eliminarForm(url_controlador,nombre_atributos,"subespecies");
    });
});
