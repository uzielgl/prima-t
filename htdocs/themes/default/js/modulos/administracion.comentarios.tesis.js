$( document ).ready(function() {
        
    //Eventos de los botones de la pagina para personalizar modals
    $('a[href="#modal_aprobar"]').click(function(){
    	var miid=$(this).attr("id_c");
    	var comentario=$(this).attr("comentario_tesis");
		$("#id_coment_aprobar").val(miid);
		$("#comentario_para_aprobar").html(comentario);
		$('#modal_aprobar').modal('show');
    });
    
    
    $('a[href="#modal_eliminar"]').click(function(){
    	var miid=$(this).attr("id_c");
    	var comentario=$(this).attr("comentario_tesis");
		$("#id_coment_eliminar").val(miid);
		$("#comentario_para_eliminar").html(comentario);
		$('#modal_eliminar').modal('show');
		
    });
    
    //Eventos de los botones de los modals
    $('#btn_aprobar').click(function() {
          $.ajax({type:'POST', dataType: 'json', url:'../admon_comentarios_c/aprobarComentTesis',data:$('#formAprobar').serialize(),success:function(data){
                if(data.success==true){
                  location.reload();
                }else{
                    msg.error(data.msg);
                }
          }});
    });
    
    $('#btn_eliminar').click(function() {
        
         $.ajax({type:'POST', dataType: 'json', url:'../admon_comentarios_c/eliminarComentTesis',data:$('#formEliminar').serialize(),success:function(data){
                if(data.success==true){
                  location.reload();
                }else{
                    msg.error(data.msg);
                }
          }});
    });
});