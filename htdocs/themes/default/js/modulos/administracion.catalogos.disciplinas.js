$( document ).ready(function() {
    
    //Eventos de los botones de la pagina para personalizar modals
    $('a[href="#modal_agregar_subdisciplinas"]').click(function(){
        asignar_valores_modal_agregar(this,'sub_id_disciplina_estudio','subdisciplinas');
    });
    $('a[href="#modal_editar"]').click(function(){
        var nombre_atributos=['dis_id_disciplina_estudio','dis_estudio_nombre'];
        asignar_valores_modal_editar(this,nombre_atributos);
    });
    $('a[href="#modal_eliminar"]').click(function(){
        var nombre_atributos=['dis_id_disciplina_estudio','dis_estudio_nombre'];
        asignar_valores_modal_eliminar(this,nombre_atributos); 
    });
    $('a[href="#modal_editar_subdisciplinas"]').click(function(){
        var nombre_atributos=['sub_id_subdisciplina','sub_nombre'];
        asignar_valores_modal_editar(this,nombre_atributos,'subdisciplinas');
    });
    $('a[href="#modal_eliminar_subdisciplinas"]').click(function(){
        var nombre_atributos=['sub_id_subdisciplina','sub_nombre'];
        asignar_valores_modal_eliminar(this,nombre_atributos,'subdisciplinas'); 
    });
    //Eventos de los botones de los modals
    $('#btn_editar').click(function() {
        var nombre_atributos=['dis_id_disciplina_estudio','dis_estudio_nombre'];
        var url_controlador='index.php/catalogos/disciplina_estudio_c/modificar';
        editarForm(url_controlador,nombre_atributos);
    });
    $('#btn_agregar').click(function() {
        var nombre_atributos=['dis_id_disciplina_estudio','dis_estudio_nombre'];
        var url_controlador='index.php/catalogos/disciplina_estudio_c/agregar';
        agregarForm(url_controlador,nombre_atributos,"subdisciplinas");
    });
    $('#btn_eliminar').click(function() {
        var nombre_atributos=['dis_id_disciplina_estudio','dis_estudio_nombre'];
        var url_controlador='index.php/catalogos/disciplina_estudio_c/eliminar';
        eliminarForm(url_controlador,nombre_atributos);
    });
    $('#btn_agregar_subdisciplinas').click(function() {
        var nombre_atributos=['sub_id_subdisciplina','sub_nombre','sub_id_disciplina_estudio'];
        var url_controlador='index.php/catalogos/disciplina_estudio_c/agregarSubdisciplina';
        agregarForm(url_controlador,nombre_atributos,"subdisciplinas",true);
    });
    $('#btn_editar_subdisciplinas').click(function() {
        var nombre_atributos=['sub_id_subdisciplina','sub_nombre','sub_id_disciplina_estudio'];
        var url_controlador='index.php/catalogos/disciplina_estudio_c/modificarSubdisciplina';
        editarForm(url_controlador,nombre_atributos,"subdisciplinas");
    });
    $('#btn_eliminar_subdisciplinas').click(function() {
        var nombre_atributos=['sub_id_subdisciplina','sub_nombre','sub_id_disciplina_estudio'];
        var url_controlador='index.php/catalogos/disciplina_estudio_c/eliminarSubdisciplina';
        eliminarForm(url_controlador,nombre_atributos,"subdisciplinas");
    });
});
