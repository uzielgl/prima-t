$( document ).ready(function() {
        //Eventos de los botones de la pagina para personalizar modals
    $('a[href="#modal_editar"]').click(function(){
          var nombre_atributos=['ins_id_institucion','ins_nombre', 'ins_siglas','ins_dependencia','ins_sede'];
        asignar_valores_modal_editar(this,nombre_atributos);
    });
    $('a[href="#modal_eliminar"]').click(function(){
          var nombre_atributos=['ins_id_institucion','ins_nombre', 'ins_siglas','ins_dependencia','ins_sede'];
        asignar_valores_modal_eliminar(this,nombre_atributos); 
    });
    
    //Eventos de los botones de los modals
    $('#btn_editar').click(function() {
        var nombre_atributos=['ins_id_institucion','ins_nombre', 'ins_siglas','ins_dependencia','ins_sede'];
        var url_controlador='index.php/catalogos/instituciones_c/modificar';
        editarForm(url_controlador,nombre_atributos);
    });
    $('#btn_agregar').click(function() {
        var nombre_atributos=['ins_id_institucion','ins_nombre', 'ins_siglas','ins_dependencia','ins_sede'];
        var url_controlador='index.php/catalogos/instituciones_c/agregar';
        agregarForm(url_controlador,nombre_atributos);
    });
    $('#btn_eliminar').click(function() {
        var nombre_atributos=['ins_id_institucion','ins_nombre', 'ins_siglas','ins_dependencia','ins_sede'];
        var url_controlador='index.php/catalogos/instituciones_c/eliminar';
        eliminarForm(url_controlador,nombre_atributos);
    });
});
