$( document ).ready(function() {
    
    //Eventos de los botones de la pagina para personalizar modals
    $('a[href="#modal_editar"]').click(function(){
        var nombre_atributos=['sub_id_subdisciplina','sub_nombre'];
        asignar_valores_modal_editar(this,nombre_atributos);
    });
    $('a[href="#modal_eliminar"]').click(function(){
        var nombre_atributos=['sub_id_subdisciplina','sub_nombre'];
        asignar_valores_modal_eliminar(this,nombre_atributos); 
    });
    
    //Eventos de los botones de los modals
    $('#btn_editar').click(function() {
        var nombre_atributos=['sub_id_subdisciplina','sub_nombre'];
        var url_controlador='index.php/catalogos/subdisciplinas_c/modificar';
        editarForm(url_controlador,nombre_atributos);
    });
    $('#btn_agregar').click(function() {
        var nombre_atributos=['sub_id_subdisciplina','sub_nombre'];
        var url_controlador='index.php/catalogos/subdisciplinas_c/agregar';
        agregarForm(url_controlador,nombre_atributos);
    });
    $('#btn_eliminar').click(function() {
        var nombre_atributos=['sub_id_subdisciplina','sub_nombre'];
        var url_controlador='index.php/catalogos/subdisciplinas_c/eliminar';
        eliminarForm(url_controlador,nombre_atributos);
    });
});
