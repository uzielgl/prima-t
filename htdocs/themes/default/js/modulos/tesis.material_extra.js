var inputsImagenes=1;
function AgregarCamposImagen(btn){
    inputsImagenes++;
    var campo= $(btn).parents("#imagen1").clone();
    campo.find("input").val("");
    campo.prop("id","imagen"+inputsImagenes);
    campo.find("img").attr('src','').attr('id','vistaPrevia_'+campo.attr('id'));
    campo.find("[name=agregar_imagen]").attr("onclick", 'QuitarCampoImagen(\'imagen'+inputsImagenes+'\');');
    campo.find("[name=agregar_imagen]").attr("class", 'btn');
    campo.find("[name=agregar_imagen]").html("-");
    $("#camposImagenes").append(campo);
    ajustarTam();
}

function QuitarCampoImagen(elemento){
    $("#"+elemento).remove();
    ajustarTam();
}
function vistaPrevia(e) {
    var oFReader = new FileReader();
    oFReader.readAsDataURL(e.files[0]);
    oFReader.onload = function (oFREvent) {
        $('#vistaPrevia_'+$(e).parent().attr('id')).attr('src',oFREvent.target.result);
    };
};
    
var inputsVideos=1;
function AgregarCamposVideo(btn){
    inputsVideos++;
    var campo= $(btn).parents("#video1").clone();
    campo.find("input").val("");
    campo.prop("id","video"+inputsVideos);
    campo.find("[name=agregar_video]").attr("onclick", 'QuitarCampoVideo(\'video'+inputsVideos+'\');');
    campo.find("[name=agregar_video]").attr("class", 'btn');
    campo.find("[name=agregar_video]").html("-");
    $("#camposVideos").append(campo);
    ajustarTam();
}

function QuitarCampoVideo(elemento){
    $("#"+elemento).remove();
    ajustarTam();
}

var inputsDocumentos=1;
function AgregarCamposDocumento(btn){
    inputsDocumentos++;
    var campo= $(btn).parents("#documento1").clone();
    campo.find("input").val("");
    campo.prop("id","documento"+inputsDocumentos);
    campo.find("[name=agregar_documento]").attr("onclick", 'QuitarCampoDocumento(\'documento'+inputsDocumentos+'\');');
    campo.find("[name=agregar_documento]").attr("class", 'btn');
    campo.find("[name=agregar_documento]").html("-");
    $("#camposDocumentos").append(campo);
    ajustarTam();
}

function QuitarCampoDocumento(elemento){
    $("#"+elemento).remove();
    ajustarTam();
}

function modalEliminarImagen(e){
    var id=$(e).attr('id_imagen');
    $('#modal_eliminar_material_extra').modal('show');
    $("#btn_eliminar").attr('eliminar-id',id);
    $("#btn_eliminar").show();
    //$("#btn_eliminar").attr('href',s_url+'/catalogos/tesis/eliminarImagen/'+id);
    $("#btn_eliminar").click(function(){
        eliminarImagen(this);
    }); 
}
function modalEliminarVideo(e){
    var id=$(e).attr('id_video');
    $('#modal_eliminar_material_extra').modal('show');
    $("#btn_eliminar").attr('eliminar-id',id);
    $("#btn_eliminar").show();
    //$("#btn_eliminar").attr('href',s_url+'/catalogos/tesis/eliminarVideo/'+id);
    $("#btn_eliminar").click(function(){
        eliminarVideo(this);
    });
}
function modalEliminarDocumento(e){
    var id=$(e).attr('id_documento');
    $('#modal_eliminar_material_extra').modal('show');
    $("#btn_eliminar").attr('eliminar-id',id);
    $("#btn_eliminar").show();
    //$("#btn_eliminar").attr('href',s_url+'/catalogos/tesis/eliminarDocumento/'+id);
    $("#btn_eliminar").click(function(){
        eliminarDocumento(this);
    });       
}
function eliminarImagen(e){
    var id=$(e).attr('eliminar-id');
    $('#modal-header').html('Eliminando');
    $('#modal_eliminar-body').html(
    '<img src="'+b_url+'themes/default/img/loading.gif" >');
    var url=s_url+'/catalogos/tesis/eliminarImagen/'+id;
    $.ajax({
        type: "POST",
        url: url,
        success: function(datos){
            $("#btn_eliminar").hide();
            
            //$('#modal_eliminar-body').html(datos);
            $('#modal_eliminar-body').html('Imagen eliminada');
            $('#imagen_materialExtra_'+id).remove();            
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('#modal_eliminar-body').html('No se pudo eliminar la imagen');
        }
    });
}
function eliminarDocumento(e){
    var id=$(e).attr('eliminar-id');
    $('#modal-header').html('Eliminando');
    $('#modal_eliminar-body').html(
    '<img src="'+b_url+'themes/default/img/loading.gif" >');
    var url=s_url+'/catalogos/tesis/eliminarDocumento/'+id;
    $.ajax({
        type: "POST",
        url: url,
        success: function(datos){
            $("#btn_eliminar").hide();
            $('#modal_eliminar-body').html(datos);
            $('#modal_eliminar-body').html('Documento eliminado');
            $('#documento_materialExtra_'+id).remove();        
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('#modal_eliminar-body').html('No se pudo eliminar el documento');
        }
    });
}
function eliminarVideo(e){
    var id=$(e).attr('eliminar-id');
    $('#modal-header').html('Eliminando');
    $('#modal_eliminar-body').html(
    '<img src="'+b_url+'themes/default/img/loading.gif" >');
    var url=s_url+'/catalogos/tesis/eliminarVideo/'+id;
    $.ajax({
        type: "POST",
        url: url,
        success: function(datos){
            $("#btn_eliminar").hide();
            $('#modal_eliminar-body').html('Video eliminado');
            $('#video_materialExtra_'+id).remove();            
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('#modal_eliminar-body').html('No se pudo eliminar el video');
        }
    });
}
function ajustarTam(){
    $('#body').height(0);
    min_tam();
    if($('#form_agregar').height()>$('#body').height()){
        $('#body').height($('#form_agregar').height());
    }
}

var inputsAudios=1;
function AgregarCamposAudio(btn){
    inputsAudios++;
    var campo= $(btn).parents("#audio1").clone();
    campo.find("input").val("");
    campo.prop("id","audio"+inputsAudios);
    campo.find("[name=agregar_audio]").attr("onclick", 'QuitarCampoAudio(\'audio'+inputsAudios+'\');');
    campo.find("[name=agregar_audio]").attr("class", 'btn');
    campo.find("[name=agregar_audio]").html("-");
    $("#camposAudios").append(campo);
    ajustarTam();
}

function QuitarCampoAudio(elemento){
    $("#"+elemento).remove();
    ajustarTam();
}

function modalEliminarAudio(e){
    var id=$(e).attr('id_audio');
    $('#modal_eliminar_material_extra').modal('show');
    $("#btn_eliminar").attr('eliminar-id',id);
    $("#btn_eliminar").show();
    //$("#btn_eliminar").attr('href',s_url+'/catalogos/tesis/eliminarDocumento/'+id);
    $("#btn_eliminar").click(function(){
        eliminarAudio(this);
    });       
}

function eliminarAudio(e){
    var id=$(e).attr('eliminar-id');
    $('#modal-header').html('Eliminando');
    $('#modal_eliminar-body').html(
    '<img src="'+b_url+'themes/default/img/loading.gif" >');
    var url=s_url+'/catalogos/tesis/eliminarAudio/'+id;
    $.ajax({
        type: "POST",
        url: url,
        success: function(datos){
            $("#btn_eliminar").hide();
            $('#modal_eliminar-body').html(datos);
            $('#modal_eliminar-body').html('Documento eliminado');
            $('#audio_materialExtra_'+id).remove();        
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('#modal_eliminar-body').html('No se pudo eliminar el audio');
        }
    });
}