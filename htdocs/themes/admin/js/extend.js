String.format = function() {
  var s = arguments[0];
  for (var i = 0; i < arguments.length - 1; i++) {       
    var reg = new RegExp("\\{" + i + "\\}", "gm");             
    s = s.replace(reg, arguments[i + 1]);
  }
  return s;
}



//Global functions
function base_url( uri ){
    uri = uri || "";
    return b_url + uri;
}
function site_url( uri ){
    uri = uri || "";
    return s_url + "/" + uri;
}


/*Obtiene el id en formato asdfasdf-algo-id*/
function get_id(cad, separator){
	separator = separator || "-";
	var t = cad.split( separator );
	return t[ t.length - 1 ];	
}


/** El administrador de mensajes*/

msg = {
	ok : function( msg ){
		return $.noty({
			"text": msg,
			"layout":"topRight",
			"type":"success"
		});
	},
	error : function( msg ){
		return $.noty({
			"text": msg,
			"layout":"topRight",
			"type":"error"
		});
	},
	information : function( msg ){
		return $.noty({
			"text": msg,
			"layout":"topRight",
			"type":"information"
		});
	}
	
}

ajax = function(){
	$.ajax( arguments );
}

post = function( obj ){
	//objeto viene con :
	/* uri : 
	 * inSuccess:
	 * data: 
	 * context
	 */
	var id_msg = msg.information( obj.downloadMsg || "Descargando ..." );
	$.ajax({
		type: "POST",
		url: site_url( obj.uri ),
		data: obj.data,
		success: function(data, textStatus, xhr){
			try{
				data = $.parseJSON( data );
			}catch(e){
				msg.error("Formato de respuesta inválido");
				return;
			}
			if( data.success ){
				obj.inSuccess( data, textStatus, xhr);
			}else{
				msg.error(data.msg);
			}
		},
		error : function(){
			msg.error("Ha ocurrido un error, inténtelo de nuevo por favor.");
		},
		complete : function(){
			$.noty.close( id_msg );
		}
		
	});
}



/* Para obtener los parametros por get al invocar un archivo**/
function $_GET(){
	var scripts = document.getElementsByTagName('script');
	var myScript = scripts[ scripts.length - 1 ];
	
	var queryString = myScript.src.replace(/^[^\?]+\??/,'');
	
	var params = parseQuery( queryString );
	
	function parseQuery ( query ) {
	  var Params = new Object ();
	  if ( ! query ) return Params; // return empty object
	  var Pairs = query.split(/[;&]/);
	  for ( var i = 0; i < Pairs.length; i++ ) {
	    var KeyVal = Pairs[i].split('=');
	    if ( ! KeyVal || KeyVal.length != 2 ) continue;
	    var key = unescape( KeyVal[0] );
	    var val = unescape( KeyVal[1] );
	    val = val.replace(/\+/g, ' ');
	    Params[key] = val;
	  }
	  return Params;
	}
	
	return params;
}

//Funcion para el dataTable
function update_dataTable( table_id, html ){
	var tmp_combo = $( "#" + table_id + "_wrapper #cmb-status");
	var tmp_search = $( "#" + table_id +"_filter input").val();
	var tpm_length = $("#" + table_id + "_length select").val();
	$("#" + table_id +"_wrapper").replaceWith( html );
	init_datatable();
	$("#" + table_id + "_wrapper .filtro").replaceWith( tmp_combo );
	$("#" + table_id + "_filter input").val(tmp_search).trigger("keyup");
	$("#" + table_id + "_length select").val(tpm_length).trigger("change");			
}


//Enginer template,
//Use : $.nano("<p>Hello {user.first_name} {user.last_name}! Your account is <strong>{user.account.status}</strong></p>", data) 
//luego podemos poner uno más avanzado- 
(function($){
  $.nano = function(template, data) {
    return template.replace(/\{([\w\.]*)\}/g, function (str, key) {
      var keys = key.split("."), value = data[keys.shift()];
      $.each(keys, function () { value = value[this]; });
      return (value === null || value === undefined) ? "" : value;
    });
  };
})(jQuery);


//Para los combos
//Para el combo de Estado, cada vez que le den click busca el de ciudad, y lo actualiza
$("body").on("change", ".fdt-direccion select[name=ubi_num_estado]", function(e){
	post({
		uri :'direccion/get_ciudades',
		data: {"id_estado" : $(this).val() },
		inSuccess: function( data ){
			$(".fdt-direccion select[name=ubi_id_municipio]").replaceWith( data.html );			
		},
		downloadMsg : "Descargando ciudades..."
	});
});

//Para el combo de Municipio, cada vez que le den click busca el de ciudad, y lo actualiza
$("body").on("change", ".fdt-direccion select[name=ubi_id_municipio]", function(e){
	post({
		uri :'direccion/get_colonias',
		data: {"id_estado" : $(".fdt-direccion select[name=ubi_num_estado]").val(), "id_municipio" : $(this).val() },
		inSuccess: function( data ){
			$(".fdt-direccion select[name=ubi_id_colonia]").replaceWith( data.html );			
		},
		downloadMsg : "Descargando colonias..."
	});
});




/** Obtienen un objeto de un array de la forma
 * [{"id":"1","text":"Casa"},{"id":"2","text":"Oficina"}]
 */ 

function get_obj_from_arr( arr, value, field_key){
	field_key = field_key || "id";
	
	var obj = {};
	for(var x in arr){
		obj = arr[x];
		if( obj[field_key] == value )
			return obj;
	}
}


