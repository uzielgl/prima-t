Cliente = {
	/** Recibe el id y hace la petición para desactivarlo
	 * */
	eliminar : function( id, callback_in_success ){
		callback_in_success = callback_in_success || function(){};
		post({
			uri : "admin/clientes/eliminar/" + id,
			inSuccess : function(data){
				msg.ok(data.msg);
				callback_in_success();
			}
		});
	},
	
	activar : function( id, callback_in_success ){
		callback_in_success = callback_in_success || function(){};
		post({
			uri : "admin/administradores/activar/" + id,
			inSuccess : function(data){
				msg.ok(data.msg);
				callback_in_success();
			}
		});
	},
	
	/** Actualiza los resultados en base a los filtros que le enviemos, la tabla*/
	actualizarResultados : function( filters ){
		post({
			uri : "admin/administradores/get_table",
			data : filters,
			inSuccess: function(data){
				update_dataTable( "no-more-tables", data.html );		
			}
		});
	},
	
	tplBtnActivar : '<a  rel="tooltip" class="btn btn-success btn-activar" title="Activar el administrador" data-loading-text="Activando..."><i class="icon-ok icon-white"></i> <span class="visible-desktop">Activar</span></a>',
	tplBtnDesactivar : '<a  rel="tooltip" class="btn btn-danger btn-desactivar" title="Desactivar el administrador" data-loading-text="Desactivando..."><i class="icon-remove icon-white"></i> <span class="visible-desktop">Desactivar</span></a>'
}


$(function(){
	
	
	//Para la lista, cuando den click en "Desactivar" 
	$("body").on("click", ".btn-eliminar", function(e){
		e.preventDefault();
		
		if( !confirm("¿Realmente desea eliminar al cliente?") ) return;
		
		$this = $(this);
		Cliente.eliminar( get_id( $this.parents("tr").attr("id") ), function(){
			$this.tooltip('hide');
			$this.parents("tr").fadeOut(function(){
				$(this).parents(".datatable").dataTable().fnDeleteRow( this );				
			});
		} );
	});

});
