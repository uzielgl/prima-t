Cliente = {
	//Una propiedad de url de controlador
	/** Recibe el id y hace la petición para desactivarlo
	 * */
	c_url : site_url() + "admin/clientes/",
	
	form : {
		/** Agrega campos para el teléfono en el formualario*/
		addPhone : function( btn){
			$(btn).tooltip('hide');
			$controls = $(Cliente.form.tplPhone).hide();
			$controls.find( ".cmb-type-phone" ).select2( Cliente.form.confComboTypePhone );
			$controls.insertAfter( $(btn).parents(".control-group") ).slideDown();
		},
		
		removePhone: function( btn){
			$to_delete = $(btn).parents(".control-group");
			$to_delete.slideUp( function(){
				$to_delete.remove();
			} )
		},
		
		tplPhone : 
			'<div class="control-group">' + 
				'<div class="controls">'+
					'<input type="text" name="pht_numero[]" class="input-small">'+
					'<input class="cmb-type-phone input-small" name="tel_id_telefono[]" type="text">'+
					'<a class="btn btn-success btn-add-phone" rel="tooltip" title="Agregar otro teléfono"><i class="icon-white icon-plus"> </i></a>'+
					'<a class="btn btn-danger btn-remove-phone" rel="tooltip" title="Eliminar este teléfono"><i class="icon-white icon-remove"> </i></a>'+
				'</div>'
			+ '</div>',
			
		confComboTypePhone : {
			createSearchChoice:function(term, data) { 
				if ( $(data).filter(function() { 
						return this.text.localeCompare(term)===0; 
					}).length===0 ) 
				{
					return {id:term, text:term};
				} 
			},
			placeholder: "Tipo",
			data: types_phone,
			initSelection : function (element, callback) {
				el = element;
			 	var tel	=  window.get_obj_from_arr( window.types_phone, el.val() ) ;
			    var data = {id: element.val(), text: tel.text };
			    callback(data);
			}
		}
	}
	
	
}


$(function(){
	//Habilitamos el datepicker
	$("#pro_fecha_nacimiento").datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange:"-100:+00"
		
    });
	
	//Inicializamos el teléfono
	$(".cmb-type-phone").select2(Cliente.form.confComboTypePhone);
	
	//Cuando dan click en Agregar teléfono
	$("body").on("click", ".btn-add-phone", function(e){
		Cliente.form.addPhone( this, e);
	});
	
	$("body").on("click", ".btn-remove-phone", function(e){
		Cliente.form.removePhone( this, e);
	});
	
	
	
});
