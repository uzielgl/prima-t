$(function(){
	//Cuando dan click en el cliente, se cargan sus mascotas
	$("body").on("change", "[name=enc_id_propietario]", function(e){
		Encuesta.updCmbDogs( $(this).val(), $("[name=enc_id_mascota]") );
	});
	
	$("[name=enc_fecha_fin], [name=enc_fecha_inicio]").datepicker();
	
	
	$("#clean-form").click(function(){
		$(this).parents("form").find("input, select").val("");
		$(this).parents("form").submit();
	});

});
