Paseador = {
	/** Recibe el id y hace la petición para desactivarlo
	 * */
	desactivar : function( id, callback_in_success ){
		callback_in_success = callback_in_success || function(){};
		post({
			uri : "admin/paseadores/desactivar/" + id,
			inSuccess : function(data){
				msg.ok(data.msg);
				callback_in_success();
			}
		});
	},
	
	activar : function( id, callback_in_success ){
		callback_in_success = callback_in_success || function(){};
		post({
			uri : "admin/paseadores/activar/" + id,
			inSuccess : function(data){
				msg.ok(data.msg);
				callback_in_success();
			}
		});
	},
	
	/** Actualiza los resultados en base a los filtros que le enviemos, la tabla*/
	actualizarResultados : function( filters ){
		post({
			uri : "admin/paseadores/get_table",
			data : filters,
			inSuccess: function(data){
				update_dataTable( "no-more-tables", data.html );		
			}
		});
	},
	
	tplBtnActivar : '<a  rel="tooltip" class="btn btn-success btn-activar" title="Activar el paseador" data-loading-text="Activando..."><i class="icon-ok icon-white"></i> <span class="visible-desktop">Activar</span></a>',
	tplBtnDesactivar : '<a  rel="tooltip" class="btn btn-danger btn-desactivar" title="Desactivar el paseador" data-loading-text="Desactivando..."><i class="icon-remove icon-white"></i> <span class="visible-desktop">Desactivar</span></a>'
}


$(function(){
	
	//Cuando se carge la vista, mover el filtro de status .filtros
	$("#no-more-tables_wrapper .filtro").replaceWith( $("#cmb-status") );
	
	//Cuando hacemos el filtro, mandamos a buscar toda la tabla otra vez
	$("body").on("change", "#cmb-status", function(e){
		Paseador.actualizarResultados( {active : $(this).val() } )
	});
	
	//Para la lista, cuando den click en "Desactivar" 
	$("body").on("click", ".btn-desactivar", function(e){
		$this = $(this);
		Paseador.desactivar( get_id( $this.parents("tr").attr("id") ), function(){
			if( $("#cmb-status").val() )
				$this.parents("tr").fadeOut(function(){
					$(this).parents(".datatable").dataTable().fnDeleteRow( this );				
				});
			else{
				$this.tooltip('hide');
				$this.replaceWith( $(Paseador.tplBtnActivar) );
			}
		} );
	});
	
	//Para la lista, cuando dan click en "Activar"
	$("body").on("click", ".btn-activar", function(e){
		$this = $(this);
		Paseador.activar( get_id( $this.parents("tr").attr("id") ), function(){
			if( $("#cmb-status").val() )
				$this.parents("tr").fadeOut(function(){
					$(this).parents(".datatable").dataTable().fnDeleteRow( this );				
				});
			else{
				$this.tooltip('hide');
				$this.replaceWith( $(Paseador.tplBtnDesactivar) );
			}
		} );
	});
	
});
