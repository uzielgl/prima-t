$(function(){
	
	//Inicializamos el combo de raza
	$("input[name=mas_id_raza]").select2( Mascota.form.confComboRaza );
	
	//$(".controls .select2-container").prop("rel", "tooltip").prop("title", "probando").tooltip()

	//Ponemos mensaje de ayuda para el combo de raza
	$(".select2-input").prop("rel", "tooltip").prop("title", "Si la raza no está se agregará automáticamente.").tooltip()
	
	var check_microchip = function(e){
		var $this = $(this);
		var checked = $this.prop("checked");
		if( checked ){
			$this.parents(".controls").find(".input-prepend").fadeIn();
		}else{
			$this.parents(".controls").find(".input-prepend").fadeOut();
		}
	}
	$("body").on("click", "[name=mas_microchip]", check_microchip);
	
	//Lo lanzamos pa que desaparezzca el input
	//$("[name=mas_microchip]").prop("checked", true);
	check_microchip.apply( $("[name=mas_microchip]") )
	
	
	//Cuando dan click a una característica:
	$("body").on("click", ".control-group.have-describe-field [type=checkbox]", function(e){
		$this = $(this);
		var checked = $this.prop("checked");
        var id = get_id( $this.prop("name"), "_" );
		if( checked ){
            $(".description-characteristic-" + id + " textarea").val("");
            $(".description-characteristic-" + id).slideDown();
		}else{
            $(".description-characteristic-" + id + " textarea").val("");
            $(".description-characteristic-" + id).slideUp();
		}
	});
	
	
	//Para ventana de seleccionar veterinaria
	$("#myModal").on("shown", function(){
		
	})
	//Cuando dan click en seleccionar veterinaria
	$("body").on("click", "#btn-seleccionar-veterinaria", function(){
		$("#win-seleccion-veterinaria").modal({
			remote : site_url( 'admin/veterinarias/seleccion' ),
		});
	});
	
	//Cuando dan click en la modal en Agregar veterinaria
	$("body").on("click", ".btn-agregar-veterinaria", function( e ){
		$("#win-agregar-veterinaria").modal({
			remote : site_url( 'admin/veterinarias/modal_agregar' ),
		});
	});
	
	//Cuando dan click en el botón de guardar veterinaria
	$(".btn-guardar-veterinaria").click(function(e){
		$(".frm-veterinaria input").jqBootstrapValidation();
		
		if( !$(".frm-veterinaria input").jqBootstrapValidation("hasErrors") ){
			
		
			var form = $(".frm-veterinaria");
			var data = form.serialize();
			
			post({
				uri : 'admin/veterinarias/j_agregar',
				data : data,
				inSuccess : function(data){
					//Cerramos las dos y ponemos la veterinaria que acabamos de crear.
					$("#win-seleccion-veterinaria").modal('hide');
					$("#win-agregar-veterinaria").modal('hide');
					$("#datos-veterinaria").html( $.nano( Mascota.form.tplVeterinaria, data.veterinaria) )  
					
				}
			});
		}else{
			alert("Revise la información requerida por el formulario, por favor.");
			$(".frm-veterinaria form input").jqBootstrapValidation();
		}
	});
	

	//Cuando dan click en seleccionar veterinaria (la primera ventana)
	$(".btn-seleccionar-veterinaria").click(function(e){
		var id = get_id( $("[name=veterinaria_selected]:checked").prop("id") );
		var veterinaria = get_obj_from_arr(veterinarias, id, "vet_id_veterinaria");
		$("#datos-veterinaria").html( $.nano( Mascota.form.tplVeterinaria, veterinaria ) );
		$("#win-seleccion-veterinaria").modal('hide');
		$("#btn-seleccionar-veterinaria").hide();
		$("#btn-quitar-veterinaria").removeClass("hidden")
		$("input[name=mas_id_veterinaria]").val( id );
	});
	
	//Click en botón quitar de veterinaria
	$("body").on("click", "#btn-quitar-veterinaria", function(e){
		$("#btn-seleccionar-veterinaria").show();
		$(this).addClass("hidden");
		$("#datos-veterinaria").html("");
		$("input[name=mas_id_veterinaria]").val( "" );
	});
	
	
	//Ponemos la veterinaria seleccionada si es que hay una en el campo mas_id_veterinaria
	if( $("[name=mas_id_veterinaria]").val() ){ 
		var veterinaria = get_obj_from_arr(veterinarias, $("[name=mas_id_veterinaria]").val(), "vet_id_veterinaria");
		$("#datos-veterinaria").html( $.nano( Mascota.form.tplVeterinaria, veterinaria ) );
		//$("#win-seleccion-veterinaria").modal('hide');
		$("#btn-seleccionar-veterinaria").hide();
		$("#btn-quitar-veterinaria").removeClass("hidden")
	} 
	
	
});
