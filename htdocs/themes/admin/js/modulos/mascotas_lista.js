
$(function(){
	
	
	//Para la lista, cuando den click en "Desactivar" 
	$("body").on("click", ".btn-eliminar", function(e){
		e.preventDefault();
		
		if( !confirm("¿Realmente desea eliminar la mascota?") ) return;
		
		$this = $(this);
		Mascota.eliminar( get_id( $this.parents("tr").attr("id") ), function(){
			$this.tooltip('hide');
			$this.parents("tr").fadeOut(function(){
				$(this).parents(".datatable").dataTable().fnDeleteRow( this );				
			});
		} );
	});

});
