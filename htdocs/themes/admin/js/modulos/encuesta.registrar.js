$(function(){
	
	//Iniciliazmaos para la fecha
	$("[name=enc_fecha]").datepicker({
      showWeek: true,
      firstDay: 1
    });
    
    if( ! $('[name=enc_fecha]').val() )  $('[name=enc_fecha]').datepicker("setDate", new Date() );
    
    //Inicializamos el combo de paseo
	$("[name=enc_id_paseo]").select2( Encuesta.form.confComboPaseo );
	
	//Inicializamos el combo de productos
	$("#productos").select2( Encuesta.form.confComboProductos );
	
	
	//Inicialimamos el tpv
	Venta.init();
	
	//Cuando dan click en el cliente, se cargan sus mascotas
	$("body").on("change", "[name=enc_id_propietario]", function(e){
		Encuesta.updCmbDogs( $(this).val(), $("[name=enc_id_mascota]") );
	})
});
