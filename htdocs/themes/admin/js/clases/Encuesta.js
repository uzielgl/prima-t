Encuesta = {
	form:{
		confComboPaseo : { //Se debería de poner todo a mayúsculas o minúsculas para que no haga doble versión a la hora de guardar en db
			createSearchChoice:function(term, data) { 
				if ( $(data).filter(function() { 
						return this.text.localeCompare(term)===0; 
					}).length===0 ) 
				{
					return {id:term, text:term};
				} 
			},
			placeholder: "Paseo",
			//data: types_razas,
			initSelection : function (element, callback) {
				el = element;
			 	var paseo	=  window.get_obj_from_arr( window.paseos, el.val() ) ;
			    var data = {id: element.val(), text: paseo.text };
			    callback(data);
			},
			width : "element"
		},
		
		confComboProductos : { //Se debería de poner todo a mayúsculas o minúsculas para que no haga doble versión a la hora de guardar en db
			createSearchChoice:function(term, data) { 
				if ( $(data).filter(function() { 
						return this.text.localeCompare(term)===0; 
					}).length===0 ) 
				{
					return {id:term, text:term};
				} 
			},
			placeholder: "Selecciona producto",
			//data: types_razas,
			initSelection : function (element, callback) {
				el = element;
			 	var producto	=  window.get_obj_from_arr( window.productos, el.val() ) ;
			    var data = {id: element.val(), text: producto.pro_nombre };
			    callback(data);
			},
			width : "element"
		}
		
	},
	
	updCmbDogs : function( owner_id, cmb_dog ){
		$cmb = $( cmb_dog );
		$cmb.find( "option" ).remove();
		$cmb.append( "<option>Descargando...</option>" );
		
		post({
			uri : 'admin/clientes/get_peets_cmb/' + owner_id,
			inSuccess :function( data ){
				$cmb.html( data.html );
			}
		});
	}
}

Venta = {
	tplRow : 
		'<tr>' +
			'<td>{0}<input type="hidden" class="productos" name="productos[]" value="{1}" /></td>' +
			'<td><input type="text" class="cantidades" name="cantidades[]" value="{2}"></td>' +
			'<td><a class="btn btn-danger btn-eliminar-producto"> <i class="icon-white icon-remove"> </i> Eliminar</a></td>' +
		'</tr>',

	
	addProduct : function( id, nombre, cantidad ){
		if( $("#productos").select2("val") ){
			if( $("#cantidad").val() ){
				//Buscamos para checar si hay un producto con ese nombre o id, en ese caso sólo aumentar la cantidad
				var added = false;
				$.each( $(".productos"), function(){
					if( $(this).val() == id ){
						var cant = +$(this).parents("tr").find(".cantidades").val();     
						$(this).parents("tr").find(".cantidades").val( cant + (+cantidad) )
						added = true;
					}
				} );
				if ( !added ) $("#venta_productos tbody").append( String.format( Venta.tplRow, nombre, id, cantidad ) );
			}else{
				alert("Debes de seleccionar una cantidad.");
			}
		}else{
			alert("Debes de seleccionar un producto.");
		}
				
	},
	
	delProduct : function( btn ){
		$(btn).parents("tr").remove();
	},
	
	init : function(){
		$("body").on("click", "#btn-agregar-producto", function(e){
			if( $("#productos").select2("val") ){
				if( $("#cantidad").val() ){
					var data = $("#productos").select2("data");
					Venta.addProduct( data.id, data.text,  $("#cantidad").val() );
				}else{
					alert("Debes de seleccionar una cantidad.");
				}
			}else{
				alert("Debes de seleccionar un producto.");
			}

		});
		
		$("body").on("click", ".btn-eliminar-producto", function(e){
			Venta.delProduct( this );
		});
		
		var stop = function(e){ if( e.keyCode == 13 ) {e.preventDefault();  }}
		$("#cantidad").
		on("keydown", stop).
		on("keypress", stop).
		on("keyup", function(e){ 
			stop(e); 
			///$("#btn-agregar-producto").trigger("click");
		});
	}
}


try{
	Encuesta.form.confComboPaseo.data = paseos;
}catch( e ){
	Encuesta.form.confComboProductos.data = {};
}

try{
	Encuesta.form.confComboProductos.data = productos;
}catch( e ){
	Encuesta.form.confComboProductos.data = {};
}

